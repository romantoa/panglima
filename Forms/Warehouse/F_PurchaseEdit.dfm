object FPurchaseEdit: TFPurchaseEdit
  Left = 0
  Top = 0
  Caption = 'FPurchaseEdit'
  ClientHeight = 261
  ClientWidth = 765
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -586
    ExplicitWidth = 1033
    Height = 144
    Width = 765
    object cxLabel1: TcxLabel
      Left = 592
      Top = 16
      Caption = 'Kode'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 592
      Top = 42
      Caption = 'Tanggal'
      ParentFont = False
    end
    object _number: TcxTextEdit
      Left = 704
      Top = 15
      Enabled = False
      ParentFont = False
      Properties.Alignment.Horz = taCenter
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 2
      Text = 'AUTO'
      Width = 217
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Lokasi'
      ParentFont = False
    end
    object cxLabel4: TcxLabel
      Left = 592
      Top = 68
      Caption = 'PIC'
      ParentFont = False
    end
    object _outlet_id: TcxLookupComboBox
      Left = 136
      Top = 41
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 3
      Width = 329
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 68
      Caption = 'Referensi'
      ParentFont = False
    end
    object _Description: TcxMemo
      Left = 136
      Top = 93
      ParentFont = False
      TabOrder = 5
      Height = 46
      Width = 329
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 94
      Caption = 'Keterangan'
      ParentFont = False
    end
    object _reference: TcxTextEdit
      Left = 136
      Top = 67
      ParentFont = False
      TabOrder = 4
      Width = 329
    end
    object _employee_id: TcxLookupComboBox
      Left = 704
      Top = 67
      Enabled = False
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 10
      Width = 217
    end
    object _dateissued: TcxDateEdit
      Left = 704
      Top = 41
      Enabled = False
      ParentFont = False
      Properties.DisplayFormat = 'dd-mm-yyyy'
      TabOrder = 11
      Width = 217
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Jenis'
      ParentFont = False
    end
    object _stockintype_id: TcxLookupComboBox
      Left = 136
      Top = 15
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 13
      Width = 329
    end
  end
end
