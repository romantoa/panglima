unit F_Producttype;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFProductType = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FProductType: TFProductType;

implementation

{$R *.dfm}

procedure TFProductType.FormCreate(Sender: TObject);
begin
  _module.Name := 'Producttype';
  inherited;
end;

initialization
  RegisterClass(TFProductType);
finalization
  UnRegisterClass(TFProductType);

end.
