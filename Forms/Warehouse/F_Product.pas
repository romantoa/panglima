unit F_Product;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFProduct = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FProduct: TFProduct;

implementation

{$R *.dfm}

procedure TFProduct.FormCreate(Sender: TObject);
begin
  _module.Name := 'Product';
  inherited;
end;

initialization
  RegisterClass(TFProduct);
finalization
  UnRegisterClass(TFProduct);

end.
