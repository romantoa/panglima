unit F_BakingEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, dxSkinWhiteprint, Vcl.ComCtrls, dxCore, cxDateUtils, cxSpinEdit,
  cxDropDownEdit, cxCalendar, cxMemo, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox, DM_Main, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGrid,
  cxDBExtLookupComboBox, cxGridLevel, dxRibbonSkins, dxRibbonCustomizationForm,
  dxRibbon, dxBar;

type
  TFBakingEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _number: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    _product_id: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    _description: TcxMemo;
    cxLabel6: TcxLabel;
    _employee_id: TcxLookupComboBox;
    _dateissued: TcxDateEdit;
    cxLabel7: TcxLabel;
    _storage_id: TcxLookupComboBox;
    _unit_id: TcxDateEdit;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    _shift: TcxSpinEdit;
    _expected_qty: TcxSpinEdit;
    _success_qty: TcxSpinEdit;
    _defect_qty: TcxSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure _product_idPropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetDefaultValues; override;
  end;

var
  FBakingEdit: TFBakingEdit;

implementation

{$R *.dfm}

procedure TFBakingEdit.FormCreate(Sender: TObject);
begin
  _module.Name := 'Baking';
  inherited;
end;

procedure TFBakingEdit.SetDefaultValues;
begin
  inherited;
  _storage_id.EditValue := DMMain.RegisterInfo.StorageId;
  _storage_id.Enabled := False;
  _shift.EditValue := DMMain.RegisterInfo.Shift;
  _shift.Enabled := False;
end;

procedure TFBakingEdit._product_idPropertiesEditValueChanged(Sender: TObject);
begin
  // Cek apakah ada resep
end;

initialization
  RegisterClass(TFBakingEdit);
finalization
  UnRegisterClass(TFBakingEdit);

end.
