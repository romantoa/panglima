unit F_StockinEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxMemo, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox,
  Vcl.ComCtrls, dxCore, cxDateUtils, cxCalendar, Fsf_Edit, DM_Main,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2010Black, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Light;

type
  TFStockinEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _number: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    _storage_id: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    _Description: TcxMemo;
    cxLabel6: TcxLabel;
    _reference: TcxTextEdit;
    _employee_id: TcxLookupComboBox;
    _dateissued: TcxDateEdit;
    cxLabel7: TcxLabel;
    _stockintype_id: TcxLookupComboBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public

  end;

var
  FStockinEdit: TFStockinEdit;

implementation

{$R *.dfm}

{ TFStockinEdit }

procedure TFStockinEdit.FormCreate(Sender: TObject);
begin
  _module.Name := 'Stockin';
  inherited;
end;

initialization
  RegisterClass(TFStockinEdit);
finalization
  UnRegisterClass(TFStockinEdit);

end.
