unit F_ProductEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxTextEdit, cxLabel, cxGroupBox, cxMemo, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxCheckBox,
  dxBarBuiltInMenu, cxPC, Vcl.Menus, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxGridCustomView, cxGrid,
  Vcl.StdCtrls, cxButtons, cxCurrencyEdit, cxSpinEdit;

type
  TFProductEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _code: TcxTextEdit;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    _short_name: TcxTextEdit;
    cxLabel7: TcxLabel;
    _barcode: TcxTextEdit;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel12: TcxLabel;
    _producttype_id: TcxLookupComboBox;
    _brand_id: TcxLookupComboBox;
    _unit_id: TcxLookupComboBox;
    _specification: TcxMemo;
    _ishighlighted: TcxCheckBox;
    _isactive: TcxCheckBox;
    _istaxed: TcxCheckBox;
    _hpp: TcxCurrencyEdit;
    _weight: TcxCurrencyEdit;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    _length: TcxCurrencyEdit;
    cxLabel15: TcxLabel;
    _width: TcxCurrencyEdit;
    cxLabel16: TcxLabel;
    _height: TcxCurrencyEdit;
    cxLabel17: TcxLabel;
    cxLabel18: TcxLabel;
    cxLabel19: TcxLabel;
    cxLabel20: TcxLabel;
    cxLabel21: TcxLabel;
    _purchaseprice: TcxCurrencyEdit;
    cxLabel22: TcxLabel;
    _retailprice: TcxCurrencyEdit;
    cxLabel23: TcxLabel;
    _partyprice: TcxCurrencyEdit;
    _partyquantity: TcxSpinEdit;
    cxLabel8: TcxLabel;
    _stock: TcxSpinEdit;
    cxLabel10: TcxLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FProductEdit: TFProductEdit;

implementation

{$R *.dfm}

initialization
  RegisterClass(TFProductEdit);
finalization
  UnRegisterClass(TFProductEdit);

end.
