unit F_SupplierEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxCurrencyEdit, cxMemo, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxTextEdit, cxLabel,
  cxGroupBox, Fsf_Edit;

type
  TFSupplierEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel2: TcxLabel;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    _province_id: TcxLookupComboBox;
    _street: TcxMemo;
    _subdistrict: TcxTextEdit;
    _region_id: TcxLookupComboBox;
    _district_id: TcxLookupComboBox;
    _postal_code: TcxTextEdit;
    _phone: TcxTextEdit;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel1: TcxLabel;
    _email: TcxTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSupplierEdit: TFSupplierEdit;

implementation

{$R *.dfm}

initialization
  RegisterClass(TFSupplierEdit);
finalization
  UnRegisterClass(TFSupplierEdit);

end.
