unit F_ProducttypeEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxMemo, cxTextEdit, cxLabel, cxGroupBox, Fsf_Edit;

type
  TFProducttypeEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _code: TcxTextEdit;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    _description: TcxMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FProducttypeEdit: TFProducttypeEdit;

implementation

{$R *.dfm}

initialization
  RegisterClass(TFProducttypeEdit);
finalization
  UnRegisterClass(TFProducttypeEdit);

end.
