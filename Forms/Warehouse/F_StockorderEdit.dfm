object FStockOrderEdit: TFStockOrderEdit
  Left = 0
  Top = 0
  Caption = 'FStockOrderEdit'
  ClientHeight = 346
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Height = 143
    Width = 761
    object cxLabel1: TcxLabel
      Left = 592
      Top = 16
      Caption = 'Kode'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 592
      Top = 42
      Caption = 'Tanggal'
      ParentFont = False
    end
    object _number: TcxTextEdit
      Left = 704
      Top = 15
      Enabled = False
      ParentFont = False
      Properties.Alignment.Horz = taCenter
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 2
      Text = 'AUTO'
      Width = 217
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Ke Lokasi'
      ParentFont = False
    end
    object cxLabel4: TcxLabel
      Left = 592
      Top = 68
      Caption = 'PIC'
      ParentFont = False
    end
    object _storageto_id: TcxLookupComboBox
      Left = 136
      Top = 41
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 3
      Width = 217
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 68
      Caption = 'Tgl. Pengiriman'
      ParentFont = False
    end
    object _description: TcxMemo
      Left = 136
      Top = 93
      ParentFont = False
      TabOrder = 4
      Height = 46
      Width = 329
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 94
      Caption = 'Keterangan'
      ParentFont = False
    end
    object _employee_id: TcxLookupComboBox
      Left = 704
      Top = 67
      Enabled = False
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 9
      Width = 217
    end
    object _dateissued: TcxDateEdit
      Left = 704
      Top = 41
      Enabled = False
      ParentFont = False
      Properties.DisplayFormat = 'dd-mm-yyyy'
      TabOrder = 10
      Width = 217
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Dari Lokasi'
      ParentFont = False
    end
    object _storagefrom_id: TcxLookupComboBox
      Left = 136
      Top = 15
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 12
      Width = 217
    end
    object _deliverydate: TcxDateEdit
      Left = 136
      Top = 67
      ParentFont = False
      Properties.DisplayFormat = 'dd-mm-yyyy'
      TabOrder = 13
      Width = 217
    end
  end
end
