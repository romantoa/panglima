unit F_BrandEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxTextEdit, cxLabel, cxGroupBox, cxMemo;

type
  TFBrandEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    _description: TcxMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBrandEdit: TFBrandEdit;

implementation

{$R *.dfm}

initialization
  RegisterClass(TFBrandEdit);
finalization
  UnRegisterClass(TFBrandEdit);

end.
