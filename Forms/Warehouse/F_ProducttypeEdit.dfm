object FProducttypeEdit: TFProducttypeEdit
  Left = 0
  Top = 0
  Caption = 'FProducttypeEdit'
  ClientHeight = 201
  ClientWidth = 765
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -244
    ExplicitTop = -228
    ExplicitWidth = 691
    ExplicitHeight = 429
    Height = 201
    Width = 765
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Kode'
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Nama'
    end
    object _code: TcxTextEdit
      Left = 136
      Top = 15
      TabOrder = 2
      Width = 217
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 41
      TabOrder = 3
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 68
      Caption = 'Keterangan'
    end
    object _description: TcxMemo
      Left = 136
      Top = 67
      TabOrder = 5
      Height = 89
      Width = 497
    end
  end
end
