unit F_Supplier;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFSupplier = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSupplier: TFSupplier;

implementation

{$R *.dfm}

procedure TFSupplier.FormCreate(Sender: TObject);
begin
  _module.Name := 'Supplier';
  inherited;
end;

initialization
  RegisterClass(TFSupplier);
finalization
  UnRegisterClass(TFSupplier);

end.
