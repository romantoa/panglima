object FBrandEdit: TFBrandEdit
  Left = 0
  Top = 0
  Caption = 'FBrandEdit'
  ClientHeight = 201
  ClientWidth = 739
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -244
    ExplicitTop = -228
    ExplicitWidth = 691
    ExplicitHeight = 429
    Height = 201
    Width = 739
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Nama'
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 15
      TabOrder = 1
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Keterangan'
    end
    object _description: TcxMemo
      Left = 136
      Top = 41
      TabOrder = 3
      Height = 89
      Width = 497
    end
  end
end
