object FWarehouseEdit: TFWarehouseEdit
  Left = 0
  Top = 0
  Caption = 'FWarehouseEdit'
  ClientHeight = 511
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Height = 511
    Width = 680
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Kode'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Nama'
      ParentFont = False
    end
    object _code: TcxTextEdit
      Left = 136
      Top = 15
      ParentFont = False
      TabOrder = 2
      Width = 217
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 41
      ParentFont = False
      TabOrder = 3
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 108
      Caption = 'Alamat'
      ParentFont = False
    end
    object _province_id: TcxLookupComboBox
      Left = 136
      Top = 189
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 7
      Width = 497
    end
    object _street: TcxMemo
      Left = 136
      Top = 107
      ParentFont = False
      TabOrder = 5
      Height = 54
      Width = 497
    end
    object _subdistrict: TcxTextEdit
      Left = 136
      Top = 163
      ParentFont = False
      TabOrder = 6
      Width = 497
    end
    object _region_id: TcxLookupComboBox
      Left = 136
      Top = 215
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 8
      Width = 497
    end
    object _district_id: TcxLookupComboBox
      Left = 136
      Top = 241
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 9
      Width = 497
    end
    object _postal_code: TcxTextEdit
      Left = 136
      Top = 267
      ParentFont = False
      TabOrder = 10
      Width = 217
    end
    object _phone: TcxTextEdit
      Left = 136
      Top = 67
      ParentFont = False
      TabOrder = 4
      Width = 217
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 164
      Caption = 'Desa'
      ParentFont = False
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 190
      Caption = 'Provinsi'
      ParentFont = False
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 216
      Caption = 'Kabupaten/Kota'
      ParentFont = False
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 242
      Caption = 'Kecamatan'
      ParentFont = False
    end
    object cxLabel8: TcxLabel
      Left = 24
      Top = 268
      Caption = 'Kode POS'
      ParentFont = False
    end
    object cxLabel9: TcxLabel
      Left = 25
      Top = 68
      Caption = 'Telepon'
      ParentFont = False
    end
    object cxLabel10: TcxLabel
      Left = 24
      Top = 310
      Caption = 'Latitude'
      ParentFont = False
    end
    object _lattitude: TcxCurrencyEdit
      Left = 136
      Top = 309
      ParentFont = False
      Properties.DisplayFormat = '#########,######'
      TabOrder = 11
      Width = 217
    end
    object cxLabel11: TcxLabel
      Left = 24
      Top = 336
      Caption = 'Logitude'
      ParentFont = False
    end
    object _longitude: TcxCurrencyEdit
      Left = 136
      Top = 335
      ParentFont = False
      Properties.DisplayFormat = '#########,######'
      TabOrder = 12
      Width = 217
    end
  end
end
