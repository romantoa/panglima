unit F_Brand;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFBrand = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBrand: TFBrand;

implementation

{$R *.dfm}

procedure TFBrand.FormCreate(Sender: TObject);
begin
  _module.Name := 'Brand';
  inherited;
end;

initialization
  RegisterClass(TFBrand);
finalization
  UnRegisterClass(TFBrand);

end.
