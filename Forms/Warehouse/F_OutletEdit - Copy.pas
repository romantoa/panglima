unit F_OutletEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxMemo, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox,
  cxCurrencyEdit;

type
  TFOutletEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _code: TcxTextEdit;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    _province_id: TcxLookupComboBox;
    _street: TcxMemo;
    _subdistrict: TcxTextEdit;
    _region_id: TcxLookupComboBox;
    _district_id: TcxLookupComboBox;
    _postal_code: TcxTextEdit;
    _phone: TcxTextEdit;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    _lattitude: TcxCurrencyEdit;
    cxLabel11: TcxLabel;
    _longitude: TcxCurrencyEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOutletEdit: TFOutletEdit;

implementation

{$R *.dfm}

initialization
  RegisterClass(TFOutletEdit);
finalization
  UnRegisterClass(TFOutletEdit);

end.
