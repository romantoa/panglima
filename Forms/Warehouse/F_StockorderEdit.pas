unit F_StockorderEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, dxSkinWhiteprint, Vcl.ComCtrls, dxCore, cxDateUtils,
  cxDropDownEdit, cxCalendar, cxMemo, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox, DM_Main;

type
  TFStockOrderEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _number: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    _storageto_id: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    _description: TcxMemo;
    cxLabel6: TcxLabel;
    _employee_id: TcxLookupComboBox;
    _dateissued: TcxDateEdit;
    cxLabel7: TcxLabel;
    _storagefrom_id: TcxLookupComboBox;
    _deliverydate: TcxDateEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FStockOrderEdit: TFStockOrderEdit;

implementation

{$R *.dfm}

procedure TFStockOrderEdit.FormCreate(Sender: TObject);
begin
  _module.Name := 'StockOrder';
  inherited;
end;

initialization
  RegisterClass(TFStockOrderEdit);
finalization
  UnRegisterClass(TFStockOrderEdit);

end.
