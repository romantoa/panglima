unit F_StockreturnEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, dxSkinWhiteprint, Vcl.ComCtrls, dxCore, cxDateUtils,
  cxDropDownEdit, cxCalendar, cxMemo, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox, Fsf_Edit;

type
  TFStockreturnEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _number: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    _storagefrom_id: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    _description: TcxMemo;
    _employee_id: TcxLookupComboBox;
    _dateissued: TcxDateEdit;
    cxLabel7: TcxLabel;
    _storageto_id: TcxLookupComboBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FStockreturnEdit: TFStockreturnEdit;

implementation

{$R *.dfm}

procedure TFStockreturnEdit.FormCreate(Sender: TObject);
begin
  _module.Name := 'StockReturn';
  inherited;
end;

initialization
  RegisterClass(TFStockreturnEdit);
finalization
  UnRegisterClass(TFStockreturnEdit);

end.
