unit F_Storage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFStorage = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FStorage: TFStorage;

implementation

{$R *.dfm}

procedure TFStorage.FormCreate(Sender: TObject);
begin
  _module.Name := 'Storage';
  inherited;
end;

initialization
  RegisterClass(TFStorage);
finalization
  UnRegisterClass(TFStorage);

end.
