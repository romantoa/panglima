unit F_PurchaseEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, dxSkinWhiteprint, Vcl.ComCtrls, dxCore, cxDateUtils,
  cxDropDownEdit, cxCalendar, cxMemo, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox;

type
  TFPurchaseEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _number: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    _outlet_id: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    _Description: TcxMemo;
    cxLabel6: TcxLabel;
    _reference: TcxTextEdit;
    _employee_id: TcxLookupComboBox;
    _dateissued: TcxDateEdit;
    cxLabel7: TcxLabel;
    _stockintype_id: TcxLookupComboBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPurchaseEdit: TFPurchaseEdit;

implementation

{$R *.dfm}

procedure TFPurchaseEdit.FormCreate(Sender: TObject);
begin
  _module.Name := 'Purchase';
  inherited;
end;

initialization
  RegisterClass(TFPurchaseEdit);
finalization
  UnRegisterClass(TFPurchaseEdit);

end.
