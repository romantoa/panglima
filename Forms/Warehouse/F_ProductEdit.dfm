object FProductEdit: TFProductEdit
  Left = 0
  Top = 0
  Caption = 'FProductEdit'
  ClientHeight = 621
  ClientWidth = 747
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Height = 561
    Width = 747
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Kode'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 94
      Caption = 'Nama Panjang'
      ParentFont = False
    end
    object _code: TcxTextEdit
      Left = 136
      Top = 15
      ParentFont = False
      TabOrder = 2
      Width = 217
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 93
      ParentFont = False
      TabOrder = 8
      Width = 537
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 68
      Caption = 'Nama Pendek'
      ParentFont = False
    end
    object _short_name: TcxTextEdit
      Left = 136
      Top = 67
      ParentFont = False
      TabOrder = 6
      Width = 217
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Barcode'
      ParentFont = False
    end
    object _barcode: TcxTextEdit
      Left = 136
      Top = 41
      ParentFont = False
      TabOrder = 4
      Width = 217
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 133
      Caption = 'Kategori'
      ParentFont = False
      Transparent = True
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 159
      Caption = 'Merek'
      ParentFont = False
      Transparent = True
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 185
      Caption = 'Satuan'
      ParentFont = False
      Transparent = True
    end
    object cxLabel9: TcxLabel
      Left = 24
      Top = 251
      Caption = 'HPP'
      ParentFont = False
      Transparent = True
    end
    object cxLabel12: TcxLabel
      Left = 389
      Top = 133
      Caption = 'Spesifikasi'
      ParentFont = False
      Transparent = True
    end
    object _producttype_id: TcxLookupComboBox
      Left = 136
      Top = 132
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 9
      Width = 217
    end
    object _brand_id: TcxLookupComboBox
      Left = 136
      Top = 158
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 10
      Width = 217
    end
    object _unit_id: TcxLookupComboBox
      Left = 136
      Top = 184
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 12
      Width = 217
    end
    object _specification: TcxMemo
      Left = 389
      Top = 158
      ParentFont = False
      TabOrder = 11
      Height = 339
      Width = 284
    end
    object _ishighlighted: TcxCheckBox
      Left = 603
      Top = 42
      Caption = 'Highlight'
      ParentFont = False
      Properties.ValueChecked = 1
      Properties.ValueGrayed = '0'
      Properties.ValueUnchecked = 0
      Style.LookAndFeel.NativeStyle = False
      Style.LookAndFeel.SkinName = 'Sharp'
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.SkinName = 'Sharp'
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.SkinName = 'Sharp'
      StyleHot.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.SkinName = 'Sharp'
      TabOrder = 5
      Transparent = True
    end
    object _isactive: TcxCheckBox
      Left = 603
      Top = 68
      Caption = 'Aktif'
      ParentFont = False
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      TabOrder = 7
      Transparent = True
    end
    object _istaxed: TcxCheckBox
      Left = 603
      Top = 16
      Caption = 'BKP'
      ParentFont = False
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      Style.LookAndFeel.NativeStyle = False
      Style.LookAndFeel.SkinName = 'Sharp'
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.SkinName = 'Sharp'
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.SkinName = 'Sharp'
      StyleHot.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.SkinName = 'Sharp'
      TabOrder = 3
      Transparent = True
    end
    object _hpp: TcxCurrencyEdit
      Left = 136
      Top = 250
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 14
      Width = 217
    end
    object _weight: TcxCurrencyEdit
      Left = 136
      Top = 395
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 19
      Width = 177
    end
    object cxLabel13: TcxLabel
      Left = 24
      Top = 396
      Caption = 'Berat'
      ParentFont = False
      Transparent = True
    end
    object cxLabel14: TcxLabel
      Left = 24
      Top = 422
      Caption = 'Panjang'
      ParentFont = False
      Transparent = True
    end
    object _length: TcxCurrencyEdit
      Left = 136
      Top = 421
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 20
      Width = 177
    end
    object cxLabel15: TcxLabel
      Left = 24
      Top = 448
      Caption = 'Lebar'
      ParentFont = False
      Transparent = True
    end
    object _width: TcxCurrencyEdit
      Left = 136
      Top = 447
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 21
      Width = 177
    end
    object cxLabel16: TcxLabel
      Left = 25
      Top = 474
      Caption = 'Tinggi'
      ParentFont = False
      Transparent = True
    end
    object _height: TcxCurrencyEdit
      Left = 136
      Top = 473
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 22
      Width = 177
    end
    object cxLabel17: TcxLabel
      Left = 319
      Top = 396
      Caption = 'mg'
      ParentFont = False
      Transparent = True
    end
    object cxLabel18: TcxLabel
      Left = 319
      Top = 422
      Caption = 'mm'
      ParentFont = False
      Transparent = True
    end
    object cxLabel19: TcxLabel
      Left = 319
      Top = 448
      Caption = 'mm'
      ParentFont = False
      Transparent = True
    end
    object cxLabel20: TcxLabel
      Left = 319
      Top = 474
      Caption = 'mm'
      ParentFont = False
      Transparent = True
    end
    object cxLabel21: TcxLabel
      Left = 24
      Top = 277
      Caption = 'Harga Beli'
      ParentFont = False
      Transparent = True
    end
    object _purchaseprice: TcxCurrencyEdit
      Left = 136
      Top = 276
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 15
      Width = 217
    end
    object cxLabel22: TcxLabel
      Left = 24
      Top = 303
      Caption = 'Harga Ritel'
      ParentFont = False
      Transparent = True
    end
    object _retailprice: TcxCurrencyEdit
      Left = 136
      Top = 302
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 16
      Width = 217
    end
    object cxLabel23: TcxLabel
      Left = 24
      Top = 329
      Caption = 'Harga Partai'
      ParentFont = False
      Transparent = True
    end
    object _partyprice: TcxCurrencyEdit
      Left = 136
      Top = 328
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 17
      Width = 217
    end
    object _partyquantity: TcxSpinEdit
      Left = 136
      Top = 354
      Properties.Alignment.Horz = taRightJustify
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 18
      Width = 217
    end
    object cxLabel8: TcxLabel
      Left = 24
      Top = 211
      Caption = 'Stok'
      ParentFont = False
      Transparent = True
    end
    object _stock: TcxSpinEdit
      Left = 136
      Top = 210
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 13
      Width = 217
    end
    object cxLabel10: TcxLabel
      Left = 24
      Top = 355
      Caption = 'Jumlah Partai'
      ParentFont = False
      Transparent = True
    end
  end
end
