unit F_Purchase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFPurchase = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPurchase: TFPurchase;

implementation

{$R *.dfm}

procedure TFPurchase.FormCreate(Sender: TObject);
begin
  _module.Name := 'Purchase';
  inherited;
end;

initialization
  RegisterClass(TFPurchase);
finalization
  UnRegisterClass(TFPurchase);

end.
