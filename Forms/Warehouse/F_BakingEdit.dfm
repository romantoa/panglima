object FBakingEdit: TFBakingEdit
  Left = 0
  Top = 0
  Caption = 'FBakingEdit'
  ClientHeight = 533
  ClientWidth = 781
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Height = 193
    Width = 781
    object cxLabel1: TcxLabel
      Left = 592
      Top = 17
      Caption = 'Kode'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 592
      Top = 43
      Caption = 'Tanggal'
      ParentFont = False
    end
    object _number: TcxTextEdit
      Left = 704
      Top = 16
      Enabled = False
      ParentFont = False
      Properties.Alignment.Horz = taCenter
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 2
      Text = 'AUTO'
      Width = 217
    end
    object cxLabel3: TcxLabel
      Left = 592
      Top = 121
      Caption = 'Shift'
      ParentFont = False
    end
    object cxLabel4: TcxLabel
      Left = 592
      Top = 69
      Caption = 'PIC'
      ParentFont = False
    end
    object _product_id: TcxLookupComboBox
      Left = 136
      Top = 16
      ParentFont = False
      Properties.ListColumns = <>
      Properties.OnEditValueChanged = _product_idPropertiesEditValueChanged
      TabOrder = 3
      Width = 369
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 17
      Caption = 'Barang'
      ParentFont = False
    end
    object _description: TcxMemo
      Left = 136
      Top = 146
      ParentFont = False
      TabOrder = 4
      Height = 46
      Width = 369
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 43
      Caption = 'Satuan'
      ParentFont = False
    end
    object _employee_id: TcxLookupComboBox
      Left = 704
      Top = 68
      Enabled = False
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 9
      Width = 217
    end
    object _dateissued: TcxDateEdit
      Left = 704
      Top = 42
      Enabled = False
      ParentFont = False
      Properties.DisplayFormat = 'dd-mm-yyyy'
      TabOrder = 10
      Width = 217
    end
    object cxLabel7: TcxLabel
      Left = 592
      Top = 95
      Caption = 'Lokasi'
      ParentFont = False
    end
    object _storage_id: TcxLookupComboBox
      Left = 704
      Top = 94
      Enabled = False
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 12
      Width = 217
    end
    object _unit_id: TcxDateEdit
      Left = 136
      Top = 42
      Enabled = False
      ParentFont = False
      Properties.DisplayFormat = 'dd-mm-yyyy'
      TabOrder = 13
      Width = 129
    end
    object cxLabel8: TcxLabel
      Left = 24
      Top = 69
      Caption = 'Jml. Target'
      ParentFont = False
    end
    object cxLabel9: TcxLabel
      Left = 24
      Top = 95
      Caption = 'Jml. Berhasil'
      ParentFont = False
    end
    object cxLabel10: TcxLabel
      Left = 24
      Top = 121
      Caption = 'Jml. Rusak'
      ParentFont = False
    end
    object cxLabel11: TcxLabel
      Left = 24
      Top = 147
      Caption = 'Keterangan'
      ParentFont = False
    end
    object _shift: TcxSpinEdit
      Left = 704
      Top = 120
      Enabled = False
      ParentFont = False
      TabOrder = 18
      Width = 129
    end
    object _expected_qty: TcxSpinEdit
      Left = 136
      Top = 68
      ParentFont = False
      Properties.Alignment.Horz = taCenter
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 19
      Width = 129
    end
    object _success_qty: TcxSpinEdit
      Left = 136
      Top = 94
      ParentFont = False
      Properties.Alignment.Horz = taCenter
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 20
      Width = 129
    end
    object _defect_qty: TcxSpinEdit
      Left = 136
      Top = 120
      ParentFont = False
      Properties.Alignment.Horz = taCenter
      Properties.UseLeftAlignmentOnEditing = False
      TabOrder = 21
      Width = 129
    end
  end
end
