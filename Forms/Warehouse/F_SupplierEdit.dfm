object FSupplierEdit: TFSupplierEdit
  Left = 0
  Top = 0
  Caption = 'FSupplierEdit'
  ClientHeight = 429
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -233
    ExplicitTop = -310
    ExplicitWidth = 680
    ExplicitHeight = 511
    Height = 429
    Width = 707
    object cxLabel2: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Nama'
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 15
      TabOrder = 0
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 114
      Caption = 'Alamat'
    end
    object _province_id: TcxLookupComboBox
      Left = 136
      Top = 195
      Properties.ListColumns = <>
      TabOrder = 10
      Width = 497
    end
    object _street: TcxMemo
      Left = 136
      Top = 113
      TabOrder = 6
      Height = 54
      Width = 497
    end
    object _subdistrict: TcxTextEdit
      Left = 136
      Top = 169
      TabOrder = 8
      Width = 497
    end
    object _region_id: TcxLookupComboBox
      Left = 136
      Top = 221
      Properties.ListColumns = <>
      TabOrder = 12
      Width = 497
    end
    object _district_id: TcxLookupComboBox
      Left = 136
      Top = 247
      Properties.ListColumns = <>
      TabOrder = 14
      Width = 497
    end
    object _postal_code: TcxTextEdit
      Left = 136
      Top = 273
      TabOrder = 16
      Width = 217
    end
    object _phone: TcxTextEdit
      Left = 136
      Top = 41
      TabOrder = 2
      Width = 497
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 170
      Caption = 'Desa'
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 196
      Caption = 'Provinsi'
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 222
      Caption = 'Kabupaten/Kota'
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 248
      Caption = 'Kecamatan'
    end
    object cxLabel8: TcxLabel
      Left = 24
      Top = 274
      Caption = 'Kode POS'
    end
    object cxLabel9: TcxLabel
      Left = 25
      Top = 42
      Caption = 'Telepon'
    end
    object cxLabel1: TcxLabel
      Left = 25
      Top = 68
      Caption = 'Email'
    end
    object _email: TcxTextEdit
      Left = 136
      Top = 67
      TabOrder = 4
      Width = 497
    end
  end
end
