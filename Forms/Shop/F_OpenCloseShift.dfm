object FOpenCloseShift: TFOpenCloseShift
  Left = 0
  Top = 0
  Caption = 'Buka / Tutup Shift'
  ClientHeight = 258
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 89
    Width = 398
    Height = 169
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet2
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpRight
    ClientRectBottom = 163
    ClientRectLeft = 2
    ClientRectRight = 371
    ClientRectTop = 6
    object cxTabSheet1: TcxTabSheet
      Caption = 'Buka Shift'
      ImageIndex = 0
      object cxLabel4: TcxLabel
        Left = 16
        Top = 21
        Caption = 'Modal'
        ParentFont = False
        Style.TextColor = clBlack
        Transparent = True
      end
      object edtInitialBalance: TcxCurrencyEdit
        Left = 129
        Top = 13
        EditValue = 0.000000000000000000
        ParentFont = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0.00'
        Properties.ReadOnly = False
        Properties.UseDisplayFormatWhenEditing = True
        Properties.UseLeftAlignmentOnEditing = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -20
        Style.Font.Name = 'Bahnschrift SemiLight'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = 'Office2007Black'
        Style.TextColor = clTeal
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        StyleDisabled.BorderStyle = ebsOffice11
        StyleDisabled.Color = clWhite
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
        StyleDisabled.TextColor = clGray
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = 'Office2007Black'
        TabOrder = 0
        Width = 209
      end
      object cxLabel7: TcxLabel
        Left = 16
        Top = 51
        Caption = 'Shift'
        ParentFont = False
        Style.TextColor = clBlack
        Transparent = True
      end
      object btnOpenShift: TcxButton
        Left = 129
        Top = 98
        Width = 209
        Height = 41
        Caption = 'BUKA SHIFT'
        Colors.Default = clTeal
        Colors.DefaultText = clWhite
        LookAndFeel.SkinName = ''
        OptionsImage.ImageIndex = 0
        OptionsImage.Margin = 20
        TabOrder = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Bahnschrift SemiLight'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnOpenShiftClick
      end
      object lblHint1: TcxLabel
        Left = 280
        Top = 105
        AutoSize = False
        Caption = 'Ctrl+S'
        ParentColor = False
        ParentFont = False
        Style.BorderColor = clWhite
        Style.BorderStyle = ebsSingle
        Style.Color = clTeal
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Bahnschrift SemiLight'
        Style.Font.Style = []
        Style.TextColor = clWhite
        Style.IsFontAssigned = True
        StyleDisabled.Color = clSilver
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Height = 24
        Width = 47
        AnchorX = 304
        AnchorY = 117
      end
      object cmbShift: TcxLookupComboBox
        Left = 129
        Top = 46
        ParentFont = False
        Properties.ListColumns = <>
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -17
        Style.Font.Name = 'Bahnschrift SemiLight'
        Style.Font.Style = []
        Style.LookAndFeel.SkinName = 'Office2007Black'
        Style.TextColor = clTeal
        Style.IsFontAssigned = True
        StyleDisabled.Color = clWhite
        StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
        StyleDisabled.TextColor = clGray
        StyleDisabled.ButtonStyle = btsDefault
        StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
        StyleHot.LookAndFeel.SkinName = 'Office2007Black'
        TabOrder = 5
        Width = 209
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Tutup Shift'
      ImageIndex = 1
      object cxLabel9: TcxLabel
        Left = 16
        Top = 17
        Caption = 'Saldo Akhir'
        ParentFont = False
        Style.TextColor = clBlack
        Transparent = True
      end
      object edtFinalBalance: TcxCurrencyEdit
        Left = 129
        Top = 11
        EditValue = 0.000000000000000000
        ParentFont = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0.00'
        Properties.ReadOnly = False
        Properties.UseDisplayFormatWhenEditing = True
        Properties.UseLeftAlignmentOnEditing = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -20
        Style.Font.Name = 'Bahnschrift SemiLight'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = 'Office2007Black'
        Style.TextColor = clTeal
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        StyleDisabled.BorderStyle = ebsOffice11
        StyleDisabled.Color = clWhite
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
        StyleDisabled.TextColor = clGray
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = 'Office2007Black'
        TabOrder = 0
        Width = 209
      end
      object btnCloseShift: TcxButton
        Left = 129
        Top = 98
        Width = 209
        Height = 41
        Caption = 'TUTUP SHIFT'
        Colors.Default = clTeal
        Colors.DefaultText = clWhite
        Enabled = False
        LookAndFeel.SkinName = ''
        OptionsImage.ImageIndex = 0
        OptionsImage.Margin = 20
        TabOrder = 2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Bahnschrift SemiLight'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnCloseShiftClick
      end
      object lblHint2: TcxLabel
        Left = 285
        Top = 105
        AutoSize = False
        Caption = 'Ctrl+S'
        Enabled = False
        ParentColor = False
        ParentFont = False
        Style.BorderColor = clWhite
        Style.BorderStyle = ebsSingle
        Style.Color = clTeal
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Bahnschrift SemiLight'
        Style.Font.Style = []
        Style.TextColor = clWhite
        Style.IsFontAssigned = True
        StyleDisabled.Color = clSilver
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Height = 24
        Width = 47
        AnchorX = 309
        AnchorY = 117
      end
    end
  end
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    ParentFont = False
    Style.LookAndFeel.SkinName = 'VS2010'
    StyleDisabled.LookAndFeel.SkinName = 'VS2010'
    TabOrder = 1
    Height = 89
    Width = 398
    object cxLabel1: TcxLabel
      Left = 18
      Top = 22
      Caption = 'Toko / Gudang'
      ParentFont = False
      Style.TextColor = clBlack
      Transparent = True
    end
    object cmbStorage: TcxLookupComboBox
      Left = 131
      Top = 17
      Enabled = False
      ParentFont = False
      Properties.ListColumns = <>
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -17
      Style.Font.Name = 'Bahnschrift SemiLight'
      Style.Font.Style = []
      Style.LookAndFeel.SkinName = 'Office2007Black'
      Style.TextColor = clTeal
      Style.IsFontAssigned = True
      StyleDisabled.Color = clWhite
      StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
      StyleDisabled.TextColor = clGray
      StyleDisabled.ButtonStyle = btsDefault
      StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
      StyleHot.LookAndFeel.SkinName = 'Office2007Black'
      TabOrder = 1
      Width = 209
    end
    object dteOpenAt: TcxDateEdit
      Left = 131
      Top = 47
      Enabled = False
      ParentFont = False
      Properties.DisplayFormat = 'dd-mm-yyyy h:n:s'
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -17
      Style.Font.Name = 'Bahnschrift SemiLight'
      Style.Font.Style = []
      Style.LookAndFeel.SkinName = 'Office2007Black'
      Style.TextColor = clTeal
      Style.IsFontAssigned = True
      StyleDisabled.Color = clWhite
      StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
      StyleDisabled.TextColor = clGray
      StyleDisabled.ButtonStyle = btsDefault
      StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
      StyleHot.LookAndFeel.SkinName = 'Office2007Black'
      TabOrder = 2
      Width = 209
    end
    object cxLabel2: TcxLabel
      Left = 18
      Top = 52
      Caption = 'Waktu Buka'
      ParentFont = False
      Style.TextColor = clBlack
      Transparent = True
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 48
    object BukaTutupShift1: TMenuItem
      Caption = 'Buka / Tutup Shift'
      ShortCut = 16467
      OnClick = BukaTutupShift1Click
    end
  end
end
