unit F_SearchResult;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxClasses,
  cxGridLevel, cxGrid, System.Generics.Collections, Product, cxContainer,
  cxGroupBox, cxCurrencyEdit;

type
  TFSearchResult = class(TForm)
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    cxGrid1: TcxGrid;
    gvData: TcxGridTableView;
    gvDataId: TcxGridColumn;
    gvDataindex: TcxGridColumn;
    gvDatacode: TcxGridColumn;
    gvDataname: TcxGridColumn;
    gvDatastock: TcxGridColumn;
    gvDataretailprice: TcxGridColumn;
    gvDatapartyprice: TcxGridColumn;
    gvDatastockistprice: TcxGridColumn;
    cxGrid1Level1: TcxGridLevel;
    procedure FormShow(Sender: TObject);
    procedure gvDataKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    procedure RefreshData;
  public
    Products : TList<TProduct>;
    SelectedIndex : integer;

    HotkeysVar: TDictionary<string, integer>;
    procedure RegisterHotkeys;
    procedure WMHotKey(var Msg: TWMHotKey); message WM_HOTKEY;
  end;

var
  FSearchResult: TFSearchResult;

implementation

{$R *.dfm}

procedure TFSearchResult.FormCreate(Sender: TObject);
begin
//  HotkeysVar := TDictionary<string, integer>.Create;
//  RegisterHotkeys;
end;

procedure TFSearchResult.FormShow(Sender: TObject);
begin
  RefreshData;
end;

procedure TFSearchResult.gvDataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ri : integer;
begin
  ri := gvData.DataController.GetFocusedRecordIndex;

  if Key = 13 then
  begin
    SelectedIndex := gvData.DataController.Values[
                      ri,
                      gvDataindex.Index];
    if gvData.DataController.Values[ri, gvDatastock.Index] <= 0 then
    begin
      ShowMessage('Stok tidak mencukupi');
    end
    else
    begin
      Close;
      ModalResult := mrOk;
    end;
  end
  else if Key = 27 then
  begin
    Self.Close;
    ModalResult := mrCancel;
  end;
end;

procedure TFSearchResult.RefreshData;
var
  ri, I : integer;
begin
  with gvData.DataController do
  begin
    RecordCount := 0;
    BeginUpdate;

    ri := 0;

    for I := 0 to Products.Count-1 do
    begin
      AppendRecord;
      Values[ri, gvDataId.Index] := Products[I].Id;
      Values[ri, gvDatacode.Index] := Products[I].Code;
      Values[ri, gvDataname.Index] := Products[I].Name;
      Values[ri, gvDatastock.Index] := Products[I].Stock;
      Values[ri, gvDataretailprice.Index] := Products[I].RetailPrice;
      Values[ri, gvDatapartyprice.Index] := Products[I].PartyPrice;
      Values[ri, gvDatastockistprice.Index] := Products[I].StockistPrice;
      Values[ri, gvDataindex.Index] := I;
      Inc(ri);
    end;

    EndUpdate;
  end;
end;

procedure TFSearchResult.RegisterHotkeys;
const
  MOD_ALT = 1;
  MOD_CONTROL = 2;
  MOD_SHIFT = 4;
  MOD_WIN = 8;
begin
  HotkeysVar.Add('esc', GlobalAddAtom('esc'));
  RegisterHotKey(Handle, HotkeysVar['esc'], 0, VK_ESCAPE);
end;

procedure TFSearchResult.WMHotKey(var Msg: TWMHotKey);
begin
  if Msg.HotKey = HotkeysVar['esc'] then
  begin
    Close;
    ModalResult := mrCancel;
  end;
end;

end.
