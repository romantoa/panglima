object FOutletEdit: TFOutletEdit
  Left = 0
  Top = 0
  Caption = 'FOutletEdit'
  ClientHeight = 511
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -318
    ExplicitWidth = 765
    ExplicitHeight = 201
    Height = 511
    Width = 680
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Kode'
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Nama'
    end
    object _code: TcxTextEdit
      Left = 136
      Top = 15
      TabOrder = 2
      Width = 217
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 41
      TabOrder = 3
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 108
      Caption = 'Alamat'
    end
    object _province_id: TcxLookupComboBox
      Left = 136
      Top = 189
      Properties.ListColumns = <>
      TabOrder = 7
      Width = 497
    end
    object _street: TcxMemo
      Left = 136
      Top = 107
      TabOrder = 5
      Height = 54
      Width = 497
    end
    object _subdistrict: TcxTextEdit
      Left = 136
      Top = 163
      TabOrder = 6
      Width = 497
    end
    object _region_id: TcxLookupComboBox
      Left = 136
      Top = 215
      Properties.ListColumns = <>
      TabOrder = 8
      Width = 497
    end
    object _district_id: TcxLookupComboBox
      Left = 136
      Top = 241
      Properties.ListColumns = <>
      TabOrder = 9
      Width = 497
    end
    object _postal_code: TcxTextEdit
      Left = 136
      Top = 267
      TabOrder = 10
      Width = 217
    end
    object _phone: TcxTextEdit
      Left = 136
      Top = 67
      TabOrder = 4
      Width = 217
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 164
      Caption = 'Desa'
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 190
      Caption = 'Provinsi'
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 216
      Caption = 'Kabupaten/Kota'
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 242
      Caption = 'Kecamatan'
    end
    object cxLabel8: TcxLabel
      Left = 24
      Top = 268
      Caption = 'Kode POS'
    end
    object cxLabel9: TcxLabel
      Left = 25
      Top = 68
      Caption = 'Telepon'
    end
    object cxLabel10: TcxLabel
      Left = 24
      Top = 310
      Caption = 'Latitude'
    end
    object _lattitude: TcxCurrencyEdit
      Left = 136
      Top = 309
      Properties.DisplayFormat = '#########,######'
      TabOrder = 11
      Width = 217
    end
    object cxLabel11: TcxLabel
      Left = 24
      Top = 336
      Caption = 'Logitude'
    end
    object _longitude: TcxCurrencyEdit
      Left = 136
      Top = 335
      Properties.DisplayFormat = '#########,######'
      TabOrder = 12
      Width = 217
    end
  end
end
