object FCustomerEdit: TFCustomerEdit
  Left = 0
  Top = 0
  Caption = 'FCustomerEdit'
  ClientHeight = 402
  ClientWidth = 696
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -11
    ExplicitTop = -27
    ExplicitWidth = 707
    ExplicitHeight = 429
    Height = 402
    Width = 696
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Nama'
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 41
      TabOrder = 2
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 140
      Caption = 'Alamat'
    end
    object _province_id: TcxLookupComboBox
      Left = 136
      Top = 221
      Properties.ListColumns = <>
      TabOrder = 12
      Width = 497
    end
    object _street: TcxMemo
      Left = 136
      Top = 139
      TabOrder = 8
      Height = 54
      Width = 497
    end
    object _subdistrict: TcxTextEdit
      Left = 136
      Top = 195
      TabOrder = 10
      Width = 497
    end
    object _region_id: TcxLookupComboBox
      Left = 136
      Top = 247
      Properties.ListColumns = <>
      TabOrder = 14
      Width = 497
    end
    object _district_id: TcxLookupComboBox
      Left = 136
      Top = 273
      Properties.ListColumns = <>
      TabOrder = 16
      Width = 497
    end
    object _postal_code: TcxTextEdit
      Left = 136
      Top = 299
      TabOrder = 18
      Width = 217
    end
    object _phone: TcxTextEdit
      Left = 136
      Top = 67
      TabOrder = 4
      Width = 497
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 196
      Caption = 'Desa'
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 222
      Caption = 'Provinsi'
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 248
      Caption = 'Kabupaten/Kota'
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 274
      Caption = 'Kecamatan'
    end
    object cxLabel8: TcxLabel
      Left = 24
      Top = 300
      Caption = 'Kode POS'
    end
    object cxLabel9: TcxLabel
      Left = 25
      Top = 68
      Caption = 'Telepon'
    end
    object cxLabel1: TcxLabel
      Left = 25
      Top = 94
      Caption = 'Email'
    end
    object _email: TcxTextEdit
      Left = 136
      Top = 93
      TabOrder = 6
      Width = 497
    end
    object _number: TcxTextEdit
      Left = 136
      Top = 15
      TabOrder = 0
      Width = 217
    end
    object cxLabel10: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Nomor'
    end
  end
end
