unit F_Sale;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, Math,
  cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxBarBuiltInMenu,
  dxSkinsdxStatusBarPainter, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxPC, dxStatusBar, cxGroupBox, cxGridCustomLayoutView, cxGridCardView,
  Vcl.StdCtrls, dximctrl, Vcl.Menus, cxButtons, System.ImageList, Vcl.ImgList,
  cxTextEdit, cxLabel, cxCurrencyEdit, cxImage, cxSpinEdit, cxMaskEdit,
  cxButtonEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxCheckBox, Vcl.ExtCtrls, dxGDIPlusClasses,
  System.Generics.Collections, frxClass, dxmdaset,
  System.JSON,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, frxDBSet,
  dxSkinTheBezier, cxDataControllerConditionalFormattingRulesManagerDialog,
  System.StrUtils,
  SfStringUtils,
  SfIIFUtils,
  Fsf_Base,
  DM_Main,
  Member,
//  Reportable,
//  Configable,
  Product,
  Storeshift,
  Order,
  OrderItem,
  Saletype,
  Card,
  F_SaleBase, SfModel, SfLookupUtils;

type
  TFSale = class(TFSaleBase)
    PopupMenu1: TPopupMenu;
    Setting1: TMenuItem;
    BuatBaru1: TMenuItem;
    Bayar1: TMenuItem;
    CetakStruk1: TMenuItem;
    Kredit1: TMenuItem;
    BukaShift1: TMenuItem;
    N1: TMenuItem;
    BukaPesanan1: TMenuItem;
    cxGroupBox14: TcxGroupBox;
    cxGroupBox1: TcxGroupBox;
    pnlR1: TcxGroupBox;
    btnPayment: TcxButton;
    btnReset: TcxButton;
    btnPrint: TcxButton;
    cxLabel11: TcxLabel;
    cxLabel17: TcxLabel;
    btnOpenCloseShift: TcxButton;
    cxLabel21: TcxLabel;
    cxLabel18: TcxLabel;
    pnlR11: TcxGroupBox;
    edtMemberCode: TcxTextEdit;
    lblWaiting: TcxLabel;
    cxLabel24: TcxLabel;
    cxGroupBox15: TcxGroupBox;
    grdCart: TcxGrid;
    gvCart: TcxGridTableView;
    gvCartId: TcxGridColumn;
    gvCartProductId: TcxGridColumn;
    gvCartProductCode: TcxGridColumn;
    gvCartProductName: TcxGridColumn;
    gvCartUnit: TcxGridColumn;
    gvCartProductPrice: TcxGridColumn;
    gvCartQuantity: TcxGridColumn;
    gvCartDiscount: TcxGridColumn;
    gvCartSubtotal: TcxGridColumn;
    cxGridLevel1: TcxGridLevel;
    cxGroupBox16: TcxGroupBox;
    cxGroupBox18: TcxGroupBox;
    cxLabel1: TcxLabel;
    edtCartSubtotal: TcxCurrencyEdit;
    cxGroupBox20: TcxGroupBox;
    cxLabel2: TcxLabel;
    cxLabel6: TcxLabel;
    edtCartDpp: TcxCurrencyEdit;
    edtCartPpn: TcxCurrencyEdit;
    cxGroupBox5: TcxGroupBox;
    cxGroupBox4: TcxGroupBox;
    lblCartGrandTotal: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel7: TcxLabel;
    cxGroupBox3: TcxGroupBox;
    cxLabel8: TcxLabel;
    lblHint1: TcxLabel;
    cxGroupBox9: TcxGroupBox;
    cxLabel9: TcxLabel;
    lblOrderNumber: TcxLabel;
    lblOrderMember: TcxLabel;
    cxLabel27: TcxLabel;
    ScanKartuAnggota1: TMenuItem;
    N2: TMenuItem;
    edtCartDiscountAmount: TcxCurrencyEdit;
    edtCartDiscountPercent: TcxCurrencyEdit;
    cxLabel15: TcxLabel;
    edtSearch: TcxButtonEdit;
    cmbSaletype: TcxLookupComboBox;
    procedure edtCartDiscountPercentPropertiesEditValueChanged(Sender: TObject);
    procedure edtCartDiscountAmountPropertiesEditValueChanged(Sender: TObject);
    procedure btnPaymentClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnPrintClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtCartTotalPropertiesEditValueChanged(Sender: TObject);
    procedure gvCartDiscountPropertiesEditValueChanged(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure FullScreen1Click(Sender: TObject);
    procedure gvCartEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure edtSearchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BuatBaru1Click(Sender: TObject);
    procedure Bayar1Click(Sender: TObject);
    procedure CetakStruk1Click(Sender: TObject);
    procedure BukaShift1Click(Sender: TObject);
    procedure btnOpenCloseShiftClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BukaPesanan1Click(Sender: TObject);
    procedure edtMemberCodeEnter(Sender: TObject);
    procedure edtMemberCodeExit(Sender: TObject);
    procedure edtMemberCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure gvCartEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ScanKartuAnggota1Click(Sender: TObject);
    procedure edtSearchPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    procedure ResetSession; override;
    function CheckShiftStatus : Boolean;
  public
    //
  end;

var
  FSale: TFSale;

implementation

{$R *.dfm}

uses
  F_Payment,
  F_SearchResult,
  F_SaleFinish,
  F_OpenCloseShift;

procedure TFSale.Bayar1Click(Sender: TObject);
begin
  btnPayment.Click;
end;

procedure TFSale.CetakStruk1Click(Sender: TObject);
begin
  btnPrint.Click;
end;

function TFSale.CheckShiftStatus : Boolean;
var
  Shf: TDataSet;
  StorageId: integer;
begin
  // Check Active Shift
  StorageId := DMMain.RegisterInfo.StorageId;
  Shf := TStoreshift.GetCurrentOpenShift(StorageId);

  if not Shf.IsEmpty then
  begin
    Self.Storeshift.Id := Shf.FieldByName('id').AsInteger;
    Self.Storeshift.Shift.Id := Shf.FieldByName('shift_id').AsInteger;
    Self.Storeshift.Shift.Name := Shf.FieldByName('shift_name').AsString;

    grdCart.Enabled := True;
    edtSearch.Enabled := True;
    btnReset.Enabled := True;

    Result := True;
  end
  else
  begin
    Self.Storeshift := nil;
    grdCart.Enabled := False;
    edtSearch.Enabled := False;
    btnReset.Enabled := False;

    Result := False;
  end;
end;

procedure TFSale.btnPrintClick(Sender: TObject);
begin
//  PrintReceipt(1);
end;

procedure TFSale.btnResetClick(Sender: TObject);
begin
  if MessageDlg('Anda yakin ingin reset?', mtConfirmation, mbYesNo, 0) = mrYes then
  begin
    ResetSession;
  end;
end;

procedure TFSale.BuatBaru1Click(Sender: TObject);
begin
  btnReset.Click;
end;

procedure TFSale.BukaPesanan1Click(Sender: TObject);
begin
  edtSearch.SetFocus;
end;

procedure TFSale.BukaShift1Click(Sender: TObject);
begin
  btnOpenCloseShift.Click;
end;

procedure TFSale.btnOpenCloseShiftClick(Sender: TObject);
begin
  FOpenCloseShift := TFOpenCloseShift.Create(Application);
  FOpenCloseShift.RegisterId := DMMain.RegisterInfo.StorageId;
  if Self.StoreShift = nil then
    FOpenCloseShift.Tag := 0
  else
  begin
    FOpenCloseShift.Tag := 1;
    FOpenCloseShift.StoreshiftId := Self.StoreShift.Id;
  end;

  FOpenCloseShift.ShowModal;
  if FOpenCloseShift.ModalResult = mrOk then
  begin
    if CheckShiftStatus then
      ResetSession;
  end;
  FOpenCloseShift.Free;
end;

procedure TFSale.btnPaymentClick(Sender: TObject);
var
//  AVouchers, ARealisasiSps: String;
  AVouchers, ARealisasiSps : TStringList;
  ri: integer;
begin
//  FPayment := TFPayment.Create(Application);
//  FPayment.Subtotal := GrandTotal;
//  FPayment.Total := GrandTotal;
//  FPayment.Position := poScreenCenter;
//  FPayment.AOrderItems := memCartItem;
//
//  if (Self.Member <> nil) and (Self.Member.Id <> 0) then
//    FPayment.Member := Self.Member
//  else
//    FPayment.HideMemberElements;
//
//  {* Credit *}
//  if cmbSaletype.EditValue > 1 then
//  begin
//    if cmbSaletype.EditValue = Ord(stpKredit) then
//      FPayment.Plafon := 5000000 //TODO : Read From Setting;
//    else if cmbSaletype.EditValue = Ord(stpStockist) then
//      FPayment.Plafon := 99999999
//    else if cmbSaletype.EditValue = Ord(stpTokoKecil) then
//      FPayment.Plafon := Self.Member.PlafonTokoKecil
//    else
//      FPayment.Plafon := 0;
//
//    if FPayment.Total > FPayment.Plafon then
//    begin
//      FPayment.CreditBase := FPayment.edtPlafon.EditValue;
//      FPayment.DownPayment := FPayment.Total - FPayment.Plafon;
//    end
//    else
//    begin
//      FPayment.CreditBase := FPayment.Total;
//      FPayment.DownPayment := 0;
//    end;
//  end
//  else
//  begin
//    FPayment.HideCreditElements;
//  end;
//
//  FPayment.pgPayment.ActivePageIndex := 0;
//  FPayment.edtMemberCode.Visible := False;
//  FPayment.lblMemberCode.Visible := False;
//
//  {* End Credit *}
//  FPayment.ShowModal;
//  if FPayment.ModalResult = mrOk then
//  begin
//    {* Credit *}
//    Self.CreditAmount := FPayment.CreditTotal;
//    Self.TermCount := FPayment.spnTermcount.EditValue;
//    Self.TermAmount := FPayment.edtTermAmount.EditValue;
//    Self.Provision := FPayment.ProvisionAmount;
//    Self.DownPayment := FPayment.DownPayment;
//    Self.Plafon := FPayment.Plafon;
//    Self.CreditBase := FPayment.CreditBase;
//    Self.Interest := FPayment.Interest;
//
//    Self.GrandTotal := Self.DownPayment + Self.Provision;
//    {* End Credit *}
//
//    Member := FPayment.Member;
//    WithVoucher := FPayment.WithVoucher;
//    CashChange := FPayment.Change;
//    WithSp := FPayment.WithSp;
//    WithCash := FPayment.WithCash;
//    WithSimpananku := FPayment.WithSimpananku;
//    CardFee := FPayment.CardFee;
//    if FPayment.cmbCard.EditValue <> null then
//      CardId := FPayment.cmbCard.EditValue;
//    CardApprNumber := VarToStr(FPayment.edtApprovalNumber.EditValue);
//
//    DeliveryFee := FPayment.DeliveryFee.Fee;
//
//    if FPayment.Delivery <> nil then
//      ADelivery := FPayment.Delivery;
//
//    AVouchers := TStringList.Create;
//    for ri := 0 to FPayment.gvVoucher.DataController.RecordCount - 1 do
//    begin
//      if FPayment.gvVoucher.DataController.Values[ri, FPayment.gvVoucherAction.Index] then
//      begin
//        AVouchers.Add(
//          IntToStr(FPayment.gvVoucher.DataController.Values[ri,
//          FPayment.gvVoucherId.Index])
//        );
//      end;
//    end;
//
//    ARealisasiSps := TStringList.Create;
//    for ri := 0 to FPayment.gvRealisasisp.DataController.RecordCount - 1 do
//    begin
//      if FPayment.gvRealisasisp.DataController.Values[ri, FPayment.gvRealisasispAction.Index] then
//      begin
//        ARealisasiSps.Add(
//          IntToStr(FPayment.gvRealisasisp.DataController.Values[ri,
//          FPayment.gvRealisasispId.Index])
//        );
//      end;
//    end;
//
//    FinalizeOrder(AVouchers, ARealisasiSps, cmbSaletype.EditValue, 1);
//    PrintReceipt(FPayment.spnPrintCount.EditValue);
//
//    FSaleFinish := TFSaleFinish.Create(nil);
//    FSaleFinish.lblChange.Caption := FormatCurr(MoneyFormat, Self.CashChange);
//    FSaleFinish.ShowModal;
//    if FSaleFinish.ModalResult = mrOk then
//      ResetSession;
//    FSaleFinish.Free;
//  end;
//  FPayment.Free;
end;

procedure TFSale.edtCartDiscountAmountPropertiesEditValueChanged
  (Sender: TObject);
begin
//  Self.DiscountAmount := TcxTextEdit(Sender).EditValue;

//  UpdateCartGrandTotal;
end;

procedure TFSale.edtCartDiscountPercentPropertiesEditValueChanged
  (Sender: TObject);
begin
  TcxTextEdit(Sender).PostEditValue;

//  UpdateCartDiscount;
end;

procedure TFSale.edtCartTotalPropertiesEditValueChanged(Sender: TObject);
begin
  TcxTextEdit(Sender).PostEditValue;

//  btnPayment.Enabled := (TcxCustomEdit(Sender).EditValue <> 0);
//  btnCredit.Enabled := ((TcxCustomEdit(Sender).EditValue <> 0) and
//                        (Member.Id <> 0) and
//                        (cmbSaletype.EditValue > 1));
end;

procedure TFSale.edtMemberCodeEnter(Sender: TObject);
begin
  lblWaiting.StyleDisabled.TextColor := clGreen;
end;

procedure TFSale.edtMemberCodeExit(Sender: TObject);
begin
  lblWaiting.StyleDisabled.TextColor := clBtnShadow;
end;

procedure TFSale.edtMemberCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  if Key = 13 then
//  begin
//    if TcxTextEdit(Sender).EditingText <> '' then
//    begin
//      Member := Member.AttemptLogin(TcxCustomEdit(Sender).EditingValue);
//      if Member <> nil then
//      begin
//        lblMemberName.Caption := Member.FirstName;
//        lblMemberType.Caption := UpperCase(Member.&Type.Name);
//        lblMemberType.Visible := Member.&Type.Id > 1;
////        btnCredit.Enabled := (Self.GrandTotal <> 0);
//
//        if Member.&Type.Id = 2 then
//          cmbSaletype.EditValue := 3
//        else if Member.&Type.Id = 3 then
//          cmbSaletype.EditValue := 4
//        else
//          cmbSaletype.EditValue := 1;
//      end
//      else
//      begin
//        cmbSaletype.EditValue := 1;
//        MessageDlg('Anggota Tidak Ditemukan', mtWarning, [mbOK], 0);
//      end;
//
//      TcxCustomEdit(Sender).EditValue := '';
//      lblWaiting.Visible := True;
//      btnCredit.Enabled := btnPayment.Enabled and (cmbSaletype.EditValue <> 1);
//    end;
//  end
//  else
//  begin
//    lblWaiting.Visible := TcxTextEdit(Sender).EditingValue = '';
//  end;
end;

procedure TFSale.edtSearchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    edtSearchPropertiesButtonClick(Sender, 0);
  end;
end;

procedure TFSale.edtSearchPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  ACode : string;
begin
  ACode := edtSearch.EditingValue;
  if Length(ACode) < 13 then
  begin
    ACode := StringReplace(ACode, '.', '', [rfReplaceAll]);
    ACode := UpperCase(LeftStr(ACode, 3)) + '.' +
      RightStr(ACode, Length(ACode) - 3).PadLeft(9, '0');
  end;

  _order := TOrder.SeachByCode(ACode);

  if _order <> nil then
  begin

    LoadOrder;

    btnPrint.Enabled := True;
    btnPayment.Enabled := (_order.status = 7); //Draft
  end;

  edtSearch.Clear;
end;

procedure TFSale.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFSale.FormCreate(Sender: TObject);
begin
  inherited;
//  lblRegister.Caption := DMMain.Config.ReadString('RegisterId_Text');

  SetLookupProperties(cmbSaletype, TSfModel.Fetch('saletype'));
  gvCart.OnEditing := gvCartEditing;
end;

procedure TFSale.FormShow(Sender: TObject);
begin
  if Self.Parent is TcxTabSheet then
    (Self.Parent as TcxTabSheet).PageControl.Properties.HideTabs := True;

  if CheckShiftStatus then
    ResetSession;

  cmbSaletype.ItemIndex := 0;
end;

procedure TFSale.FullScreen1Click(Sender: TObject);
begin
  Self.BorderStyle := bsNone;
  Self.WindowState := wsMaximized;
end;

procedure TFSale.gvCartDiscountPropertiesEditValueChanged(Sender: TObject);
var
  id, ri, q: integer;
  d, p, newD: Currency;
  found: Boolean;
begin
  ri := gvCart.DataController.GetFocusedRecordIndex;

  id := gvCart.DataController.Values[ri, gvCartId.Index];

  found := memCartItem.Locate('Id', id, [loPartialKey]);
  newD := TcxCurrencyEdit(Sender).EditingValue;

  if found then
  begin

    memCartItem.Edit;
    memCartItem.FieldByName('Discount').Value := newD;
    q := memCartItem.FieldByName('Quantity').AsInteger;
    p := memCartItem.FieldByName('ProductPrice').AsFloat;
    memCartItem.FieldByName('Subtotal').Value := (q * p) - newD;
    memCartItem.Post;

    RefreshCart;
  end;
end;

procedure TFSale.gvCartEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  ri, rc, ci : integer;
begin
  inherited;
  rc := Sender.DataController.RecordCount;
  ri := Sender.DataController.GetFocusedRecordIndex;
  ci := TcxGridTableView(Sender).Controller.FocusedColumnIndex;

  AAllow := (ri = (rc-1)) or (ci = 3);
end;

procedure TFSale.gvCartEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
var
  ri, id, q0, AQuantity: integer;
  p: TProduct;
begin
  if (Key = 13) and (AEdit.EditingValue <> null) then
  begin
    ri := gvCart.DataController.GetFocusedRecordIndex;
    q0 := gvCart.DataController.Values[ri, gvCartQuantity.Index];
    id := gvCart.DataController.Values[ri, gvCartId.Index];

    AEdit.PostEditValue;

    if (gvCart.Controller.FocusedColumnIndex = 0) then
    begin
      if ProcessSearch(AEdit.EditValue) then
        AppendNewItem;
    end
    else if (gvCart.Controller.FocusedColumnIndex = 3) then
    begin
      if id = 0 then
      begin
        // Jika Set Quantity before Insert, set focus ke kolom code
        gvCart.Controller.FocusedColumnIndex := 0;
      end
      else
      begin
        // Jika Edit Existing Quantity, Update Selected Record
        AQuantity := AEdit.EditValue - q0;

        // Update Quantity
        p := TProduct.Create;
        p.id := gvCart.DataController.Values[ri, gvCartProductId.Index];

        memCartItem.Locate('ProductId', p.id, [loCaseInsensitive]);

        p.Code := memCartItem.FieldByName('ProductCode').AsString;
        p.Name := memCartItem.FieldByName('ProductName').AsString;
        p.ProductUnit.id := memCartItem.FieldByName('ProductUnit').AsInteger;

        p.RetailPrice := memCartItem.FieldByName('RetailPrice').AsFloat;
        p.RetailDiscount := memCartItem.FieldByName('RetailDiscount').AsFloat;
        p.PartyPrice := memCartItem.FieldByName('PartyPrice').AsFloat;
        p.PartyPriceQuantity := memCartItem.FieldByName('PartyPriceQuantity')
          .AsInteger;
        p.StockistPrice := memCartItem.FieldByName('StockistPrice').AsFloat;
        p.IsTaxed := memCartItem.FieldByName('IsTaxed').AsBoolean;

        UpdateCart(p, AQuantity, 0);

        AppendNewItem;
      end;
    end;
  end;
end;

procedure TFSale.ResetSession;
begin
  RefreshCart;
  AppendNewItem;
  _grdCart.SetFocus;
end;

procedure TFSale.ScanKartuAnggota1Click(Sender: TObject);
begin
  inherited;
  edtMemberCode.SetFocus;
end;

initialization
  RegisterClass(TFSale);
finalization
  UnRegisterClass(TFSale);

end.
