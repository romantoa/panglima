object FSearchResult: TFSearchResult
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Search Result'
  ClientHeight = 343
  ClientWidth = 842
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    Caption = 'Cari Produk'
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.Color = clTeal
    Style.LookAndFeel.NativeStyle = False
    Style.LookAndFeel.SkinName = ''
    Style.TextColor = clWhite
    Style.TextStyle = [fsBold]
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.SkinName = ''
    TabOrder = 0
    Height = 343
    Width = 842
    object cxGroupBox2: TcxGroupBox
      Left = 2
      Top = 21
      Align = alClient
      PanelStyle.Active = True
      ParentFont = False
      Style.LookAndFeel.SkinName = 'Whiteprint'
      StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
      TabOrder = 0
      ExplicitLeft = 328
      ExplicitTop = 120
      ExplicitHeight = 318
      Height = 320
      Width = 838
      object cxGrid1: TcxGrid
        AlignWithMargins = True
        Left = 6
        Top = 6
        Width = 826
        Height = 308
        Align = alClient
        TabOrder = 0
        LookAndFeel.SkinName = 'Metropolis'
        ExplicitLeft = 5
        ExplicitTop = 26
        ExplicitWidth = 832
        ExplicitHeight = 312
        object gvData: TcxGridTableView
          OnKeyDown = gvDataKeyDown
          Navigator.Buttons.CustomButtons = <>
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object gvDataId: TcxGridColumn
            Caption = 'Id'
            Visible = False
          end
          object gvDataindex: TcxGridColumn
            Caption = 'Index'
            Visible = False
          end
          object gvDatacode: TcxGridColumn
            Caption = 'Kode'
            Width = 114
          end
          object gvDataname: TcxGridColumn
            Caption = 'Nama'
            Width = 252
          end
          object gvDatastock: TcxGridColumn
            Caption = 'Stok'
            Width = 80
          end
          object gvDataretailprice: TcxGridColumn
            Caption = 'Harga Eceran'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.DisplayFormat = '##,#0.#0'
            Properties.UseLeftAlignmentOnEditing = False
            HeaderAlignmentHorz = taRightJustify
            Width = 120
          end
          object gvDatapartyprice: TcxGridColumn
            Caption = 'Harga Partai'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.DisplayFormat = '##,#0.#0'
            Properties.UseLeftAlignmentOnEditing = False
            HeaderAlignmentHorz = taRightJustify
            Width = 120
          end
          object gvDatastockistprice: TcxGridColumn
            Caption = 'Harga Stockist'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.DisplayFormat = '##,#0.#0'
            Properties.UseLeftAlignmentOnEditing = False
            HeaderAlignmentHorz = taRightJustify
            Width = 120
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = gvData
        end
      end
    end
  end
end
