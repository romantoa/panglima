unit F_Payment;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxLabel, cxTextEdit, cxCurrencyEdit, cxGroupBox,
  cxRadioGroup, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxMaskEdit, cxButtonEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMemo,
  Payment, PaymentMethod, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,

  Member, Address,
  Simpananku, cxCalendar, cxCheckBox, Generics.Collections,
  dxSkinTheBezier, cxDataControllerConditionalFormattingRulesManagerDialog,
  dxBarBuiltInMenu, cxPC, cxSpinEdit, Math, Delivery, Card, dxmdaset, Voucher;

type
  TFPayment = class(TForm)
    cxGroupBox4: TcxGroupBox;
    memCard: TdxMemData;
    memCardcard_id: TIntegerField;
    memCardcard_name: TStringField;
    memCardcard_feerate: TFloatField;
    DataSource1: TDataSource;
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    cxGroupBox5: TcxGroupBox;
    pnlMember: TcxGroupBox;
    lblVoucher: TcxLabel;
    lblMemberName: TcxLabel;
    cxLabel10: TcxLabel;
    edtMemberCode: TcxTextEdit;
    lblMemberCode: TcxLabel;
    cxGroupBox9: TcxGroupBox;
    btnCancel: TcxButton;
    btnPrint: TcxButton;
    cxGroupBox7: TcxGroupBox;
    edtWithCash: TcxCurrencyEdit;
    lblKembali: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel1: TcxLabel;
    lblDeliveryFeeLabel: TcxLabel;
    cxLabel3: TcxLabel;
    lblWithCash: TcxLabel;
    lblWithRealisasiSpLabel: TcxLabel;
    lblWithVoucherLabel: TcxLabel;
    lblGrandTotal: TcxLabel;
    lblDeliveryFee: TcxLabel;
    lblWithVoucher: TcxLabel;
    lblChange: TcxLabel;
    lblPaid: TcxLabel;
    lblTotal: TcxLabel;
    pgPayment: TcxPageControl;
    tsVoucher: TcxTabSheet;
    tsRealisasiSp: TcxTabSheet;
    tsCard: TcxTabSheet;
    tsDelivery: TcxTabSheet;
    grbPengiriman: TcxGroupBox;
    cxLabel5: TcxLabel;
    mmAlamat: TcxMemo;
    btnAddress1: TcxButton;
    btnAddress2: TcxButton;
    btnAddress3: TcxButton;
    btnAddress4: TcxButton;
    btnAddress5: TcxButton;
    cxLabel11: TcxLabel;
    lblDistanceX: TcxLabel;
    edtDistance: TcxCurrencyEdit;
    edtWeight: TcxCurrencyEdit;
    edtDeliveryFee: TcxCurrencyEdit;
    cxLabel15: TcxLabel;
    cxLabel16: TcxLabel;
    cxLabel13: TcxLabel;
    lblWithSimpanankuLabel: TcxLabel;
    edtWithSimpananku: TcxCurrencyEdit;
    lblWithSp: TcxLabel;
    lblPrintCount: TcxLabel;
    spnPrintCount: TcxSpinEdit;
    cxLabel12: TcxLabel;
    lblRealisasiSp: TcxLabel;
    cxLabel17: TcxLabel;
    lblSimpananku: TcxLabel;
    rdgMetodePengiriman: TcxRadioGroup;
    cxGroupBox6: TcxGroupBox;
    cxGrid1: TcxGrid;
    gvVoucher: TcxGridTableView;
    gvVoucherId: TcxGridColumn;
    gvVoucherNumber: TcxGridColumn;
    gvVoucherAmount: TcxGridColumn;
    gvVoucherDateExpired: TcxGridColumn;
    gvVoucherAction: TcxGridColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGroupBox8: TcxGroupBox;
    cxGrid2: TcxGrid;
    gvRealisasisp: TcxGridTableView;
    gvRealisasispId: TcxGridColumn;
    gvRealisasispNumber: TcxGridColumn;
    gvRealisasispAmount: TcxGridColumn;
    gvRealisasispDateexpired: TcxGridColumn;
    gvRealisasispAction: TcxGridColumn;
    cxGridLevel1: TcxGridLevel;
    cxGroupBox10: TcxGroupBox;
    cxLabel20: TcxLabel;
    cxLabel23: TcxLabel;
    edtCardFee: TcxCurrencyEdit;
    cmbCard: TcxLookupComboBox;
    cxLabel24: TcxLabel;
    edtApprovalNumber: TcxTextEdit;
    cxLabel18: TcxLabel;
    cxLabel19: TcxLabel;
    tsCredit: TcxTabSheet;
    cxGroupBox12: TcxGroupBox;
    cxLabel6: TcxLabel;
    edtCreditBase: TcxCurrencyEdit;
    cxLabel8: TcxLabel;
    edtProvisionAmount: TcxCurrencyEdit;
    edtProvisionPercent: TcxCurrencyEdit;
    cxLabel21: TcxLabel;
    spnTermcount: TcxSpinEdit;
    cxLabel22: TcxLabel;
    cxLabel25: TcxLabel;
    edtTotal: TcxCurrencyEdit;
    cxLabel26: TcxLabel;
    edtPlafon: TcxCurrencyEdit;
    cxLabel27: TcxLabel;
    edtInterestPercent: TcxCurrencyEdit;
    edtInterestAmount: TcxCurrencyEdit;
    cxGroupBox13: TcxGroupBox;
    cxLabel28: TcxLabel;
    edtTermAmount: TcxCurrencyEdit;
    cxLabel29: TcxLabel;
    edtDownPayment: TcxCurrencyEdit;
    cxLabel30: TcxLabel;
    edtCreditTotal: TcxCurrencyEdit;
    cxLabel31: TcxLabel;
    cxLabel32: TcxLabel;
    lblLine: TcxLabel;
    lblProvisionLabel: TcxLabel;
    lblProvisionAmount: TcxLabel;
    cxLabel35: TcxLabel;
    cxLabel36: TcxLabel;
    cxLabel37: TcxLabel;
    lblCreditLabel: TcxLabel;
    lblCreditBase: TcxLabel;
    lblKurangLabel: TcxLabel;
    lblKurang: TcxLabel;
    procedure btnCancelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure edtCashPropertiesEditValueChanged(Sender: TObject);
    procedure edtMemberCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure edtMemberCodeEnter(Sender: TObject);
    procedure edtMemberCodeExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gvVoucherActionPropertiesEditValueChanged(Sender: TObject);
    procedure btnAddress1Click(Sender: TObject);
    procedure cmbCardPropertiesEditValueChanged(Sender: TObject);
    procedure edtWithCashKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cmbCardKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtApprovalNumberKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure spnPrintCountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rdgMetodePengirimanClick(Sender: TObject);
    procedure gvRealisasispActionPropertiesEditValueChanged(Sender: TObject);
    procedure edtInterestPercentPropertiesChange(Sender: TObject);
    procedure edtInterestAmountPropertiesChange(Sender: TObject);
    procedure edtProvisionPercentPropertiesChange(Sender: TObject);
    procedure edtProvisionAmountPropertiesChange(Sender: TObject);
    procedure spnTermcountPropertiesChange(Sender: TObject);
    procedure edtCreditBasePropertiesChange(Sender: TObject);
  private
    ASaldoVc,
    ASaldoSp,
    ASaldoSimpananku,
    ASubtotal,
    AWithVoucher,
    AWithSp,
    APaid,
    ACardFee,
    AChange : Currency;

    ADeliveryFee: RDeliveryFee;

    addrBtn: TDictionary<TcxButton, TAddress>;

    procedure RefreshBalances;
    procedure RefreshAddresses;
    procedure RecalculatePayment;
    procedure RecalculateCredit;
    procedure FetchLookups;
    procedure RefreshMemberProperties;
    procedure ResetDeliveryFee;

    function GetSubtotal: Currency;
    procedure SetSubtotal(const Value: Currency);
    function GetChange: Currency;
    function GetPaid: Currency;
    function GetTotal: Currency;
    function GetWithCash: Currency;
    function GetWithSp: Currency;
    function GetWithVoucher: Currency;
    procedure SetChange(const Value: Currency);
    procedure SetDeliveryFee(const Value: RDeliveryFee);
    procedure SetPaid(const Value: Currency);
    procedure SetTotal(const Value: Currency);
    procedure SetWithCash(const Value: Currency);
    procedure SetWithSp(const Value: Currency);
    procedure SetWithVoucher(const Value: Currency);
    function GetSaldoVc: Currency;
    procedure SetSaldoVc(const Value: Currency);
    function GetSaldoSp: Currency;
    procedure SetSaldoSp(const Value: Currency);
    function GetDeliveryFee: RDeliveryFee;
    function GetCardFee: Currency;
    procedure SetCardFee(const Value: Currency);
    function GetSaldoSimpananku: Currency;
    procedure SetSaldoSimpananku(const Value: Currency);
    function GetWithSimpananku: Currency;
    procedure SetWithSimpananku(const Value: Currency);
    function GetProvisionAmount: Currency;
    procedure SetProvisionAmount(const Value: Currency);
    function GetCreditBase: Currency;
    procedure SetCreditBase(const Value: Currency);
    function GetPlafon: Currency;
    procedure SetPlafon(const Value: Currency);
    function GetDownPayment: Currency;
    procedure SetDownPayment(const Value: Currency);
    function GetCreditTotal: Currency;
    procedure SetCreditTotal(const Value: Currency);
    function GetTermAmount: Currency;
    procedure SetTermAmount(const Value: Currency);
    function GetInterest: Currency;
    procedure SetInterest(const Value: Currency);
    function GetTermCount: Integer;
    procedure SetTermCount(const Value: Integer);

  public
    { Public declarations }
    Member : TMember;
    Delivery : TDelivery;
    AOrderItems : TDataSet;

    procedure HideCreditElements;
    procedure HideMemberElements;

    property Subtotal: Currency read GetSubtotal write SetSubtotal;
    property WithVoucher: Currency read GetWithVoucher write SetWithVoucher;
    property WithSp: Currency read GetWithSp write SetWithSp;
    property WithCash: Currency read GetWithCash write SetWithCash;
    property Total: Currency read GetTotal write SetTotal;
    property Paid: Currency read GetPaid write SetPaid;
    property Change: Currency read GetChange write SetChange;
    property SaldoVc: Currency read GetSaldoVc write SetSaldoVc;
    property SaldoSp: Currency read GetSaldoSp write SetSaldoSp;
    property DeliveryFee: RDeliveryFee read GetDeliveryFee write SetDeliveryFee;
    property CardFee: Currency read GetCardFee write SetCardFee;
    property SaldoSimpananku: Currency read GetSaldoSimpananku write SetSaldoSimpananku;
    property WithSimpananku: Currency read GetWithSimpananku write SetWithSimpananku;

    {* Credits *}
    property ProvisionAmount: Currency read GetProvisionAmount write SetProvisionAmount;
    property CreditBase: Currency read GetCreditBase write SetCreditBase;
    property Plafon: Currency read GetPlafon write SetPlafon;
    property DownPayment: Currency read GetDownPayment write SetDownPayment;
    property CreditTotal: Currency read GetCreditTotal write SetCreditTotal;
    property TermAmount: Currency read GetTermAmount write SetTermAmount;
    property Interest: Currency read GetInterest write SetInterest;
    property TermCount: Integer read GetTermCount write SetTermCount;
    {* End Credits *}
  end;

const
  MoneyFormat = '##,#0.##';

var
  FPayment: TFPayment;

implementation

{$R *.dfm}

uses DM_Main;

procedure TFPayment.btnAddress1Click(Sender: TObject);
var
  addr: TAddress;
  originId: integer;
  ordItms: TList<ROrderItem>;
  ordItm: ROrderItem;
  I: Integer;
begin
  inherited;
  addr := addrBtn[TcxButton(Sender)];
  mmAlamat.Text := addr.FullAddress.Text;

  originId := DMMain.IniFile.ReadInteger('kasir', 'storage_id', 0);

  ordItms := TList<ROrderItem>.Create;

  AOrderItems.First;
  for I := 0 to AOrderItems.RecordCount-1 do
  begin
    ordItm.ProductId := AOrderItems.FieldByName('ProductId').AsInteger;
    ordItm.Quantity := AOrderItems.FieldByName('Quantity').AsInteger;

    ordItms.Add(ordItm);

    AOrderItems.Next;
  end;

  DeliveryFee := TDelivery.Fee(originId, addr.Id, ordItms);

  Delivery := TDelivery.Create;
  Delivery.Cost := DeliveryFee.Fee;
  Delivery.Address := addr;
end;

procedure TFPayment.btnCancelClick(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;

procedure TFPayment.cmbCardKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    edtApprovalNumber.SetFocus;
end;

procedure TFPayment.cmbCardPropertiesEditValueChanged(Sender: TObject);
var
  cardId : integer;
begin
  memCard.Locate('card_id', cmbCard.EditValue, [loPartialKey]);
  CardFee := WithCash * memCard.FieldByName('card_feerate').AsFloat/100;
end;

procedure TFPayment.btnPrintClick(Sender: TObject);
begin
  if (Paid + CreditBase) < Total then
  begin
    MessageDlg('Pembayaran Belum Lunas', mtWarning, [mbOK], 0);
  end
  else if Change > WithCash then
  begin
    MessageDlg('Kembalian tidak boleh lebih besar dari uang tunai', mtWarning,
      [mbOK], 0);
  end
  else
  begin
    Close;
    ModalResult := mrOk;
  end;
end;

procedure TFPayment.edtApprovalNumberKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    spnPrintCount.SetFocus;
end;

procedure TFPayment.edtCashPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  // Set nilai ke maksimum Saldo SP jika angka yang dimasukkan melebihi saldo
  if (Sender = edtWithSimpananku) and (edtWithSimpananku.EditValue > SaldoSimpananku) then
  begin
    MessageDlg('Mohon maaf, saldo Simpanan anda tidak mencukupi.',
      mtWarning, [mbOK], 0);
    WithSimpananku := SaldoSimpananku;
    Abort;
  end;

  RecalculatePayment;
end;

procedure TFPayment.edtCreditBasePropertiesChange(Sender: TObject);
begin
  TcxCustomEdit(Sender).PostEditValue;
  RecalculateCredit;
  RecalculatePayment;
end;

procedure TFPayment.edtInterestAmountPropertiesChange(Sender: TObject);
begin
  if TcxTextEdit(Sender).Name = ActiveControl.Parent.Name then
    edtInterestPercent.EditValue := 100 * TcxTextEdit(Sender).EditingValue / Self.CreditBase;

  TcxCustomEdit(Sender).PostEditValue;
  RecalculateCredit;
end;

procedure TFPayment.edtInterestPercentPropertiesChange(Sender: TObject);
begin
  if TcxTextEdit(Sender).Name = ActiveControl.Parent.Name then
    edtInterestAmount.EditValue := TcxTextEdit(Sender).EditingValue * 0.01 * Self.CreditBase;
end;

procedure TFPayment.edtMemberCodeEnter(Sender: TObject);
begin
  inherited;
  lblMemberCode.StyleDisabled.TextColor := clGreen;
end;

procedure TFPayment.edtMemberCodeExit(Sender: TObject);
begin
  inherited;
  lblMemberCode.StyleDisabled.TextColor := clBtnShadow;
end;

procedure TFPayment.edtMemberCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    if TcxTextEdit(Sender).EditingText <> '' then
    begin
      Member := Member.AttemptLogin(TcxCustomEdit(Sender).EditingValue);
      if Member <> nil then
      begin
        RefreshMemberProperties;
        rdgMetodePengiriman.Enabled := True;
      end
      else
      begin
        rdgMetodePengiriman.Enabled := False;
        grbPengiriman.Enabled := False;
        ResetDeliveryFee;
        MessageDlg('Anggota Tidak Ditemukan', mtWarning, [mbOK], 0);
      end;

      TcxCustomEdit(Sender).EditValue := '';
    end
    else
    begin
      edtWithCash.SetFocus;
    end;
  end;
end;

procedure TFPayment.edtProvisionAmountPropertiesChange(Sender: TObject);
begin
  if TcxTextEdit(Sender).Name = ActiveControl.Parent.Name then
    edtProvisionPercent.EditValue := 100 * TcxTextEdit(Sender).EditingValue / Self.CreditBase;

  TcxCustomEdit(Sender).PostEditValue;
  RecalculateCredit;
  RecalculatePayment;
end;

procedure TFPayment.edtProvisionPercentPropertiesChange(Sender: TObject);
begin
  if TcxTextEdit(Sender).Name = ActiveControl.Parent.Name then
    Self.ProvisionAmount := TcxTextEdit(Sender).EditingValue * 0.01 * Self.CreditBase;
end;

procedure TFPayment.edtWithCashKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    btnPrint.SetFocus;
end;

procedure TFPayment.FormCreate(Sender: TObject);
begin
  inherited;
  Member := TMember.Create;
  Member.Id := 0;
  Member.FirstName := 'Customer Tunai';
  lblMemberName.Caption := 'Customer Tunai';
  FetchLookups;
end;

procedure TFPayment.FormShow(Sender: TObject);
begin
  inherited;
  RecalculatePayment;
  if (Self.Member <> nil) and (Self.Member.Id <> 0) then
    RefreshMemberProperties
//  else
//  begin
//    edtMemberCode.SetFocus;
//  end;
end;

function TFPayment.GetCreditBase: Currency;
begin
  Result := edtCreditBase.EditValue;
end;

function TFPayment.GetCreditTotal: Currency;
begin
  Result := edtCreditTotal.EditValue;
end;

function TFPayment.GetCardFee: Currency;
begin
  Result := ACardFee;
end;

function TFPayment.GetChange: Currency;
begin
  Result := AChange;
end;

function TFPayment.GetDeliveryFee: RDeliveryFee;
begin
  Result := ADeliveryFee;
end;

function TFPayment.GetDownPayment: Currency;
begin
  Result := edtDownPayment.EditValue;
end;

function TFPayment.GetInterest: Currency;
begin
  Result := edtInterestAmount.EditValue;
end;

function TFPayment.GetPaid: Currency;
begin
  Result := APaid;
end;

function TFPayment.GetPlafon: Currency;
begin
  Result := edtPlafon.EditValue;
end;

function TFPayment.GetProvisionAmount: Currency;
begin
  Result := edtProvisionAmount.EditValue;
end;

function TFPayment.GetSaldoSimpananku: Currency;
begin
  Result := ASaldoSimpananku;
end;

function TFPayment.GetSaldoSp: Currency;
begin
  Result := ASaldoSp;
end;

function TFPayment.GetSaldoVc: Currency;
begin
  Result := ASaldoVc;
end;

function TFPayment.GetSubtotal: Currency;
begin
  Result := ASubtotal;
end;

function TFPayment.GetTermAmount: Currency;
begin
  Result := edtTermAmount.EditValue;
end;

function TFPayment.GetTermCount: Integer;
begin
  Result := spnTermcount.EditValue;
end;

function TFPayment.GetTotal: Currency;
begin
  Result := edtTotal.EditValue;
end;

function TFPayment.GetWithCash: Currency;
begin
  Result := edtWithCash.EditValue;
end;

function TFPayment.GetWithSimpananku: Currency;
begin
  Result := edtWithSimpananku.EditValue;
end;

function TFPayment.GetWithSp: Currency;
begin
  Result := AWithSp;
end;

function TFPayment.GetWithVoucher: Currency;
begin
  Result := AWithVoucher;
end;

procedure TFPayment.gvRealisasispActionPropertiesEditValueChanged(
  Sender: TObject);
var
  I: integer;
  Total: Currency;
begin
  inherited;
  TcxCustomEdit(Sender).PostEditValue;
  Total := 0;

  for I := 0 to gvRealisasisp.DataController.RecordCount - 1 do
  begin
    if gvRealisasisp.DataController.Values[I, gvRealisasispAction.Index] then
    begin
      Total := Total + gvRealisasisp.DataController.Values
        [I, gvRealisasispAmount.Index];
    end;
  end;

  WithSp := Total;

  RecalculatePayment;
end;

procedure TFPayment.gvVoucherActionPropertiesEditValueChanged(Sender: TObject);
var
  I: integer;
  Total: Currency;
begin
  inherited;
  TcxCustomEdit(Sender).PostEditValue;
  Total := 0;

  for I := 0 to gvVoucher.DataController.RecordCount - 1 do
  begin
    if gvVoucher.DataController.Values[I, gvVoucherAction.Index] then
    begin
      Total := Total + gvVoucher.DataController.Values
        [I, gvVoucherAmount.Index];
    end;
  end;

  WithVoucher := Total;

  RecalculatePayment;
end;

procedure TFPayment.HideCreditElements;
begin
  tsCredit.TabVisible := False;

  lblProvisionLabel.Enabled := False;
  lblProvisionAmount.Enabled := False;
  lblCreditLabel.Enabled := False;
  lblCreditBase.Enabled := False;
  lblKurangLabel.Enabled := False;
  lblKurang.Enabled := False;
end;

procedure TFPayment.HideMemberElements;
begin
  HideCreditElements;

  tsVoucher.TabVisible := False;
  tsRealisasiSp.TabVisible := False;
  tsDelivery.TabVisible := False;
  lblDeliveryFeeLabel.Enabled := False;
  lblDeliveryFee.Enabled := False;
  lblWithVoucherLabel.Enabled := False;
  lblWithVoucher.Enabled := False;
  lblWithRealisasiSpLabel.Enabled := False;
  lblWithSp.Enabled := False;
  lblWithSimpanankuLabel.Enabled := False;
  edtWithSimpananku.Enabled := False;
  lblKurangLabel.Enabled := False;
  lblKurang.Enabled := False;
end;

procedure TFPayment.rdgMetodePengirimanClick(Sender: TObject);
begin
  if TcxRadioGroup(Sender).ItemIndex = 0 then
  begin
    grbPengiriman.Enabled := False;
    ResetDeliveryFee;
  end
  else
  begin
    grbPengiriman.Enabled := True;
  end;
end;

procedure TFPayment.RecalculateCredit;
begin
  if Total > Plafon then
  begin
    DownPayment := Total - Plafon;
    CreditBase := Plafon;
  end
  else
  begin
    DownPayment := 0;
    CreditBase := Total;
  end;

  CreditTotal := CreditBase + Interest;

  if TermCount = 0 then
    TermAmount := 0
  else
    TermAmount := CreditTotal / TermCount;
end;

procedure TFPayment.RecalculatePayment;
begin
  Total := Subtotal + DeliveryFee.Fee + ProvisionAmount;
  Paid := WithVoucher + WithSp + WithSimpananku + WithCash;
  Change := CreditBase + Paid - Total;
end;

procedure TFPayment.RefreshAddresses;
var
  btn: TcxButton;
  I: integer;
  Key: string;
begin
  addrBtn := TDictionary<TcxButton, TAddress>.Create;
  Member.PullAddresses;

  I := 1;
  for Key in Member.Addresses.Keys do
  begin
    btn := (FindComponent('btnAddress' + IntToStr(I)) as TcxButton);
    btn.Caption := Member.Addresses[Key].Name;
    btn.Enabled := True;
    addrBtn.Add(btn, Member.Addresses[Key]);
    Inc(I);
  end;
end;

procedure TFPayment.RefreshMemberProperties;
begin
  lblMemberName.Caption := Member.FirstName;
  RefreshBalances;
  RefreshAddresses;
  edtWithSimpananku.Enabled := True;
  edtWithSimpananku.SetFocus;
end;

procedure TFPayment.ResetDeliveryFee;
var
  dFee : RDeliveryFee;
begin
  mmAlamat.Text := '';
  with dFee do
  begin
    Weight := 0;
    Tariff := 0;
    Distance := 0;
    Fee := 0;
  end;
  DeliveryFee := dFee;

  if Assigned(Delivery) then
    Delivery.Free;
end;

procedure TFPayment.RefreshBalances;
var
  ASimpananku: TSimpananku;
  AVoucherList: TList<TVoucher>;
  I: integer;
begin
  ASimpananku := TSimpananku.GetByMember(Member.Id);
  AVoucherList := TVoucher.ListVoucher(Member.Id);

  gvVoucher.BeginUpdate;
  gvVoucher.DataController.RecordCount := 0;

  SaldoVc := 0;
  SaldoSp := 0;
  SaldoSimpananku := 0;

  for I := 0 to AVoucherList.Count - 1 do
  begin
    gvVoucher.DataController.AppendRecord;
    gvVoucher.DataController.Values[I, gvVoucherId.Index] :=
      AVoucherList[I].Id;
    gvVoucher.DataController.Values[I, gvVoucherNumber.Index] :=
      AVoucherList[I].Number;
    gvVoucher.DataController.Values[I, gvVoucherAmount.Index] :=
      AVoucherList[I].Amount;
    gvVoucher.DataController.Values[I, gvVoucherDateExpired.Index] :=
      AVoucherList[I].DateExpired;
    gvVoucher.DataController.Values[I, gvVoucherAction.Index] := false;

    SaldoVc := SaldoVc + AVoucherList[I].Amount;
  end;

  gvVoucher.EndUpdate;

  SaldoSimpananku := ASimpananku.Total;
end;

procedure TFPayment.FetchLookups;
var
  cards : TList<TCard>;
  I: Integer;
begin
  cards := TCard.ListCard;
  memCard.Open;
  for I := 0 to cards.Count-1 do
  begin
    memCard.Append;
    memCard.FieldByName('card_id').Value := cards[I].Id;
    memCard.FieldByName('card_name').Value := cards[I].Name;
    memCard.FieldByName('card_feerate').Value := cards[I].FeeRate;
    memCard.Post;
  end;
end;

procedure TFPayment.SetCreditBase(const Value: Currency);
begin
  edtCreditBase.EditValue := Value;
  lblCreditBase.Caption := FormatCurr(MoneyFormat, Value);
  lblKurang.Caption := lblCreditBase.Caption;
end;

procedure TFPayment.SetCreditTotal(const Value: Currency);
begin
  edtCreditTotal.EditValue := Value;
end;

procedure TFPayment.SetCardFee(const Value: Currency);
begin
  ACardFee := Value;
  edtCardFee.EditValue := Value;
end;

procedure TFPayment.SetChange(const Value: Currency);
begin
  AChange := Value;
  lblChange.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetDeliveryFee(const Value: RDeliveryFee);
begin
  ADeliveryFee := Value;

  edtDistance.EditValue := Value.Distance;
  edtWeight.EditValue := Value.Weight;
  edtDeliveryFee.EditValue := Value.Fee;
  lblDeliveryFee.Caption := FormatCurr(MoneyFormat, Value.Fee);
end;

procedure TFPayment.SetDownPayment(const Value: Currency);
begin
  edtDownPayment.EditValue := Value;
end;

procedure TFPayment.SetInterest(const Value: Currency);
begin
  edtInterestPercent.EditValue := Value;
end;

procedure TFPayment.SetPaid(const Value: Currency);
begin
  APaid := Value;
  lblPaid.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetPlafon(const Value: Currency);
begin
  edtPlafon.EditValue := Value;
end;

procedure TFPayment.SetProvisionAmount(const Value: Currency);
begin
  edtProvisionAmount.EditValue := Value;
  lblProvisionAmount.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetSaldoSimpananku(const Value: Currency);
begin
  ASaldoSimpananku := Value;
  lblSimpananku.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetSaldoSp(const Value: Currency);
begin
  ASaldoSp := Value;
  lblRealisasiSp.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetSaldoVc(const Value: Currency);
begin
  ASaldoVc := Value;
  lblVoucher.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetSubtotal(const Value: Currency);
begin
  ASubtotal := Value;
  lblGrandTotal.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetTermAmount(const Value: Currency);
begin
  edtTermAmount.EditValue := Value;
end;

procedure TFPayment.SetTermCount(const Value: Integer);
begin
  spnTermcount.EditValue := Value;
end;

procedure TFPayment.SetTotal(const Value: Currency);
begin
  lblTotal.Caption := FormatCurr(MoneyFormat, Value);
  edtTotal.EditValue := Value;
end;

procedure TFPayment.SetWithCash(const Value: Currency);
begin
  edtWithCash.EditValue := Value;
end;

procedure TFPayment.SetWithSimpananku(const Value: Currency);
begin
  edtWithSimpananku.EditValue := Value;
end;

procedure TFPayment.SetWithSp(const Value: Currency);
begin
  AWithSp := Value;
  lblWithSp.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.SetWithVoucher(const Value: Currency);
begin
  AWithVoucher := Value;
  lblWithVoucher.Caption := FormatCurr(MoneyFormat, Value);
end;

procedure TFPayment.spnPrintCountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    btnPrint.SetFocus;
end;

procedure TFPayment.spnTermcountPropertiesChange(Sender: TObject);
begin
  TcxCustomEdit(Sender).PostEditValue;
  RecalculateCredit;
end;

end.
