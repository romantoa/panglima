unit F_Register;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFRegister = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRegister: TFRegister;

implementation

{$R *.dfm}

procedure TFRegister.FormCreate(Sender: TObject);
begin
  _module.Name := 'Register';
  inherited;
end;

initialization
  RegisterClass(TFRegister);
finalization
  UnRegisterClass(TFRegister);

end.
