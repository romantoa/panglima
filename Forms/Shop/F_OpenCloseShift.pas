unit F_OpenCloseShift;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxMaskEdit,
  cxSpinEdit, cxTextEdit, cxCurrencyEdit, cxLabel, cxGroupBox, Vcl.ComCtrls,
  dxCore, cxDateUtils, cxDropDownEdit, cxCalendar, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxCheckBox, DB, System.JSON,
  dxBarBuiltInMenu,
  cxPC,
  ShopRegister, SfModel, cxClasses, SfLookupUtils, Shift;

type
  TFOpenCloseShift = class(TForm)
    PopupMenu1: TPopupMenu;
    BukaTutupShift1: TMenuItem;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxLabel4: TcxLabel;
    edtInitialBalance: TcxCurrencyEdit;
    cxLabel7: TcxLabel;
    cxLabel9: TcxLabel;
    edtFinalBalance: TcxCurrencyEdit;
    btnCloseShift: TcxButton;
    lblHint2: TcxLabel;
    btnOpenShift: TcxButton;
    lblHint1: TcxLabel;
    cmbShift: TcxLookupComboBox;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cmbStorage: TcxLookupComboBox;
    dteOpenAt: TcxDateEdit;
    cxLabel2: TcxLabel;
    procedure FormShow(Sender: TObject);
    procedure BukaTutupShift1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOpenShiftClick(Sender: TObject);
    procedure btnCloseShiftClick(Sender: TObject);
  private
    procedure OpenStoreShift;
    procedure CloseStoreShift;
  public
    RegisterId : Integer;
    StoreshiftId : Integer;
  end;

var
  FOpenCloseShift: TFOpenCloseShift;

implementation

{$R *.dfm}

uses DM_Main, Storeshift;

procedure TFOpenCloseShift.btnCloseShiftClick(Sender: TObject);
begin
  if MessageDlg('Anda yakin ingin menutup Shift?', mtConfirmation, mbYesNo, 0) = mrYes then
  begin
    CloseStoreShift;
    Close;
    ModalResult := mrOk;
  end;
end;

procedure TFOpenCloseShift.btnOpenShiftClick(Sender: TObject);
begin
  OpenStoreShift;
  Close;
  ModalResult := mrOk;
end;

procedure TFOpenCloseShift.BukaTutupShift1Click(Sender: TObject);
begin
  if btnOpenShift.Enabled then
    btnOpenShift.Click
  else
    btnCloseShift.Click;
end;

procedure TFOpenCloseShift.CloseStoreShift;
var
  params : TSfStringVariantPair;
  shift : TStoreshift;
  res : TSfActionResult;
begin
  params := TSfStringVariantPair.Create;

  with params do
  begin
    Add('storage_id', cmbStorage.EditValue);
    Add('shift_id', cmbShift.EditValue);
    Add('closedat', FormatDateTime('yyyy-mm-dd hh:mm:ss', Now));
    Add('closedby_employee_id', DMMain.LoginInfo.User.EmployeeId);
    Add('finalbalance', edtFinalBalance.EditValue);
  end;

  shift := TStoreshift.Create;
  res := shift.Update(Self.StoreshiftId, params);
  DMMain.alrMain.Show(res.MessageTitle, res.Message);
  shift.Free;
end;

procedure TFOpenCloseShift.FormCreate(Sender: TObject);
begin
  SetLookupProperties(cmbStorage, TSfModel.Fetch('storage'));
  SetLookupProperties(cmbShift, TSfModel.Fetch('shift'));
  cxTabSheet1.TabVisible := False;
  cxTabSheet2.TabVisible := False;
end;

procedure TFOpenCloseShift.FormShow(Sender: TObject);
begin
  cmbStorage.EditValue := Self.RegisterId;
  cxPageControl1.ActivePageIndex := Self.Tag;
  if Self.Tag = 0 then
  begin
    dteOpenAt.EditValue := Date + Time;

    edtInitialBalance.Enabled := True;
    cmbShift.Enabled := True;

    edtFinalBalance.Enabled := False;

    btnOpenShift.Enabled := True;
    lblHint1.Enabled := True;
  end
  else
  begin
    edtInitialBalance.Enabled := False;
    cmbShift.Enabled := False;

    edtFinalBalance.Enabled := True;

    btnCloseShift.Enabled := True;
    lblHint2.Enabled := True;
  end;
end;

procedure TFOpenCloseShift.OpenStoreShift;
var
  params : TSfStringVariantPair;
  shift : TStoreshift;
  res : TSfActionResult;
begin
  params := TSfStringVariantPair.Create;

  with params do
  begin
    Add('storage_id', cmbStorage.EditValue);
    Add('shift_id', cmbShift.EditValue);
    Add('openat', FormatDateTime('yyyy-mm-dd hh:mm:ss', Now));
    Add('openby_employee_id', DMMain.LoginInfo.User.EmployeeId);
    Add('initialbalance', edtInitialBalance.EditValue);
  end;

  shift := TStoreshift.Create;
  res := shift.Insert(params);
  DMMain.alrMain.Show(res.MessageTitle, res.Message);
  shift.Free;
end;

end.
