unit F_Customer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFCustomer = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCustomer: TFCustomer;

implementation

{$R *.dfm}

procedure TFCustomer.FormCreate(Sender: TObject);
begin
  _module.Name := 'Customer';
  inherited;
end;

initialization
  RegisterClass(TFCustomer);
finalization
  UnRegisterClass(TFCustomer);

end.
