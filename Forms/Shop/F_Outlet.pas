unit F_Outlet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFOutlet = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOutlet: TFOutlet;

implementation

{$R *.dfm}

procedure TFOutlet.FormCreate(Sender: TObject);
begin
  _module.Name := 'Outlet';
  inherited;
end;

initialization
  RegisterClass(TFOutlet);
finalization
  UnRegisterClass(TFOutlet);

end.
