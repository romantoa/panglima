object FPayment: TFPayment
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Pembayaran'
  ClientHeight = 700
  ClientWidth = 1024
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox4: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    Caption = 'Pembayaran'
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.Color = clTeal
    Style.LookAndFeel.SkinName = ''
    Style.TextColor = clWhite
    Style.TextStyle = [fsBold]
    StyleDisabled.LookAndFeel.SkinName = ''
    TabOrder = 0
    Height = 700
    Width = 1024
    object cxGroupBox1: TcxGroupBox
      Left = 2
      Top = 21
      Align = alClient
      PanelStyle.Active = True
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      Style.Color = 14671839
      Style.LookAndFeel.SkinName = ''
      StyleDisabled.LookAndFeel.SkinName = ''
      TabOrder = 0
      Height = 677
      Width = 1020
      object cxGroupBox2: TcxGroupBox
        AlignWithMargins = True
        Left = 5
        Top = 63
        Align = alClient
        PanelStyle.Active = True
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        Style.Color = clSilver
        Style.LookAndFeel.SkinName = 'Whiteprint'
        StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
        TabOrder = 0
        Height = 558
        Width = 1010
        object cxGroupBox7: TcxGroupBox
          AlignWithMargins = True
          Left = 6
          Top = 6
          Align = alLeft
          PanelStyle.Active = True
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          Style.Color = clTeal
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          TabOrder = 0
          Height = 546
          Width = 324
          object edtWithCash: TcxCurrencyEdit
            Left = 164
            Top = 279
            EditValue = 0.000000000000000000
            ParentFont = False
            Properties.Alignment.Horz = taRightJustify
            Properties.DisplayFormat = ',0.00'
            Properties.ReadOnly = False
            Properties.UseDisplayFormatWhenEditing = True
            Properties.UseLeftAlignmentOnEditing = False
            Properties.OnEditValueChanged = edtCashPropertiesEditValueChanged
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = 'Office2007Black'
            Style.TextColor = clTeal
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            StyleDisabled.BorderStyle = ebsNone
            StyleDisabled.Color = clWhite
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
            StyleDisabled.TextColor = clBlack
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = 'Office2007Black'
            TabOrder = 2
            OnKeyDown = edtWithCashKeyDown
            Width = 141
          end
          object lblKembali: TcxLabel
            Left = 19
            Top = 451
            Caption = 'Sisa/Kembali'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object cxLabel4: TcxLabel
            Left = 18
            Top = 389
            Caption = 'Dibayar'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object cxLabel1: TcxLabel
            Left = 18
            Top = 29
            Caption = 'Total Tagihan'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblDeliveryFeeLabel: TcxLabel
            Left = 18
            Top = 59
            Caption = 'Biaya Pengiriman'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object cxLabel3: TcxLabel
            Left = 18
            Top = 358
            Caption = 'Total Akhir'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblWithCash: TcxLabel
            Left = 18
            Top = 287
            Caption = 'Cash'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblWithRealisasiSpLabel: TcxLabel
            Left = 18
            Top = 222
            Caption = 'Wajib Beli'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblWithVoucherLabel: TcxLabel
            Left = 18
            Top = 192
            Caption = 'Voucher'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblGrandTotal: TcxLabel
            Left = 164
            Top = 24
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.BorderStyle = ebsNone
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
          object lblDeliveryFee: TcxLabel
            Left = 164
            Top = 54
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
          object lblWithVoucher: TcxLabel
            Left = 164
            Top = 187
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
          object lblChange: TcxLabel
            Left = 165
            Top = 446
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 306
          end
          object lblPaid: TcxLabel
            Left = 164
            Top = 384
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
          object lblTotal: TcxLabel
            Left = 164
            Top = 353
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
          object lblWithSimpanankuLabel: TcxLabel
            Left = 18
            Top = 255
            Caption = 'Simpanan'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object edtWithSimpananku: TcxCurrencyEdit
            Left = 164
            Top = 247
            EditValue = 0.000000000000000000
            ParentFont = False
            Properties.Alignment.Horz = taRightJustify
            Properties.DisplayFormat = ',0.00'
            Properties.ReadOnly = False
            Properties.UseDisplayFormatWhenEditing = True
            Properties.UseLeftAlignmentOnEditing = False
            Properties.OnEditValueChanged = edtCashPropertiesEditValueChanged
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = 'Office2007Black'
            Style.TextColor = clTeal
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            StyleDisabled.BorderStyle = ebsNone
            StyleDisabled.Color = clTeal
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
            StyleDisabled.TextColor = clBtnShadow
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = 'Office2007Black'
            TabOrder = 0
            OnKeyDown = edtWithCashKeyDown
            Width = 141
          end
          object lblWithSp: TcxLabel
            Left = 164
            Top = 217
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
          object lblPrintCount: TcxLabel
            Left = 19
            Top = 506
            Caption = 'Jml. Struk'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object spnPrintCount: TcxSpinEdit
            Left = 165
            Top = 498
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.UseLeftAlignmentOnEditing = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 3
            Value = 1
            OnKeyDown = spnPrintCountKeyDown
            Width = 141
          end
          object lblProvisionLabel: TcxLabel
            Left = 18
            Top = 93
            Caption = 'Provisi'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblProvisionAmount: TcxLabel
            Left = 164
            Top = 88
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
          object cxLabel35: TcxLabel
            Left = 18
            Top = 8
            Caption = 'TAGIHAN'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = [fsBold, fsUnderline]
            Style.TextColor = clWhite
            Style.IsFontAssigned = True
            Transparent = True
          end
          object cxLabel36: TcxLabel
            Left = 18
            Top = 138
            Caption = 'PEMBAYARAN'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = [fsBold, fsUnderline]
            Style.TextColor = clWhite
            Style.IsFontAssigned = True
            Transparent = True
          end
          object cxLabel37: TcxLabel
            Left = 18
            Top = 339
            Caption = 'SUMMARY'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = [fsBold, fsUnderline]
            Style.TextColor = clWhite
            Style.IsFontAssigned = True
            Transparent = True
          end
          object lblCreditLabel: TcxLabel
            Left = 19
            Top = 163
            Caption = 'Kredit'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblCreditBase: TcxLabel
            Left = 165
            Top = 158
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 306
          end
          object lblKurangLabel: TcxLabel
            Left = 18
            Top = 420
            Caption = 'Kurang'
            ParentFont = False
            Style.TextColor = clWhite
            Transparent = True
          end
          object lblKurang: TcxLabel
            Left = 164
            Top = 415
            AutoSize = False
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -20
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clWhite
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.LineOptions.Alignment = cxllaBottom
            Properties.LineOptions.Visible = True
            Transparent = True
            Height = 28
            Width = 141
            AnchorX = 305
          end
        end
        object pgPayment: TcxPageControl
          AlignWithMargins = True
          Left = 336
          Top = 6
          Width = 671
          Height = 549
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alClient
          Color = clSilver
          ParentBackground = False
          ParentColor = False
          TabOrder = 1
          Properties.ActivePage = tsCard
          Properties.CustomButtons.Buttons = <>
          LookAndFeel.SkinName = 'Whiteprint'
          ClientRectBottom = 544
          ClientRectLeft = 2
          ClientRectRight = 666
          ClientRectTop = 31
          object tsCard: TcxTabSheet
            Caption = 'Kartu'
            ImageIndex = 2
            object cxGroupBox10: TcxGroupBox
              AlignWithMargins = True
              Left = 15
              Top = 15
              Margins.Left = 15
              Margins.Top = 15
              Margins.Right = 15
              Margins.Bottom = 15
              Align = alClient
              ParentFont = False
              Style.LookAndFeel.SkinName = 'Whiteprint'
              StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
              TabOrder = 0
              Height = 483
              Width = 634
              object cxLabel20: TcxLabel
                Left = 9
                Top = 20
                Caption = 'Kartu'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object cxLabel23: TcxLabel
                Left = 9
                Top = 59
                Caption = 'Biaya Kartu'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object edtCardFee: TcxCurrencyEdit
                Left = 217
                Top = 53
                EditValue = 0.000000000000000000
                Enabled = False
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnEditValueChanged = edtCashPropertiesEditValueChanged
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clBtnShadow
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 2
                Width = 250
              end
              object cmbCard: TcxLookupComboBox
                Left = 217
                Top = 16
                AutoSize = False
                ParentFont = False
                Properties.ImmediatePost = True
                Properties.KeyFieldNames = 'card_id'
                Properties.ListColumns = <
                  item
                    FieldName = 'card_name'
                  end>
                Properties.ListOptions.ShowHeader = False
                Properties.ListSource = DataSource1
                Properties.OnEditValueChanged = cmbCardPropertiesEditValueChanged
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -15
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 3
                OnKeyDown = cmbCardKeyDown
                Height = 32
                Width = 250
              end
              object cxLabel24: TcxLabel
                Left = 9
                Top = 93
                Caption = 'Kode Approval'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object edtApprovalNumber: TcxTextEdit
                Left = 217
                Top = 87
                ParentFont = False
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.IsFontAssigned = True
                TabOrder = 5
                OnKeyDown = edtApprovalNumberKeyDown
                Width = 250
              end
            end
          end
          object tsVoucher: TcxTabSheet
            Caption = 'Voucher'
            ImageIndex = 0
            object cxGroupBox6: TcxGroupBox
              AlignWithMargins = True
              Left = 15
              Top = 15
              Margins.Left = 15
              Margins.Top = 15
              Margins.Right = 15
              Margins.Bottom = 15
              Align = alClient
              PanelStyle.Active = True
              ParentFont = False
              Style.LookAndFeel.SkinName = 'Whiteprint'
              StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
              TabOrder = 0
              Height = 483
              Width = 634
              object cxGrid1: TcxGrid
                AlignWithMargins = True
                Left = 6
                Top = 6
                Width = 622
                Height = 471
                Align = alClient
                TabOrder = 0
                LookAndFeel.SkinName = 'Metropolis'
                object gvVoucher: TcxGridTableView
                  Navigator.Buttons.CustomButtons = <>
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooterMultiSummaries = True
                  object gvVoucherId: TcxGridColumn
                    Caption = 'Id'
                    Visible = False
                  end
                  object gvVoucherNumber: TcxGridColumn
                    Caption = 'Kode'
                    HeaderAlignmentHorz = taCenter
                    Width = 160
                  end
                  object gvVoucherAmount: TcxGridColumn
                    Caption = 'Nilai'
                    PropertiesClassName = 'TcxCurrencyEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    Properties.DisplayFormat = ',0.00;(,0.00)'
                    Properties.UseLeftAlignmentOnEditing = False
                    HeaderAlignmentHorz = taCenter
                    Width = 160
                  end
                  object gvVoucherDateExpired: TcxGridColumn
                    Caption = 'Tgl. Expired'
                    PropertiesClassName = 'TcxDateEditProperties'
                    Properties.Alignment.Horz = taCenter
                    Properties.DisplayFormat = 'dd-mm-yyyy'
                    HeaderAlignmentHorz = taCenter
                    Width = 160
                  end
                  object gvVoucherAction: TcxGridColumn
                    PropertiesClassName = 'TcxCheckBoxProperties'
                    Properties.Alignment = taCenter
                    Properties.NullStyle = nssUnchecked
                    Properties.OnEditValueChanged = gvVoucherActionPropertiesEditValueChanged
                    Width = 37
                  end
                end
                object cxGrid1Level1: TcxGridLevel
                  Caption = 'Voucher'
                  GridView = gvVoucher
                end
              end
            end
          end
          object tsRealisasiSp: TcxTabSheet
            Caption = 'Wajib Beli'
            ImageIndex = 1
            object cxGroupBox8: TcxGroupBox
              AlignWithMargins = True
              Left = 15
              Top = 15
              Margins.Left = 15
              Margins.Top = 15
              Margins.Right = 15
              Margins.Bottom = 15
              Align = alClient
              PanelStyle.Active = True
              ParentFont = False
              Style.LookAndFeel.SkinName = 'Whiteprint'
              StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
              TabOrder = 0
              Height = 483
              Width = 634
              object cxGrid2: TcxGrid
                AlignWithMargins = True
                Left = 6
                Top = 6
                Width = 622
                Height = 471
                Align = alClient
                TabOrder = 0
                LookAndFeel.SkinName = 'Metropolis'
                object gvRealisasisp: TcxGridTableView
                  Navigator.Buttons.CustomButtons = <>
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooterMultiSummaries = True
                  object gvRealisasispId: TcxGridColumn
                    Caption = 'Id'
                    Visible = False
                  end
                  object gvRealisasispNumber: TcxGridColumn
                    Caption = 'Kode'
                    HeaderAlignmentHorz = taCenter
                    Width = 160
                  end
                  object gvRealisasispAmount: TcxGridColumn
                    Caption = 'Nilai'
                    PropertiesClassName = 'TcxCurrencyEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    Properties.DisplayFormat = ',0.00;(,0.00)'
                    Properties.UseLeftAlignmentOnEditing = False
                    HeaderAlignmentHorz = taCenter
                    Width = 160
                  end
                  object gvRealisasispDateexpired: TcxGridColumn
                    Caption = 'Exp'
                    PropertiesClassName = 'TcxDateEditProperties'
                    Properties.Alignment.Horz = taCenter
                    Properties.DisplayFormat = 'dd-mm-yyyy'
                    HeaderAlignmentHorz = taCenter
                    Width = 160
                  end
                  object gvRealisasispAction: TcxGridColumn
                    PropertiesClassName = 'TcxCheckBoxProperties'
                    Properties.Alignment = taCenter
                    Properties.NullStyle = nssUnchecked
                    Properties.OnEditValueChanged = gvRealisasispActionPropertiesEditValueChanged
                    Width = 37
                  end
                end
                object cxGridLevel1: TcxGridLevel
                  GridView = gvRealisasisp
                end
              end
            end
          end
          object tsDelivery: TcxTabSheet
            Caption = 'Pengiriman'
            ImageIndex = 3
            object grbPengiriman: TcxGroupBox
              AlignWithMargins = True
              Left = 15
              Top = 118
              Margins.Left = 15
              Margins.Right = 15
              Margins.Bottom = 15
              Align = alClient
              Enabled = False
              PanelStyle.Active = True
              ParentFont = False
              Style.LookAndFeel.SkinName = 'Whiteprint'
              StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
              TabOrder = 0
              Height = 380
              Width = 634
              object cxLabel5: TcxLabel
                Left = 13
                Top = 9
                Caption = 'Alamat'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object mmAlamat: TcxMemo
                Left = 111
                Top = 54
                Enabled = False
                ParentFont = False
                StyleDisabled.TextColor = 12615680
                TabOrder = 1
                Height = 87
                Width = 519
              end
              object btnAddress1: TcxButton
                Left = 111
                Top = 9
                Width = 98
                Height = 39
                Caption = 'No Address'
                Enabled = False
                LookAndFeel.SkinName = 'SharpPlus'
                TabOrder = 2
                OnClick = btnAddress1Click
              end
              object btnAddress2: TcxButton
                Left = 213
                Top = 9
                Width = 98
                Height = 39
                Caption = 'No Address'
                Enabled = False
                LookAndFeel.SkinName = 'SharpPlus'
                TabOrder = 3
                OnClick = btnAddress1Click
              end
              object btnAddress3: TcxButton
                Left = 315
                Top = 9
                Width = 111
                Height = 39
                Caption = 'No Address'
                Enabled = False
                LookAndFeel.SkinName = 'SharpPlus'
                TabOrder = 4
                OnClick = btnAddress1Click
              end
              object btnAddress4: TcxButton
                Left = 430
                Top = 9
                Width = 98
                Height = 39
                Caption = 'No Address'
                Enabled = False
                LookAndFeel.SkinName = 'SharpPlus'
                TabOrder = 5
                OnClick = btnAddress1Click
              end
              object btnAddress5: TcxButton
                Left = 532
                Top = 9
                Width = 98
                Height = 39
                Caption = 'No Address'
                Enabled = False
                LookAndFeel.SkinName = 'SharpPlus'
                TabOrder = 6
                OnClick = btnAddress1Click
              end
              object cxLabel11: TcxLabel
                Left = 15
                Top = 153
                Caption = 'Jarak'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object lblDistanceX: TcxLabel
                Left = 203
                Top = 153
                AutoSize = False
                Caption = 'km'
                ParentFont = False
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -15
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.TextColor = clBlack
                Style.TextStyle = []
                Style.IsFontAssigned = True
                Properties.Alignment.Horz = taRightJustify
                Transparent = True
                Height = 22
                Width = 25
                AnchorX = 228
              end
              object edtDistance: TcxCurrencyEdit
                Left = 111
                Top = 147
                EditValue = 0.000000000000000000
                Enabled = False
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnEditValueChanged = edtCashPropertiesEditValueChanged
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clMedGray
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 9
                Width = 75
              end
              object edtWeight: TcxCurrencyEdit
                Left = 111
                Top = 175
                EditValue = 0.000000000000000000
                Enabled = False
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnEditValueChanged = edtCashPropertiesEditValueChanged
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clMedGray
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 10
                Width = 75
              end
              object edtDeliveryFee: TcxCurrencyEdit
                Left = 111
                Top = 203
                EditValue = 0.000000000000000000
                Enabled = False
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnEditValueChanged = edtCashPropertiesEditValueChanged
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clBtnShadow
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 11
                Width = 118
              end
              object cxLabel15: TcxLabel
                Left = 15
                Top = 181
                Caption = 'Berat'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object cxLabel16: TcxLabel
                Left = 15
                Top = 209
                Caption = 'Biaya'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object cxLabel13: TcxLabel
                Left = 203
                Top = 181
                AutoSize = False
                Caption = 'kg'
                ParentFont = False
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -15
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.TextColor = clBlack
                Style.TextStyle = []
                Style.IsFontAssigned = True
                Properties.Alignment.Horz = taLeftJustify
                Transparent = True
                Height = 22
                Width = 25
              end
            end
            object rdgMetodePengiriman: TcxRadioGroup
              AlignWithMargins = True
              Left = 15
              Top = 15
              Margins.Left = 15
              Margins.Top = 15
              Margins.Right = 15
              Align = alTop
              Caption = 'Metode Pengiriman'
              ParentFont = False
              Properties.Items = <
                item
                  Caption = 'Diambil di tempat'
                end
                item
                  Caption = 'Dikirim ke rumah'
                end>
              ItemIndex = 0
              Style.LookAndFeel.SkinName = 'Whiteprint'
              StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
              TabOrder = 1
              OnClick = rdgMetodePengirimanClick
              Height = 97
              Width = 634
            end
          end
          object tsCredit: TcxTabSheet
            Caption = 'Kredit'
            ImageIndex = 4
            object cxGroupBox12: TcxGroupBox
              AlignWithMargins = True
              Left = 15
              Top = 15
              Margins.Left = 15
              Margins.Top = 15
              Margins.Right = 15
              Margins.Bottom = 15
              Align = alClient
              PanelStyle.Active = True
              ParentFont = False
              Style.LookAndFeel.SkinName = 'Whiteprint'
              StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
              TabOrder = 0
              Height = 483
              Width = 634
              object cxLabel6: TcxLabel
                Left = 18
                Top = 80
                AutoSize = False
                Caption = 'Pokok Kredit'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
                Height = 22
                Width = 150
              end
              object edtCreditBase: TcxCurrencyEdit
                Left = 170
                Top = 74
                EditValue = 0.000000000000000000
                Enabled = False
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnChange = edtCreditBasePropertiesChange
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clGray
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 10
                Width = 236
              end
              object cxLabel8: TcxLabel
                Left = 18
                Top = 146
                AutoSize = False
                Caption = 'Provisi'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
                Height = 22
                Width = 150
              end
              object edtProvisionAmount: TcxCurrencyEdit
                Left = 252
                Top = 140
                EditValue = 0.000000000000000000
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnChange = edtProvisionAmountPropertiesChange
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clGray
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 3
                Width = 154
              end
              object edtProvisionPercent: TcxCurrencyEdit
                Left = 169
                Top = 140
                EditValue = 0.000000000000000000
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00 %'
                Properties.EditFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnChange = edtProvisionPercentPropertiesChange
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clBlack
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 2
                Width = 82
              end
              object cxLabel21: TcxLabel
                Left = 18
                Top = 179
                AutoSize = False
                Caption = 'Kali Angsuran'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
                Height = 22
                Width = 150
              end
              object spnTermcount: TcxSpinEdit
                Left = 169
                Top = 173
                ParentFont = False
                Properties.Alignment.Horz = taCenter
                Properties.AssignedValues.MinValue = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnChange = spnTermcountPropertiesChange
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 4
                Value = 1
                Width = 82
              end
              object cxLabel22: TcxLabel
                Left = 256
                Top = 179
                Caption = 'Kali'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
              end
              object cxLabel25: TcxLabel
                Left = 18
                Top = 14
                AutoSize = False
                Caption = 'Total Belanja'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
                Height = 22
                Width = 150
              end
              object edtTotal: TcxCurrencyEdit
                Left = 170
                Top = 8
                EditValue = 0.000000000000000000
                Enabled = False
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clGray
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 5
                Width = 236
              end
              object cxLabel26: TcxLabel
                Left = 18
                Top = 47
                AutoSize = False
                Caption = 'Plafon'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
                Height = 22
                Width = 150
              end
              object edtPlafon: TcxCurrencyEdit
                Left = 170
                Top = 41
                EditValue = 0.000000000000000000
                Enabled = False
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clGray
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 7
                Width = 236
              end
              object cxLabel27: TcxLabel
                Left = 18
                Top = 113
                AutoSize = False
                Caption = 'Interest'
                ParentFont = False
                Style.TextColor = clBlack
                Transparent = True
                Height = 22
                Width = 150
              end
              object edtInterestPercent: TcxCurrencyEdit
                Left = 169
                Top = 107
                EditValue = 0.000000000000000000
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00 %'
                Properties.EditFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnChange = edtInterestPercentPropertiesChange
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clBlack
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 0
                Width = 82
              end
              object edtInterestAmount: TcxCurrencyEdit
                Left = 252
                Top = 107
                EditValue = 0.000000000000000000
                ParentFont = False
                Properties.Alignment.Horz = taRightJustify
                Properties.DisplayFormat = ',0.00'
                Properties.ReadOnly = False
                Properties.UseDisplayFormatWhenEditing = True
                Properties.UseLeftAlignmentOnEditing = False
                Properties.OnChange = edtInterestAmountPropertiesChange
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -20
                Style.Font.Name = 'Bahnschrift SemiLight'
                Style.Font.Style = []
                Style.LookAndFeel.NativeStyle = False
                Style.LookAndFeel.SkinName = 'Office2007Black'
                Style.TextColor = clTeal
                Style.TextStyle = [fsBold]
                Style.IsFontAssigned = True
                StyleDisabled.BorderStyle = ebsNone
                StyleDisabled.Color = clWhite
                StyleDisabled.LookAndFeel.NativeStyle = False
                StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                StyleDisabled.TextColor = clGray
                StyleFocused.LookAndFeel.NativeStyle = False
                StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                StyleHot.LookAndFeel.NativeStyle = False
                StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                TabOrder = 1
                Width = 154
              end
              object cxGroupBox13: TcxGroupBox
                AlignWithMargins = True
                Left = 18
                Top = 220
                Margins.Bottom = 90
                PanelStyle.Active = True
                ParentBackground = False
                ParentColor = False
                ParentFont = False
                Style.BorderStyle = ebsNone
                Style.Color = clTeal
                Style.LookAndFeel.SkinName = ''
                StyleDisabled.LookAndFeel.SkinName = ''
                TabOrder = 15
                Height = 133
                Width = 401
                object cxLabel28: TcxLabel
                  Left = 19
                  Top = 80
                  AutoSize = False
                  Caption = 'Angsuran Per Bulan'
                  ParentFont = False
                  Style.TextColor = clWhite
                  Transparent = True
                  Height = 22
                  Width = 150
                end
                object edtTermAmount: TcxCurrencyEdit
                  Left = 185
                  Top = 74
                  EditValue = 0.000000000000000000
                  Enabled = False
                  ParentFont = False
                  Properties.Alignment.Horz = taRightJustify
                  Properties.DisplayFormat = ',0.00'
                  Properties.ReadOnly = False
                  Properties.UseDisplayFormatWhenEditing = True
                  Properties.UseLeftAlignmentOnEditing = False
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -20
                  Style.Font.Name = 'Bahnschrift SemiLight'
                  Style.Font.Style = []
                  Style.LookAndFeel.NativeStyle = False
                  Style.LookAndFeel.SkinName = 'Office2007Black'
                  Style.TextColor = clTeal
                  Style.TextStyle = [fsBold]
                  Style.IsFontAssigned = True
                  StyleDisabled.BorderStyle = ebsNone
                  StyleDisabled.Color = clTeal
                  StyleDisabled.LookAndFeel.NativeStyle = False
                  StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                  StyleDisabled.TextColor = clWhite
                  StyleFocused.LookAndFeel.NativeStyle = False
                  StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                  StyleHot.LookAndFeel.NativeStyle = False
                  StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                  TabOrder = 1
                  Width = 206
                end
                object cxLabel29: TcxLabel
                  Left = 19
                  Top = 14
                  AutoSize = False
                  Caption = 'Uang Muka'
                  ParentFont = False
                  Style.TextColor = clWhite
                  Transparent = True
                  Height = 22
                  Width = 150
                end
                object edtDownPayment: TcxCurrencyEdit
                  Left = 185
                  Top = 8
                  EditValue = 0.000000000000000000
                  Enabled = False
                  ParentFont = False
                  Properties.Alignment.Horz = taRightJustify
                  Properties.DisplayFormat = ',0.00'
                  Properties.ReadOnly = False
                  Properties.UseDisplayFormatWhenEditing = True
                  Properties.UseLeftAlignmentOnEditing = False
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -20
                  Style.Font.Name = 'Bahnschrift SemiLight'
                  Style.Font.Style = []
                  Style.LookAndFeel.NativeStyle = False
                  Style.LookAndFeel.SkinName = 'Office2007Black'
                  Style.TextColor = clTeal
                  Style.TextStyle = [fsBold]
                  Style.IsFontAssigned = True
                  StyleDisabled.BorderStyle = ebsNone
                  StyleDisabled.Color = clTeal
                  StyleDisabled.LookAndFeel.NativeStyle = False
                  StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                  StyleDisabled.TextColor = clWhite
                  StyleFocused.LookAndFeel.NativeStyle = False
                  StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                  StyleHot.LookAndFeel.NativeStyle = False
                  StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                  TabOrder = 3
                  Width = 206
                end
                object cxLabel30: TcxLabel
                  Left = 19
                  Top = 47
                  AutoSize = False
                  Caption = 'Total Kredit'
                  ParentFont = False
                  Style.TextColor = clWhite
                  Transparent = True
                  Height = 22
                  Width = 150
                end
                object edtCreditTotal: TcxCurrencyEdit
                  Left = 185
                  Top = 41
                  EditValue = 0.000000000000000000
                  Enabled = False
                  ParentFont = False
                  Properties.Alignment.Horz = taRightJustify
                  Properties.DisplayFormat = ',0.00'
                  Properties.ReadOnly = False
                  Properties.UseDisplayFormatWhenEditing = True
                  Properties.UseLeftAlignmentOnEditing = False
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -20
                  Style.Font.Name = 'Bahnschrift SemiLight'
                  Style.Font.Style = []
                  Style.LookAndFeel.NativeStyle = False
                  Style.LookAndFeel.SkinName = 'Office2007Black'
                  Style.TextColor = clTeal
                  Style.TextStyle = [fsBold]
                  Style.IsFontAssigned = True
                  StyleDisabled.BorderStyle = ebsNone
                  StyleDisabled.Color = clTeal
                  StyleDisabled.LookAndFeel.NativeStyle = False
                  StyleDisabled.LookAndFeel.SkinName = 'Office2007Black'
                  StyleDisabled.TextColor = clWhite
                  StyleFocused.LookAndFeel.NativeStyle = False
                  StyleFocused.LookAndFeel.SkinName = 'Office2007Black'
                  StyleHot.LookAndFeel.NativeStyle = False
                  StyleHot.LookAndFeel.SkinName = 'Office2007Black'
                  TabOrder = 5
                  Width = 206
                end
                object cxLabel31: TcxLabel
                  Left = 185
                  Top = 63
                  AutoSize = False
                  ParentColor = False
                  ParentFont = False
                  Style.Color = clTeal
                  Style.TextColor = clWhite
                  StyleDisabled.Color = clTeal
                  StyleDisabled.TextColor = clWhite
                  Properties.LineOptions.Alignment = cxllaBottom
                  Properties.LineOptions.Visible = True
                  Height = 8
                  Width = 206
                end
                object cxLabel32: TcxLabel
                  Left = 185
                  Top = 96
                  AutoSize = False
                  ParentColor = False
                  ParentFont = False
                  Style.Color = clTeal
                  Style.TextColor = clWhite
                  StyleDisabled.Color = clTeal
                  StyleDisabled.TextColor = clWhite
                  Properties.LineOptions.Alignment = cxllaBottom
                  Properties.LineOptions.Visible = True
                  Height = 8
                  Width = 206
                end
                object lblLine: TcxLabel
                  Left = 185
                  Top = 31
                  AutoSize = False
                  ParentColor = False
                  ParentFont = False
                  Style.Color = clTeal
                  Style.TextColor = clWhite
                  StyleDisabled.Color = clTeal
                  StyleDisabled.TextColor = clWhite
                  Properties.LineOptions.Alignment = cxllaBottom
                  Properties.LineOptions.Visible = True
                  Height = 8
                  Width = 206
                end
              end
            end
          end
        end
      end
      object cxGroupBox5: TcxGroupBox
        Left = 2
        Top = 2
        Align = alTop
        PanelStyle.Active = True
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = 'Whiteprint'
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
        TabOrder = 1
        Height = 58
        Width = 1016
        object pnlMember: TcxGroupBox
          AlignWithMargins = True
          Left = 576
          Top = 7
          Margins.Left = 0
          Margins.Top = 4
          Margins.Bottom = 4
          Align = alRight
          PanelStyle.Active = True
          ParentFont = False
          Style.LookAndFeel.SkinName = 'Whiteprint'
          StyleDisabled.LookAndFeel.SkinName = 'Whiteprint'
          TabOrder = 1
          Height = 44
          Width = 434
          object lblVoucher: TcxLabel
            Left = 102
            Top = 20
            Caption = '0,00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clGreen
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 141
          end
          object lblMemberName: TcxLabel
            Left = 3
            Top = 3
            Caption = 'CUSTOMER TUNAI'
            ParentFont = False
            Style.TextStyle = [fsBold]
            StyleDisabled.TextStyle = []
            Transparent = True
          end
          object cxLabel10: TcxLabel
            Left = 3
            Top = 20
            Caption = 'VC'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clGreen
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 28
          end
          object cxLabel12: TcxLabel
            Left = 138
            Top = 20
            Caption = ' | WB'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clBlue
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
          end
          object lblRealisasiSp: TcxLabel
            Left = 244
            Top = 20
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = clBlue
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 283
          end
          object cxLabel17: TcxLabel
            Left = 280
            Top = 20
            Caption = ' | SP'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = 8388863
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
          end
          object lblSimpananku: TcxLabel
            Left = 386
            Top = 20
            Caption = '0.00'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -16
            Style.Font.Name = 'Bahnschrift SemiLight'
            Style.Font.Style = []
            Style.TextColor = 8388863
            Style.TextStyle = [fsBold]
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 425
          end
        end
        object edtMemberCode: TcxTextEdit
          AlignWithMargins = True
          Left = 6
          Top = 6
          Align = alLeft
          ParentFont = False
          Properties.Alignment.Horz = taCenter
          Properties.EchoMode = eemPassword
          Properties.UseLeftAlignmentOnEditing = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -20
          Style.Font.Name = 'Bahnschrift SemiLight'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.BorderColor = clGrayText
          TabOrder = 0
          OnEnter = edtMemberCodeEnter
          OnExit = edtMemberCodeExit
          OnKeyDown = edtMemberCodeKeyDown
          Width = 309
        end
        object lblMemberCode: TcxLabel
          Left = 6
          Top = 8
          AutoSize = False
          Caption = 'SCAN MEMBER CARD HERE'
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 43
          Width = 309
          AnchorX = 161
          AnchorY = 30
        end
      end
      object cxGroupBox9: TcxGroupBox
        Left = 2
        Top = 624
        Align = alBottom
        PanelStyle.Active = True
        ParentFont = False
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.SkinName = 'Metropolis'
        StyleDisabled.LookAndFeel.SkinName = 'Metropolis'
        TabOrder = 2
        Height = 51
        Width = 1016
        object btnCancel: TcxButton
          AlignWithMargins = True
          Left = 272
          Top = 6
          Width = 181
          Height = 41
          Margins.Left = 0
          Margins.Top = 0
          Margins.Bottom = 0
          Cancel = True
          Caption = '  BATAL'
          Colors.Default = clTeal
          Colors.DefaultText = clWhite
          LookAndFeel.SkinName = ''
          OptionsImage.ImageIndex = 3
          OptionsImage.Margin = 20
          TabOrder = 1
          OnClick = btnCancelClick
        end
        object btnPrint: TcxButton
          AlignWithMargins = True
          Left = 458
          Top = 6
          Width = 242
          Height = 41
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Caption = '  SIMPAN && CETAK'
          Colors.Default = clTeal
          Colors.DefaultText = clWhite
          LookAndFeel.SkinName = ''
          OptionsImage.ImageIndex = 2
          OptionsImage.Margin = 20
          TabOrder = 0
          OnClick = btnPrintClick
        end
        object cxLabel18: TcxLabel
          Left = 395
          Top = 14
          AutoSize = False
          Caption = 'Esc'
          ParentColor = False
          ParentFont = False
          Style.BorderColor = clWhite
          Style.BorderStyle = ebsSingle
          Style.Color = clTeal
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Bahnschrift SemiLight'
          Style.Font.Style = []
          Style.TextColor = clWhite
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Height = 24
          Width = 47
          AnchorX = 419
          AnchorY = 26
        end
        object cxLabel19: TcxLabel
          Left = 645
          Top = 14
          AutoSize = False
          Caption = 'Ctrl+S'
          ParentColor = False
          ParentFont = False
          Style.BorderColor = clWhite
          Style.BorderStyle = ebsSingle
          Style.Color = clTeal
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Bahnschrift SemiLight'
          Style.Font.Style = []
          Style.TextColor = clWhite
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Height = 24
          Width = 47
          AnchorX = 669
          AnchorY = 26
        end
      end
    end
  end
  object memCard: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 424
    Top = 36
    object memCardcard_id: TIntegerField
      FieldName = 'card_id'
    end
    object memCardcard_name: TStringField
      FieldName = 'card_name'
      Size = 50
    end
    object memCardcard_feerate: TFloatField
      FieldName = 'card_feerate'
    end
  end
  object DataSource1: TDataSource
    DataSet = memCard
    Left = 544
    Top = 40
  end
end
