unit F_SaleBase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Configable,
  System.Generics.Collections, Data.DB, dxmdaset,
  frxClass, frxDBSet, cxStyles, cxClasses, DM_Main, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog,
  cxDBData, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, System.JSON,
  cxContainer, cxLabel, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxCurrencyEdit, frxBarcode,

  SfGridUtils,
  Member,
  Order,
  OrderItem,
  Reportable,
  Storeshift,
  Product,
  Saletype,
  Delivery, SfStringUtils, Fsf_Base, SfModel, System.DateUtils;

type
  TSearchMode = (smBarcode = 1, smName = 2, smOrder = 3);

type
  TFSaleBase = class(TFsfBase)
    memCartMaster: TdxMemData;
    memCartMasterDpp: TCurrencyField;
    memCartMasterDiscount: TCurrencyField;
    memCartMasterCash: TCurrencyField;
    memCartMasterVoucher: TCurrencyField;
    memCartMasterRealisasiSp: TCurrencyField;
    memCartMasterDateIssued: TDateTimeField;
    memCartMasterOrderNumber: TStringField;
    memCartMasterMemberInfo: TStringField;
    memCartMasterTermCount: TIntegerField;
    memCartMasterProvision: TCurrencyField;
    memCartMasterTermAmount: TCurrencyField;
    memCartMasterPpn: TCurrencyField;
    memCartMasterSubtotal: TCurrencyField;
    memCartMasterMemberId: TIntegerField;
    memCartMasterMemberName: TStringField;
    memCartMasterDiscountPercent: TFloatField;
    memCartMasterDiscountAmount: TCurrencyField;
    memCartMasterGrandTotal: TCurrencyField;
    memCartMasterPaid: TCurrencyField;
    memCartMasterCreditAmount: TCurrencyField;
    memCartMasterCashChange: TCurrencyField;
    memCartMasterDeliveryFee: TFloatField;
    memCartMasterCardId: TIntegerField;
    memCartMasterCardFee: TIntegerField;
    memCartMasterCardApprNumber: TStringField;
    memCartMasterCashier: TStringField;
    memCartMasterRegister: TStringField;
    memCartMasterStoreshiftId: TIntegerField;
    memCartMasterStoreshiftNumber: TStringField;
    memCartItem: TdxMemData;
    memCartItemId: TIntegerField;
    memCartItemProductId: TIntegerField;
    memCartItemProductCode: TStringField;
    memCartItemProductName: TStringField;
    memCartItemProductPrice: TCurrencyField;
    memCartItemQuantity: TFloatField;
    memCartItemDiscount: TCurrencyField;
    memCartItemSubtotal: TCurrencyField;
    memCartItemProductUnit: TIntegerField;
    memCartItemDpp: TCurrencyField;
    memCartItemPpn: TCurrencyField;
    memCartItemRetailPrice: TCurrencyField;
    memCartItemRetailDiscount: TCurrencyField;
    memCartItemPartyPrice: TCurrencyField;
    memCartItemPartyPriceQuantity: TIntegerField;
    memCartItemStockistPrice: TCurrencyField;
    memCartItemIsTaxed: TBooleanField;
    dsCartMaster: TfrxDBDataset;
    dsCartItem: TfrxDBDataset;
    rptStruk: TfrxReport;
    memCartMasterSimpananku: TCurrencyField;
    memCartMasterDownPayment: TCurrencyField;
    memCartMasterPlafon: TCurrencyField;
    memCartMasterInterest: TCurrencyField;
    memCartMasterCreditBase: TCurrencyField;
    rptStrukOrder: TfrxReport;
    frxBarCodeObject1: TfrxBarCodeObject;
    memCartMasterStoreshiftName: TStringField;
    procedure FormCreate(Sender: TObject);
  private
    function GetWithSimpananku: Currency;
    procedure SetWithSimpananku(const Value: Currency);
    function GetDownPayment: Currency;
    procedure SetDownPayment(const Value: Currency);
    function GetPlafon: Currency;
    procedure SetPlafon(const Value: Currency);
    function GetInterest: Currency;
    procedure SetInterest(const Value: Currency);
    function GetCreditBase: Currency;
    procedure SetCreditBase(const Value: Currency);
    function GetCardOwner: String;
    procedure SetCardOwner(const Value: String);
    function GetCardNumber: String;
    procedure SetCardNumber(const Value: String);
    { Private declarations }
  protected
    _report: IReportable;
    _config: IConfigable;
    _order: TOrder;
    _items: TList<TOrderItem>;

    _grdCart: TcxGrid;
    _gvCart: TcxGridtableView;

    AMember: TMember;
    AStoreShift: TStoreshift;
    ADelivery: TDelivery;

    procedure SetCustomEditValue(AName: String; AValue: Variant);
    procedure SetLabelCaption(AName: String; ACaption: String);

    procedure ResetSession; virtual;
    procedure SavePicture(APicture: TPicture; var AValue: AnsiString);
    procedure PrintReceipt(ACopy: integer);
    function ProcessSearch(AKeyword: String): Boolean;
    procedure ShowHintLabels(AVisible: Boolean);
    procedure RefreshCart;
    procedure DeleteCart(AProductId: integer);
    procedure AddProductToCart(AProduct: TProduct; AQuantity: integer = 1);
    procedure AddCart(AProduct: TProduct; AQuantity: integer = 1);
    procedure UpdateCart(AProduct: TProduct; AQuantity: integer;
      ADiscount: Currency);
    procedure UpdateCartDiscount;
    procedure UpdateCartGrandTotal;
    procedure FinalizeOrder(AVouchers, ARealisasiSp: TStringList;
      ASaleType: integer; AAction: integer);
    procedure LoadOrder;
    procedure AppendNewItem;

    function GetSubtotal: Currency;
    procedure SetSubtotal(const Value: Currency);
    function GetWithCash: Currency;
    procedure SetWithCash(const Value: Currency);
    function GetWithVoucher: Currency;
    procedure SetWithVoucher(const Value: Currency);
    function GetWithSp: Currency;
    procedure SetWithSp(const Value: Currency);
    function GetPpn: Currency;
    procedure SetPpn(const Value: Currency);
    function GetMember: TMember;
    procedure SetMember(const Value: TMember);
    function GetGrandTotal: Currency;
    procedure SetGrandTotal(const Value: Currency);
    function GetDiscountAmount: Currency;
    procedure SetDiscountAmount(const Value: Currency);
    function GetDiscountPercent: Double;
    procedure SetDiscountPercent(const Value: Double);
    function GetDpp: Currency;
    procedure SetDpp(const Value: Currency);
    function GetDiscount: Currency;
    procedure SetDiscount(const Value: Currency);
    function GetTermCount: integer;
    procedure SetTermCount(const Value: integer);
    function GetTermAmount: Currency;
    procedure SetTermAmount(const Value: Currency);
    function GetProvision: Currency;
    procedure SetProvision(const Value: Currency);
    function GetCreditAmount: Currency;
    procedure SetCreditAmount(const Value: Currency);
    function GetCashChange: Currency;
    procedure SetCashChange(const Value: Currency);
    function GetCardId: integer;
    procedure SetCardId(const Value: integer);
    function GetCardFee: Currency;
    procedure SetCardFee(const Value: Currency);
    function GetCardApprNumber: String;
    procedure SetCardApprNumber(const Value: String);
    function GetOrderNumber: String;
    procedure SetOrderNumber(const Value: String);
    function GetDeliveryFee: Currency;
    procedure SetDeliveryFee(const Value: Currency);
    function GetCashRegister: String;
    procedure SetCashRegister(const Value: String);
    function GetCashier: String;
    procedure SetCashier(const Value: String);
    function GetDateIssued: TDate;
    procedure SetDateIssued(const Value: TDate);
    function GetStoreShift: TStoreshift;
    procedure SetStoreShift(const Value: TStoreshift);
    property DeliveryFee: Currency read GetDeliveryFee write SetDeliveryFee;
    property DateIssued: TDate read GetDateIssued write SetDateIssued;

  public
    property Subtotal: Currency read GetSubtotal write SetSubtotal;
    property WithCash: Currency read GetWithCash write SetWithCash;
    property WithVoucher: Currency read GetWithVoucher write SetWithVoucher;
    property WithSp: Currency read GetWithSp write SetWithSp;
    property WithSimpananku: Currency read GetWithSimpananku
      write SetWithSimpananku;
    property Dpp: Currency read GetDpp write SetDpp;
    property Ppn: Currency read GetPpn write SetPpn;
    property GrandTotal: Currency read GetGrandTotal write SetGrandTotal;
    property Member: TMember read GetMember write SetMember;
    property DiscountAmount: Currency read GetDiscountAmount
      write SetDiscountAmount;
    property DiscountPercent: Double read GetDiscountPercent
      write SetDiscountPercent;
    property Discount: Currency read GetDiscount write SetDiscount;
    property TermCount: integer read GetTermCount write SetTermCount;
    property TermAmount: Currency read GetTermAmount write SetTermAmount;
    property Provision: Currency read GetProvision write SetProvision;
    property CreditAmount: Currency read GetCreditAmount write SetCreditAmount;
    property CashChange: Currency read GetCashChange write SetCashChange;
    property CardId: integer read GetCardId write SetCardId;
    property CardFee: Currency read GetCardFee write SetCardFee;
    property CardApprNumber: String read GetCardApprNumber
      write SetCardApprNumber;
    property OrderNumber: String read GetOrderNumber write SetOrderNumber;
    property CashRegister: String read GetCashRegister write SetCashRegister;
    property Cashier: String read GetCashier write SetCashier;
    property Storeshift: TStoreshift read GetStoreShift write SetStoreShift;
    property DownPayment: Currency read GetDownPayment write SetDownPayment;
    property Plafon: Currency read GetPlafon write SetPlafon;
    property Interest: Currency read GetInterest write SetInterest;
    property CreditBase: Currency read GetCreditBase write SetCreditBase;
    property CardOwner: String read GetCardOwner write SetCardOwner;
    property CardNumber: String read GetCardNumber write SetCardNumber;
  end;

var
  FSaleBase: TFSaleBase;

implementation

{$R *.dfm}

uses F_SearchResult;

{ TFSaleBase }

procedure TFSaleBase.AddCart(AProduct: TProduct; AQuantity: integer);
var
  price, Discount, Dpp, Ppn: Currency;
begin
  with memCartItem do
  begin
    Append;
    // Stockist Price
    if (Member <> nil) and (Member.&Type.Id = 2) then
    begin
      price := AProduct.StockistPrice;
    end
    else
      price := AProduct.RetailPrice;

    Discount := AProduct.DiscountAmount;

    FieldByName('Id').Value := Random(100);
    FieldByName('ProductId').Value := AProduct.Id;
    FieldByName('ProductName').Value := AProduct.Name;
    FieldByName('ProductPrice').Value := price;
    FieldByName('Quantity').Value := AQuantity;
    FieldByName('Discount').Value := Discount;
    FieldByName('Subtotal').Value := (AQuantity * price) - Discount;
    FieldByName('ProductUnit').Value := AProduct.ProductUnit.Id;
    FieldByName('ProductCode').Value := AProduct.Code;

    FieldByName('RetailPrice').Value := AProduct.RetailPrice;
    FieldByName('RetailDiscount').Value := AProduct.RetailDiscount;
    FieldByName('PartyPrice').Value := AProduct.PartyPrice;
    FieldByName('PartyPriceQuantity').Value := AProduct.PartyPriceQuantity;
    FieldByName('StockistPrice').Value := AProduct.StockistPrice;
    FieldByName('IsTaxed').Value := AProduct.IsTaxed;

    if AProduct.IsTaxed then
    begin
      Ppn := Round(AQuantity * price / 11);
      Dpp := (AQuantity * price) - Ppn;
    end
    else
    begin
      Ppn := 0;
      Dpp := 0;
    end;
    FieldByName('Ppn').Value := Ppn;
    FieldByName('Dpp').Value := Dpp;
    Post;
  end;

  RefreshCart;
end;

procedure TFSaleBase.AddProductToCart(AProduct: TProduct; AQuantity: integer);
begin
  if memCartItem.Locate('ProductId', AProduct.Id, [loCaseInsensitive]) then
    UpdateCart(AProduct, AQuantity, 0)
  else
    AddCart(AProduct, AQuantity);
end;

procedure TFSaleBase.AppendNewItem;
var
  ri: integer;
begin
  _gvCart.DataController.AppendRecord;
  ri := (_gvCart.DataController.RecordCount - 1);
  _gvCart.DataController.FocusedRecordIndex := ri;

  _gvCart.DataController.Values[ri, GetColumnIndexByName(_gvCart,
    'gvCartId')] := 0;
  _gvCart.DataController.Values[ri, GetColumnIndexByName(_gvCart,
    'gvCartQuantity')] := 1;
end;

procedure TFSaleBase.DeleteCart(AProductId: integer);
var
  I: integer;
begin
  while memCartItem.Locate('ProductId', AProductId, [loPartialKey]) do
  begin
    memCartItem.Delete;
  end;
end;

procedure TFSaleBase.FinalizeOrder(AVouchers, ARealisasiSp: TStringList;
  ASaleType: integer; AAction: integer);
var
  parOrdMaster, parOrdItem : TSfStringVariantPair;
  parOrdItemList : TList<TSfStringVariantPair>;

  order : TOrder;
  res : TSfActionResult;

  I: integer;
  url, raw, ref: String;
  ordSum: ROrderSum;
begin
  {
    1 : Tunai
    2 : Pengajuan Kredit
    3 : Finalize Draft
  }
  parOrdMaster := TSfStringVariantPair.Create;

  with parOrdMaster do
  begin
//    Add('number', '');
    Add('dateissued', Now());
    Add('dateexpired', IncDay(Now(), 7));
    Add('customer_id', Member.Id);
    Add('customertype_id', Member.&Type.Id);
    Add('channel_id', 0);
    Add('status_id', 1);
    Add('subtotal', Subtotal);
    Add('discount', Discount);
    Add('creditamount', CreditAmount);
    Add('deliveryfee', DeliveryFee);
    Add('cardfee', CardFee);
    Add('total', GrandTotal);
    Add('paidbycash', WithCash);
    Add('paidbyvoucher', WithVoucher);
    Add('storage_id', DMMain.RegisterInfo.StorageId);
    Add('card_id', CardId);
    Add('cardnumber', CardNumber);
    Add('cardowner', CardOwner);
    Add('cardapprovalnumber', CardApprNumber);
//    Add('reference', );
    Add('employee_id', DMMain.LoginInfo.User.EmployeeId);
    {
    Master.AddPair('order_number', '');
    Master.AddPair('order_dateissued', FormatDateTime('yyyy-mm-dd', Date) + 'T' +
    FormatDateTime('hh:nn:ss', Time));
    if Self.Member.id > 0 then
    Master.AddPair('order_member', IntToStr(Self.Member.id));
    Master.AddPair('order_iscredit', BoolToStr((CreditAmount > 0)));
    Master.AddPair('order_dateexpired', FormatDateTime('yyyy-mm-dd', Date) + 'T' +
    FormatDateTime('hh:nn:ss', Time));
    Master.AddPair('order_channel', '3'); // TODO : Read from config
    Master.AddPair('order_subtotal', FloatToJson(Self.Subtotal));
    Master.AddPair('order_discountpercent', FloatToJson(DiscountPercent));
    Master.AddPair('order_discountamount', FloatToJson(DiscountAmount));
    Master.AddPair('order_total', FloatToJson(GrandTotal));

    Master.AddPair('order_item', Items);
    if ADelivery <> nil then
    begin
    Shipment.AddPair('oder_delivery_id', '0');
    Shipment.AddPair('order_id', '0');
    Shipment.AddPair('order_delivery_address', IntToStr(ADelivery.Address.Id));
    //    Shipment.AddPair('order_delivery_courier', 'null');
    Shipment.AddPair('order_delivery_cost', FloatToJson(ADelivery.Cost));
    //    Shipment.AddPair('order_delivery_begin', 'null');
    //    Shipment.AddPair('order_delivery_end', 'null');
    Shipment.AddPair('order_delivery_status', '1');
    Shipment.AddPair('order_delivery_description', '');
    Shipment.AddPair('_order_delivery_row_active', '1');

    Shipments.Add(Shipment);
    end;

    Master.AddPair('order_delivery', Shipments);

    if AVouchers <> nil then
    begin
    for I := 0 to AVouchers.Count-1 do
    begin
    Vouchers.Add(AVouchers[I]);
    end;

    Master.AddPair('order_voucher', Vouchers);
    end;

    if ARealisasiSp <> nil then
    begin
    for I := 0 to ARealisasiSp.Count-1 do
    begin
    RealisasiSps.Add(ARealisasiSp[I]);
    end;
    Master.AddPair('order_realisasisp', RealisasiSps);
    end;

    Master.AddPair('order_simpananku_amount', FloatToJson(WithSimpananku));

    Master.AddPair('order_deliveryfee', FloatToJson(DeliveryFee));
    Master.AddPair('order_paidbyvoucher', FloatToJson(WithVoucher));
    Master.AddPair('order_paidbyrealisasisp', FloatToJson(WithSp));
    Master.AddPair('order_paidbycash', FloatToJson(WithCash));
    Master.AddPair('order_cardfee', FloatToJson(CardFee));
    if Self.CardId <> 0 then
    Master.AddPair('order_card', IntToarray[0..10] of Integer = ();Str(CardId));
    Master.AddPair('order_cardapprovalnumber', CardApprNumber);
    Master.AddPair('order_creditamount', FloatToJson(CreditAmount));
    Master.AddPair('order_storage', IntToStr(DMMain.Config.ReadInteger('StorageId')));
    Master.AddPair('order_reference', ref);
    Master.AddPair('order_pic', IntToStr(DMMain.Session.User.id));
    Master.AddPair('order_saletype', IntToStr(ASaleType));
    Master.AddPair('order_action', IntToStr(AAction));

    if AAction = 2 then // draft
    Master.AddPair('order_status', '7')
    else // if AAction = 1 then
    begin
    if ADelivery <> nil then // paid, pending
    Master.AddPair('order_status', '1')
    else // finished
    Master.AddPair('order_status', '9');
    end;

    Master.AddPair('order_termcount', IntToStr(TermCount));
    Master.AddPair('order_termamount', FloatToJson(TermAmount));
    Master.AddPair('order_provision', FloatToJson(Provision));
    Master.AddPair('order_downpayment', FloatToJson(DownPayment));
    Master.AddPair('order_interest', FloatToJson(Interest));
    Master.AddPair('order_plafon', FloatToJson(Plafon));
    }
  end;
  {
  url := 'order/insert.mod?_dc=CF9EF0A17A2B';

  params := TStringList.Create;
  params.Add('_json=' + Master.ToString);
  response := DMMain.Rest.Post(url, params);
  DMMain.Logger.Debug(response.Text, url + params.Text);
  ordSum := TOrder.LoadOrderSum(ref);
  OrderNumber := ordSum.number;

  memCartItem.First;
  for I := 0 to memCartItem.RecordCount - 1 do
  begin
    Item := TJSONObject.Create;
    Item.AddPair('order_item_id', '');
    Item.AddPair('order_item_product', memCartItem.FieldByName('ProductId').AsString);
    Item.AddPair('order_item_unit', memCartItem.FieldByName('ProductUnit').AsString);
    Item.AddPair('order_item_unitprice', memCartItem.FieldByName('ProductPrice').AsString);
    Item.AddPair('order_item_amount', memCartItem.FieldByName('Quantity').AsString);
    Item.AddPair('order_item_subtotal', memCartItem.FieldByName('Subtotal').AsString);
    Item.AddPair('order_item_discountpercent', '0');
    Item.AddPair('order_item_discountamount', memCartItem.FieldByName('Discount').AsString);
    Item.AddPair('order_item_tax', '9');
    Item.AddPair('order_item_total', FloatToJson(memCartItem.FieldByName('Subtotal').AsFloat - memCartItem.FieldByName('Discount').AsFloat));

    Items.Add(Item);
  memCartItem.Next;
  end;
  }
end;

procedure TFSaleBase.FormCreate(Sender: TObject);
begin
  _grdCart := (FindComponent('grdCart') as TcxGrid);
  _gvCart := (FindComponent('gvCart') as TcxGridtableView);

  AStoreShift := TStoreshift.Create;
  AMember := TMember.Create;
  ADelivery := TDelivery.Create;

  memCartMaster.Open;
end;

function TFSaleBase.GetCardApprNumber: String;
begin
  Result := memCartMaster.FieldByName('CardApprNumber').AsString;
end;

function TFSaleBase.GetCardFee: Currency;
begin
  Result := memCartMaster.FieldByName('CardFee').AsFloat;
end;

function TFSaleBase.GetCardId: integer;
begin
  Result := memCartMaster.FieldByName('CardId').AsInteger;
end;

function TFSaleBase.GetCardNumber: String;
begin
  Result := memCartMaster.FieldByName('CardNumber').AsString;
end;

function TFSaleBase.GetCardOwner: String;
begin
  Result := memCartMaster.FieldByName('CardOwner').AsString;
end;

function TFSaleBase.GetWithCash: Currency;
begin
  Result := memCartMaster.FieldByName('Cash').AsFloat;
end;

function TFSaleBase.GetCashChange: Currency;
begin
  Result := memCartMaster.FieldByName('CashChange').AsFloat;
end;

function TFSaleBase.GetCashier: String;
begin
  Result := memCartMaster.FieldByName('Cashier').AsString;
end;

function TFSaleBase.GetCashRegister: String;
begin
  Result := memCartMaster.FieldByName('Register').AsString;
end;

function TFSaleBase.GetCreditAmount: Currency;
begin
  Result := memCartMaster.FieldByName('CreditAmount').AsFloat;
end;

function TFSaleBase.GetCreditBase: Currency;
begin
  Result := memCartMaster.FieldByName('CreditBase').AsFloat;
end;

function TFSaleBase.GetDateIssued: TDate;
begin
  Result := memCartMaster.FieldByName('DateIssued').AsDateTime;
end;

function TFSaleBase.GetDeliveryFee: Currency;
begin
  Result := memCartMaster.FieldByName('DeliveryFee').AsFloat;
end;

function TFSaleBase.GetDiscount: Currency;
begin
  Result := memCartMaster.FieldByName('Discount').AsFloat;
end;

function TFSaleBase.GetDiscountAmount: Currency;
begin
  Result := memCartMaster.FieldByName('DiscountAmount').AsFloat;
end;

function TFSaleBase.GetDiscountPercent: Double;
begin
  Result := memCartMaster.FieldByName('DiscountPercent').AsFloat;
end;

function TFSaleBase.GetDownPayment: Currency;
begin
  Result := memCartMaster.FieldByName('DownPayment').AsFloat;
end;

function TFSaleBase.GetDpp: Currency;
begin
  Result := memCartMaster.FieldByName('Dpp').AsFloat;
end;

function TFSaleBase.GetGrandTotal: Currency;
begin
  Result := memCartMaster.FieldByName('GrandTotal').AsFloat;
end;

function TFSaleBase.GetInterest: Currency;
begin
  Result := memCartMaster.FieldByName('Interest').AsFloat;
end;

function TFSaleBase.GetMember: TMember;
begin
  Result := Self.AMember;
end;

function TFSaleBase.GetOrderNumber: String;
begin
  Result := memCartMaster.FieldByName('OrderNumber').AsString;
end;

function TFSaleBase.GetPlafon: Currency;
begin
  Result := memCartMaster.FieldByName('Plafon').AsFloat;
end;

function TFSaleBase.GetPpn: Currency;
begin
  Result := memCartMaster.FieldByName('Ppn').AsFloat;
end;

function TFSaleBase.GetProvision: Currency;
begin
  Result := memCartMaster.FieldByName('Provision').AsFloat;
end;

function TFSaleBase.GetWithSimpananku: Currency;
begin
  Result := memCartMaster.FieldByName('Simpananku').AsFloat;
end;

function TFSaleBase.GetWithSp: Currency;
begin
  Result := memCartMaster.FieldByName('RealisasiSp').AsFloat;
end;

function TFSaleBase.GetStoreShift: TStoreshift;
begin
  Result := Self.AStoreShift;
end;

function TFSaleBase.GetSubtotal: Currency;
begin
  Result := memCartMaster.FieldByName('Subtotal').AsFloat;
end;

function TFSaleBase.GetTermAmount: Currency;
begin
  Result := memCartMaster.FieldByName('TermAmount').AsFloat;
end;

function TFSaleBase.GetTermCount: integer;
begin
  Result := memCartMaster.FieldByName('TermCount').AsInteger;
end;

function TFSaleBase.GetWithVoucher: Currency;
begin
  Result := memCartMaster.FieldByName('Voucher').AsFloat;
end;

procedure TFSaleBase.LoadOrder;
var
  oi, Item: TOrderItem;
begin
  // Load existing transaction
  Member := _order.Member;

  OrderNumber := _order.number;
  DiscountPercent := _order.DiscountPercent;
  DiscountAmount := _order.DiscountAmount;
  WithCash := _order.paidbycash;
  WithVoucher := _order.paidbyvoucher;
  WithSp := _order.paidbyrealisasisp;
  WithSimpananku := _order.paidbysukarela;
  DateIssued := _order.DateIssued;
  TermCount := _order.TermCount;
  TermAmount := _order.TermAmount;
  Provision := _order.Provision;
  Subtotal := _order.Subtotal;
  GrandTotal := _order.total;
  CreditAmount := _order.CreditAmount;
  DownPayment := _order.DownPayment;
  Plafon := _order.Plafon;

  oi := TOrderItem.Create;

  try
    _items := oi.ListOrderItem(_order.Id);

    memCartItem.Close;
    memCartItem.Open;

    for Item in _items do
    begin
      memCartItem.Append;
      memCartItem.FieldByName('Id').Value := Item.Id;
      memCartItem.FieldByName('ProductCode').Value := Item.Product.Code;
      memCartItem.FieldByName('ProductId').Value := Item.Product.Id;
      memCartItem.FieldByName('ProductName').Value := Item.Product.Name;
      memCartItem.FieldByName('ProductPrice').Value := Item.unitprice;
      memCartItem.FieldByName('Quantity').Value := Item.amount;
      memCartItem.FieldByName('Discount').Value := Item.DiscountAmount;
      memCartItem.FieldByName('Subtotal').Value := Item.Subtotal;
      memCartItem.FieldByName('ProductUnit').Value := Item.&unit.Id;
      memCartItem.FieldByName('Ppn').Value :=
        Round(Item.amount * Item.taxamount);
      memCartItem.FieldByName('Dpp').Value := 0;
      memCartItem.Post;
    end;

    RefreshCart;
  finally
    oi.Free;
  end;
end;

procedure TFSaleBase.PrintReceipt(ACopy: integer);
var
  I: integer;
begin
  rptStruk.PrepareReport;

{$IFDEF DEBUG}
  rptStruk.ShowReport;
{$ELSE}
  rptStruk.PrintOptions.ShowDialog := False;
  for I := 0 to ACopy - 1 do
  begin
    rptStruk.Print;
  end;
{$ENDIF}
end;

function TFSaleBase.ProcessSearch(AKeyword: String): Boolean;
var
  p: TProduct;
  r: TList<TProduct>;
  o: TOrder;
  os: TList<TOrder>;
  AQuantity: integer;
begin
  p := TProduct.Create;
  r := p.SearchByBarcode(AKeyword);
  AQuantity := _gvCart.DataController.Values
    [_gvCart.DataController.GetFocusedRecordIndex, GetColumnIndexByName(_gvCart,
    'gvCartQuantity')];

  if r.Count = 0 then
  begin
    Result := False;
  end
  else if r.Count = 1 then
  begin
    if (r[0].stock > 0) { or (DMMain.Setting.AllowExceedStock) } then
    begin
      AddProductToCart(r[0], AQuantity);
      Result := True;
      r[0].stock := r[0].stock - 1;
    end
    else
      MessageDlg('Out Of Stock', mtWarning, [mbOK], 0);
  end
  else if r.Count > 1 then
  begin
    FSearchResult := TFSearchResult.Create(Self);
    FSearchResult.Products := r;
    FSearchResult.Position := poScreenCenter;
    FSearchResult.ShowModal;
    if FSearchResult.ModalResult = mrOk then
    begin
      AddProductToCart(r[FSearchResult.SelectedIndex], AQuantity);
      Result := True;
    end;
    FSearchResult.Free;
  end;
  // end;
end;

procedure TFSaleBase.RefreshCart;
var
  ri: integer;
  ADiscount, ASubtotal, ADpp, APpn: Currency;
  lblCart: TcxLabel;
begin
  _gvCart.DataController.BeginUpdate;
  _gvCart.DataController.RecordCount := 0;

  ri := 0;
  memCartItem.First;
  ASubtotal := 0;
  while not memCartItem.Eof do
  begin
    with _gvCart.DataController do
    begin
      AppendRecord;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartId')] :=
        memCartItem.FieldByName('Id').AsInteger;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartProductId')] :=
        memCartItem.FieldByName('ProductId').AsInteger;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartProductCode')] :=
        memCartItem.FieldByName('ProductCode').AsString;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartProductName')] :=
        memCartItem.FieldByName('ProductName').AsString;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartProductPrice')] :=
        memCartItem.FieldByName('ProductPrice').AsFloat;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartQuantity')] :=
        memCartItem.FieldByName('Quantity').AsFloat;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartDiscount')] :=
        memCartItem.FieldByName('Discount').AsFloat;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartSubtotal')] :=
        memCartItem.FieldByName('Subtotal').AsFloat;
      Values[ri, GetColumnIndexByName(_gvCart, 'gvCartUnit')] :=
        memCartItem.FieldByName('ProductUnit').AsInteger;
    end;
    ASubtotal := ASubtotal + memCartItem.FieldByName('Subtotal').AsFloat;
    ADpp := ADpp + memCartItem.FieldByName('Dpp').AsFloat;
    APpn := APpn + memCartItem.FieldByName('Ppn').AsFloat;
    ADiscount := ADiscount + memCartItem.FieldByName('Discount').AsFloat;
    Inc(ri);
    memCartItem.Next;
  end;

  _gvCart.EndUpdate;

  Self.Subtotal := ASubtotal;
  Self.Discount := ADiscount;
  Self.Dpp := ADpp;
  Self.Ppn := APpn;

  UpdateCartGrandTotal;
  lblCart := (FindComponent('lblCart') as TcxLabel);
  if lblCart <> nil then
  begin
    lblCart.Show;
    lblCart.Caption := IntToStr(_gvCart.DataController.RecordCount);
  end;
end;

procedure TFSaleBase.ResetSession;
var
  m: TMember;
begin
  memCartItem.Close;
  memCartItem.Open;

  memCartMaster.Close;
  memCartMaster.Open;

  RefreshCart;

  m := TMember.Create;
  m.FirstName := 'CUSTOMER TUNAI';
  Member := m;
  OrderNumber := 'SAL.**************';
  Cashier := DMMain.LoginInfo.User.Username;
  CashRegister := DMMain.IniFile.ReadString('kasir', 'register_code', 'REG#01');
  DateIssued := Date;

  DiscountPercent := 0;
  DiscountAmount := 0;
end;

procedure TFSaleBase.SavePicture(APicture: TPicture; var AValue: AnsiString);
var
  AStream: TMemoryStream;
begin
  if IsPictureAssigned(APicture) then
  begin
    AStream := TMemoryStream.Create;
    try
      APicture.Graphic.SaveToStream(AStream);
      AStream.Position := 0;
      SetLength(AValue, AStream.Size);
      AStream.ReadBuffer(AValue[1], AStream.Size);
    finally
      AStream.Free;
    end;
  end
  else
    AValue := '';
end;

procedure TFSaleBase.SetCardApprNumber(const Value: String);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('CardApprNumber').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetCardFee(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('CardFee').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetCardId(const Value: integer);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('CardId').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetCardNumber(const Value: String);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('CardNumber').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetCardOwner(const Value: String);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('CardOwner').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetWithCash(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Cash').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetCashChange(const Value: Currency);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('CashChange').Value := Value;
    Post;
  end;
end;

procedure TFSaleBase.SetCashier(const Value: String);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('Cashier').Value := Value;
    Post;
  end;
end;

procedure TFSaleBase.SetCashRegister(const Value: String);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('Register').Value := Value;
    Post;
  end;
end;

procedure TFSaleBase.SetCreditAmount(const Value: Currency);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('CreditAmount').Value := Value;
    Post;
  end;
  SetCustomEditValue('edtCartCreditAmount', Value);
end;

procedure TFSaleBase.SetCreditBase(const Value: Currency);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('CreditBase').Value := Value;
    Post;
  end;
end;

procedure TFSaleBase.SetCustomEditValue(AName: String; AValue: Variant);
var
  c: TcxCustomEdit;
begin
  c := FindComponent(AName) as TcxCustomEdit;
  if c <> nil then
    c.EditValue := AValue;
end;

procedure TFSaleBase.SetDateIssued(const Value: TDate);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('DateIssued').Value := Value;
    Post;
  end;
end;

procedure TFSaleBase.SetDeliveryFee(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('DeliveryFee').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetDiscount(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Discount').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetDiscountAmount(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('DiscountAmount').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartDiscountAmount', Value);
end;

procedure TFSaleBase.SetDiscountPercent(const Value: Double);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('DiscountPercent').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartDiscountPercent', Value);
end;

procedure TFSaleBase.SetDownPayment(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('DownPayment').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartDownPayment', Value);
end;

procedure TFSaleBase.SetDpp(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Dpp').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartDpp', Value);
end;

procedure TFSaleBase.SetGrandTotal(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('GrandTotal').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartTotal', Value);
  SetLabelCaption('lblCartGrandTotal', FormatFloat('##,#0.#0', Value));
end;

procedure TFSaleBase.SetInterest(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Interest').Value := Value;
  memCartMaster.Post;

  // (FindComponent('edtCartInterest') as TcxCustomEdit).EditValue := Value;
end;

procedure TFSaleBase.SetLabelCaption(AName, ACaption: String);
var
  lbl: TcxLabel;
begin
  lbl := (FindComponent(AName) as TcxLabel);
  if lbl <> nil then
    lbl.Caption := ACaption;
end;

// procedure TFSaleBase.SetLkProperties;
// var
// dsSaletype: TDataSource;
// ASaletype: TSaletype;
// begin
// ASaletype := TSaletype.Create;
// dsSaletype := TDataSource.Create(nil);
// dsSaletype.DataSet := ASaletype.Lookup('saletype_id', 'saletype_name');
// with cmbSaletype do
// begin
// Properties.BeginUpdate;
// Properties.ListOptions.ShowHeader := False;
// Properties.ListSource := dsSaletype;
// Properties.KeyFieldNames := 'id';
// Properties.ListFieldNames := 'name';
// Properties.EndUpdate;
// end;
// end;

procedure TFSaleBase.SetMember(const Value: TMember);
begin
  AMember := Value;
  memCartMaster.Edit;
  memCartMaster.FieldByName('MemberId').Value := Value.Id;
  memCartMaster.FieldByName('MemberName').Value := Value.FirstName;
  memCartMaster.Post;

  SetLabelCaption('lblOrderMember', Value.FirstName);
  SetLabelCaption('lblMemberName', Value.FirstName);
end;

procedure TFSaleBase.SetOrderNumber(const Value: String);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('OrderNumber').Value := Value;
  memCartMaster.Post;

  SetLabelCaption('lblOrderNumber', Value);
end;

procedure TFSaleBase.SetPlafon(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Plafon').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartPlafon', Value);
end;

procedure TFSaleBase.SetPpn(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Ppn').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartPpn', Value);
end;

procedure TFSaleBase.SetProvision(const Value: Currency);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('Provision').Value := Value;
    Post;
  end;

  SetCustomEditValue('edtProvision', Value);
end;

procedure TFSaleBase.SetWithSimpananku(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Simpananku').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetWithSp(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('RealisasiSp').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.SetStoreShift(const Value: TStoreshift);
begin
  AStoreShift := Value;

  if Value <> nil then
  begin
    memCartMaster.Edit;
    memCartMaster.FieldByName('StoreshiftId').Value := Value.Id;
    memCartMaster.FieldByName('StoreshiftName').Value := Value.Shift.Name;
    memCartMaster.Post;

    SetLabelCaption('lblShift', Value.Shift.Name);
  end
  else
  begin
    memCartMaster.Edit;
    memCartMaster.FieldByName('StoreshiftId').Value := 0;
    memCartMaster.FieldByName('StoreshiftNumber').Value := '';
    memCartMaster.Post;

    SetLabelCaption('lblShift', '{ ## }');
  end;
end;

procedure TFSaleBase.SetSubtotal(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Subtotal').Value := Value;
  memCartMaster.Post;

  SetCustomEditValue('edtCartSubtotal', Value);
end;

procedure TFSaleBase.SetTermAmount(const Value: Currency);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('TermAmount').Value := Value;
    Post;
  end;

  SetCustomEditValue('edtTermAmount', Value);
end;

procedure TFSaleBase.SetTermCount(const Value: integer);
begin
  with memCartMaster do
  begin
    Edit;
    FieldByName('TermCount').Value := Value;
    Post;
  end;

  SetLabelCaption('lblTermCount', IntToStr(Value) + ' x ');
end;

procedure TFSaleBase.SetWithVoucher(const Value: Currency);
begin
  memCartMaster.Edit;
  memCartMaster.FieldByName('Voucher').Value := Value;
  memCartMaster.Post;
end;

procedure TFSaleBase.ShowHintLabels(AVisible: Boolean);
begin

end;

procedure TFSaleBase.UpdateCart(AProduct: TProduct; AQuantity: integer;
  ADiscount: Currency);
var
  quantityR, quantityP: integer;

  Dpp, Ppn: Currency;

  found: Boolean;
  Id: integer;
begin
  found := memCartItem.Locate('ProductId', AProduct.Id, [loPartialKey]);
  Id := memCartItem.FieldByName('Id').AsInteger;

  if found then
  begin
    with memCartItem do
    begin
      quantityP := 0;
      quantityR := 0;

      First;
      while not Eof do
      begin
        if FieldByName('ProductId').AsInteger = AProduct.Id then
          quantityR := quantityR + FieldByName('Quantity').AsInteger;
        Next;
      end;

      quantityR := quantityR + AQuantity;

      if AProduct.PartyPriceQuantity > 0 then
      begin
        quantityP := quantityR - (quantityR mod AProduct.PartyPriceQuantity);
        quantityR := quantityR mod AProduct.PartyPriceQuantity;
      end;

      // Clean Up
      DeleteCart(AProduct.Id);

      // Insert Party Sale
      if quantityP > 0 then
      begin
        Append;
        FieldByName('Id').Value := Random(100);
        FieldByName('ProductId').Value := AProduct.Id;
        FieldByName('ProductCode').Value := AProduct.Code;
        FieldByName('ProductName').Value := AProduct.Name;
        FieldByName('ProductPrice').Value := AProduct.PartyPrice;
        FieldByName('ProductUnit').Value := AProduct.ProductUnit.Id;
        FieldByName('Quantity').Value := quantityP;
        FieldByName('Discount').Value := AProduct.PartyDiscount * quantityP;
        FieldByName('Subtotal').Value := (AProduct.PartyPrice * quantityP) -
          (AProduct.PartyDiscount * quantityP);
        FieldByName('RetailPrice').Value := AProduct.RetailPrice;
        FieldByName('RetailDiscount').Value := AProduct.RetailDiscount;
        FieldByName('PartyPrice').Value := AProduct.PartyPrice;
        FieldByName('PartyPriceQuantity').Value := AProduct.PartyPriceQuantity;
        FieldByName('IsTaxed').Value := AProduct.IsTaxed;

        if AProduct.IsTaxed then
        begin
          Ppn := Round(quantityP * AProduct.PartyPrice / 11);
          Dpp := (quantityP * AProduct.PartyPrice) - Ppn;
        end
        else
        begin
          Ppn := 0;
          Dpp := 0;
        end;

        FieldByName('Ppn').Value := Ppn;
        FieldByName('Dpp').Value := Dpp;
        Post;
      end;
      // - End of Insert Party Sale

      // Insert Retail Sale
      if quantityR > 0 then
      begin
        Append;
        FieldByName('Id').Value := Random(100);
        FieldByName('ProductId').Value := AProduct.Id;
        FieldByName('ProductCode').Value := AProduct.Code;
        FieldByName('ProductName').Value := AProduct.Name;
        FieldByName('ProductPrice').Value := AProduct.RetailPrice;
        FieldByName('ProductUnit').Value := AProduct.ProductUnit.Id;
        FieldByName('Quantity').Value := quantityR;
        FieldByName('Discount').Value := AProduct.RetailDiscount * quantityR;
        FieldByName('Subtotal').Value := (AProduct.RetailPrice * quantityR) -
          (AProduct.RetailDiscount * quantityR);

        FieldByName('RetailPrice').Value := AProduct.RetailPrice;
        FieldByName('RetailDiscount').Value := AProduct.RetailDiscount;
        FieldByName('PartyPrice').Value := AProduct.PartyPrice;
        FieldByName('PartyPriceQuantity').Value := AProduct.PartyPriceQuantity;
        FieldByName('IsTaxed').Value := AProduct.IsTaxed;

        if AProduct.IsTaxed then
        begin
          Ppn := Round(quantityR * AProduct.RetailPrice / 11);
          Dpp := (quantityR * AProduct.RetailPrice) - Ppn;
        end
        else
        begin
          Ppn := 0;
          Dpp := 0;
        end;

        FieldByName('Ppn').Value := Ppn;
        FieldByName('Dpp').Value := Dpp;
        Post;
      end;
      // - End of Insert Retail Sale
    end;
  end;

  RefreshCart;
end;

procedure TFSaleBase.UpdateCartGrandTotal;
begin
  Self.GrandTotal := Self.Subtotal - Self.DiscountAmount;
end;

procedure TFSaleBase.UpdateCartDiscount;
begin
  DiscountPercent := (FindComponent('edtCartDiscountPercent') as TcxCustomEdit)
    .EditValue;
  DiscountAmount := DiscountPercent * 0.01 * Subtotal;
end;

end.
