object FSaleBase: TFSaleBase
  Left = 0
  Top = 0
  Caption = 'FSaleBase'
  ClientHeight = 398
  ClientWidth = 668
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object memCartMaster: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 224
    Top = 176
    object memCartMasterDpp: TCurrencyField
      FieldName = 'Dpp'
    end
    object memCartMasterDiscount: TCurrencyField
      FieldName = 'Discount'
    end
    object memCartMasterCash: TCurrencyField
      FieldName = 'Cash'
    end
    object memCartMasterVoucher: TCurrencyField
      FieldName = 'Voucher'
    end
    object memCartMasterRealisasiSp: TCurrencyField
      FieldName = 'RealisasiSp'
    end
    object memCartMasterDateIssued: TDateTimeField
      FieldName = 'DateIssued'
    end
    object memCartMasterOrderNumber: TStringField
      FieldName = 'OrderNumber'
    end
    object memCartMasterMemberInfo: TStringField
      FieldName = 'MemberInfo'
      Size = 255
    end
    object memCartMasterTermCount: TIntegerField
      FieldName = 'TermCount'
    end
    object memCartMasterProvision: TCurrencyField
      FieldName = 'Provision'
    end
    object memCartMasterTermAmount: TCurrencyField
      FieldName = 'TermAmount'
    end
    object memCartMasterPpn: TCurrencyField
      FieldName = 'Ppn'
    end
    object memCartMasterSubtotal: TCurrencyField
      FieldName = 'Subtotal'
    end
    object memCartMasterMemberId: TIntegerField
      FieldName = 'MemberId'
    end
    object memCartMasterMemberName: TStringField
      FieldName = 'MemberName'
      Size = 255
    end
    object memCartMasterDiscountPercent: TFloatField
      FieldName = 'DiscountPercent'
    end
    object memCartMasterDiscountAmount: TCurrencyField
      FieldName = 'DiscountAmount'
    end
    object memCartMasterGrandTotal: TCurrencyField
      FieldName = 'GrandTotal'
    end
    object memCartMasterPaid: TCurrencyField
      FieldName = 'Paid'
    end
    object memCartMasterCreditAmount: TCurrencyField
      FieldName = 'CreditAmount'
    end
    object memCartMasterCashChange: TCurrencyField
      FieldName = 'CashChange'
    end
    object memCartMasterDeliveryFee: TFloatField
      FieldName = 'DeliveryFee'
    end
    object memCartMasterCardId: TIntegerField
      FieldName = 'CardId'
    end
    object memCartMasterCardFee: TIntegerField
      FieldName = 'CardFee'
    end
    object memCartMasterCardApprNumber: TStringField
      FieldName = 'CardApprNumber'
      Size = 50
    end
    object memCartMasterCashier: TStringField
      FieldName = 'Cashier'
      Size = 100
    end
    object memCartMasterRegister: TStringField
      FieldName = 'Register'
      Size = 100
    end
    object memCartMasterStoreshiftId: TIntegerField
      FieldName = 'StoreshiftId'
    end
    object memCartMasterStoreshiftNumber: TStringField
      FieldName = 'StoreshiftNumber'
      Size = 10
    end
    object memCartMasterStoreshiftName: TStringField
      FieldName = 'StoreshiftName'
      Size = 255
    end
    object memCartMasterSimpananku: TCurrencyField
      FieldName = 'Simpananku'
    end
    object memCartMasterDownPayment: TCurrencyField
      FieldName = 'DownPayment'
    end
    object memCartMasterPlafon: TCurrencyField
      FieldName = 'Plafon'
    end
    object memCartMasterInterest: TCurrencyField
      FieldName = 'Interest'
    end
    object memCartMasterCreditBase: TCurrencyField
      FieldName = 'CreditBase'
    end
  end
  object memCartItem: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 336
    Top = 152
    object memCartItemId: TIntegerField
      FieldName = 'Id'
    end
    object memCartItemProductId: TIntegerField
      FieldName = 'ProductId'
    end
    object memCartItemProductCode: TStringField
      FieldName = 'ProductCode'
      Size = 50
    end
    object memCartItemProductName: TStringField
      FieldName = 'ProductName'
      Size = 100
    end
    object memCartItemProductPrice: TCurrencyField
      FieldName = 'ProductPrice'
    end
    object memCartItemQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object memCartItemDiscount: TCurrencyField
      FieldName = 'Discount'
    end
    object memCartItemSubtotal: TCurrencyField
      FieldName = 'Subtotal'
    end
    object memCartItemProductUnit: TIntegerField
      FieldName = 'ProductUnit'
    end
    object memCartItemDpp: TCurrencyField
      FieldName = 'Dpp'
    end
    object memCartItemPpn: TCurrencyField
      FieldName = 'Ppn'
    end
    object memCartItemRetailPrice: TCurrencyField
      FieldName = 'RetailPrice'
    end
    object memCartItemRetailDiscount: TCurrencyField
      FieldName = 'RetailDiscount'
    end
    object memCartItemPartyPrice: TCurrencyField
      FieldName = 'PartyPrice'
    end
    object memCartItemPartyPriceQuantity: TIntegerField
      FieldName = 'PartyPriceQuantity'
    end
    object memCartItemStockistPrice: TCurrencyField
      FieldName = 'StockistPrice'
    end
    object memCartItemIsTaxed: TBooleanField
      FieldName = 'IsTaxed'
    end
  end
  object dsCartMaster: TfrxDBDataset
    UserName = 'CartMaster'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RecId=RecId'
      'Dpp=Dpp'
      'Discount=Discount'
      'Cash=Cash'
      'Voucher=Voucher'
      'RealisasiSp=RealisasiSp'
      'DateIssued=DateIssued'
      'OrderNumber=OrderNumber'
      'MemberInfo=MemberInfo'
      'TermCount=TermCount'
      'Provision=Provision'
      'TermAmount=TermAmount'
      'Ppn=Ppn'
      'Subtotal=Subtotal'
      'MemberId=MemberId'
      'MemberName=MemberName'
      'DiscountPercent=DiscountPercent'
      'DiscountAmount=DiscountAmount'
      'GrandTotal=GrandTotal'
      'Paid=Paid'
      'CreditAmount=CreditAmount'
      'CashChange=CashChange'
      'DeliveryFee=DeliveryFee'
      'CardId=CardId'
      'CardFee=CardFee'
      'CardApprNumber=CardApprNumber'
      'Cashier=Cashier'
      'Register=Register'
      'StoreshiftId=StoreshiftId'
      'StoreshiftNumber=StoreshiftNumber'
      'Simpananku=Simpananku'
      'DownPayment=DownPayment'
      'Plafon=Plafon'
      'Interest=Interest'
      'CreditBase=CreditBase')
    DataSet = memCartMaster
    BCDToCurrency = False
    Left = 200
    Top = 272
  end
  object dsCartItem: TfrxDBDataset
    UserName = 'CartItem'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RecId=RecId'
      'Id=Id'
      'ProductId=ProductId'
      'ProductCode=ProductCode'
      'ProductName=ProductName'
      'ProductPrice=ProductPrice'
      'Quantity=Quantity'
      'Discount=Discount'
      'Subtotal=Subtotal'
      'ProductUnit=ProductUnit'
      'Dpp=Dpp'
      'Ppn=Ppn'
      'RetailPrice=RetailPrice'
      'RetailDiscount=RetailDiscount'
      'PartyPrice=PartyPrice'
      'PartyPriceQuantity=PartyPriceQuantity'
      'StockistPrice=StockistPrice'
      'IsTaxed=IsTaxed')
    DataSet = memCartItem
    BCDToCurrency = False
    Left = 432
    Top = 232
  end
  object rptStruk: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43676.416606469900000000
    ReportOptions.LastChange = 43801.228742916670000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);      ' +
        '                                                                '
      'var'
      
        '  IsCredit : Boolean;                                           ' +
        '                 '
      'begin'
      '{               '
      '  if <CartItem."Discount"> = 0 then'
      '  begin              '
      '    mmDiscount1.Visible := False;'
      
        '    mmDiscount1.Height := 0;                                    ' +
        '                             '
      '    mmDiscount2.Visible := False;'
      
        '    mmDiscount2.Height := 0;                                    ' +
        '                          '
      '    CartDiscount.Visible := False;'
      
        '    CartDiscount.Height := 0;                                   ' +
        '                           '
      '  end                '
      '  else'
      '  begin              '
      '    mmDiscount1.Visible := True;'
      '    mmDiscount1.Height := 0.5;        '
      '    mmDiscount2.Visible := True;'
      '    mmDiscount2.Height := 0.5;        '
      '    CartDiscount.Visible := True;'
      '    CartDiscount.Height := 0.5;        '
      '  end;'
      '}'
      ''
      '  IsCredit := (<CartMaster."CreditAmount"> > 0);'
      '      '
      '  Memo43.Visible := IsCredit;'
      '  Memo44.Visible := IsCredit;'
      '  Memo45.Visible := IsCredit;      '
      '  Memo46.Visible := IsCredit;'
      '  Memo47.Visible := IsCredit;'
      '  Memo48.Visible := IsCredit;'
      '  Memo49.Visible := IsCredit;'
      '  Memo50.Visible := IsCredit;'
      '  Memo51.Visible := IsCredit;'
      '  Memo52.Visible := IsCredit;'
      '  Memo53.Visible := IsCredit;'
      '  Memo54.Visible := IsCredit;'
      '  Memo55.Visible := IsCredit;'
      '  Memo56.Visible := IsCredit;'
      '  Memo57.Visible := IsCredit;'
      '  Memo58.Visible := IsCredit;'
      '  Memo59.Visible := IsCredit;      '
      'end;'
      ''
      'procedure CartDiscountOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <CartItem."Discount"> = 0 then'
      '  begin'
      '    mmDiscount1.Height := 0;'
      '    mmDiscount2.Height := 0;'
      
        '    CartDiscount.Height := 0;                                   ' +
        '                         '
      '  end;            '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 336
    Top = 288
    Datasets = <
      item
        DataSet = dsCartItem
        DataSetName = 'CartItem'
      end
      item
        DataSet = dsCartMaster
        DataSetName = 'CartMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 100.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      EndlessHeight = True
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 147.401670000000000000
        Top = 18.897650000000000000
        Width = 340.157700000000000000
        object Memo1: TfrxMemoView
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Kopwan SETIA BHAKTI WANITA')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 18.897650000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Jl. Jemur Andayani 55 Surabaya')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 37.795300000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Telp. (031) 8434055')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Top = 56.692949999999990000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'NPWP: 01.145.691.0-609.000')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 75.590600000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'No')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Top = 94.488250000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Tgl')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 113.385900000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nama')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 52.913420000000000000
          Top = 75.590600000000000000
          Width = 7.559060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 52.913420000000000000
          Top = 94.488250000000000000
          Width = 7.559060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 52.913420000000000000
          Top = 113.385900000000000000
          Width = 7.559060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 60.472480000000000000
          Top = 75.590600000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[CartMaster."OrderNumber"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 60.472480000000000000
          Top = 94.488250000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatDateTime('#39'dd-mm-yyyy'#39', <CartMaster."DateIssued">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 60.472480000000000000
          Top = 113.385900000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[CartMaster."MemberName"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Top = 138.063080000000000000
          Width = 340.157700000000000000
          Height = 1.889763780000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop, ftBottom]
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 58.472480000000000000
        Top = 226.771800000000000000
        Width = 340.157700000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = dsCartItem
        DataSetName = 'CartItem'
        RowCount = 0
        Stretched = True
        object CartProductName: TfrxMemoView
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            '[CartItem."ProductName"]')
          ParentFont = False
        end
        object CartQuantity: TfrxMemoView
          Left = 15.118120000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          DataField = 'Quantity'
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartItem."Quantity"]')
          ParentFont = False
        end
        object CartProductPrice: TfrxMemoView
          Left = 49.133890000000000000
          Top = 18.897650000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          DataField = 'ProductPrice'
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartItem."ProductPrice"]')
          ParentFont = False
        end
        object CartSubtotal: TfrxMemoView
          Left = 185.196970000000000000
          Top = 18.897650000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataField = 'Subtotal'
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartItem."Subtotal"]')
          ParentFont = False
        end
        object CartDiscount: TfrxMemoView
          Left = 185.196970000000000000
          Top = 39.574829999999990000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'CartDiscountOnBeforePrint'
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            '- [CartItem."Discount"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 34.015770000000000000
          Top = 18.897650000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            'x')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object mmDiscount2: TfrxMemoView
          Left = 170.078850000000000000
          Top = 39.574829999999990000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object mmDiscount1: TfrxMemoView
          Left = 49.133890000000000000
          Top = 39.574829999999990000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            'Pot')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 389.275820000000000000
        Top = 347.716760000000000000
        Width = 340.157700000000000000
        Stretched = True
        object Memo18: TfrxMemoView
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[COUNT(MasterData1)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 18.897650000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Items')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 75.590600000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 185.196970000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<CartItem."Subtotal">,MasterData1)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 170.078850000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 75.590600000000000000
          Top = 37.897650000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Tunai')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 185.196970000000000000
          Top = 37.897650000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."Cash"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 170.078850000000000000
          Top = 37.897650000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 75.590600000000000000
          Top = 56.795300000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Voucher')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 185.196970000000000000
          Top = 56.795300000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."Voucher"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 170.078850000000000000
          Top = 56.795300000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 75.590600000000000000
          Top = 75.692950000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Wajib Beli')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 185.196970000000000000
          Top = 75.692950000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."RealisasiSp"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 170.078850000000000000
          Top = 75.692950000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 75.590600000000000000
          Top = 115.267780000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Kembali')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 185.196970000000000000
          Top = 115.267780000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 170.078850000000000000
          Top = 115.267780000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Total Diskon')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 185.196970000000000000
          Top = 18.897650000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[<CartMaster."DiscountAmount"> + <CartMaster."Discount">]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Top = 163.842610000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'DPP = [CartMaster."Dpp"] PPN = [CartMaster."Ppn"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 185.196970000000000000
          Top = 115.267780000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."CashChange"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Top = 183.181200000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Memo.UTF8W = (
            
              '[<CartMaster."Cashier">] / [<CartMaster."Register">] / [FormatDa' +
              'teTime('#39'dd-mm-yyyy h:n:s'#39', date + time)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 75.590600000000000000
          Top = 95.488250000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Simpananku')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 185.196970000000000000
          Top = 95.488250000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."Simpananku"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 170.078850000000000000
          Top = 95.488250000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 211.637910000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Penerima,')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 177.637910000000000000
          Top = 211.637910000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Penjual,')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Top = 298.567100000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Jumlah angsuran')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 185.196970000000000000
          Top = 298.567100000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            '[CartMaster."TermCount"] x')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 170.078850000000000000
          Top = 298.567100000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Top = 317.464750000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Total Kredit')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 185.196970000000000000
          Top = 317.464750000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."CreditAmount"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 170.078850000000000000
          Top = 317.464750000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Top = 336.362400000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Angsuran per bulan')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 185.196970000000000000
          Top = 336.362400000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."TermAmount"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 170.078850000000000000
          Top = 336.362400000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Top = 355.260050000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Provisi')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 185.196970000000000000
          Top = 355.260050000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."Provision"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 170.078850000000000000
          Top = 355.260050000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 75.590600000000000000
          Top = 134.063080000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          Memo.UTF8W = (
            'Sisa')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 170.078850000000000000
          Top = 134.063080000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haCenter
          Memo.UTF8W = (
            '=')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 185.196970000000000000
          Top = 134.063080000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDouble
          HAlign = haRight
          Memo.UTF8W = (
            '[CartMaster."CreditBase"]')
          ParentFont = False
        end
      end
    end
  end
  object rptStrukOrder: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43676.416606469900000000
    ReportOptions.LastChange = 43801.264883784700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 464
    Top = 104
    Datasets = <
      item
        DataSet = dsCartMaster
        DataSetName = 'CartMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page2: TfrxReportPage
      PaperWidth = 100.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      EndlessHeight = True
      object ReportTitle2: TfrxReportTitle
        FillType = ftBrush
        Height = 162.299320000000000000
        Top = 18.897650000000000000
        Width = 340.157700000000000000
        object Memo60: TfrxMemoView
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Kopwan SETIA BHAKTI WANITA')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Top = 18.897650000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Jl. Jemur Andayani 55 Surabaya')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Top = 37.795300000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Telp. (031) 8434055 / NPWP: 01.145.691.0-609.000')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Top = 75.590600000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'No')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Top = 94.488250000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Tgl')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Top = 113.385900000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nama')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 52.913420000000000000
          Top = 75.590600000000000000
          Width = 7.559060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 52.913420000000000000
          Top = 94.488250000000000000
          Width = 7.559060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 52.913420000000000000
          Top = 113.385900000000000000
          Width = 7.559060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 60.472480000000000000
          Top = 75.590600000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[CartMaster."OrderNumber"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 60.472480000000000000
          Top = 94.488250000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatDateTime('#39'dd-mm-yyyy'#39', <CartMaster."DateIssued">)]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 60.472480000000000000
          Top = 113.385900000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[CartMaster."MemberName"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Top = 132.063080000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 60.472480000000000000
          Top = 132.063080000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          DisplayFormat.FormatStr = '##,#0.#0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          Memo.UTF8W = (
            '[CartMaster."GrandTotal"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 52.913420000000000000
          Top = 132.063080000000000000
          Width = 7.559060000000000000
          Height = 18.897650000000000000
          DataSet = dsCartItem
          DataSetName = 'CartItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDashDotDot
          HAlign = haCenter
          Memo.UTF8W = (
            ':')
          ParentFont = False
        end
        object Barcode2D1: TfrxBarcode2DView
          Left = 252.448980000000000000
          Top = 66.960730000000000000
          Width = 84.000000000000000000
          Height = 84.000000000000000000
          BarType = bcCodeQR
          BarProperties.Encoding = qrAuto
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecL
          BarProperties.PixelSize = 4
          BarProperties.CodePage = 0
          DataField = 'OrderNumber'
          DataSet = dsCartMaster
          DataSetName = 'CartMaster'
          Rotation = 0
          ShowText = False
          Text = '12345678'
          Zoom = 1.000000000000000000
          FontScaled = True
          QuietZone = 0
        end
      end
    end
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 504
    Top = 184
  end
end
