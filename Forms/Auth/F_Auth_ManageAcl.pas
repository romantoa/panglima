unit F_Auth_ManageAcl;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, Data.DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxSplitter, Vcl.ExtCtrls,
  cxInplaceContainer, cxVGrid, cxDBVGrid, Data.Win.ADODB, cxDBLookupComboBox,
  cxNavigator, cxDBNavigator, Vcl.StdCtrls, cxContainer,
  cxLabel, cxPCdxBarPopupMenu, cxPC, cxMemo, cxTL, cxTLdxBarBuiltInMenu,
  cxTLData, cxDBTL, cxMaskEdit, cxCheckBox, Vcl.Menus,
  dxSkinOffice2007Silver, cxTLExportLink, dxSkinOffice2013White,
  dxSkinOffice2007Green, F_MdiChild, dxSkinMoneyTwins, cxButtons, 
  dxBarBuiltInMenu, dxRibbonCustomizationForm, dxRibbonSkins,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxBar, dxRibbon, cxButtonEdit,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Pink, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TFAuthManageAcl = class(TFMdiChild)
    Panel2: TPanel;
    cxSplitter1: TcxSplitter;
    cxSplitter2: TcxSplitter;
    Panel1: TPanel;
    cxGrid2: TcxGrid;
    gvUser: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    spUser: TADOStoredProc;
    dsUser: TDataSource;
    gvUserusername: TcxGridDBColumn;
    gvUserdisplay_name: TcxGridDBColumn;
    gvUseris_su: TcxGridDBColumn;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow8: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow9: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow10: TcxDBEditorRow;
    Panel3: TPanel;
    Label1: TLabel;
    Panel4: TPanel;
    cxLabel1: TcxLabel;
    spRole: TADOStoredProc;
    dsRole: TDataSource;
    spRoleMember: TADOStoredProc;
    dsRoleMember: TDataSource;
    pgRoleDetail: TcxPageControl;
    tsRoleMember: TcxTabSheet;
    cxGrid4: TcxGrid;
    gvRoleMember: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    spRoleid: TAutoIncField;
    spRolekode: TStringField;
    spRolenama: TStringField;
    spRoleid_jabatan: TIntegerField;
    spRoledeskripsi: TStringField;
    Panel5: TPanel;
    Label2: TLabel;
    Panel6: TPanel;
    spRoleMemberid_user: TIntegerField;
    gvRoleMemberid_user: TcxGridDBColumn;
    spAcl: TADOStoredProc;
    dsAcl: TDataSource;
    Panel7: TPanel;
    Panel8: TPanel;
    Label3: TLabel;
    spRoleMemberid_role: TIntegerField;
    spAclid: TAutoIncField;
    spAclid_parent: TIntegerField;
    spAclkode: TStringField;
    spAclnama: TStringField;
    spAclis_crud: TBooleanField;
    spAclid_acl: TAutoIncField;
    spAclid_role: TIntegerField;
    spAclid_permission: TIntegerField;
    spAclr: TBooleanField;
    spAclc: TBooleanField;
    spAclu: TBooleanField;
    spAcld: TBooleanField;
    spAclp: TBooleanField;
    spAcle: TBooleanField;
    spAcldr: TBooleanField;
    pgAcl: TcxPageControl;
    ACL: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    spPermissions: TADOStoredProc;
    dsPermissions: TDataSource;
    spPermissionsid: TAutoIncField;
    spPermissionsid_parent: TIntegerField;
    spPermissionskode: TStringField;
    spPermissionsnama: TStringField;
    spPermissionsis_crud: TBooleanField;
    treeAcl: TcxDBTreeList;
    treePermissions: TcxDBTreeList;
    Panel10: TPanel;
    treePermissionskode: TcxDBTreeListColumn;
    treePermissionsnama: TcxDBTreeListColumn;
    treePermissionsis_crud: TcxDBTreeListColumn;
    treeAclkode: TcxDBTreeListColumn;
    treeAclnama: TcxDBTreeListColumn;
    treeAclr: TcxDBTreeListColumn;
    treeAclc: TcxDBTreeListColumn;
    treeAclu: TcxDBTreeListColumn;
    treeAcld: TcxDBTreeListColumn;
    treeAclp: TcxDBTreeListColumn;
    treeAcle: TcxDBTreeListColumn;
    treeAcldr: TcxDBTreeListColumn;
    Panel11: TPanel;
    spUserid: TAutoIncField;
    spUserusername: TStringField;
    spUserpassword: TStringField;
    spUserdisplay_name: TStringField;
    spUseris_su: TBooleanField;
    spUseris_suspended: TBooleanField;
    spUsersecurity_question: TStringField;
    spUsersecurity_answer: TStringField;
    spUserrecovery_email: TStringField;
    spUserrecovery_phone: TStringField;
    spUserlast_ip: TStringField;
    spUserlast_login: TDateTimeField;
    spUserlast_password_change: TDateTimeField;
    spUserlast_logout: TDateTimeField;
    Label5: TLabel;
    gv1: TcxGrid;
    gvAclMore: TcxGridDBTableView;
    gvAclMorekode: TcxGridDBColumn;
    gvAclMorenama: TcxGridDBColumn;
    gvAclMoregrant: TcxGridDBColumn;
    gv1Level1: TcxGridLevel;
    dsAclMore: TDataSource;
    spAclMore: TADOStoredProc;
    spAclMoreid: TAutoIncField;
    spAclMoreid_parent: TIntegerField;
    spAclMorekode: TStringField;
    spAclMorenama: TStringField;
    spAclMoreis_crud: TBooleanField;
    spAclMoreid_1: TAutoIncField;
    spAclMoreid_role: TIntegerField;
    spAclMoreid_permission: TIntegerField;
    spAclMoregrant: TBooleanField;
    spUserid_pegawai: TIntegerField;
    spPermissionsclass_name: TStringField;
    treePermissionscAction: TcxDBTreeListColumn;
    spRoleid_parent: TIntegerField;
    treeRole: TcxDBTreeList;
    cxdbtrlstclmntree1kode: TcxDBTreeListColumn;
    cxdbtrlstclmntree1nama: TcxDBTreeListColumn;
    cxdbtrlstclmntree1id_jabatan: TcxDBTreeListColumn;
    btnPrintAcl: TcxButton;
    btnExportAcl: TcxButton;
    navData: TcxDBNavigator;
    nav1: TcxDBNavigator;
    nav2: TcxDBNavigator;
    nav3: TcxDBNavigator;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1: TdxBarManager;
    btnSyncAll: TcxButton;
    gvUseractions: TcxGridDBColumn;
    cxdbtrlstclmnRolecxDBTreeListunit: TcxDBTreeListColumn;
    spRoleunit: TStringField;
    pmUser: TPopupMenu;
    ResetPassword1: TMenuItem;
    pmRole: TPopupMenu;
    CloneRole: TMenuItem;
    treePermissionsId: TcxDBTreeListColumn;
    btnPermAddChild: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure spUserBeforePost(DataSet: TDataSet);
    procedure spRoleMemberBeforePost(DataSet: TDataSet);
    procedure tsRoleMemberShow(Sender: TObject);
    procedure btnNewChildPermClick(Sender: TObject);
    procedure btnDeletePermClick(Sender: TObject);
    procedure spRoleAfterScroll(DataSet: TDataSet);
    procedure ACLShow(Sender: TObject);
    procedure spUserBeforeDelete(DataSet: TDataSet);
    procedure btnSyncAclByKategoriClick(Sender: TObject);
    procedure treePermissionsEditValueChanged(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn);
    procedure spAclAfterScroll(DataSet: TDataSet);
    procedure spRoleAfterDelete(DataSet: TDataSet);
    procedure btnExportAclClick(Sender: TObject);
    procedure btnNewPermClick(Sender: TObject);
    procedure spPermissionsAfterPost(DataSet: TDataSet);
    procedure spPermissionsAfterDelete(DataSet: TDataSet);
    procedure spUserAfterPost(DataSet: TDataSet);
    procedure btnSyncAllClick(Sender: TObject);
    procedure gvUserresetpassPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure spAclBeforeDelete(DataSet: TDataSet);
    procedure spUserAfterDelete(DataSet: TDataSet);
    procedure treeAclFocusedNodeChanged(Sender: TcxCustomTreeList;
      APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure ResetPassword1Click(Sender: TObject);
    procedure CloneRoleClick(Sender: TObject);
    procedure btnPermAddChildClick(Sender: TObject);
  private
    { Private declarations }
    _IdParentPerm : Integer;
    _IsShown      : Boolean;
    _AclShown     : Boolean;
    _RoleShown    : Boolean;

    procedure RefreshUser;
    procedure RefreshRole;
    procedure RefreshRoleMember;
    procedure RefreshAcl;
    procedure RefreshPermissions;
    procedure RefreshAclMore;

  public
    { Public declarations }

  end;

var
  FAuthManageAcl: TFAuthManageAcl;

implementation

{$R *.dfm}

uses U.Crypt, U.Auth, DM_Main, U.Lookups, U.App.Log, F_Auth_CloneRole;

procedure TFAuthManageAcl.ACLShow(Sender: TObject);
begin
  if _IsShown then
  begin
    RefreshAcl;
//    RefreshAclByKategori;
  end;
end;

procedure TFAuthManageAcl.btnSyncAclByKategoriClick(Sender: TObject);
begin
  inherited;
//  SyncAclKat(spRoleid.AsInteger);
//  RefreshAclByKategori;
end;

procedure TFAuthManageAcl.btnSyncAllClick(Sender: TObject);
begin
  inherited;
  SyncAcl(spRoleid.AsInteger);
  RefreshAcl;
end;

procedure TFAuthManageAcl.CloneRoleClick(Sender: TObject);
var
  f: TFAuthCloneRole;
  is_sukses: boolean;
begin
  inherited;
  FAuthCloneRole := TFAuthCloneRole.Create(Self);
  FAuthCloneRole.IdRole := spRole.FieldByName('id').AsInteger;
  FAuthCloneRole.ShowModal;

  if FAuthCloneRole.ModalResult = mrOk then
    is_sukses := FAuthCloneRole.IsSukses;

  FAuthCloneRole.Free;

  if is_sukses then
    RefreshRole;
end;

procedure TFAuthManageAcl.btnPermAddChildClick(Sender: TObject);
var
  idx : integer;
begin
  inherited;
  idx := spPermissionsid.AsInteger;
  spPermissions.Append;
  spPermissions.FieldByName('id_parent').Value := idx;
  treePermissions.Edit;
end;

procedure TFAuthManageAcl.btnDeletePermClick(Sender: TObject);
begin
  inherited;
  if MessageDlg('Anda yakin ingin menghapus?', mtConfirmation, mbYesNo, 0) = mrYes then
    spPermissions.Delete;
end;

procedure TFAuthManageAcl.btnExportAclClick(Sender: TObject);
begin
  try
    cxExportTLToExcel('Exports/Permissions.xls', treePermissions, True, True, True, 'xls');

//    Alert('Export Sukses', 'Export berhasil.' + #13 +
//        'File excel tersimpan di Exports/Permission.xls', aicSukses);
  except
    MessageDlg('Export Failed', mtWarning, [mbOK], 0);
  end;
end;

procedure TFAuthManageAcl.btnNewChildPermClick(Sender: TObject);
begin
  inherited;
  _IdParentPerm := 0;
  if not spPermissions.IsEmpty then
    _IdParentPerm := spPermissionsid.AsInteger;
  spPermissions.Insert;
  spPermissionsid_parent.Value := _IdParentPerm;
  _IdParentPerm := 0;
end;

procedure TFAuthManageAcl.btnNewPermClick(Sender: TObject);
begin
  inherited;
  _IdParentPerm := 0;
  if not spPermissions.IsEmpty then
    _IdParentPerm := spPermissionsid_parent.AsInteger;
  spPermissions.Insert;
  spPermissionsid_parent.Value := _IdParentPerm;
  _IdParentPerm := 0;
end;

procedure TFAuthManageAcl.FormCreate(Sender: TObject);
begin
  inherited;
//  PopulateKaryawanLk;
  ///PopulateJenisPegawaiLk;

//  HandleOwnShortcut := True;
  grbScD.Visible := False;
  splBotD.Visible := False;

  ToInplaceGrid([gvUser,gvRoleMember,gvAclMore]);
  ToInplaceGrid([treeRole,treePermissions]);

//  dsJabatanLk := TDataSource.Create(nil);
//  dsJabatanLk.DataSet := GetJabatanLk;
//  treeRole.Columns[2].PropertiesClass := TcxLookupComboBoxProperties;
//  with TcxLookupComboBoxProperties(treeRole.Columns[2].Properties) do
//  begin
//    BeginUpdate;
//    ListOptions.ShowHeader := False;
//    ListSource := dsJabatanLk;
//    ListFieldNames := 'nama';
//    KeyFieldNames := 'id';
//    EndUpdate;
//  end;

//  dsUserLk := TDataSource.Create(nil);
//  dsUserLk.DataSet := GetUserLk;
//  gvRoleMember.Columns[0].PropertiesClass := TcxLookupComboBoxProperties;
//  with TcxLookupComboBoxProperties(gvRoleMember.Columns[0].Properties) do
//  begin
//    BeginUpdate;
//    ListOptions.ShowHeader := False;
//    ListSource := dsUserLk;
//    ListFieldNames := 'username';
//    KeyFieldNames := 'id';
//    EndUpdate;
//  end;

  gvUseractions.ApplyBestFit;
end;

procedure TFAuthManageAcl.FormShow(Sender: TObject);
begin
//  inherited;
//  pnlButtons.Visible := False;

  RefreshUser;
  RefreshRole;
  RefreshPermissions;
  RefreshAcl;

  pgRoleDetail.ActivePageIndex := 0;
  pgAcl.ActivePageIndex := 0;

  _IsShown := True;
end;

procedure TFAuthManageAcl.gvUserresetpassPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  inherited;
  if AButtonIndex = 0 then
    U.Auth.ResetPassword(spUserusername.AsString);
end;

procedure TFAuthManageAcl.RefreshAcl;
begin
  _AclShown := False;
  with spAcl do
  begin
    Close;
    Parameters[1].Value  := spRoleid.AsInteger;
    Open;
  end;

  if not spPermissions.IsEmpty then
  begin
    RefreshAclMore;
//    btnSyncAcl.Enabled := CheckAclCount(spRoleid.AsInteger);
    treeAcl.FullExpand;
  end;
  _AclShown := True;
end;

procedure TFAuthManageAcl.RefreshAclMore;
begin
  with spAclMore do
  begin
    Close;
    Parameters[1].Value  := spAclid_role.AsInteger;
    Parameters[2].Value  := spAclid_permission.AsInteger;
    Open;
  end;
end;

//procedure TFAuthManageAcl.RefreshAclByKategori;
//begin
//  with spAclByKategori do
//  begin
//    Close;
//    Parameters[1].Value  := spRoleid.AsInteger;
//    Open;
//  end;
//
//  btnSyncAclByKategori.Enabled := CheckAclKatCount(spRoleid.AsInteger);
//
//  treeAclKat.FullExpand;
//
//end;

procedure TFAuthManageAcl.RefreshPermissions;
begin
  with spPermissions do
  begin
    Close;
    Open;
  end;
  treePermissions.FullExpand;
  btnPermAddChild.Enabled := (not spPermissions.IsEmpty);
end;

procedure TFAuthManageAcl.RefreshRole;
begin
  _RoleShown := False;
  with spRole do
  begin
    Close;
    Open;
  end;

  if not spRole.IsEmpty then
    RefreshRoleMember;
  _RoleShown := True;
end;

procedure TFAuthManageAcl.RefreshRoleMember;
begin
  with spRoleMember do
  begin
    Close;
    Parameters[1].Value  := spRoleid.AsInteger;
    Open;
  end;
end;

procedure TFAuthManageAcl.RefreshUser;
begin
  with spUser do
  begin
    Close;
    Open;
  end;

  gvUseris_su.Options.Editing := Session.FIsSu;
end;

procedure TFAuthManageAcl.ResetPassword1Click(Sender: TObject);
var
  q : TAdoQuery;
  con : TADOConnection;
  pass, username : string;
begin
  DMMain.CreateConnection(con);
  q := TADOQuery.Create(nil);
  q.Connection := con;

  pass := Encrypt('default');
  username := spUserusername.AsString;

  q.SQL.Add('update auth_user set password = '+ QuotedStr(pass));
  q.SQL.Add(' where username = ' + QuotedStr(username));

  try
    try
      con.Open;
      q.ExecSQL;

      ShowMessage('Password berhasil diubah');
    except on E: Exception do
      ShowMessage(E.Message);
    end;
  finally
    q.Free;
    con.Free;
  end;
end;

procedure TFAuthManageAcl.spAclAfterScroll(DataSet: TDataSet);
begin
  inherited;
//  if _AclShown then
//    RefreshAclMore;
end;

procedure TFAuthManageAcl.spAclBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  Abort;
end;

procedure TFAuthManageAcl.spPermissionsAfterDelete(DataSet: TDataSet);
begin
  inherited;
  SyncAcls;
end;

procedure TFAuthManageAcl.spPermissionsAfterPost(DataSet: TDataSet);
begin
  inherited;
  SyncAcls;
end;

procedure TFAuthManageAcl.spRoleAfterDelete(DataSet: TDataSet);
begin
  inherited;
  CleanUpAcl;
end;

procedure TFAuthManageAcl.spRoleAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if _RoleShown then
  begin
    RefreshAcl;
    RefreshRoleMember;
  end;
end;

procedure TFAuthManageAcl.spRoleMemberBeforePost(DataSet: TDataSet);
begin
  inherited;
  //Prevent multi role
  //TODO: Next, you should be able to configure this. Be alarmed for the side effect :-D
  if UserHasRole(gvRoleMember.DataController.DataSet.FieldByName('id_user').AsInteger) then
  begin
    ShowUMessage('User sudah terdaftar dalam group');
    Abort;
  end;
  spRoleMemberid_role.Value := spRoleid.AsInteger;
end;

procedure TFAuthManageAcl.spUserAfterDelete(DataSet: TDataSet);
begin
  inherited;
  CleanUpAcl;
  RefreshRoleMember;
end;

procedure TFAuthManageAcl.spUserAfterPost(DataSet: TDataSet);
begin
  inherited;
  Log('Security','Menambah user baru [user:'+DataSet.FieldByName('username').AsString+']');
end;

procedure TFAuthManageAcl.spUserBeforeDelete(DataSet: TDataSet);
begin
  if spUseris_su.AsBoolean then
  begin
    MessageDlg('Maaf, Super User tidak bisa dihapus', mtError, [mbOK], 0);
    Abort;
  end;
end;

procedure TFAuthManageAcl.spUserBeforePost(DataSet: TDataSet);
begin
  inherited;
  if spUser.State = dsInsert then
    spUserpassword.Value := Encrypt('default');
end;

procedure TFAuthManageAcl.treeAclFocusedNodeChanged(Sender: TcxCustomTreeList;
  APrevFocusedNode, AFocusedNode: TcxTreeListNode);
begin
  inherited;
  RefreshAclMore;
end;

procedure TFAuthManageAcl.treePermissionsEditValueChanged(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn);
begin
  inherited;
  if AColumn.ItemIndex = 2 then
    spPermissions.Post;
end;

procedure TFAuthManageAcl.tsRoleMemberShow(Sender: TObject);
begin
  inherited;
  if not spRole.IsEmpty then
    RefreshRoleMember
  else
    spRoleMember.Close;
end;

end.
