object FAuthChpass: TFAuthChpass
  Left = 0
  Top = 0
  Caption = 'Change Password'
  ClientHeight = 145
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object grb1: TcxGroupBox
    Left = 0
    Top = 42
    Align = alClient
    PanelStyle.Active = True
    ParentBackground = False
    ParentColor = False
    Style.BorderStyle = ebsNone
    Style.Color = clWhite
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Height = 103
    Width = 418
    object lbl1: TcxLabel
      Left = 16
      Top = 16
      Caption = 'Password Lama'
      Transparent = True
    end
    object lbl2: TcxLabel
      Left = 16
      Top = 39
      Caption = 'Pasword Baru'
      Transparent = True
    end
    object edtOldpass: TcxTextEdit
      Left = 136
      Top = 15
      Properties.EchoMode = eemPassword
      Style.LookAndFeel.SkinName = 'Office2007Blue'
      StyleDisabled.LookAndFeel.SkinName = 'Office2007Blue'
      StyleFocused.LookAndFeel.SkinName = 'Office2007Blue'
      StyleHot.LookAndFeel.SkinName = 'Office2007Blue'
      TabOrder = 2
      OnExit = edtOldpassExit
      OnKeyDown = edtOldpassKeyDown
      Width = 257
    end
    object edtNewPass: TcxTextEdit
      Left = 136
      Top = 38
      Enabled = False
      Properties.EchoMode = eemPassword
      Style.LookAndFeel.SkinName = 'Office2010Blue'
      StyleDisabled.LookAndFeel.SkinName = 'Office2010Blue'
      StyleFocused.LookAndFeel.SkinName = 'Office2010Blue'
      StyleHot.LookAndFeel.SkinName = 'Office2010Blue'
      TabOrder = 3
      Width = 257
    end
    object edtNewPassAgain: TcxTextEdit
      Left = 136
      Top = 61
      Enabled = False
      Properties.EchoMode = eemPassword
      Style.LookAndFeel.SkinName = 'Office2010Blue'
      StyleDisabled.LookAndFeel.SkinName = 'Office2010Blue'
      StyleFocused.LookAndFeel.SkinName = 'Office2010Blue'
      StyleHot.LookAndFeel.SkinName = 'Office2010Blue'
      TabOrder = 4
      Width = 257
    end
    object lbl3: TcxLabel
      Left = 16
      Top = 62
      Caption = 'Pasword Baru (ulangi)'
      Transparent = True
    end
  end
  object grbModif: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.Kind = lfUltraFlat
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.Kind = lfUltraFlat
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.Kind = lfUltraFlat
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.Kind = lfUltraFlat
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 1
    Transparent = True
    Height = 42
    Width = 418
    object btnSave: TcxButton
      AlignWithMargins = True
      Left = 304
      Top = 5
      Width = 109
      Height = 32
      Align = alRight
      Caption = 'Ganti Password'
      OptionsImage.ImageIndex = 12
      OptionsImage.Images = DMMain.il16
      TabOrder = 0
      OnClick = btnSaveClick
    end
  end
end
