object FAuthManageAcl: TFAuthManageAcl
  Left = 0
  Top = 0
  Caption = 'User Management'
  ClientHeight = 623
  ClientWidth = 1026
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 317
    Top = 126
    Width = 709
    Height = 497
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 5
    object cxSplitter2: TcxSplitter
      Left = 281
      Top = 1
      Width = 4
      Height = 495
      Control = Panel4
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 280
      Height = 495
      Align = alLeft
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 0
      object pgRoleDetail: TcxPageControl
        Left = 0
        Top = 221
        Width = 280
        Height = 274
        Margins.Left = 0
        Margins.Right = 0
        Align = alBottom
        TabOrder = 2
        Properties.ActivePage = tsRoleMember
        Properties.CustomButtons.Buttons = <>
        ClientRectBottom = 270
        ClientRectLeft = 4
        ClientRectRight = 276
        ClientRectTop = 24
        object tsRoleMember: TcxTabSheet
          Caption = 'Role Member'
          ImageIndex = 1
          OnShow = tsRoleMemberShow
          object cxGrid4: TcxGrid
            AlignWithMargins = True
            Left = 3
            Top = 35
            Width = 266
            Height = 208
            Align = alClient
            TabOrder = 1
            LookAndFeel.NativeStyle = False
            LookAndFeel.SkinName = 'Office2010Blue'
            object gvRoleMember: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dsRoleMember
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object gvRoleMemberid_user: TcxGridDBColumn
                Caption = 'Username'
                DataBinding.FieldName = 'id_user'
                PropertiesClassName = 'TcxLookupComboBoxProperties'
                Properties.KeyFieldNames = 'id'
                Properties.ListColumns = <
                  item
                    FieldName = 'username'
                  end>
                Properties.ListOptions.ShowHeader = False
                Properties.ListSource = dsUser
                HeaderAlignmentHorz = taCenter
                Width = 263
              end
            end
            object cxGridLevel3: TcxGridLevel
              GridView = gvRoleMember
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 272
            Height = 32
            Align = alTop
            BevelOuter = bvNone
            Color = clSkyBlue
            Padding.Right = 3
            Padding.Bottom = 3
            ParentBackground = False
            TabOrder = 0
            object nav2: TcxDBNavigator
              Left = 157
              Top = 0
              Width = 112
              Height = 30
              BorderStyle = nbsNone
              Buttons.CustomButtons = <>
              Buttons.Images = DMMain.il24
              Buttons.First.Visible = False
              Buttons.PriorPage.Visible = False
              Buttons.Prior.Visible = False
              Buttons.Next.Visible = False
              Buttons.NextPage.Visible = False
              Buttons.Last.Visible = False
              Buttons.Insert.Hint = 'Tambah Data'
              Buttons.Insert.ImageIndex = 1
              Buttons.Append.Visible = False
              Buttons.Delete.Hint = 'Hapus'
              Buttons.Delete.ImageIndex = 3
              Buttons.Edit.Hint = 'Edit'
              Buttons.Edit.ImageIndex = 2
              Buttons.Edit.Visible = False
              Buttons.Post.Hint = 'Simpan'
              Buttons.Post.ImageIndex = 8
              Buttons.Cancel.Hint = 'Batalkan'
              Buttons.Cancel.ImageIndex = 10
              Buttons.Refresh.Hint = 'Segarkan'
              Buttons.Refresh.ImageIndex = 11
              Buttons.Refresh.Visible = False
              Buttons.SaveBookmark.Visible = False
              Buttons.GotoBookmark.Visible = False
              Buttons.Filter.Visible = False
              DataSource = dsRoleMember
              LookAndFeel.Kind = lfUltraFlat
              LookAndFeel.NativeStyle = False
              LookAndFeel.SkinName = 'Office2010Blue'
              Align = alRight
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 0
            end
          end
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 280
        Height = 32
        Align = alTop
        BevelOuter = bvNone
        Color = clSkyBlue
        Padding.Right = 3
        Padding.Bottom = 3
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          Left = 3
          Top = 9
          Width = 28
          Height = 16
          Caption = 'Role'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object navData: TcxDBNavigator
          Left = 165
          Top = 0
          Width = 112
          Height = 30
          BorderStyle = nbsNone
          Buttons.CustomButtons = <>
          Buttons.Images = DMMain.il24
          Buttons.First.Visible = False
          Buttons.PriorPage.Visible = False
          Buttons.Prior.Visible = False
          Buttons.Next.Visible = False
          Buttons.NextPage.Visible = False
          Buttons.Last.Visible = False
          Buttons.Insert.Hint = 'Tambah Data'
          Buttons.Insert.ImageIndex = 1
          Buttons.Append.Visible = False
          Buttons.Delete.Hint = 'Hapus'
          Buttons.Delete.ImageIndex = 3
          Buttons.Edit.Hint = 'Edit'
          Buttons.Edit.ImageIndex = 2
          Buttons.Edit.Visible = False
          Buttons.Post.Hint = 'Simpan'
          Buttons.Post.ImageIndex = 8
          Buttons.Cancel.Hint = 'Batalkan'
          Buttons.Cancel.ImageIndex = 10
          Buttons.Refresh.Hint = 'Segarkan'
          Buttons.Refresh.ImageIndex = 11
          Buttons.Refresh.Visible = False
          Buttons.SaveBookmark.Visible = False
          Buttons.GotoBookmark.Visible = False
          Buttons.Filter.Visible = False
          DataSource = dsRole
          LookAndFeel.Kind = lfUltraFlat
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'Office2010Blue'
          Align = alRight
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 0
        end
      end
      object treeRole: TcxDBTreeList
        AlignWithMargins = True
        Left = 0
        Top = 35
        Width = 280
        Height = 183
        Margins.Left = 0
        Margins.Right = 0
        Align = alClient
        Bands = <
          item
          end>
        DataController.DataSource = dsRole
        DataController.ParentField = 'id_parent'
        DataController.KeyField = 'id'
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = 'Office2010Blue'
        Navigator.Buttons.CustomButtons = <>
        PopupMenu = pmRole
        RootValue = -1
        TabOrder = 1
        object cxdbtrlstclmntree1kode: TcxDBTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = 'Kode'
          DataBinding.FieldName = 'kode'
          Width = 69
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxdbtrlstclmntree1nama: TcxDBTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = 'Nama'
          DataBinding.FieldName = 'nama'
          Width = 152
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxdbtrlstclmntree1id_jabatan: TcxDBTreeListColumn
          Visible = False
          Caption.AlignHorz = taCenter
          Caption.Text = 'Jabatan'
          DataBinding.FieldName = 'id_jabatan'
          Width = 143
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxdbtrlstclmnRolecxDBTreeListunit: TcxDBTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = 'Unit'
          DataBinding.FieldName = 'unit'
          Width = 71
          Position.ColIndex = 3
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
    end
    object Panel7: TPanel
      Left = 285
      Top = 1
      Width = 423
      Height = 495
      Align = alClient
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 2
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 423
        Height = 32
        Align = alTop
        BevelOuter = bvNone
        Color = clSkyBlue
        Padding.Right = 3
        Padding.Bottom = 3
        ParentBackground = False
        TabOrder = 0
        object Label3: TLabel
          Left = 3
          Top = 9
          Width = 97
          Height = 16
          Caption = 'Access Control'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object pgAcl: TcxPageControl
        Left = 0
        Top = 32
        Width = 423
        Height = 463
        Align = alClient
        TabOrder = 1
        Properties.ActivePage = cxTabSheet2
        Properties.CustomButtons.Buttons = <>
        LookAndFeel.SkinName = 'Office2010Blue'
        ClientRectBottom = 459
        ClientRectLeft = 4
        ClientRectRight = 419
        ClientRectTop = 24
        object ACL: TcxTabSheet
          Caption = 'ACL'
          ImageIndex = 0
          OnShow = ACLShow
          object treeAcl: TcxDBTreeList
            AlignWithMargins = True
            Left = 3
            Top = 35
            Width = 409
            Height = 218
            Align = alClient
            Bands = <
              item
              end>
            DataController.DataSource = dsAcl
            DataController.ParentField = 'id_parent'
            DataController.KeyField = 'id'
            LookAndFeel.NativeStyle = False
            LookAndFeel.SkinName = 'Office2010Blue'
            Navigator.Buttons.CustomButtons = <>
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.GoToNextCellOnTab = True
            OptionsBehavior.FocusCellOnCycle = True
            RootValue = -1
            TabOrder = 1
            OnFocusedNodeChanged = treeAclFocusedNodeChanged
            object treeAclkode: TcxDBTreeListColumn
              Caption.AlignHorz = taCenter
              Caption.Text = 'Kode'
              DataBinding.FieldName = 'kode'
              Options.Editing = False
              Width = 166
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAclnama: TcxDBTreeListColumn
              Caption.AlignHorz = taCenter
              Caption.Text = 'Nama'
              DataBinding.FieldName = 'nama'
              Options.Editing = False
              Width = 154
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAclr: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Caption.AlignHorz = taCenter
              Caption.Text = 'R'
              DataBinding.FieldName = 'r'
              Width = 25
              Position.ColIndex = 2
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAclc: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Caption.AlignHorz = taCenter
              Caption.Text = 'C'
              DataBinding.FieldName = 'c'
              Width = 25
              Position.ColIndex = 3
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAclu: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Caption.AlignHorz = taCenter
              Caption.Text = 'U'
              DataBinding.FieldName = 'u'
              Width = 25
              Position.ColIndex = 4
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAcld: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Caption.AlignHorz = taCenter
              Caption.Text = 'D'
              DataBinding.FieldName = 'd'
              Width = 25
              Position.ColIndex = 5
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAclp: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Caption.AlignHorz = taCenter
              Caption.Text = 'P'
              DataBinding.FieldName = 'p'
              Width = 25
              Position.ColIndex = 6
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAcle: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Caption.AlignHorz = taCenter
              Caption.Text = 'E'
              DataBinding.FieldName = 'e'
              Width = 25
              Position.ColIndex = 7
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treeAcldr: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Caption.AlignHorz = taCenter
              Caption.Text = 'DR'
              DataBinding.FieldName = 'dr'
              Width = 25
              Position.ColIndex = 8
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 415
            Height = 32
            Align = alTop
            BevelOuter = bvNone
            Color = clSkyBlue
            Padding.Right = 3
            Padding.Bottom = 3
            ParentBackground = False
            TabOrder = 0
            object Label5: TLabel
              Left = 3
              Top = 9
              Width = 84
              Height = 16
              Caption = 'ACL by Menu'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clTeal
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
            object btnSyncAll: TcxButton
              Left = 337
              Top = 0
              Width = 75
              Height = 29
              Align = alRight
              Caption = 'Sync All'
              OptionsImage.ImageIndex = 2
              OptionsImage.Images = DMMain.il16
              TabOrder = 0
              OnClick = btnSyncAllClick
            end
          end
          object gv1: TcxGrid
            AlignWithMargins = True
            Left = 0
            Top = 259
            Width = 412
            Height = 173
            Margins.Left = 0
            Align = alBottom
            TabOrder = 2
            LookAndFeel.NativeStyle = False
            LookAndFeel.SkinName = 'Office2010Blue'
            object gvAclMore: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dsAclMore
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object gvAclMorekode: TcxGridDBColumn
                AlternateCaption = '8`'
                Caption = 'Kode'
                DataBinding.FieldName = 'kode'
                HeaderAlignmentHorz = taCenter
                Width = 80
              end
              object gvAclMorenama: TcxGridDBColumn
                Caption = 'Nama'
                DataBinding.FieldName = 'nama'
                HeaderAlignmentHorz = taCenter
                Width = 223
              end
              object gvAclMoregrant: TcxGridDBColumn
                Caption = 'Grant'
                DataBinding.FieldName = 'grant'
                PropertiesClassName = 'TcxCheckBoxProperties'
                Properties.ImmediatePost = True
                HeaderAlignmentHorz = taCenter
                Width = 56
              end
            end
            object gv1Level1: TcxGridLevel
              GridView = gvAclMore
            end
          end
        end
        object cxTabSheet2: TcxTabSheet
          Caption = 'Permissions'
          ImageIndex = 1
          object treePermissions: TcxDBTreeList
            AlignWithMargins = True
            Left = 3
            Top = 36
            Width = 409
            Height = 396
            Align = alClient
            Bands = <
              item
              end>
            DataController.DataSource = dsPermissions
            DataController.ParentField = 'id_parent'
            DataController.KeyField = 'id'
            LookAndFeel.NativeStyle = False
            LookAndFeel.SkinName = 'Office2010Blue'
            Navigator.Buttons.CustomButtons = <>
            OptionsData.Inserting = True
            RootValue = -1
            TabOrder = 1
            OnEditValueChanged = treePermissionsEditValueChanged
            object treePermissionskode: TcxDBTreeListColumn
              Caption.AlignHorz = taCenter
              Caption.Text = 'Kode'
              DataBinding.FieldName = 'kode'
              Width = 167
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treePermissionsnama: TcxDBTreeListColumn
              Caption.AlignHorz = taCenter
              Caption.Text = 'Nama'
              DataBinding.FieldName = 'nama'
              Width = 161
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treePermissionsis_crud: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.NullStyle = nssUnchecked
              Caption.AlignHorz = taCenter
              Caption.Text = 'CRUD'
              DataBinding.FieldName = 'is_crud'
              Width = 48
              Position.ColIndex = 2
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treePermissionscAction: TcxDBTreeListColumn
              Caption.AlignHorz = taCenter
              Caption.Text = 'Class Name'
              DataBinding.FieldName = 'class_name'
              Width = 250
              Position.ColIndex = 3
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object treePermissionsId: TcxDBTreeListColumn
              Visible = False
              DataBinding.FieldName = 'id'
              Position.ColIndex = 4
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 415
            Height = 33
            Align = alTop
            BevelOuter = bvNone
            Color = clSkyBlue
            Padding.Right = 3
            ParentBackground = False
            TabOrder = 0
            object btnPrintAcl: TcxButton
              AlignWithMargins = True
              Left = 1
              Top = 1
              Width = 33
              Height = 31
              Hint = 'Print'
              Margins.Left = 1
              Margins.Top = 1
              Margins.Right = 1
              Margins.Bottom = 1
              Align = alLeft
              Caption = 'Print'
              OptionsImage.ImageIndex = 5
              OptionsImage.Images = DMMain.il24
              PaintStyle = bpsGlyph
              TabOrder = 1
              ExplicitHeight = 28
            end
            object btnExportAcl: TcxButton
              AlignWithMargins = True
              Left = 36
              Top = 1
              Width = 33
              Height = 31
              Hint = 'Export'
              Margins.Left = 1
              Margins.Top = 1
              Margins.Right = 1
              Margins.Bottom = 1
              Align = alLeft
              Caption = 'Export'
              OptionsImage.ImageIndex = 6
              OptionsImage.Images = DMMain.il24
              PaintStyle = bpsGlyph
              TabOrder = 2
              OnClick = btnExportAclClick
              ExplicitHeight = 28
            end
            object nav3: TcxDBNavigator
              AlignWithMargins = True
              Left = 269
              Top = 0
              Width = 140
              Height = 32
              Margins.Top = 0
              BorderStyle = nbsNone
              Buttons.CustomButtons = <>
              Buttons.Images = DMMain.il24
              Buttons.First.Visible = False
              Buttons.PriorPage.Visible = False
              Buttons.Prior.Visible = False
              Buttons.Next.Visible = False
              Buttons.NextPage.Visible = False
              Buttons.Last.Visible = False
              Buttons.Insert.Hint = 'Tambah Data'
              Buttons.Insert.ImageIndex = 1
              Buttons.Append.ImageIndex = 4
              Buttons.Append.Visible = False
              Buttons.Delete.Hint = 'Hapus'
              Buttons.Delete.ImageIndex = 3
              Buttons.Edit.Hint = 'Edit'
              Buttons.Edit.ImageIndex = 2
              Buttons.Edit.Visible = False
              Buttons.Post.Hint = 'Simpan'
              Buttons.Post.ImageIndex = 8
              Buttons.Cancel.Hint = 'Batalkan'
              Buttons.Cancel.ImageIndex = 10
              Buttons.Refresh.Hint = 'Segarkan'
              Buttons.Refresh.ImageIndex = 11
              Buttons.Refresh.Visible = False
              Buttons.SaveBookmark.Visible = False
              Buttons.GotoBookmark.Visible = False
              Buttons.Filter.Visible = False
              DataSource = dsPermissions
              LookAndFeel.Kind = lfUltraFlat
              LookAndFeel.NativeStyle = True
              LookAndFeel.SkinName = 'Office2010Blue'
              Align = alRight
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 0
            end
            object btnPermAddChild: TcxButton
              Left = 225
              Top = 0
              Width = 41
              Height = 33
              Margins.Top = 0
              Margins.Bottom = 0
              Align = alRight
              Enabled = False
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = True
              OptionsImage.ImageIndex = 4
              OptionsImage.Images = DMMain.il24
              PaintStyle = bpsGlyph
              TabOrder = 3
              OnClick = btnPermAddChildClick
              ExplicitLeft = 230
            end
          end
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 313
    Top = 126
    Width = 4
    Height = 497
    Control = Panel1
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 313
    Height = 497
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object cxGrid2: TcxGrid
      AlignWithMargins = True
      Left = 3
      Top = 35
      Width = 310
      Height = 184
      Margins.Right = 0
      Align = alClient
      TabOrder = 1
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2010Blue'
      object gvUser: TcxGridDBTableView
        PopupMenu = pmUser
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsUser
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object gvUserusername: TcxGridDBColumn
          Caption = 'Usename'
          DataBinding.FieldName = 'username'
          HeaderAlignmentHorz = taCenter
          Width = 87
        end
        object gvUserdisplay_name: TcxGridDBColumn
          Caption = 'Name'
          DataBinding.FieldName = 'display_name'
          HeaderAlignmentHorz = taCenter
          Width = 164
        end
        object gvUseris_su: TcxGridDBColumn
          Caption = 'SU'
          DataBinding.FieldName = 'is_su'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          Width = 36
        end
        object gvUseractions: TcxGridDBColumn
          Caption = 'Actions'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Caption = 'Reset Password'
              Default = True
              Kind = bkText
            end>
          Properties.OnButtonClick = gvUserresetpassPropertiesButtonClick
          HeaderAlignmentHorz = taCenter
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = gvUser
      end
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      AlignWithMargins = True
      Left = 3
      Top = 242
      Width = 310
      Height = 252
      Margins.Right = 0
      Align = alBottom
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2010Blue'
      OptionsView.RowHeaderWidth = 141
      Navigator.Buttons.CustomButtons = <>
      TabOrder = 3
      DataController.DataSource = dsUser
      Version = 1
      object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
        Properties.Caption = 'Last IP'
        Properties.DataBinding.FieldName = 'last_ip'
        Properties.Options.Editing = False
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
        Properties.Caption = 'Last Login'
        Properties.DataBinding.FieldName = 'last_login'
        Properties.Options.Editing = False
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow
        Properties.Caption = 'Last Logout'
        Properties.DataBinding.FieldName = 'last_logout'
        Properties.Options.Editing = False
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow
        Properties.Caption = 'Last Password Change'
        Properties.DataBinding.FieldName = 'last_password_change'
        Properties.Options.Editing = False
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow
        Properties.Caption = 'Recovery Email'
        Properties.DataBinding.FieldName = 'recovery_email'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow
        Properties.Caption = 'Recovery Phone'
        Properties.DataBinding.FieldName = 'recovery_phone'
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow8: TcxDBEditorRow
        Properties.Caption = 'Security Question'
        Properties.DataBinding.FieldName = 'security_question'
        Properties.Options.Editing = False
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow9: TcxDBEditorRow
        Properties.Caption = 'Security Answer'
        Properties.DataBinding.FieldName = 'security_answer'
        Properties.Options.Editing = False
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow10: TcxDBEditorRow
        Properties.Caption = 'Suspended'
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.DataBinding.FieldName = 'is_suspended'
        ID = 8
        ParentID = -1
        Index = 8
        Version = 1
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 313
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Color = clSkyBlue
      Padding.Right = 3
      Padding.Bottom = 3
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        Left = 3
        Top = 9
        Width = 29
        Height = 16
        Caption = 'User'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clTeal
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object nav1: TcxDBNavigator
        Left = 198
        Top = 0
        Width = 112
        Height = 30
        BorderStyle = nbsNone
        Buttons.CustomButtons = <>
        Buttons.Images = DMMain.il24
        Buttons.First.Visible = False
        Buttons.PriorPage.Visible = False
        Buttons.Prior.Visible = False
        Buttons.Next.Visible = False
        Buttons.NextPage.Visible = False
        Buttons.Last.Visible = False
        Buttons.Insert.Hint = 'Tambah Data'
        Buttons.Insert.ImageIndex = 1
        Buttons.Append.Visible = False
        Buttons.Delete.Hint = 'Hapus'
        Buttons.Delete.ImageIndex = 3
        Buttons.Edit.Hint = 'Edit'
        Buttons.Edit.ImageIndex = 2
        Buttons.Edit.Visible = False
        Buttons.Post.Hint = 'Simpan'
        Buttons.Post.ImageIndex = 8
        Buttons.Cancel.Hint = 'Batalkan'
        Buttons.Cancel.ImageIndex = 10
        Buttons.Refresh.Hint = 'Segarkan'
        Buttons.Refresh.ImageIndex = 11
        Buttons.Refresh.Visible = False
        Buttons.SaveBookmark.Visible = False
        Buttons.GotoBookmark.Visible = False
        Buttons.Filter.Visible = False
        DataSource = dsUser
        LookAndFeel.Kind = lfUltraFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = 'Office2010Blue'
        Align = alRight
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 0
      end
    end
    object cxLabel1: TcxLabel
      Left = 0
      Top = 222
      Align = alBottom
      Caption = 'Detail'
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1026
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = 'User Management'
      Groups = <>
      Index = 0
    end
  end
  object spUser: TADOStoredProc
    Connection = DMMain.Connection
    CursorType = ctStatic
    BeforePost = spUserBeforePost
    AfterPost = spUserAfterPost
    BeforeDelete = spUserBeforeDelete
    AfterDelete = spUserAfterDelete
    ProcedureName = 'usp_auth_user_data;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 136
    Top = 96
    object spUserid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object spUserusername: TStringField
      FieldName = 'username'
      Size = 50
    end
    object spUserpassword: TStringField
      FieldName = 'password'
      Size = 50
    end
    object spUserdisplay_name: TStringField
      FieldName = 'display_name'
      Size = 100
    end
    object spUseris_su: TBooleanField
      FieldName = 'is_su'
    end
    object spUseris_suspended: TBooleanField
      FieldName = 'is_suspended'
    end
    object spUsersecurity_question: TStringField
      FieldName = 'security_question'
      Size = 255
    end
    object spUsersecurity_answer: TStringField
      FieldName = 'security_answer'
      Size = 50
    end
    object spUserrecovery_email: TStringField
      FieldName = 'recovery_email'
      Size = 50
    end
    object spUserrecovery_phone: TStringField
      FieldName = 'recovery_phone'
      Size = 50
    end
    object spUserlast_ip: TStringField
      FieldName = 'last_ip'
      Size = 50
    end
    object spUserlast_login: TDateTimeField
      FieldName = 'last_login'
    end
    object spUserlast_password_change: TDateTimeField
      FieldName = 'last_password_change'
    end
    object spUserlast_logout: TDateTimeField
      FieldName = 'last_logout'
    end
    object spUserid_pegawai: TIntegerField
      FieldName = 'id_pegawai'
    end
  end
  object dsUser: TDataSource
    DataSet = spUser
    Left = 192
    Top = 96
  end
  object spRole: TADOStoredProc
    Connection = DMMain.Connection
    CursorType = ctStatic
    AfterDelete = spRoleAfterDelete
    AfterScroll = spRoleAfterScroll
    ProcedureName = 'usp_auth_role;1'
    Parameters = <>
    Left = 400
    Top = 168
    object spRoleid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object spRoleid_parent: TIntegerField
      FieldName = 'id_parent'
    end
    object spRolekode: TStringField
      FieldName = 'kode'
      Size = 50
    end
    object spRolenama: TStringField
      FieldName = 'nama'
      Size = 50
    end
    object spRoleid_jabatan: TIntegerField
      FieldName = 'id_jabatan'
    end
    object spRoledeskripsi: TStringField
      FieldName = 'deskripsi'
      Size = 100
    end
    object spRoleunit: TStringField
      FieldName = 'unit'
      Size = 50
    end
  end
  object dsRole: TDataSource
    DataSet = spRole
    Left = 416
    Top = 176
  end
  object spRoleMember: TADOStoredProc
    Connection = DMMain.Connection
    CursorType = ctStatic
    BeforePost = spRoleMemberBeforePost
    ProcedureName = 'usp_auth_role_member_data;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@id_role'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 456
    object spRoleMemberid_role: TIntegerField
      FieldName = 'id_role'
    end
    object spRoleMemberid_user: TIntegerField
      FieldName = 'id_user'
    end
  end
  object dsRoleMember: TDataSource
    DataSet = spRoleMember
    Left = 296
    Top = 464
  end
  object spAcl: TADOStoredProc
    Connection = DMMain.Connection
    CursorType = ctStatic
    BeforeDelete = spAclBeforeDelete
    AfterScroll = spAclAfterScroll
    ProcedureName = 'usp_auth_acl_data;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@id_role'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 640
    Top = 296
    object spAclid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object spAclid_parent: TIntegerField
      FieldName = 'id_parent'
    end
    object spAclkode: TStringField
      FieldName = 'kode'
      Size = 50
    end
    object spAclnama: TStringField
      FieldName = 'nama'
      Size = 100
    end
    object spAclis_crud: TBooleanField
      FieldName = 'is_crud'
    end
    object spAclid_acl: TAutoIncField
      FieldName = 'id_acl'
      ReadOnly = True
    end
    object spAclid_role: TIntegerField
      FieldName = 'id_role'
    end
    object spAclid_permission: TIntegerField
      FieldName = 'id_permission'
    end
    object spAclr: TBooleanField
      FieldName = 'r'
    end
    object spAclc: TBooleanField
      FieldName = 'c'
    end
    object spAclu: TBooleanField
      FieldName = 'u'
    end
    object spAcld: TBooleanField
      FieldName = 'd'
    end
    object spAclp: TBooleanField
      FieldName = 'p'
    end
    object spAcle: TBooleanField
      FieldName = 'e'
    end
    object spAcldr: TBooleanField
      FieldName = 'dr'
    end
  end
  object dsAcl: TDataSource
    DataSet = spAcl
    Left = 672
    Top = 296
  end
  object spPermissions: TADOStoredProc
    Connection = DMMain.Connection
    CursorType = ctStatic
    AfterPost = spPermissionsAfterPost
    AfterDelete = spPermissionsAfterDelete
    ProcedureName = 'usp_auth_permissions_data;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 776
    Top = 120
    object spPermissionsid: TAutoIncField
      FieldName = 'id'
    end
    object spPermissionsid_parent: TIntegerField
      FieldName = 'id_parent'
    end
    object spPermissionskode: TStringField
      FieldName = 'kode'
      Size = 50
    end
    object spPermissionsnama: TStringField
      FieldName = 'nama'
      Size = 100
    end
    object spPermissionsis_crud: TBooleanField
      FieldName = 'is_crud'
    end
    object spPermissionsclass_name: TStringField
      FieldName = 'class_name'
      Size = 100
    end
  end
  object dsPermissions: TDataSource
    DataSet = spPermissions
    Left = 832
    Top = 112
  end
  object dsAclMore: TDataSource
    DataSet = spAclMore
    Left = 664
    Top = 544
  end
  object spAclMore: TADOStoredProc
    Connection = DMMain.Connection
    CursorType = ctStatic
    ProcedureName = 'usp_auth_acl_more_data;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@id_role'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@id_parent'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 656
    Top = 536
    object spAclMoreid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object spAclMoreid_parent: TIntegerField
      FieldName = 'id_parent'
    end
    object spAclMorekode: TStringField
      FieldName = 'kode'
      Size = 50
    end
    object spAclMorenama: TStringField
      FieldName = 'nama'
      Size = 100
    end
    object spAclMoreis_crud: TBooleanField
      FieldName = 'is_crud'
    end
    object spAclMoreid_1: TAutoIncField
      FieldName = 'id_1'
      ReadOnly = True
    end
    object spAclMoreid_role: TIntegerField
      FieldName = 'id_role'
    end
    object spAclMoreid_permission: TIntegerField
      FieldName = 'id_permission'
    end
    object spAclMoregrant: TBooleanField
      FieldName = 'grant'
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 416
    Top = 40
    DockControlHeights = (
      0
      0
      0
      0)
  end
  object pmUser: TPopupMenu
    Left = 80
    Top = 208
    object ResetPassword1: TMenuItem
      Caption = 'Reset Password'
      OnClick = ResetPassword1Click
    end
  end
  object pmRole: TPopupMenu
    Left = 448
    Top = 224
    object CloneRole: TMenuItem
      Caption = 'Clone Role'
      OnClick = CloneRoleClick
    end
  end
end
