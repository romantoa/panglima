unit F_Auth_CloneRole;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,  Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxMaskEdit, cxSpinEdit, cxTextEdit, cxLabel, F_Base,
  DB, ADODB;

type
  TFAuthCloneRole = class(TFBase)
    cxLabel1: TcxLabel;
    edtKode: TcxTextEdit;
    btnCari: TcxButton;
    cxLabel2: TcxLabel;
    edtNama: TcxTextEdit;
    cxLabel3: TcxLabel;
    edtUnit: TcxTextEdit;
    procedure btnCariClick(Sender: TObject);
  private
    { Private declarations }
    FIsSukses: boolean;
    FIdRole: integer;
  public
    { Public declarations }
    property IsSukses: boolean read FIsSukses write FIsSukses;
    property IdRole:integer read FIdRole write FIdRole;
  end;

var
  FAuthCloneRole: TFAuthCloneRole;

implementation

{$R *.dfm}

uses DM_Main;

procedure TFAuthCloneRole.btnCariClick(Sender: TObject);
var
  sp: TADOStoredProc;
  con: TAdoConnection;
begin
  inherited;

  DMMain.CreateConnection(con);
  sp := TADOStoredProc.Create(nil);
  sp.ProcedureName := 'usp_auth_clone_role';
  sp.Connection := con;
  try
    try
      sp.Parameters.CreateParameter('@id_role', ftInteger, pdInput, MaxInt, FIdRole);
      sp.Parameters.CreateParameter('@kode', ftString, pdInput, 50, edtKode.Text);
      sp.Parameters.CreateParameter('@nama', ftString, pdInput, 50, edtNama.Text);
      sp.Parameters.CreateParameter('@unit', ftString, pdInput, 50, edtUnit.Text);
      con.Open;
      sp.ExecProc;
      FIsSukses := True;
      Close;
      ModalResult := mrOk;
    except
      on E: Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    sp.Free;
    con.Free;
  end;
end;

end.
