object FAuthAclModif: TFAuthAclModif
  Left = 0
  Top = 0
  Caption = 'Access Control'
  ClientHeight = 150
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 482
    Height = 150
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object cxLabel1: TcxLabel
      Left = 16
      Top = 16
      Caption = 'Parent'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 16
      Top = 52
      Caption = 'Kode'
      ParentFont = False
    end
    object edtKode: TcxTextEdit
      Left = 120
      Top = 51
      ParentFont = False
      TabOrder = 4
      Width = 150
    end
    object cxLabel3: TcxLabel
      Left = 16
      Top = 75
      Caption = 'Nama'
      ParentFont = False
    end
    object edtNama: TcxTextEdit
      Left = 120
      Top = 74
      ParentFont = False
      TabOrder = 5
      Width = 329
    end
    object cmbParent: TcxLookupComboBox
      Left = 120
      Top = 15
      Properties.KeyFieldNames = 'id'
      Properties.ListColumns = <
        item
          FieldName = 'nama'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = DMLookups.dsPermissionLk
      EditValue = 0
      Style.LookAndFeel.SkinName = 'Office2010Blue'
      StyleDisabled.LookAndFeel.SkinName = 'Office2010Blue'
      StyleFocused.LookAndFeel.SkinName = 'Office2010Blue'
      StyleHot.LookAndFeel.SkinName = 'Office2010Blue'
      TabOrder = 3
      Width = 329
    end
  end
  object spModif: TADOStoredProc
    Connection = DMMain.Connection
    ProcedureName = 'usp_auth_permissions_modif;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@faction'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ermes'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 2000
        Value = Null
      end
      item
        Name = '@erno'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@username'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ip'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@id'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@id_parent'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@kode'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@nama'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@is_crud'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 232
    Top = 72
  end
end
