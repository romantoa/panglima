unit F_Auth_AclModif;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, F_ModifBase, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxLabel, Data.DB, Data.Win.ADODB;

type
  TFAuthAclModif = class(TFModifBase)
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    edtKode: TcxTextEdit;
    cxLabel3: TcxLabel;
    edtNama: TcxTextEdit;
    spModif: TADOStoredProc;
    cmbParent: TcxLookupComboBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ValidateInput: Boolean; override;
    procedure SetFields; override;
  end;

var
  FAuthAclModif: TFAuthAclModif;

implementation

{$R *.dfm}

uses DM_Main, DM_Lookups;

{ TForm1 }

procedure TFAuthAclModif.FormCreate(Sender: TObject);
begin
  inherited;
  PopulatePermissionLk;
end;

procedure TFAuthAclModif.SetFields;
begin
  with FSpModif do
  begin
    //if cmbParent.EditValue <> 0 then
    Parameters.ParamByName('@id_parent').Value := cmbParent.EditValue;
    Parameters.ParamByName('@kode').Value := edtKode.Text;
    Parameters.ParamByName('@nama').Value := edtNama.Text;
    Parameters.ParamByName('@is_crud').Value := True;
  end;
end;

function TFAuthAclModif.ValidateInput: Boolean;
begin
  if edtKode.Text = '' then
    MessageDlg('Kode harus diisi', mtWarning, [mbOK], 0)
  else if edtNama.Text = '' then
    MessageDlg('Nama harus diisi', mtWarning, [mbOK], 0)
  else
    Result := True;
end;

end.
