unit F_Auth_Chpass;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, 
  Vcl.ComCtrls, dxCore, cxDateUtils, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxSpinEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxLabel,
  cxGroupBox;

type
  TFAuthChpass = class(TForm)
    grb1: TcxGroupBox;
    lbl1: TcxLabel;
    lbl2: TcxLabel;
    grbModif: TcxGroupBox;
    btnSave: TcxButton;
    edtOldpass: TcxTextEdit;
    edtNewPass: TcxTextEdit;
    edtNewPassAgain: TcxTextEdit;
    lbl3: TcxLabel;
    procedure btnSaveClick(Sender: TObject);
    procedure edtOldpassExit(Sender: TObject);
    procedure edtOldpassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAuthChpass: TFAuthChpass;

implementation

{$R *.dfm}

uses U.Auth, DM_Main;

procedure TFAuthChpass.btnSaveClick(Sender: TObject);
begin
  if edtNewPass.Text <> edtNewPassAgain.Text then
    MessageDlg('Password tidak sama', mtWarning, [mbOK], 0)
  else if not UpdatePassword(Session.FUsername, edtNewPass.Text) then
    MessageDlg('Gagal mengubah password', mtWarning, [mbOK], 0)
  else
  begin
    MessageDlg('Password berhasil diubah. Silahkan login.', mtWarning, [mbOK], 0);
  end;
end;

procedure TFAuthChpass.edtOldpassExit(Sender: TObject);
begin
  if Login(Session.FUsername, edtOldpass.Text) then
  begin
    edtNewPass.Enabled := True;
    edtNewPassAgain.Enabled := True;
  end
  else
  begin
    edtNewPass.Enabled := False;
    edtNewPassAgain.Enabled := False;
    MessageDlg('Password lama salah', mtInformation, [mbOK], 0);
  end;
end;

procedure TFAuthChpass.edtOldpassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    edtOldpass.OnExit(edtOldpass);
end;

end.
