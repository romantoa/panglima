unit F_EmployeeEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxMemo, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox;

type
  TFEmployeeEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _number: TcxTextEdit;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    _gender_id: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    _street: TcxMemo;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    _phone: TcxTextEdit;
    _email: TcxTextEdit;
    _subdistrict: TcxTextEdit;
    _province_id: TcxLookupComboBox;
    _region_id: TcxLookupComboBox;
    _district_id: TcxLookupComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEmployeeEdit: TFEmployeeEdit;

implementation

{$R *.dfm}

initialization
  RegisterClass(TFEmployeeEdit);
finalization
  UnRegisterClass(TFEmployeeEdit);

end.
