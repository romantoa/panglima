unit F_Employee;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFEmployee = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEmployee: TFEmployee;

implementation

{$R *.dfm}

procedure TFEmployee.FormCreate(Sender: TObject);
begin
  _module.Name := 'Employee';
  inherited;
end;

initialization
  RegisterClass(TFEmployee);
finalization
  UnRegisterClass(TFEmployee);

end.
