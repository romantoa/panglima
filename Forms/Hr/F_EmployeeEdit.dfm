object FEmployeeEdit: TFEmployeeEdit
  Left = 0
  Top = 0
  Caption = 'FEmployeeEdit'
  ClientHeight = 394
  ClientWidth = 659
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitTop = 9
    Height = 377
    Width = 659
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Kode'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Nama'
      ParentFont = False
    end
    object _number: TcxTextEdit
      Left = 136
      Top = 15
      ParentFont = False
      TabOrder = 2
      Width = 217
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 41
      ParentFont = False
      TabOrder = 3
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 68
      Caption = 'Jenis Kelamin'
      ParentFont = False
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 94
      Caption = 'Telepon'
      ParentFont = False
    end
    object _gender_id: TcxLookupComboBox
      Left = 136
      Top = 67
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 4
      Width = 217
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 120
      Caption = 'Email'
      ParentFont = False
    end
    object _street: TcxMemo
      Left = 136
      Top = 145
      ParentFont = False
      TabOrder = 7
      Height = 50
      Width = 497
    end
    object cxLabel6: TcxLabel
      Left = 24
      Top = 146
      Caption = 'Alamat'
      ParentFont = False
    end
    object cxLabel7: TcxLabel
      Left = 24
      Top = 224
      Caption = 'Provinsi'
      ParentFont = False
    end
    object cxLabel8: TcxLabel
      Left = 24
      Top = 250
      Caption = 'Kabupaten'
      ParentFont = False
    end
    object cxLabel9: TcxLabel
      Left = 24
      Top = 276
      Caption = 'Kecamatan'
      ParentFont = False
    end
    object cxLabel10: TcxLabel
      Left = 24
      Top = 198
      Caption = 'Desa'
      ParentFont = False
    end
    object _phone: TcxTextEdit
      Left = 136
      Top = 93
      ParentFont = False
      TabOrder = 5
      Width = 329
    end
    object _email: TcxTextEdit
      Left = 136
      Top = 119
      ParentFont = False
      TabOrder = 6
      Width = 329
    end
    object _subdistrict: TcxTextEdit
      Left = 136
      Top = 197
      ParentFont = False
      TabOrder = 8
      Width = 329
    end
    object _province_id: TcxLookupComboBox
      Left = 136
      Top = 223
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 9
      Width = 329
    end
    object _region_id: TcxLookupComboBox
      Left = 136
      Top = 249
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 10
      Width = 329
    end
    object _district_id: TcxLookupComboBox
      Left = 136
      Top = 275
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 11
      Width = 329
    end
  end
end
