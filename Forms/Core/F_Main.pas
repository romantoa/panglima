unit F_Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DM_Main,

  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxLookAndFeels, cxContainer, cxEdit, cxStyles, cxClasses,
  dxSkinTheBezier;

type
  TFMain = class(TForm)
    cxStyleRepository1: TcxStyleRepository;
    styCartContent: TcxStyle;
    sty11Bold: TcxStyle;
    cxDefaultEditStyleController1: TcxDefaultEditStyleController;
    cxEditStyleController1: TcxEditStyleController;
    cxLookAndFeelController1: TcxLookAndFeelController;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure InitConfig;
  public
    { Public declarations }
  end;

var
  FMain: TFMain;

implementation

{$R *.dfm}

uses F_Login, F_Home;

procedure TFMain.FormCreate(Sender: TObject);
begin
  InitConfig;

  FLogin := TFLogin.Create(Application);
  FLogin.Position := poScreenCenter;
  FLogin.ShowModal;
  if FLogin.ModalResult = mrOk then
  begin
    DMMain.LoginInfo := FLogin.LoginResult;
    FHome := TFHome.Create(Application);
  end
  else
    Application.Terminate;

  FLogin.Free;
end;

procedure TFMain.InitConfig;
begin
  //
end;

end.
