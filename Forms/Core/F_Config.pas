unit F_Config;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxVGrid, cxInplaceContainer, cxDropDownEdit, SfIIFUtils,
  SfRegistryConfig, Configable, cxTextEdit, dxSkinWhiteprint;

type
  TFConfig = class(TForm)
    vgSetting: TcxVerticalGrid;
    vgSettingCategoryRow1: TcxCategoryRow;
    _db_main_hostname: TcxEditorRow;
    _db_main_username: TcxEditorRow;
    _db_main_password: TcxEditorRow;
    vgSettingCategoryRow2: TcxCategoryRow;
    _db_backup_hostname: TcxEditorRow;
    _db_backup_username: TcxEditorRow;
    _db_backup_password: TcxEditorRow;
    vgSettingCategoryRow3: TcxCategoryRow;
    _active_server: TcxEditorRow;
    _db_main_database: TcxEditorRow;
    _db_backup_database: TcxEditorRow;
    procedure vgSettingEditValueChanged(Sender: TObject;
      ARowProperties: TcxCustomEditorRowProperties);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    _conf : IConfigable;
    procedure RefreshData;
  public
    { Public declarations }
  end;

var
  FConfig: TFConfig;

implementation

{$R *.dfm}

{ TFConfig }

procedure TFConfig.vgSettingEditValueChanged(Sender: TObject;
  ARowProperties: TcxCustomEditorRowProperties);
var
  AName : String;
  AVal,
  AText : Variant;
begin
  AName := vgSetting.Rows[vgSetting.FocusedRow.Index].Name;

//  if ARowProperties.EditPropertiesClass = TcxLookupComboBoxProperties then
//  begin
//    AVal := IfVarnull(vgSetting.InplaceEditor.EditValue, 0);
//    DMMain.Config.WriteInteger(AName, AVal);
//    AText := IfVarNull((vgSetting.InplaceEditor as TcxLookupComboBox).Text, '');
//    DMMain.Config.WriteString(AName+'_Text', AText);
//  end
//  else if ARowProperties.EditPropertiesClass = TcxComboBoxProperties then
//  begin
//    AVal := (vgSetting.InplaceEditor as TcxComboBox).ItemIndex;
//    DMMain.Config.WriteInteger(AName, AVal);
//  end
//  else
  begin
    AVal := IfVarnull(vgSetting.InplaceEditor.EditValue, '');
    _conf.WriteString('', AName, AVal);
  end;
end;

procedure TFConfig.FormCreate(Sender: TObject);
begin
  _conf := TSfRegistryConfig.Create;
end;

procedure TFConfig.FormShow(Sender: TObject);
begin
  RefreshData;
end;

procedure TFConfig.RefreshData;
begin
  _db_main_hostname.Properties.Value := _conf.ReadString('','_db_main_hostname');
  _db_main_username.Properties.Value := _conf.ReadString('','_db_main_username');
  _db_main_password.Properties.Value := _conf.ReadString('','_db_main_password');
  _db_main_database.Properties.Value := _conf.ReadString('','_db_main_database');

  _db_backup_hostname.Properties.Value := _conf.ReadString('','_db_backup_hostname');
  _db_backup_username.Properties.Value := _conf.ReadString('','_db_backup_username');
  _db_backup_password.Properties.Value := _conf.ReadString('','_db_backup_password');
  _db_backup_database.Properties.Value := _conf.ReadString('','_db_backup_database');

  _active_server.Properties.Value := _conf.ReadString('','_active_server');

//  _prefix_stockin.Properties.Value := _conf.ReadString('_prefix_stockin');
//  _prefix_purchase.Properties.Value := _conf.ReadString('_prefix_purchase');
//  _prefix_cosignation.Properties.Value := _conf.ReadString('_prefix_cosignation');
//  _prefix_sale.Properties.Value := _conf.ReadString('_prefix_sale');
//  _prefix_salereturn.Properties.Value := _conf.ReadString('_prefix_salereturn');
//  _prefix_purchasereturn.Properties.Value := _conf.ReadString('_prefix_purchasereturn');
end;

end.
