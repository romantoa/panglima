object FAppConfig: TFAppConfig
  Left = 0
  Top = 0
  Caption = 'Application Configuration'
  ClientHeight = 441
  ClientWidth = 639
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object vgSetting: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 639
    Height = 441
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    LookAndFeel.SkinName = 'VS2010'
    OptionsView.RowHeaderWidth = 273
    ParentFont = False
    TabOrder = 0
    OnEditValueChanged = vgSettingEditValueChanged
    Version = 1
    object vgSettingCategoryRow1: TcxCategoryRow
      Properties.Caption = 'Auto Numbering'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object vgSettingCategoryRow2: TcxCategoryRow
      Properties.Caption = 'Barang Masuk'
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object _stockin_sequence_prefix: TcxEditorRow
      Properties.Caption = 'Prefix'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object _stockin_sequence_padder: TcxEditorRow
      Properties.Caption = 'Padder'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 3
      ParentID = 1
      Index = 1
      Version = 1
    end
    object _stockin_sequence_length: TcxEditorRow
      Properties.Caption = 'Length'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 4
      ParentID = 1
      Index = 2
      Version = 1
    end
    object _stockin_sequence_resetat: TcxEditorRow
      Properties.Caption = 'Reset At'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 5
      ParentID = 1
      Index = 3
      Version = 1
    end
    object vgSettingCategoryRow3: TcxCategoryRow
      Properties.Caption = 'Penjualan'
      ID = 6
      ParentID = 0
      Index = 1
      Version = 1
    end
    object _sale_sequence_prefix: TcxEditorRow
      Properties.Caption = 'Prefix'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object _sale_sequence_padder: TcxEditorRow
      Properties.Caption = 'Padder'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 8
      ParentID = 6
      Index = 1
      Version = 1
    end
    object _sale_sequence_length: TcxEditorRow
      Properties.Caption = 'Length'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 9
      ParentID = 6
      Index = 2
      Version = 1
    end
    object _sale_sequence_resetat: TcxEditorRow
      Properties.Caption = 'Reset At'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 10
      ParentID = 6
      Index = 3
      Version = 1
    end
  end
end
