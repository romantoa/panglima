object FConfig: TFConfig
  Left = 0
  Top = 0
  Caption = 'System Setting'
  ClientHeight = 281
  ClientWidth = 503
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object vgSetting: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 503
    Height = 281
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    LookAndFeel.SkinName = 'VS2010'
    OptionsView.RowHeaderWidth = 108
    ParentFont = False
    TabOrder = 0
    OnEditValueChanged = vgSettingEditValueChanged
    Version = 1
    object vgSettingCategoryRow1: TcxCategoryRow
      Properties.Caption = 'Main Server'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object _db_main_database: TcxEditorRow
      Properties.Caption = 'Database'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object _db_main_username: TcxEditorRow
      Properties.Caption = 'Username'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object _db_main_password: TcxEditorRow
      Properties.Caption = 'Password'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.EchoMode = eemPassword
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 3
      ParentID = 0
      Index = 2
      Version = 1
    end
    object _db_main_hostname: TcxEditorRow
      Properties.Caption = 'Host'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 4
      ParentID = 0
      Index = 3
      Version = 1
    end
    object vgSettingCategoryRow2: TcxCategoryRow
      Properties.Caption = 'Backup Server'
      ID = 5
      ParentID = -1
      Index = 1
      Version = 1
    end
    object _db_backup_hostname: TcxEditorRow
      Properties.Caption = 'Host'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 6
      ParentID = 5
      Index = 0
      Version = 1
    end
    object _db_backup_database: TcxEditorRow
      Properties.Caption = 'Database'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 7
      ParentID = 5
      Index = 1
      Version = 1
    end
    object _db_backup_password: TcxEditorRow
      Properties.Caption = 'Password'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.EchoMode = eemPassword
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 8
      ParentID = 5
      Index = 2
      Version = 1
    end
    object _db_backup_username: TcxEditorRow
      Properties.Caption = 'Username'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 9
      ParentID = 5
      Index = 3
      Version = 1
    end
    object vgSettingCategoryRow3: TcxCategoryRow
      Properties.Caption = 'Active Connection'
      ID = 10
      ParentID = -1
      Index = 2
      Version = 1
    end
    object _active_server: TcxEditorRow
      Properties.Caption = 'Server'
      Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
      Properties.EditProperties.Items.Strings = (
        'Main'
        'Backup')
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 11
      ParentID = 10
      Index = 0
      Version = 1
    end
  end
end
