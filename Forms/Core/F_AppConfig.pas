unit F_AppConfig;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxInplaceContainer, cxVGrid, SfRegistryConfig, SfIIFUtils,
  dxSkinWhiteprint;

type
  TFAppConfig = class(TForm)
    vgSetting: TcxVerticalGrid;
    vgSettingCategoryRow1: TcxCategoryRow;
    vgSettingCategoryRow2: TcxCategoryRow;
    _stockin_sequence_prefix: TcxEditorRow;
    _stockin_sequence_length: TcxEditorRow;
    _stockin_sequence_padder: TcxEditorRow;
    _stockin_sequence_resetat: TcxEditorRow;
    vgSettingCategoryRow3: TcxCategoryRow;
    _sale_sequence_prefix: TcxEditorRow;
    _sale_sequence_length: TcxEditorRow;
    _sale_sequence_padder: TcxEditorRow;
    _sale_sequence_resetat: TcxEditorRow;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure vgSettingEditValueChanged(Sender: TObject;
      ARowProperties: TcxCustomEditorRowProperties);
  private
//    _conf : TSfRegistryConfig;
    procedure RenderGridProperties;
    procedure RefreshData;
  public
    { Public declarations }
  end;

var
  FAppConfig: TFAppConfig;

implementation

{$R *.dfm}

procedure TFAppConfig.vgSettingEditValueChanged(Sender: TObject;
  ARowProperties: TcxCustomEditorRowProperties);
var
  AName : String;
  AVal, AText : Variant;
begin
//  AName := vgSetting.Rows[vgSetting.FocusedRow.Index].Name;
//
////  if ARowProperties.EditPropertiesClass = TcxLookupComboBoxProperties then
////  begin
////    AVal := IfVarnull(vgSetting.InplaceEditor.EditValue, 0);
////    DMMain.Config.WriteInteger(AName, AVal);
////    AText := IfVarNull((vgSetting.InplaceEditor as TcxLookupComboBox).Text, '');
////    DMMain.Config.WriteString(AName+'_Text', AText);
////  end
////  else if ARowProperties.EditPropertiesClass = TcxComboBoxProperties then
////  begin
////    AVal := (vgSetting.InplaceEditor as TcxComboBox).ItemIndex;
////    DMMain.Config.WriteInteger(AName, AVal);
////  end
////  else
//  begin
//    AVal := IfVarnull(vgSetting.InplaceEditor.EditValue, '');
//    _conf.WriteString(AName, AVal);
//  end;
end;

procedure TFAppConfig.FormCreate(Sender: TObject);
begin
//  _conf := TSfRegistryConfig.Create;
end;

procedure TFAppConfig.FormShow(Sender: TObject);
begin
  RenderGridProperties;
  RefreshData;
end;

procedure TFAppConfig.RefreshData;
begin
//  _stockin_sequence_prefix.Properties.Value := _conf.ReadString('_stockin_sequence_prefix');
//  _stockin_sequence_length.Properties.Value := _conf.ReadString('_stockin_sequence_length');
//  _stockin_sequence_padder.Properties.Value := _conf.ReadString('_stockin_sequence_padder');
//  _stockin_sequence_resetat.Properties.Value := _conf.ReadString('_stockin_sequence_resetat');
//
//  _sale_sequence_prefix.Properties.Value := _conf.ReadString('_sale_sequence_prefix');
//  _sale_sequence_length.Properties.Value := _conf.ReadString('_sale_sequence_length');
//  _sale_sequence_padder.Properties.Value := _conf.ReadString('_sale_sequence_length');
//  _sale_sequence_resetat.Properties.Value := _conf.ReadString('_sale_sequence_resetat');
end;

procedure TFAppConfig.RenderGridProperties;
begin
  //
end;

end.
