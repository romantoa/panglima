unit F_FormulatypeEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTextEdit, cxLabel, cxGroupBox, Fsf_Edit;

type
  TFFormulatypeEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _code: TcxTextEdit;
    _name: TcxTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFormulatypeEdit: TFFormulatypeEdit;

implementation

{$R *.dfm}

initialization
  RegisterClass(TFFormulatypeEdit);
finalization
  UnRegisterClass(TFFormulatypeEdit);

end.
