unit F_Formulatype;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFFormulatype = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFormulatype: TFFormulatype;

implementation

{$R *.dfm}

procedure TFFormulatype.FormCreate(Sender: TObject);
begin
  _module.Name := 'Formulatype';
  inherited;
end;

initialization
  RegisterClass(TFFormulatype);
finalization
  UnRegisterClass(TFFormulatype);

end.
