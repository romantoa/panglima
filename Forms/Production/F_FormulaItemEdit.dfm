object FFormulaItemEdit: TFFormulaItemEdit
  Left = 0
  Top = 0
  Caption = 'FFormulaItemEdit'
  ClientHeight = 224
  ClientWidth = 584
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitHeight = 237
    Height = 224
    Width = 584
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Nama'
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Produk'
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 15
      TabOrder = 2
      Width = 385
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 68
      Caption = 'Satuan'
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 94
      Caption = 'Jumlah'
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 120
      Caption = 'Keterangan'
    end
    object _description: TcxMemo
      Left = 136
      Top = 119
      TabOrder = 6
      Height = 89
      Width = 385
    end
    object _unit_id: TcxLookupComboBox
      Left = 136
      Top = 67
      Properties.ListColumns = <>
      TabOrder = 7
      Width = 217
    end
    object _product_id: TcxLookupComboBox
      Left = 136
      Top = 41
      Properties.ListColumns = <>
      TabOrder = 8
      Width = 385
    end
    object _amount: TcxCurrencyEdit
      Left = 136
      Top = 93
      TabOrder = 9
      Width = 217
    end
  end
end
