unit F_UnitEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Edit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxTextEdit, cxLabel, cxGroupBox;

type
  TFUnitEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _short_name: TcxTextEdit;
    _name: TcxTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FUnitEdit: TFUnitEdit;

implementation

{$R *.dfm}

uses DM_Main;

{ TFUnitEdit }

initialization
  RegisterClass(TFUnitEdit);
finalization
  UnRegisterClass(TFUnitEdit);

end.
