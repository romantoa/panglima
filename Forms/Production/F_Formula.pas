unit F_Formula;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Fsf_Index, Data.DB, DBAccess, Uni, dxBar;

type
  TFFormula = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Add; override;
  end;

var
  FFormula: TFFormula;

implementation

{$R *.dfm}

uses DM_Main;

procedure TFFormula.Add;
begin
  //TODO: Handled by Edit Mode
  OpenEditForm(msInsert);
end;

procedure TFFormula.FormCreate(Sender: TObject);
begin
  _module.Name := 'Formula';
  inherited;
end;

procedure TFFormula.FormShow(Sender: TObject);
begin
  inherited;
  //TODO: Handled by Edit Mode
  btnEdit.Visible := ivAlways;
end;

initialization
  RegisterClass(TFFormula);
finalization
  UnRegisterClass(TFFormula);

end.
