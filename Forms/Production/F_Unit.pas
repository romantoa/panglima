unit F_Unit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Index;

type
  TFUnit = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FUnit: TFUnit;

implementation

{$R *.dfm}

procedure TFUnit.FormCreate(Sender: TObject);
begin
  _module.Name := 'Unit';
  inherited;
end;

initialization
  RegisterClass(TFUnit);
finalization
  UnRegisterClass(TFUnit);

end.
