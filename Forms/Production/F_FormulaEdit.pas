unit F_FormulaEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,

  Fsf_Edit, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinDevExpressStyle, dxSkinOffice2010Blue,
  dxSkinSharp, dxSkinSharpPlus, dxSkinTheBezier, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVS2010, cxLabel, cxGroupBox, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  dxBarBuiltInMenu, cxPC, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxGridLevel, cxGrid,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, cxMemo, System.StrUtils, dxSkinWhiteprint;

type
  TFFormulaEdit = class(TFsfEdit)
    __master: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _code: TcxTextEdit;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    _formulatype_id: TcxLookupComboBox;
    _product_id: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    _description: TcxMemo;
  public

  end;

var
  FFormulaEdit: TFFormulaEdit;

implementation

{$R *.dfm}

initialization

RegisterClass(TFFormulaEdit);

finalization

UnRegisterClass(TFFormulaEdit);

end.
