unit F_FormulaItemEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxTextEdit, cxLabel, cxGroupBox, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxMemo, cxCurrencyEdit,
  Fsf_ItemEdit;

type
  TFFormulaItemEdit = class(TFsfItemEdit)
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    _name: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    _description: TcxMemo;
    _unit_id: TcxLookupComboBox;
    _product_id: TcxLookupComboBox;
    _amount: TcxCurrencyEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFormulaItemEdit: TFFormulaItemEdit;

implementation

{$R *.dfm}

end.
