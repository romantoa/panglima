object FFormulatypeEdit: TFFormulatypeEdit
  Left = 0
  Top = 0
  Caption = 'FFormulatypeEdit'
  ClientHeight = 429
  ClientWidth = 691
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -141
    ExplicitWidth = 832
    ExplicitHeight = 129
    Height = 429
    Width = 691
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Kode'
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Nama'
    end
    object _code: TcxTextEdit
      Left = 136
      Top = 15
      TabOrder = 2
      Width = 217
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 41
      TabOrder = 3
      Width = 497
    end
  end
end
