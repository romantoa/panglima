object FFormulaEdit: TFFormulaEdit
  Left = 0
  Top = 0
  Caption = 'FFormulaEdit'
  ClientHeight = 387
  ClientWidth = 832
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object __master: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    ParentFont = False
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Height = 174
    Width = 832
    object cxLabel1: TcxLabel
      Left = 24
      Top = 16
      Caption = 'Kode'
      ParentFont = False
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 42
      Caption = 'Nama'
      ParentFont = False
    end
    object _code: TcxTextEdit
      Left = 136
      Top = 15
      ParentFont = False
      TabOrder = 2
      Width = 217
    end
    object _name: TcxTextEdit
      Left = 136
      Top = 41
      ParentFont = False
      TabOrder = 3
      Width = 497
    end
    object cxLabel3: TcxLabel
      Left = 24
      Top = 68
      Caption = 'Jenis'
      ParentFont = False
    end
    object cxLabel4: TcxLabel
      Left = 24
      Top = 94
      Caption = 'Keluaran'
      ParentFont = False
    end
    object _formulatype_id: TcxLookupComboBox
      Left = 136
      Top = 67
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 6
      Width = 217
    end
    object _product_id: TcxLookupComboBox
      Left = 136
      Top = 93
      ParentFont = False
      Properties.ListColumns = <>
      TabOrder = 7
      Width = 497
    end
    object cxLabel5: TcxLabel
      Left = 24
      Top = 120
      Caption = 'Keterangan'
      ParentFont = False
    end
    object _description: TcxMemo
      Left = 136
      Top = 119
      ParentFont = False
      TabOrder = 9
      Height = 50
      Width = 497
    end
  end
end
