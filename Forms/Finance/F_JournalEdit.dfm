object FJournalEdit: TFJournalEdit
  Left = 0
  Top = 0
  Caption = 'Jurnal'
  ClientHeight = 460
  ClientWidth = 783
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    PanelStyle.Active = True
    Style.BorderStyle = ebsNone
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    TabOrder = 0
    ExplicitLeft = -3
    ExplicitTop = -3
    Height = 89
    Width = 783
    object cxLabel1: TcxLabel
      Left = 24
      Top = 24
      Caption = 'Kode'
      Transparent = True
    end
    object cxLabel2: TcxLabel
      Left = 24
      Top = 50
      Caption = 'Uraian'
      Transparent = True
    end
  end
  object cxGrid1: TcxGrid
    AlignWithMargins = True
    Left = 3
    Top = 92
    Width = 777
    Height = 365
    Align = alClient
    TabOrder = 1
    object cxGrid1TableView1: TcxGridTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGrid1TableView1Column1: TcxGridColumn
      end
      object cxGrid1TableView1Column2: TcxGridColumn
      end
      object cxGrid1TableView1Column3: TcxGridColumn
      end
      object cxGrid1TableView1Column4: TcxGridColumn
      end
      object cxGrid1TableView1Column5: TcxGridColumn
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1TableView1
    end
  end
end
