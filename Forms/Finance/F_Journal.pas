unit F_Journal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, dxSkinDevExpressStyle, dxSkinOffice2010Blue, StrUtils, cxContainer, cxGroupBox,

  Fsf_Index,
  F_JournalEdit,
  Journal;

type
  TFJournal = class(TFsfIndex)
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  FJournal: TFJournal;

implementation

{$R *.dfm}

uses DM_Main;

procedure TFJournal.FormCreate(Sender: TObject);
begin
  _module.Name := 'Journal';
  inherited;
end;

end.
