unit DM_Main;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  dxSkinsCore, dxSkinDevExpressStyle, dxSkinOffice2010Blue,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVS2010,
  cxClasses, cxLookAndFeels, dxSkinSharp, dxSkinSharpPlus, Generics.Collections,

  Forms,
  SfUniConnection,
  SfLanguage,
  SfConfig,
  User,
  Session,
  Logable,
  Configable,
  CsLogger,
//  FileLogger,
  System.ImageList, Vcl.ImgList, Vcl.Controls, cxGraphics,
  dxSkinTheBezier, cxImageList, cxLookAndFeelPainters, dxAlertWindow,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2010Black, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Light,
  SfAuth, System.IniFiles, SfDatConfig, Uni;
//  Fsf_MdiChild,
//  Fsf_ModalEdit;

//type
//  TMdiFormList = TDictionary<string, TMdiChildType>;

type
  TModifState = ( msInsert = 0, msEdit = 1, msView = 2 );
  TPrintMode = ( pmDirect, pmPreview );
  TGuidType = (gt8, gt12, gt32);
  TPaymentStatus = ( pmsDraft, pmsPaid );

  TRegisterInfo = record
    Shift : integer;
    StorageId : integer;
  end;

type
  TDMMain = class(TDataModule)
    ilDev32: TcxImageList;
    ilDev16: TcxImageList;
    alrMain: TdxAlertWindowManager;
    ilFlat32: TcxImageList;
    cxLookAndFeelController1: TcxLookAndFeelController;
    procedure DataModuleCreate(Sender: TObject);
  private
    _lang : TSfLanguage;
    _conn : TSfUniConnection;
    _logger : ILogable;
    _session : TSession;
    _sysConf : IConfigable;
    _datConf : TSfDatConfig;

    function GetSession: TSession;
    procedure SetSession(const Value: TSession);
    function GetServerDate: TDateTime;

  public
    LoginInfo : RLoginResult;
    Terminated : Boolean;

    RegisterInfo : TRegisterInfo;
    IniFile : TIniFile;

    property Conn : TSfUniConnection read _conn;
    property Lang : TSfLanguage read _lang;
    property SysConf: IConfigable read _sysConf;
    property DatConf: TSfDatConfig read _datConf;

    property Logger: ILogable read _logger;
    property Session: TSession read GetSession write SetSession;
    property ServerDate: TDateTime read GetServerDate;
    property Alert: TdxAlertWindowManager read alrMain;
  end;

var
  DMMain: TDMMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses F_Login, F_ServerPicker;

{$R *.dfm}

{ TDMMain }

procedure TDMMain.DataModuleCreate(Sender: TObject);
begin
  IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'database.ini');

  _logger := TCsLogger.Create;
  _datConf := TSfDatConfig.Create('Persh.dat');

  FServerPicker := TFServerPicker.Create(Application);
  FServerPicker.ShowModal;
  if FServerPicker.ModalResult = mrOk then
  begin
    IniFile.WriteString('terkini', 'server', LowerCase(FServerPicker.cmbServer.Text));

    _conn := TSfUniConnection.Create;

    FLogin := TFLogin.Create(Application);
    FLogin.Position := poScreenCenter;
    FLogin.ShowModal;
    if FLogin.ModalResult = mrOk then
    begin
      //TODO: Lang Code should be fetched from config
      _lang := TSfLanguage.Create('ID');
      _session := TSession.Create;

      // TODO: Dummy
      _session.User.Id := 1;
    end
    else
      Terminated := True;
    FLogin.Free;
  end
  else
    Terminated := True;
  FServerPicker.Free;
end;

function TDMMain.GetServerDate: TDateTime;
begin
  //Dummy
  Result := Date;
end;

function TDMMain.GetSession: TSession;
begin
  Result := _session;
end;

procedure TDMMain.SetSession(const Value: TSession);
begin
  _session := Value;
end;

end.
