unit Employee;

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  SfIIFUtils,
  Address,
  SfModel;

type
  TEmployee = class(TSfModel)
    private
//      function GetName: String;
    protected

    public
      Id          : Integer;
      FirstName   : String;
      LastName    : String;
      FrontTitle  : String;
      BackTitle   : String;
      Addresses   : TDictionary<String, TAddress>;

      constructor Create;
      destructor Destroy;

      procedure PullAddresses;
      //TODO : Refactor this function later
      function GetAddresses(AEmployeeId: Integer) : TDictionary<String, TAddress>;

//      property Name: String read GetName;
  end;

implementation

{ TEmployee }

uses DM_Main;

constructor TEmployee.Create;
begin
//  Addresses := TDictionary<String, TAddress>.Create;
end;

destructor TEmployee.Destroy;
begin
//  if Assigned(Addresses) then
//    Addresses.Free;
end;

function TEmployee.GetAddresses(
  AEmployeeId: Integer): TDictionary<String, TAddress>;
//var
//  jsonResponse, jsonVal, jsonItem : TJsonValue;
//  jsonArr : TJsonArray;
//  response, params : TStringList;
//  url : String;
//  recordCount : integer;
//  addr : TAddress;
begin
//  Result := TDictionary<String, TAddress>.Create;
//  response := TStringList.Create;
//
//  url := 'employee/addresses.json?employee_id=' + IntToStr(AEmployeeId);
//
//  response := DMMain.Rest.Get(url);
//
//  if response.Count > 0 then
//  begin
//    jsonResponse := TJSONObject.ParseJSONValue(response.Text);
//    jsonVal := (jsonResponse as TJSONObject).GetValue('rows');
//    jsonArr := (jsonVal as TJSONArray);
//
//    recordCount := (jsonResponse as TJSONObject).GetValue('total').Value.ToInteger;
//
//    if recordCount > 0 then
//    begin
//      for JsonItem in JsonArr do
//      begin
//        if JsonItem <> nil then
//        begin
//          addr := TAddress.Create;
//
//          addr.Id := (jsonItem as TJSONObject).GetValue('employee_address_id').Value.ToInteger;
//          addr.Name := (jsonItem as TJSONObject).GetValue('employee_address_name').Value;
//          addr.Street := (jsonItem as TJSONObject).GetValue('employee_address_street').Value;
//          addr.PostalCode := (jsonItem as TJSONObject).GetValue('employee_address_postalcode').Value.ToInteger;
//          addr.Longitude := JsonToFloat((jsonItem as TJSONObject).GetValue('employee_address_longitude').Value);
//          addr.Lattitude := JsonToFloat((jsonItem as TJSONObject).GetValue('employee_address_lattitude').Value);
//          addr.Subdistric := (jsonItem as TJSONObject).GetValue('employee_address_subdistrict').Value;
//          addr.District.Id := (jsonItem as TJSONObject).GetValue('employee_address_district').Value.ToInteger;
//          addr.District.Name := (jsonItem as TJSONObject).GetValue('district_name').Value;
//          addr.Regency.Id := (jsonItem as TJSONObject).GetValue('employee_address_regency').Value.ToInteger;
//          addr.Regency.Name := (jsonItem as TJSONObject).GetValue('city_name').Value;
//          addr.Province.Id := (jsonItem as TJSONObject).GetValue('employee_address_province').Value.ToInteger;
//          addr.Province.Name := (jsonItem as TJSONObject).GetValue('province_name').Value;
////          addr.Country.Id := (jsonItem as TJSONObject).GetValue('employee_address_country').Value;
////          addr.Country.Name := (jsonItem as TJSONObject).GetValue('country_name').Value;
//          Result.Add(addr.Name, addr);
//        end;
//      end;
//    end;
//  end;
end;

//function TEmployee.GetName: String;
//begin
//  Result := IIF(Self.FrontTitle = '', '', Self.FrontTitle + ' ') +
//            IIF(Self.FirstName = '', '', Self.FirstName + ' ') +
//            IIF(Self.LastName = '', '', Self.LastName) +
//            IIF(Self.BackTitle = '', '', ', ' + Self.BackTitle);
//  Result := FrontTitle + FirstName + BackTitle;
//end;

procedure TEmployee.PullAddresses;
begin
//  Address is assigned by demand
  Addresses := GetAddresses(Id);
end;

end.
