unit Address;

interface

uses
  Classes,
  Generics.Collections,
  SysUtils,
  JSON,
  SfModel;

type
  TDistrict = class(TSfModel)
    public
      Id : Integer;
      Code: String;
      Name: String;
  end;

  TCity = class(TSfModel)
    public
      Id : Integer;
      Code: String;
      Name: String;
  end;

  TProvince = class(TSfModel)
    public
      Id : Integer;
      Code: String;
      Name: String;
  end;

  TCountry = class(TSfModel)
    public
      Id : Integer;
      Code: String;
      Name: String;
  end;

type
  TAddress = class(TSfModel)
  private
    function GetFullAddress: TStringList;
    public
      Id: Integer;
      Name: String;
      Street: String;
      Subdistric: String;
      District: TDistrict;
      Regency: TCity;
      Province: TProvince;
      Country: TCountry;
      PostalCode: Integer;
      Longitude: Double;
      Lattitude: Double;

      constructor Create;
      destructor Destroy;

      function GetDistance(AOriginId, ADestinationId: integer; AOrderItems: TJsonArray) : Double;
      property FullAddress: TStringList read GetFullAddress;
  end;

implementation

{ TAddress }

uses DM_Main;

constructor TAddress.Create;
begin
  District := TDistrict.Create;
  Regency := TCity.Create;
  Province := TProvince.Create;
  Country := TCountry.Create;
end;

destructor TAddress.Destroy;
begin
  if Assigned(District) then
    District.Free;
  if Assigned(Regency) then
    Regency.Free;
  if Assigned(Province) then
    Province.Free;
  if Assigned(Country) then
    Country.Free;
end;

function TAddress.GetDistance(AOriginId, ADestinationId: integer; AOrderItems: TJsonArray): Double;
//var
//  url : string;
//  response, params : TStringList;
//  jsonResponse : TJsonValue;
//  orderItems : TJsonArray;
begin
//  url := 'delivery/fee.json';
//
//  params := TStringList.Create;
//  params.Add('origin_id='+AOriginId.ToString);
//  params.Add('destination_id='+ADestinationId.ToString);
//
//  response := DMMain.Rest.Post(url, params);
//
//  if response.Count > 0 then
//  begin
//    jsonResponse := TJSONObject.ParseJSONValue(response.Text);
//    Result := JsonToFloat((jsonResponse as TJSONObject).GetValue('distance').Value);
//  end;
end;

function TAddress.GetFullAddress: TStringList;
begin
  Result := TStringList.Create;

  Result.Add(Street);
  Result.Add(Subdistric + ', ' + District.Name + ', ' + Regency.Name);
  Result.Add(Province.Name + ', ' + PostalCode.ToString);
end;

end.
