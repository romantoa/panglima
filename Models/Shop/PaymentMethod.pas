unit PaymentMethod;

interface

uses
  Classes,
  System.Generics.Collections,
  SfModel;

type
  TPaymentMethod = class(TSfModel)
    class procedure GetList(var AList: TDictionary<Integer, String>);
  end;

implementation

{ TPaymentMethod }

class procedure TPaymentMethod.GetList(var AList: TDictionary<Integer, String>);
begin
  AList := TDictionary<Integer, String>.Create(5);
  with AList do
  begin
    AList.Add(1, 'Tunai');
    AList.Add(2, 'Simpananku');
    AList.Add(3, 'Diangsur');
    AList.Add(4, 'Kartu Debet');
    AList.Add(5, 'Kartu Kredit');
  end;
end;

end.
