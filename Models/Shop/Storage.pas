unit Storage;

interface

uses
  Classes,
  DB,
  SfCollectionUtils,
  DM_Main,
  SfModel;

type
  TStorage = class(TSfModel)
    public
      Id : integer;
      Code : String;
      Name : String;
      Latitude : Double;
      Longitude : Double;

      constructor Create;
  end;

implementation

{ TStorage }

{ TStorage }

constructor TStorage.Create;
begin

  inherited;
end;

end.

