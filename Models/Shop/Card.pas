unit Card;

interface

uses
  Classes, System.JSON, DM_Main, DB, System.Generics.Collections,
  System.SysUtils, SfModel;

type
  TCardType = class(TSfModel)
    public
      Id : integer;
      Name : string;
  end;

  TCard = class(TSfModel)
    public
      Id : integer;
      Name : string;
      CardType : TCardType;
      FeeRate : Double;

      constructor Create;
      destructor Destroy;

      class function ListCard : TList<TCard>;
  end;

implementation

{ TCard }

constructor TCard.Create;
begin
  CardType := TCardType.Create;
end;

destructor TCard.Destroy;
begin
  if Assigned(CardType) then
    CardType.Free;
end;

class function TCard.ListCard: TList<TCard>;
//var
//  response : TStringList;
//  url : String;
//  params : TStringList;
//  responseJson, jsonVal, item : TJSONValue;
//  JSONObj : TJSONObject;
//  JSONArr : TJSONArray;
//  card : TCard;
begin
//  url := 'card/index.mod?_dc=1569577012578';
//
//  params := TStringList.Create;
//  params.Add('select=["card_id","card_name","card_bank__bank_name","card_type__cardtype_name","card_feerate","card_description"]');
//
//  response := DMMain.Rest.Post(url, params);
//
//  if response.Count > 0 then
//  begin
//    responseJson := TJSONObject.ParseJSONValue(response.Text);
//    JSONVal := (responseJson as TJSONObject).GetValue('rows');
//    JSONArr := (JSONVal as TJSONArray);
//
//    Result := TList<TCard>.Create;
//
//    for item in JSONArr do
//    begin
//      if item <> nil then
//      begin
//        card := TCard.Create;
//
//        card.Id := (item as TJSONObject).GetValue('card_id').Value.ToInteger;
//        card.Name := (item as TJSONObject).GetValue('card_name').Value;
//        card.FeeRate := JsonToFloat((item as TJSONObject).GetValue('card_feerate').Value);
//
//        Result.Add(card);
//      end;
//    end;
//
//    Result.Pack;
//  end;
end;

end.
