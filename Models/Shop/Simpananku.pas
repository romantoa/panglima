unit Simpananku;

interface

uses
  Classes,
  Generics.Collections,
  JSON,
  SysUtils,
  SfDateTimeUtils,
  SfModel;

type
  TSimpanankuSukarela = class(TSfModel)
  public
    Id: Integer;
    DateIssued: TDateTime;
    Addition: Currency;
    Deduction: Currency;
    Reference: String;
    Description: String;
  end;

type
  TSimpananku = class(TObject)
  private

  public
    Id : Integer;
    Total: Currency;

    Sukarelas: TList<TSimpanankuSukarela>;

    constructor Create;

    class function GetByMember(AMemberId: Integer): TSimpananku;
  end;

implementation

{ TSimpananku }

uses DM_Main;

constructor TSimpananku.Create;
begin

end;

class function TSimpananku.GetByMember(AMemberId: Integer): TSimpananku;
//var
//  url: string;
//  params, response: TStringList;
//  JsonVal, ResponseJson, item: TJsonValue;
//  JsonObj: TJsonObject;
//  JsonArr: TJsonArray;
begin
//  url := 'simpananku/index.mod?_dc=B7FC706E552D';
//  response := TStringList.Create;
//  params := TStringList.Create;
//
//  params.Add('select=["simpananku_id","simpananku_total"]');
//  params.Add('advsearch=[{"logical":"and","data":[{"field_name":"simpananku_member__employee_id","operator":"e","data_type":"NUM","field_translation":"[Anggota/Karyawan][ID]","value":["'+IntToStr(AMemberId)+'"]}],"children":[]}]');
//
//  response := DMMain.Rest.Post(url, params);
//
//  ResponseJson := TJsonObject.ParseJSONValue(response.Text);
//  JsonVal := (ResponseJson as TJsonObject).GetValue('rows');
//  JsonArr := (JsonVal as TJsonArray);
//
//  Result := TSimpananku.Create;
//  if JsonArr.Count > 0 then
//  begin
//    for item in JsonArr do
//    begin
//        Result.Id := (item as TJsonObject).GetValue('simpananku_id').Value.ToInteger;
//        Result.Total := JsonToFloat((item as TJsonObject).GetValue('simpananku_total').Value);
//        Break;
//    end;
//  end;
end;

end.
