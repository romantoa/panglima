unit Shift;

interface

uses
  Classes,
//  System.SysUtils,
  SfModel;

type
  RShift = record
    Id : Integer;
    Name : String;

    constructor Create(AId : Integer; AName: String);
  end;

type
  TShift = class(TSfModel)
  private

  public
    FShift : RShift;

    constructor Create;

    property Id : Integer read FShift.Id write FShift.Id;
    property Name : String read FShift.Name write FShift.Name;
  end;

implementation

{ TStoreshift }

constructor TShift.Create;
begin
  _module.Name := 'shift';
  inherited;
end;

{ RShift }

constructor RShift.Create(AId: Integer; AName: String);
begin
  Self.Id := AId;
  Self.Name := AName;
end;

end.
