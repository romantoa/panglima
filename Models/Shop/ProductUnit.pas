unit ProductUnit;

interface

uses
  Classes,
  SfModel;

type
  TProductUnit = class(TSfModel)
    public
      Id : Integer;
      Code : String;
      Name : String;
  end;

implementation

end.
