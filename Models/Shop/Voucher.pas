unit Voucher;

interface

uses
  Classes,
  System.JSON,
  DM_Main,
  DB,
  System.Generics.Collections,
  System.SysUtils,
  SfDateTimeUtils,
  SfModel;

type
  TVoucherStatus = class(TSfModel)
    public
      Id : integer;
      Name : string;
  end;

  TVoucher = class(TObject)
    public
      Id : integer;
      Number : string;
      DateIssued : TDate;
      DateExpired : TDate;
      Amount : Currency;
      Status : TVoucherStatus;

      constructor Create;
      destructor Destroy;

      class function ListVoucher(AMemberId : integer) : TList<TVoucher>;
  end;

implementation

{ TVoucher }

constructor TVoucher.Create;
begin
  Status := TVoucherStatus.Create;
end;

destructor TVoucher.Destroy;
begin
  if Assigned(Status) then
    Status.Free;
end;

class function TVoucher.ListVoucher(AMemberId: integer): TList<TVoucher>;
//var
//  response : TStringList;
//  url : String;
//  params : TStringList;
//  responseJson, jsonVal, item : TJSONValue;
//  JSONObj : TJSONObject;
//  JSONArr : TJSONArray;
//  voucher : TVoucher;
begin
//  url := 'voucher/index.mod?_dc=1569577012578';
//
//  params := TStringList.Create;
//  params.Add('select=["voucher_id","voucher_number","voucher_status__voucherstatus_id","voucher_status__voucherstatus_name","voucher_amount","voucher_dateissued","voucher_dateexpired"]');
//  params.Add('advsearch=[{"logical":"and","data":['+
//              '{"field_name":"voucher_member__employee_id","operator":"e","data_type":"NUM","field_translation":"[LBL_FIELD_VOUCHER_MEMBER][ID]","value":["'+IntToStr(AMemberId)+'"]},'+
//              '{"field_name":"voucher_status__voucherstatus_id","operator":"e","data_type":"NUM","field_translation":"[Status][ID]","value":["1"]}],"children":[]}]');
//
//  response := DMMain.Rest.Post(url, params);
//
//  if response.Count > 0 then
//  begin
//    responseJson := TJSONObject.ParseJSONValue(response.Text);
//    JSONVal := (responseJson as TJSONObject).GetValue('rows');
//    JSONArr := (JSONVal as TJSONArray);
//
//    Result := TList<TVoucher>.Create;
//
//    for item in JSONArr do
//    begin
//      if item <> nil then
//      begin
//        voucher := TVoucher.Create;
//
//        voucher.Id := (item as TJSONObject).GetValue('voucher_id').Value.ToInteger;
//        voucher.Number := (item as TJSONObject).GetValue('voucher_number').Value;
//        voucher.DateIssued := StrToDate((item as TJsonObject).GetValue('voucher_dateissued').Value, TDateTimeUtils.FormatSettings);
//        voucher.DateExpired := StrToDate((item as TJsonObject).GetValue('voucher_dateexpired').Value, TDateTimeUtils.FormatSettings);
//        voucher.Amount := JsonToFloat((item as TJSONObject).GetValue('voucher_amount').Value);
//        voucher.Status.Id := 1; //(item as TJSONObject).GetValue('voucher_status__voucherstatus_id').Value.ToInteger;
//        voucher.Status.Name := 'Asdf'; //(item as TJSONObject).GetValue('voucher_status__voucherstatus_name').Value;
//        Result.Add(voucher);
//      end;
//    end;
//
//    Result.Pack;
//  end;
end;

end.
