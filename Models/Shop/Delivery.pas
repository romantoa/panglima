unit Delivery;

interface

uses
  Classes,
  JSON,
  System.Generics.Collections,
  SysUtils,
  DM_Main,
  Address,
  SfModel;

type
  RDeliveryFee = record
    Weight : Double;
    Tariff : Currency;
    Distance : Double;
    Fee : Currency;
  end;

type
  ROrderItem = record
    ProductId : Integer;
    Quantity : Integer;
  end;

type
  TDelivery = class(TSfModel)
    public
      Id : integer;
      Cost : Currency;
      Address : TAddress;

      class function Fee(AOriginId, ADestinationId : Integer; AOrderItems: TList<ROrderItem>) : RDeliveryFee;
  end;

implementation

{ TDelivery }

class function TDelivery.Fee(AOriginId, ADestinationId: Integer;
  AOrderItems: TList<ROrderItem>): RDeliveryFee;
//var
//  url : string;
//  response, params : TStringList;
//  jsonResponse : TJsonValue;
//  orderItems : TJsonArray;
//  orderItem, resObj : TJsonObject;
//  I : integer;
begin
//  url := 'delivery/fee.json';
//
//  params := TStringList.Create;
//  params.Add('origin_id='+AOriginId.ToString);
//  params.Add('destination_id='+ADestinationId.ToString);
//
//  orderItems := TJsonArray.Create;
//  for I := 0 to AOrderItems.Count-1 do
//  begin
//    orderItem := TJsonObject.Create;
//
//    orderItem.AddPair('order_item_product', AOrderItems[I].ProductId.ToString);
//    orderItem.AddPair('order_item_quantity', AOrderItems[I].Quantity.ToString);
//
//    orderItems.Add(orderItem);
//  end;
//
//  params.Add('order_items='+orderItems.ToString);
//
//  response := DMMain.Rest.Post(url, params);
//
//  if response.Count > 0 then
//  begin
//    jsonResponse := TJSONObject.ParseJSONValue(response.Text);
//    ResObj := ((jsonResponse as TJSONObject).GetValue('rows') as TJSONObject);
//
//    Result.Weight := JsonToFloat(resObj.GetValue('weight').Value);
//    Result.Tariff := JsonToFloat(resObj.GetValue('tariff').Value);
//    Result.Distance := JsonToFloat(resObj.GetValue('distance').Value);
//    Result.Fee := Round(JsonToFloat(resObj.GetValue('fee').Value));
//  end;
end;

end.
