unit Lookup;

interface

uses
  Classes, 
  DB, 
  System.Generics.Collections, 
  DxMDaset,
  Configable, 
  PaymentMethod;

type
  TIntStrDictionary = TDictionary<Integer, String>;
  TStrStrDictionary = TDictionary<String, String>;

type
  TLookup = class(TObject)
  private
    _config : IConfigable;
  public
    Constructor Create(AConfig : IConfigable);
    class function KeyValueToDataSet(Val: TIntStrDictionary; ValLength: integer) : TDataSet;
    class function PaymentMethodLk : TDataSet;
  end;

implementation

{ TLookup }

constructor TLookup.Create(AConfig: IConfigable);
begin
  Self._config := AConfig;
end;

class function TLookup.KeyValueToDataSet(Val: TDictionary<integer, string>;
  ValLength: integer): TDataSet;
var
  x: TdxMemData;
  f1, f2: TField;
  nama: string;
  key: integer;
begin
  x := TdxMemData.Create(nil);
  f1 := TIntegerField.Create(nil);
  f1.FieldName := 'id';
  f1.Dataset := x;

  f2 := TStringField.Create(nil);
  f2.FieldName := 'nama';
  f2.Dataset := x;
  f2.Size := ValLength;

  x.Close;
  x.Open;

  for key in Val.Keys do
  begin
    x.Append;
    x.FieldByName('id').Value := key;
    Val.TryGetValue(key,nama);
    x.FieldByName('nama').Value := nama;
    x.Post;
  end;

  Result := x;
end;

class function TLookup.PaymentMethodLk: TDataSet;
var
  d : TIntStrDictionary;
begin
  d := TIntStrDictionary.Create;
  try
    TPaymentMethod.GetList(d);

    Result := KeyValueToDataSet(d, 20);
  finally
    d.Free;
  end;
end;

end.
