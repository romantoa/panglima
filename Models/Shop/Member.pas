unit Member;

interface

uses
  Classes,
  Employee,
  JSON,
  SysUtils,
  SfIIFUtils,
  SfModel;

type
  TMemberType = class(TsfModel)
    public
      Id : Integer;
      Name : String;
  end;

type
  TMember = class(TEmployee)
    public
      &Type : TMemberType;
      PlafonTokoKecil : Currency;

      constructor Create;
      destructor Destroy;
      function AttemptLogin(ASecretCode : String) : TMember;
  end;

implementation

{ TMember }

uses DM_Main;

function TMember.AttemptLogin(ASecretCode: String): TMember;
//var
//  response, params : TStringList;
//  jsonResponse, jsonVal, jsonItem : TJSonValue;
//  jsonObj : TJsonObject;
//  jsonArr : TJsonArray;
//  url, select : string;
//  recordCount : integer;
begin
//  response := TStringList.Create;
//  url := 'employee/index.mod?_dc=A06F3A94';
//
//  params := TStringList.Create;
//  params.Add('select=['
//    +'"employee_id",'
//    +'"employee_number",'
//    +'"employee_firstname",'
//    +'"employee_lastname",'
//    +'"employee_fronttitle",'
//    +'"employee_backtitle",'
//    +'"employee_plafontokokecil",'
//    +'"employee_membertype__membertype_id",'
//    +'"employee_membertype__membertype_name"'
//    +']');
//
//  params.Add('advsearch=[{"logical":"and","data":[{"field_name":"employee_number","operator":"c","data_type":"TXT","field_translation":"[Kode]","value":["'+ASecretCode+'"]}],"children":[]}]');
//
//  response := DMMain.Rest.Post(url, params);
//
//  if response.Count > 0 then
//  begin
//    jsonResponse := TJSONObject.ParseJSONValue(response.Text);
//    jsonVal := (jsonResponse as TJSONObject).GetValue('rows');
//    jsonArr := (jsonVal as TJSONArray);
//
//    recordCount := (jsonResponse as TJSONObject).GetValue('total').Value.ToInteger;
//
//    if recordCount > 0 then
//    begin
//      for JsonItem in JsonArr do
//      begin
//        if JsonItem <> nil then
//        begin
//          Result := TMember.Create;
//          Result.Id := (jsonItem as TJSONObject).GetValue('employee_id').Value.ToInteger;
//          Result.FirstName := (jsonItem as TJSONObject).GetValue('employee_firstname').Value;
//          Result.LastName := (jsonItem as TJSONObject).GetValue('employee_lastname').Value;
//          Result.FrontTitle := (jsonItem as TJSONObject).GetValue('employee_fronttitle').Value;
//          Result.BackTitle := (jsonItem as TJSONObject).GetValue('employee_backtitle').Value;
//          Result.&Type.Id := StrToInt((jsonItem as TJSONObject).GetValue('employee_membertype__membertype_id').Value);
//          Result.&Type.Name := (jsonItem as TJSONObject).GetValue('employee_membertype__membertype_name').Value;
//          if Result.&Type.Id = Ord(mbtTokoKecil) then
//            Result.PlafonTokoKecil := JsonToFloat(IfNull((jsonItem as TJSONObject).GetValue('employee_plafontokokecil').Value, '0')) //Get from tokokecil config
//          else
//            Result.PlafonTokoKecil := 0;
//          Break;
//        end;
//      end;
//    end;
//  end;
end;

constructor TMember.Create;
begin
  &Type := TMemberType.Create;
end;

destructor TMember.Destroy;
begin
  if Assigned(&Type) then
    &Type.Free;
end;

end.
