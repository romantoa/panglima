unit Storeshift;

interface

uses
  Classes,
//  System.SysUtils,
  SfModel,
  Uni,
  DB,
  Shift;

type
  TStoreshift = class(TSfModel)
    public
      Id : integer;
      Shift : TShift;

      constructor Create;
      destructor Destroy;

      class function GetCurrentOpenShift(AStorageId : Integer) : TDataSet;
  end;

implementation

{ TStoreshift }

uses DM_Main;

constructor TStoreshift.Create;
begin
  _module.Name := 'storeshift';
  Shift := TShift.Create;
  inherited;
end;

destructor TStoreshift.Destroy;
begin
  if Assigned(Shift) then
    Shift.Free;
end;

class function TStoreshift.GetCurrentOpenShift(AStorageId : Integer): TDataSet;
begin
  Result := TUniQuery.Create(nil);
  (Result as TUniQuery).Connection := DMMain.Conn.Connection;

  (Result as TUniQuery).SQL.Add('select ss.*, s.name as shift_name from storeshift as ss');
  (Result as TUniQuery).SQL.Add('join shift as s on s.id = ss.shift_id');
  (Result as TUniQuery).SQL.Add('where cast(ss.openat as date) = cast(now() as date)');
  (Result as TUniQuery).SQL.Add('and ss.closedat is null');
  (Result as TUniQuery).SQL.Add('and ss.storage_id = 2');// :storage_id');

//  (Result as TUniQuery).Params.ParamByName('storage_id').Value := AStorageId;

  (Result as TUniQuery).Connection.Connected := True;
  (Result as TUniQuery).Open;
end;

end.
