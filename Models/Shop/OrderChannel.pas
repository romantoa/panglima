unit OrderChannel;

interface

uses
  Classes,
  DB,
  SfCollectionUtils,
  DM_Main,
  SfModel;

type
  TOrderChannel = class(TSfModel)
    public
      Id : integer;
      Code : String;
      Name : String;
  end;

implementation

{ TOrderChannel }

end.
