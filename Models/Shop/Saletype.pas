unit Saletype;

interface

uses
  Classes,
  DB,
  SfCollectionUtils,
  DM_Main,
  Storage,
  JSON,
  SysUtils,
  SfModel;

type
  TSaletype = class(TSfModel)
    public
      Id : Integer;
      Code : String;
      Name : String;
  end;

implementation

{ TSaletype }

end.

