unit Warehouse;

interface

uses
  Classes,
  DB,
  SfCollectionUtils,
  DM_Main,
  SfModel;

type
  TWarehouse = class(TSfModel)
    public
      Id : integer;
      Code : String;
      Name : String;
      Latitude : Double;
      Longitude : Double;

      constructor Create;
  end;

implementation

{ TWarehouse }

constructor TWarehouse.Create;
begin

end;

end.

