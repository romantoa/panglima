unit Shop;

interface

uses
  Classes,
  DB,
  SfCollectionUtils,
  DM_Main,
  SfModel;

type
  TShop = class(TSfModel)
    public
      Id : Integer;
      Code : String;
      Name : String;
      Latitude : Double;
      Longitude : Double;

      function Single(AId: integer) : TShop;
  end;

implementation

{ TShop }

function TShop.Single(AId: integer): TShop;
begin
  //
  Result := Self;
end;

end.
