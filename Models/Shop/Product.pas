unit Product;

interface

uses
  Classes,
  Configable,
  StrUtils,
  SysUtils,
  System.Generics.Collections,
  Variants,
  System.JSON,
  SfIIFUtils,
  ProductUnit,
  SfModel;

type
  TProductType = class(TSfModel)
    public
      Id : Integer;
      Code : String;
      Name : String;
      Parent : TProductType;
      IsHeader : Boolean;
  end;

type
  TProduct = class(TSfModel)
    public
      Id : Integer;
      Code : String;
      Name : String;
      ProductUnit : TProductUnit;
      Hpp : Currency;
      RetailPrice : Currency;
      RetailDiscount : Currency;
      PartyPrice : Currency;
      PartyDiscount : Currency;
      StockistPrice : Currency;
      StockistDiscount : Currency;
      PartyPriceQuantity : Integer;
      Barcode : String;
      IsTaxed : Boolean;
      Stock : Integer;
      RepImage : String;
      DiscountAmount : Currency;

      constructor Create;

      function SearchByBarcode(ABarcode : String) : TList<TProduct>;
      class function ListProductType(AParent : TProductType) : TList<TProductType>;
      function ListProduct(AProductType : TProductType) : TObjectList<TProduct>;
  end;

implementation

{ TProduct }

uses DM_Main;

constructor TProduct.Create;
begin
  Self.ProductUnit := TProductUnit.Create;
end;

function TProduct.ListProduct(AProductType: TProductType): TObjectList<TProduct>;
//var
//  response : TStringList;
//  url : String;
//  params : TStringList;
//  ProdUnit : TProductUnit;
//  JSONVal, ArrayElement : TJSONValue;
//  JSONObj : TJSONObject;
//  JSONArr : TJSONArray;
//  Prod : TProduct;
//  PodUnit : TProductUnit;
//  VarVal : Variant;
begin
//  Result := TObjectList<TProduct>.Create;
//
//  url := 'product/list.json?storage_id=' + IntToStr(DMMain.Config.ReadInteger('StorageId')) + '&producttype_id=';
//
//  if AProductType = nil then
//    url := url + '-1'
//  else
//    url := url + IntToStr(AProductType.Id) + '&producttype_isheader=' + IIF(AProductType.IsHeader, '1', '0');
//
//  response := DMMain.Rest.Get(url);
//
//  if response.Count > 0 then
//  begin
//    JSONVal := TJSONObject.ParseJSONValue(response.Text);
//    JSONVal := (JSONVal as TJSONObject).GetValue('rows');
//    JSONArr := (JSONVal as TJSONArray);
//
//    for ArrayElement in JSONArr do
//    begin
//      if ArrayElement <> nil then
//      begin
//        Prod := TProduct.Create;
//
//        Prod.Id := (ArrayElement as TJSONObject).GetValue('product_id').Value.ToInteger;
//        Prod.Name := (ArrayElement as TJSONObject).GetValue('product_name').Value;
//        Prod.RepImage := (ArrayElement as TJSONObject).GetValue('product_repimage').Value;
//        Prod.RetailPrice := JsonToFloat((ArrayElement as TJSONObject).GetValue('retail_price').Value);
//        Prod.RetailDiscount := JsonToFloat((ArrayElement as TJSONObject).GetValue('retail_discount').Value);
//        Prod.PartyPrice := JsonToFloat((ArrayElement as TJSONObject).GetValue('party_price').Value);
//        Prod.PartyDiscount := JsonToFloat((ArrayElement as TJSONObject).GetValue('party_discount').Value);
//        Prod.StockistPrice := JsonToFloat((ArrayElement as TJSONObject).GetValue('stockist_price').Value);
//        Prod.StockistDiscount := JsonToFloat((ArrayElement as TJSONObject).GetValue('stockist_discount').Value);
//        Prod.PartyPriceQuantity := (ArrayElement as TJSONObject).GetValue('party_price_quantity').Value.ToInteger;
//        Prod.Stock := IfNull((ArrayElement as TJSONObject).GetValue('product_stock_available').Value, '0').ToInteger;
//        Prod.IsTaxed := ((ArrayElement as TJSONObject).GetValue('product_istaxed').Value = '1');
//        Prod.ProductUnit.Id := (ArrayElement as TJSONObject).GetValue('product_unit').Value.ToInteger;
//        Prod.ProductUnit.Code := (ArrayElement as TJSONObject).GetValue('unit_code').Value;
//        Prod.ProductUnit.Name := (ArrayElement as TJSONObject).GetValue('unit_name').Value;
//
//        Result.Add(Prod);
//      end;
//    end;
//
////    Result := Result.Pack;
//  end;
end;

class function TProduct.ListProductType(AParent: TProductType): TList<TProductType>;
//var
//  response : TStringList;
//  url : String;
//  params : TStringList;
//  ProdUnit : TProductUnit;
//  JSONVal, ArrayElement : TJSONValue;
//  JSONObj : TJSONObject;
//  JSONArr : TJSONArray;
//  ProdType, ProdTypeParent : TProductType;
//  VarVal : Variant;
begin
//  url := 'producttype/index.mod?_dc=1564435614904';
//
//  params := TStringList.Create;
//  params.Add('select=["producttype_id","producttype_name","producttype_isheader","producttype_parent__producttype_id","producttype_parent__producttype_name"]');
//  if AParent = nil then
//    params.Add('advsearch=[{"logical":"and","data":[{"field_name":"producttype_parent__producttype_id","operator":"ib","data_type":"NUM","field_translation":"[Parent][ID]","value":[]}],"children":[]}]')
//  else
//    params.Add('advsearch=[{"logical":"and","data":[{"field_name":"producttype_parent__producttype_id","operator":"e","data_type":"NUM","field_translation":"[Parent][ID]","value":["'+IntToStr(AParent.Id)+'"]}],"children":[]}]');
//
//  response := DMMain.Rest.Post(url, params);
//
//  if response.Count > 0 then
//  begin
//    JSONVal := TJSONObject.ParseJSONValue(response.Text);
//    JSONVal := (JSONVal as TJSONObject).GetValue('rows');
//    JSONArr := (JSONVal as TJSONArray);
//
//    Result := TList<TProductType>.Create;
//
//    for ArrayElement in JSONArr do
//    begin
//      if ArrayElement <> nil then
//      begin
//        ProdType := TProductType.Create;
//
//        ProdType.Id := (ArrayElement as TJSONObject).GetValue('producttype_id').Value.ToInteger;
//        ProdType.Name := (ArrayElement as TJSONObject).GetValue('producttype_name').Value;
//
//        ProdTypeParent := TProductType.Create;
//        VarVal := (ArrayElement as TJSONObject).GetValue('producttype_parent__producttype_id').Value;
//        if (VarVal <> (null)) and (VarVal <> 'null') then
//        begin
//          ProdTypeParent.Id := VarVal;
//          ProdTypeParent.Name := (ArrayElement as TJSONObject).GetValue('producttype_parent__producttype_name').Value;
//        end;
//        ProdType.Parent := ProdTypeParent;
//        ProdType.IsHeader := ((ArrayElement as TJSONObject).GetValue('producttype_isheader').Value.ToInteger = 1);
//
//        Result.Add(ProdType);
//      end;
//    end;
//
//    Result.Pack;
//  end;
end;

function TProduct.SearchByBarcode(ABarcode: String): TList<TProduct>;
//var
//  response : TStringList;
//  url : String;
//  ProdUnit : TProductUnit;
//  ResponseJson, JSONVal, item : TJSONValue;
//  ItemObj : TJSONObject;
//  JSONArr : TJSONArray;
//  RowCount : Integer;
//  pp : TProduct;
begin
//  Result := TList<TProduct>.Create;
//  url := 'product/barcodesearch.json?storage_id=' + IntToStr(DMMain.Config.ReadInteger('StorageId')) + '&barcode='+ABarcode;
//
//  response := DMMain.Rest.Get(url);
//
//  ResponseJson := TJSONObject.ParseJSONValue(response.Text);
//  JSONVal := (ResponseJson as TJSONObject).GetValue('rows');
//  RowCount := (ResponseJson as TJSONObject).GetValue('count').Value.ToInteger;
//  JSONArr := (JSONVal as TJSONArray);
//
//  if RowCount > 0 then
//  begin
//    for item in JSONArr do
//    begin
//      if item <> nil then
//      begin
//        pp := TProduct.Create;
//        itemObj := (item as TJSONObject);
////        try
//          pp.Id := itemObj.GetValue('product_id').Value.ToInteger;
//          pp.Code := itemObj.GetValue('product_code').Value;
//          pp.Name := itemObj.GetValue('product_name').Value;
//          pp.RepImage := itemObj.GetValue('product_repimage').Value;
//          pp.RetailPrice := JsonToFloat(itemObj.GetValue('retail_price').Value);
//          pp.RetailDiscount := JsonToFloat(itemObj.GetValue('retail_discount').Value);
//          pp.PartyPrice := JsonToFloat(itemObj.GetValue('party_price').Value);
//          pp.PartyDiscount := JsonToFloat(itemObj.GetValue('party_discount').Value);
//          // TODO: Calculated Price. What if product_price of stockist exists?
//          pp.StockistPrice := JsonToFloat(itemObj.GetValue('stockist_cprice').Value);
//          pp.StockistDiscount := JsonToFloat(itemObj.GetValue('stockist_discount').Value);
//          pp.PartyPriceQuantity := itemObj.GetValue('party_price_quantity').Value.ToInteger;
//          pp.Stock := itemObj.GetValue('product_stock_available').Value.ToInteger;
//          pp.ProductUnit.Id := itemObj.GetValue('product_unit').Value.ToInteger;
//          pp.ProductUnit.Code := itemObj.GetValue('unit_code').Value;
//          pp.ProductUnit.Name := itemObj.GetValue('unit_name').Value;
////        except
//
////        end;
//
//        Result.Add(pp);
//      end;
//    end;
//  end;
//  Result.Pack;
end;

end.
