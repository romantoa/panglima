unit Order;

interface

uses
  Classes, DM_Main, JSON, SysUtils, OrderItem, SfDateTimeUtils, Generics.Collections,
  SfIIFUtils,
  Member,
  OrderChannel,
  Saletype,
  Storage,
  Card,
  Employee,
  SfModel;

type
  ROrderSum = record
    Number : String;
  end;

type
  TOrder = class(TSfModel)
    public
      id : integer;
      number : string;
      dateissued : TDateTime;
      dateexpired : TDateTime;
      member : TMember;
      channel : TOrderChannel;
      status : integer;
      subtotal : Currency;
      discountpercent : Currency;
      discountamount : Currency;
      deliveryfee : Currency;
      total : Currency;
      paidbyvoucher : Currency;
      paidbyrealisasisp : Currency;
      paidbycash : Currency;
      paidbysukarela : Currency;
      iscredit : Boolean;
      provision : Currency;
      interest : Currency;
      downpayment : Currency;
      creditamount : Currency;
      termcount : integer;
      termamount : Currency;
      cardfee : Currency;
      saletype : TSaletype;
      isstockist : Boolean;
      storage : TStorage;
      card : TCard;
      cardnumber : String;
      cardapprovalnumber : String;
      reference : String;
      pic : TEmployee;
      plafon : Currency;
      items: TList<TOrderItem>;

      constructor Create;
      destructor Destroy;
      class function SeachByCode(ACode: String) : TOrder;
      class function LoadOrderSum(ARef: string) : ROrderSum;

      function SubmitOrder(AMaster: TSfStringVariantPair; AItems: TList<TSfStringVariantPair>) : TSfActionResult;
  end;

implementation

{ TOrder }

constructor TOrder.Create;
begin
  _module.Name := 'sale';

  items := TList<TOrderItem>.Create;

  member := TMember.Create;
  channel := TOrderChannel.Create;
  saletype := TSaletype.Create;
  storage := TStorage.Create;
  card := TCard.Create;
  pic := TEmployee.Create;

  inherited;
end;

destructor TOrder.Destroy;
begin
  if Assigned(items) then items.Free;
  if Assigned(member) then member.Free;
  if Assigned(channel) then channel.Free;
  if Assigned(saletype) then saletype.Free;
  if Assigned(storage) then storage.Free;
  if Assigned(card) then card.Free;
  if Assigned(pic) then pic.Free;
end;

class function TOrder.LoadOrderSum(ARef: string): ROrderSum;
//var
//  response : TStringList;
//  JsonVal : TJsonValue;
//  itemObj, detailItemObj : TJSONObject;
//  JsonArr, detailArr : TJSONArray;
//  url : string;
begin
//  url := 'order/ordersum.json?order_reference=' + ARef;
//  response := DMMain.Rest.Get(url);
//
//  JsonVal := (TJSONObject.ParseJSONValue(response.Text) as TJSONObject).GetValue('row');
//
//  if JsonVal <> nil then
//  begin
//    Result.Number := (JsonVal as TJSONObject).GetValue('order_number').Value;
//  end;
end;

class function TOrder.SeachByCode(ACode: String): TOrder;
//var
//  response : TStringList;
//  JsonVal, itemVal, detailVal, detailItemVal : TJsonValue;
//  itemObj, detailItemObj : TJSONObject;
//  JsonArr, detailArr : TJSONArray;
//  url : string;
//  o : TOrder;
begin
//  url := 'order/searchbycode.json?order_number=' + ACode;
//  response := DMMain.Rest.Get(url);
//
//  JsonVal := TJSONObject.ParseJSONValue(response.Text);
//  JsonVal := (JsonVal as TJSONObject).GetValue('row');
//  itemObj := (JsonVal as TJSONObject);
//
//  Result := TOrder.Create;
//
//  with Result do
//  begin
//    id := itemObj.GetValue('order_id').Value.ToInteger;
//    number := itemObj.GetValue('order_number').Value;
//    dateissued := StrToDateTime(itemObj.GetValue('order_dateissued').Value, TDateTimeUtils.FormatSettings);
//    dateexpired := StrToDateTime(itemObj.GetValue('order_dateexpired').Value, TDateTimeUtils.FormatSettings);
//    status := itemObj.GetValue('order_status').Value.ToInteger;
//    subtotal := JsonToFloat(IfNull(itemObj.GetValue('order_subtotal').Value, '0'));
//    discountpercent := JsonToFloat(IfNull(itemObj.GetValue('order_discountpercent').Value, '0'));
//    discountamount := JsonToFloat(IfNull(itemObj.GetValue('order_discountamount').Value, '0'));
//    creditamount := JsonToFloat(IfNull(itemObj.GetValue('order_creditamount').Value, '0'));
//    deliveryfee := JsonToFloat(IfNull(itemObj.GetValue('order_deliveryfee').Value, '0'));
//    cardfee := JsonToFloat(IfNull(itemObj.GetValue('order_cardfee').Value, '0'));
//    total := JsonToFloat(IfNull(itemObj.GetValue('order_total').Value, '0'));
//    paidbyvoucher := JsonToFloat(IfNull(itemObj.GetValue('order_paidbyvoucher').Value, '0'));
//    paidbyrealisasisp := JsonToFloat(IfNull(itemObj.GetValue('order_paidbyrealisasisp').Value, '0'));
//    paidbycash := JsonToFloat(IfNull(itemObj.GetValue('order_paidbycash').Value, '0'));
//    paidbycash := JsonToFloat(IfNull(itemObj.GetValue('order_paidbysukarela').Value, '0'));
//    iscredit := (itemObj.GetValue('order_iscredit').Value.ToInteger = 1);
//    isstockist := (IfNull(itemObj.GetValue('order_isstockist').Value, '0').ToInteger = 1);
//    cardnumber := itemObj.GetValue('order_cardnumber').Value;
//    cardapprovalnumber := itemObj.GetValue('order_cardapprovalnumber').Value;
//    reference := itemObj.GetValue('order_reference').Value;
//    termcount := IfNull(itemObj.GetValue('order_termcount').Value, '0').ToInteger;
//    termamount := JsonToFloat(IfNull(itemObj.GetValue('order_termamount').Value, '0'));
//    provision := JsonToFloat(IfNull(itemObj.GetValue('order_provision').Value, '0'));
//    interest := JsonToFloat(IfNull(itemObj.GetValue('order_interest').Value, '0'));
//    downpayment := JsonToFloat(IfNull(itemObj.GetValue('order_downpayment').Value, '0'));
//    plafon := JsonToFloat(IfNull(itemObj.GetValue('order_plafon').Value, '0'));
//    member.Id := IfNull(itemObj.GetValue('order_member').Value, '0').ToInteger;
//    member.FirstName := IfNull(itemObj.GetValue('order_member_name').Value, 'CUSTOMER TUNAI');
//    channel.Id := IfNull(itemObj.GetValue('order_channel').Value, '').ToInteger;
//    channel.Name := itemObj.GetValue('order_channel_name').Value;
//    saletype.Id := IfNull(itemObj.GetValue('order_saletype').Value, '0').ToInteger;
//    saletype.Name := itemObj.GetValue('order_saletype_name').Value;
//    storage.Id := IfNull(itemObj.GetValue('order_storage').Value, '0').ToInteger;
//    storage.Name := itemObj.GetValue('order_storage_name').Value;
//    card.Id := IfNull(itemObj.GetValue('order_card').Value, '0').ToInteger;
//    card.Name := itemObj.GetValue('order_card_name').Value;
//    pic.Id := IfNull(itemObj.GetValue('order_pic').Value, '0').ToInteger;
//    pic.FirstName := itemObj.GetValue('order_pic_name').Value;
//  end;
end;

function TOrder.SubmitOrder(AMaster: TSfStringVariantPair;
  AItems: TList<TSfStringVariantPair>): TSfActionResult;
begin
  //
end;

end.
