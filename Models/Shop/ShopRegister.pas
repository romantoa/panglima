unit ShopRegister;

interface

uses
  Classes,
  DB,
  SfCollectionUtils,
  DM_Main,
  Storage,
  JSON,
  SysUtils,
  Employee,
  SfModel;

type
  TRegister = class(TSfModel)
    public
      Id : Integer;
      Code : String;
      Name : String;
      Cashier : TEmployee;
      Storage : TStorage;

      constructor Create;
      destructor Destroy;
  end;

implementation

{ TRegister }

constructor TRegister.Create;
begin
  Storage := TStorage.Create;
  Cashier := TEmployee.Create;
end;

destructor TRegister.Destroy;
begin
  if Assigned(Storage) then
    Storage.Free;
end;

end.

