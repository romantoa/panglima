unit OrderItem;

interface

uses
  Classes,
  Generics.Collections,
  JSON,
  SysUtils,
  DM_Main,
  ProductUnit,
  Product;

type
  TOrderItem = class(TObject)
    public
      id : integer;
      product : TProduct;
      &unit : TProductUnit;
      amount : Currency;
      unitprice : Currency;
      subtotal : Currency;
      discountpercent : Currency;
      discountamount : Currency;
      tax : integer;
      taxamount : Currency;
      total : Currency;

      constructor Create;
      destructor Destroy;
      function ListOrderItem(AOrderId: integer) : TList<TOrderItem>;
  end;

implementation

{ TOrderItem }

constructor TOrderItem.Create;
begin
  Self.product := TProduct.Create;
  Self.&unit := TProductUnit.Create;
end;

destructor TOrderItem.Destroy;
begin
  //
end;

function TOrderItem.ListOrderItem(AOrderId: integer): TList<TOrderItem>;
//var
//  url : string;
//  response : TStringList;
//  responseJson, item, jsonVal : TJsonValue;
//  arr : TJsonArray;
//  itemObj : TJSONObject;
//  orderItem : TOrderItem;
begin
//  Result := TList<TOrderItem>.Create;
//  url := 'order/items.json?order_id='+AOrderId.ToString;
//
//  response := DMMain.Rest.Get(url);
//
//  if response.Count > 0 then
//  begin
//    responseJson := TJSONObject.ParseJSONValue(response.Text);
//    jsonVal := (responseJson as TJSONObject).GetValue('rows');
//    arr := (jsonVal as TJSONArray);
//
//    for item in  arr do
//    begin
//      itemObj := (item as TJSONObject);
//      orderItem := TOrderItem.Create;
//
//      orderItem.id := itemObj.GetValue('order_item_id').Value.ToInteger;
//      orderItem.product.Id := itemObj.GetValue('order_item_product').Value.ToInteger;
//      orderItem.product.Code := itemObj.GetValue('product_code').Value;
//      orderItem.product.Name := itemObj.GetValue('product_name').Value;
//      orderItem.&unit.Id := itemObj.GetValue('order_item_unit').Value.ToInteger;
//      orderItem.&unit.Code := itemObj.GetValue('unit_code').Value;
//      orderItem.amount := JsonToFloat(itemObj.GetValue('order_item_amount').Value);
//      orderItem.unitprice := JsonToFloat(itemObj.GetValue('order_item_unitprice').Value);
//      orderItem.subtotal := JsonToFloat(itemObj.GetValue('order_item_subtotal').Value);
//      orderItem.discountpercent := JsonToFloat(itemObj.GetValue('order_item_discountpercent').Value);
//      orderItem.discountamount := JsonToFloat(itemObj.GetValue('order_item_discountamount').Value);
//      orderItem.tax := itemObj.GetValue('order_item_tax').Value.ToInteger;
//      orderItem.total := JsonToFloat(itemObj.GetValue('order_item_total').Value);
//
//      Result.Add(orderItem);
//    end;
//
//    Result.Pack;
//  end;
end;

end.
