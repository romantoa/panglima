unit User;

interface

uses
  Classes,
  Employee,
  SfModel,
  Uni,
  SysUtils;

type
  RUser = record
    Id : Integer;
    EmployeeId : Integer;
    Username : String;
    Name : String;
    NickName : String;
  end;

type
  TUser = class(TSfModel)
    private
      function GetEmployee: TEmployee;
    public
      FUser : RUser;

      property Employee: TEmployee read GetEmployee;
      property Id: Integer read FUser.Id write FUser.Id;
  end;

implementation

{ TUser }

uses DM_Main;

function TUser.GetEmployee: TEmployee;
var
  q : TUniQuery;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  q.SQL.Add('select * from user_employee where user_id = :userid');
  q.Params.ParamByName('userid').Value := Self.Id;

  try
    try
      q.Connection.Connected := True;
      q.Open;
    except on E: Exception do
      raise;
    end;
  finally
    q.Free;
  end;
end;

end.
