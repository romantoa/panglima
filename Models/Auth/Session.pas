unit Session;

interface

uses
  Classes, User;

type
  TSession = class(TObject)
    public
      User : TUser;

      constructor Create;
      destructor Destroy;
  end;

implementation

{ TSession }

constructor TSession.Create;
begin
  User := TUser.Create;
end;

destructor TSession.Destroy;
begin
  if Assigned(User) then
    User.Free;
end;

end.
