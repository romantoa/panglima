unit F_Login;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxTextEdit, cxGroupBox,
  cxLabel, cxImage, dxGDIPlusClasses, DM_Main,
  dxSkinTheBezier, cxMaskEdit, cxDropDownEdit, SfAuth, SfModel, SfLookupUtils,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TFLogin = class(TForm)
    cxGroupBox1: TcxGroupBox;
    edtUsername: TcxTextEdit;
    edtPassword: TcxTextEdit;
    btnLogin: TcxButton;
    imgPassword: TcxImage;
    imgUsername: TcxImage;
    cxButton1: TcxButton;
    cxImage1: TcxImage;
    cxLabel1: TcxLabel;
    PopupMenu1: TPopupMenu;
    MainServer1: TMenuItem;
    BackupServer1: TMenuItem;
    Development1: TMenuItem;
    N1: TMenuItem;
    Configure1: TMenuItem;
    cxImage2: TcxImage;
    cmbShift: TcxLookupComboBox;
    procedure btnLoginClick(Sender: TObject);
    procedure edtUsernameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPasswordKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    procedure SetLogo;

  public
    LoginResult : RLoginResult;

    procedure CleanUp;
  end;

var
  FLogin: TFLogin;

implementation

{$R *.dfm}

uses Shift;

procedure TFLogin.CleanUp;
begin
  //Clean Up Everything, Don't leave any trace!
  edtUsername.Text := '';
  edtPassword.Text := '';
end;

procedure TFLogin.cxButton1Click(Sender: TObject);
begin
  Close;
//  ModalResult := mrCancel;
//  Application.Terminate;
end;

procedure TFLogin.SetLogo;
begin
//  imgLogo.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'images/logo.png');
end;

procedure TFLogin.btnLoginClick(Sender: TObject);
var
  pswkompas,
  user1,
  nama1,
  shift : string;
  editable : Boolean;
begin
  user1 := uppercase(edtUsername.EditingValue);
  if (user1 ='') or (user1 = NULL) then
  begin
    Beep;
    MessageDlg('Anda belum memasukkan user name, silahkan ulangi lagi!', mtInformation, [mbOk], 0);
    edtUsername.Clear;
    edtPassword.Clear;
    edtUsername.SetFocus;
  end
  else
  begin
    try
      Screen.Cursor := crSQLWait;

      LoginResult := TSfAuth.AttemptLogin(edtUsername.EditValue, edtPassword.EditValue, RShift.Create(cmbShift.EditValue, cmbShift.Text));
      if not LoginResult.Success then
      begin
        if LoginResult.Message = 'user_not_found' then
        begin
          Beep;
          MessageDlg('User '+ edtUsername.EditingValue +
             ' Tidak Ada atau Tidak Aktif. Silahkan Coba Lagi!', mtInformation, [mbOk], 0);
          edtUsername.SetFocus;
        end
        else if LoginResult.Message = 'password_not_correct' then
        begin
          Beep;
          MessageDlg('Password yang Anda Masukkan Salah. Silahkan diulang kembali !',mtInformation,[mbOK],0);
          edtPassword.Clear;
          edtPassword.SetFocus;
        end;
      end
      else
      begin
        DMMain.LoginInfo := LoginResult;

        with DMMain.RegisterInfo do
        begin
          //TODO: Dummy
          Shift := 1;
          StorageId := 2;
        end;

        Self.Close;
        ModalResult := mrOk;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFLogin.edtPasswordKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    btnLogin.SetFocus;
    btnLogin.Click;
  end;
end;

procedure TFLogin.edtUsernameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    edtPassword.SetFocus;
end;

procedure TFLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult <> mrOk then
    Application.Terminate;
end;

procedure TFLogin.FormCreate(Sender: TObject);
begin
  SetLookupProperties(cmbShift, TSfModel.Fetch('shift'));
  {$IFDEF DEBUG}
    edtUsername.Text := 'rohmat';
    edtPassword.Text := '888999';
  {$ENDIF}
  SetLogo;
end;

procedure TFLogin.FormShow(Sender: TObject);
begin
  //TODO: Baca dari setting, cocokkan dengan jam sekarang
  cmbShift.EditValue := 1;
end;

end.
