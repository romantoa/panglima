unit SfUniConnection;

interface

uses
  Classes,
  DBAccess,
  Uni,
  MySqlUniProvider,
  SysUtils,
  System.IniFiles,
  Vcl.Forms,
  SfRegistryConfig,
  SfConfig;

type
  TSfUniConnection = class(TObject)
    class function Connection: TUniConnection;
  end;

implementation

{ TSfUniConnection }

uses DM_Main;

//class function TSfUniConnection.Connection: TUniConnection;
//var
//  seg : string;
//  conf : TSfRegistryConfig;
//begin
//  Result := TUniConnection.Create(nil);
//  conf := TSfRegistryConfig.Create;
//
//  seg := LowerCase(conf.ReadString('_active_server'));
//
//  with Result do
//  begin
//    ProviderName := 'MySQL';
//    Port := 3306;
//    LoginPrompt := False;
//    Server := conf.ReadString('_db_' + seg + '_hostname');
//    Username := conf.ReadString('_db_' + seg + '_username');
//    Password := conf.ReadString('_db_' + seg + '_password');
//    Database := conf.ReadString('_db_' + seg + '_database');
//  end;
//end;

class function TSfUniConnection.Connection: TUniConnection;
type
  RDefault = record
    Server : String;
    Port : Integer;
    Database : String;
    Username : String;
  end;
var
  serverName,serverdb,db,user_db,psw_db : string;
  nPort : Integer;
  iniFile : TIniFile;
  //default : RDefault;
begin
  Result := TUniConnection.Create(nil);

  iniFile := TIniFile.Create(ExtractFilePath(Application.EXEName) + '\database.ini');

  serverName := Trim(iniFile.ReadString('terkini', 'nmserver', 'server lokal'));
  serverdb := Trim(iniFile.ReadString('terkini', 'server', 'localhost'));
  db := Trim(iniFile.ReadString('terkini', 'db', 'dbpanglima'));
  nPort := iniFile.ReadInteger('terkini', 'port', 3306);
  if LowerCase(serverName)='server lokal' then
  begin
    user_db := userdbloc;
    psw_db := pswdbloc;
  end else if LowerCase(serverName)='server pusat' then
  begin
    user_db := userdb;
    psw_db := pswdb;
  end else if LowerCase(serverName)='server backup' then
  begin
    user_db := userdbbu;
    psw_db := pswdbbu;
  end;

  with Result do
  begin
    ProviderName := 'MySQL';
    LoginPrompt := False;

    Server := serverdb;
    Port := nPort;
    Database := db;
    Username := user_db;
    Password := psw_db;
    DMMain.Logger.Debug(serverdb);
    DMMain.Logger.Debug(db);
   // DMMain.Logger.Debug(user_db);
   // DMMain.Logger.Debug(psw_db);
  end;

  iniFile.Free;
end;

end.
