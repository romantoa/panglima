unit F_ServerPicker;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,System.IniFiles,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, dxSkinWhiteprint, Vcl.Menus, cxMaskEdit, cxDropDownEdit,
  cxLabel, Vcl.StdCtrls, cxButtons, cxTextEdit, dxGDIPlusClasses, cxImage,
  cxGroupBox;

type
  TFServerPicker = class(TForm)
    cxGroupBox1: TcxGroupBox;
    cxImage1: TcxImage;
    btnConnect: TcxButton;
    cxLabel1: TcxLabel;
    cmbServer: TcxComboBox;
    PopupMenu1: TPopupMenu;
    MainServer1: TMenuItem;
    BackupServer1: TMenuItem;
    Development1: TMenuItem;
    N1: TMenuItem;
    Configure1: TMenuItem;
    btnCancel: TcxButton;
    procedure btnCancelClick(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FServerPicker: TFServerPicker;

implementation

{$R *.dfm}

procedure TFServerPicker.btnConnectClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOk;
end;

procedure TFServerPicker.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult <> mrOk then
    Application.Terminate;
end;

procedure TFServerPicker.FormCreate(Sender: TObject);
var
  koneksi : TIniFile;
begin
  try
    koneksi := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'database.ini');
    koneksi.ReadSection('konek',cmbServer.Properties.Items);
    cmbServer.Text := koneksi.ReadString('default','server','default');
  finally
    koneksi.Free;
  end;
end;

procedure TFServerPicker.btnCancelClick(Sender: TObject);
begin
  Close;
end;

end.
