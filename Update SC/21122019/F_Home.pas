unit F_Home;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxNavBarPainter,
  dxSkinsdxNavBarAccordionViewPainter, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxRibbonCustomizationForm, dxSkinsdxBarPainter, dxBar, cxClasses, dxRibbon,
  dxNavBar, cxPC, dxSkinscxPCPainter, dxBarBuiltInMenu, dxTabbedMDI,
  Data.DB, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, dxNavBarCollns, dxNavBarBase,
  Vcl.ComCtrls, cxContainer, cxEdit, cxTreeView, System.Generics.Collections,
  dxSkinsForm, cxCheckComboBox, cxBarEditItem, cxGridTableView,
  System.ImageList, Vcl.ImgList, dxNavBarGroupItems, dxSkinTheBezier,
  cxCustomData, cxStyles, cxTL, cxTextEdit, cxTLdxBarBuiltInMenu,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxInplaceContainer,

  SfMenuManager,
  DM_Main,
  Fsf_Edit,
  Fsf_Base,
  Fsf_MdiChild, System.StrUtils, cxGroupBox, dxGDIPlusClasses, cxImage, cxLabel,
  dxStatusBar;

type
  TFHome = class(TForm)
    dxNavBar1: TdxNavBar;
    dxTabbedMDIManager1: TdxTabbedMDIManager;
    grpTransaksi: TdxNavBarGroup;
    grpTransaksiControl: TdxNavBarGroupControl;
    grpDataMaster: TdxNavBarGroup;
    grpDataMasterControl: TdxNavBarGroupControl;
    grpLaporan: TdxNavBarGroup;
    grpLaporanControl: TdxNavBarGroupControl;
    cxDefaultEditStyleController1: TcxDefaultEditStyleController;
    cxGroupBox1: TcxGroupBox;
    cxImage1: TcxImage;
    cxGroupBox2: TcxGroupBox;
    lblUserRealName: TcxLabel;
    lblUsername: TcxLabel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Close1: TMenuItem;
    N1: TMenuItem;
    CloseAll1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    Documentation1: TMenuItem;
    N2: TMenuItem;
    stbHome: TdxStatusBar;
    Preference1: TMenuItem;
    Appearance1: TMenuItem;
    lblShift: TcxLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnShowGroupClick(Sender: TObject);
    procedure SideMenuClick(Sender: TObject);
    procedure dxBarLargeButton1Click(Sender: TObject);
    procedure dxBarLargeButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CloseAll1Click(Sender: TObject);

  private
    _forms: TDictionary<string, TMdiChildType>;

    procedure PopulateSidebar;
    procedure CloseAllOpenForms;

    function FindOpenModule(AName: string) : TFsfMdiChild;
  public
    { Public declarations }
  end;

var
  FHome: TFHome;

implementation

{$R *.dfm}

uses Fsf_Index, F_Login, F_AppConfig;

procedure TFHome.btnShowGroupClick(Sender: TObject);
var
  gvData: TcxGridtableView;
begin
  gvData := TcxGridtableView(Self.ActiveMDIChild.FindComponent('gvData'));
  if gvData <> nil then
  begin
    gvData.OptionsView.GroupByBox := not gvData.OptionsView.GroupByBox;
  end;
end;

procedure TFHome.CloseAll1Click(Sender: TObject);
begin
  CloseAllOpenForms;
end;

procedure TFHome.CloseAllOpenForms;
var
  I : integer;
begin
  for I := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[I] <> Self then
      Screen.Forms[I].Close;
  end;
end;

procedure TFHome.dxBarLargeButton1Click(Sender: TObject);
begin
  CloseAllOpenForms;
end;

procedure TFHome.dxBarLargeButton2Click(Sender: TObject);
begin
  FAppConfig := TFAppConfig.Create(Self);
  FAppConfig.Position := poScreenCenter;
  FAppConfig.Show;
  FAppConfig.Free;
end;

function TFHome.FindOpenModule(AName: string): TFsfMdiChild;
var
  I: Integer;
  x: TFsfMdiChild;
  s: string;
begin
  for I := 0 to Self.MDIChildCount-1 do
  begin
    x := (MDIChildren[I] as TFsfMdiChild);
    s := x.pg.Pages[0].Controls[0].Name;
    if 'T'+s = AName then
      Result := x;
  end;
end;

procedure TFHome.FormCreate(Sender: TObject);
begin
  if not DMMain.Terminated then
  begin
    Self.Caption := DMMain.Lang.LBL('LBL_APP_NAME');

    _forms := TDictionary<string, TMdiChildType>.Create;

    PopulateSidebar;
  end;
end;

procedure TFHome.FormShow(Sender: TObject);
begin
  lblUserRealName.Caption := DMMain.LoginInfo.User.Name;
  lblUsername.Caption := DMMain.LoginInfo.User.Username;
  lblShift.Caption := DMMain.LoginInfo.Shift;

  stbHome.Panels[0].Text := DMMain.IniFile.ReadString('terkini', 'nmserver', 'default');
  stbHome.Panels[1].Text := 'Connected';

  Self.Caption := nmPersh;

end;

procedure TFHome.PopulateSidebar;
var
  mm: TSfMenuManager;
  tl, tr, lp: TcxTreeList;
begin
  // Data Master
  mm := TSfMenuManager.Create(nil);
  tl := mm.ConstructMenu('001');
  tl.Parent := grpDataMasterControl;
  tl.Columns[1].Width := tl.Parent.Width;
  tl.OnClick := SideMenuClick;

  tr := mm.ConstructMenu('002');
  tr.Parent := grpTransaksiControl;
  tr.Columns[1].Width := tr.Parent.Width;
  tr.OnClick := SideMenuClick;

  lp := mm.ConstructMenu('003');
  lp.Parent := grpLaporanControl;
  lp.Columns[1].Width := lp.Parent.Width;
  lp.OnClick := SideMenuClick;
end;

procedure TFHome.SideMenuClick(Sender: TObject);
var
  LBL, code, form: string;
  lvl, I, J: integer;
  node: TcxTreeListNode;

  fc: TFsfBaseType;
  f: TFsfBase;
  c: TClass;
  mdiWrapper : TFsfMdiChild;
  ts : TcxTabSheet;
  x : TFsfMdiChild;
  ActionRibbon, AModuleRibbon : TdxRibbon;
  ARibbonContext : TdxRibbonContext;
  ARibbonTab : TdxRibbonTab;
  ARibbonTabGroup : TdxRibbonTabGroup;
begin
  node := TcxTreeList(Sender).Selections[0];
  code := TcxTreeList(Sender).Columns[0].Values[node];
  LBL := TcxTreeList(Sender).Columns[1].Values[node];
  lvl := TcxTreeList(Sender).Columns[2].Values[node];
  form := TcxTreeList(Sender).Columns[3].Values[node];

  if lvl > 1 then
  begin
    if form <> '' then
    begin
      x := FindOpenModule(form);
      if x <> nil then
      begin
        x.BringToFront;
      end
      else
      begin
        mdiWrapper := TFsfMdiChild.Create(Application);
        ts := mdiWrapper.FindComponent('__index') as TcxTabSheet;

        c := FindClass(form);
        fc := TFsfBaseType(c);
        f := fc.Create(Self);

        f.Caption := DMMain.Lang.LBL('LBL_MOD_' + f._module.Name);
        ts.Caption := 'Daftar ' + f.Caption;

        mdiWrapper.Caption := f.Caption;

        f.Parent := ts;

        AModuleRibbon := mdiWrapper.ModuleRibbon;
        AModuleRibbon.BeginUpdate;
        ActionRibbon := f.ActionRibbon;

        if ActionRibbon <> nil then
        begin
          ARibbonContext := AModuleRibbon.Contexts.Add;
          ARibbonContext.Visible := False;
          ARibbonContext.caption := f.Caption;

          for I := 0 to ActionRibbon.Tabs.Count - 1 do
          begin
            ARibbonTab := AModuleRibbon.Tabs.Add;
            ARibbonTab.caption := ActionRibbon.Tabs.Items[I].caption;
            ARibbonTab.Context := ARibbonContext;

            for J := 0 to ActionRibbon.Tabs.Items[I].Groups.Count - 1 do
            begin
              if ActionRibbon.Tabs.Items[I].Groups.Items[J].Visible then
              begin
                ARibbonTabGroup := ARibbonTab.Groups.Add;
                ARibbonTabGroup.ToolBar := AModuleRibbon.BarManager.Bars.Add;
                ARibbonTabGroup.ToolBar.caption := ActionRibbon.Tabs.Items[I].Groups.Items[J].caption;
                ARibbonTabGroup.ToolBar.Visible := True;
                ARibbonTabGroup.caption := ActionRibbon.Tabs.Items[I].Groups.Items[J].caption;
                ARibbonTabGroup.ToolBar.Merge(ActionRibbon.Tabs.Items[I].Groups.Items[J].ToolBar);
              end;
            end;
          end;

          ARibbonContext.Visible := True;
          ARibbonContext.Activate;
          f.ActionRibbon.Free;
        end;

        AModuleRibbon.EndUpdate;

        f.WindowState := wsMaximized;
        f.BorderStyle := bsNone;
        f.Top := 0;
        f.Left := 0;
        f.Width := f.Parent.Width;
        f.Height := f.Parent.Height;
        f.Update;
      end;
    end;
  end;
end;

end.
