unit SfDatConfig;

interface

uses
  Classes,
  Generics.Collections,
  Uni,
  SysUtils,
  SfConfig,
  Forms;

type
  TSfDatConfig = class(TObject)
    private
      _datFileName : String;
      _conf : TSfConfigList;

      function FetchConfig : TSfConfigList;
    public
      function Read(AName: String) : TSfConfig;
      function ReadString(AName: String; ADefault: String = '') : String;

      constructor Create(ADatFileName: String; AAutoPath: Boolean = true);
  end;

const
  abc: array[1..66] of string=('m','o','y','n','8','U','N','0','c','L','E','b','Z','M','w','j','k','X','f','R',
  'O','6','C','v','5','a','D','s','9','i','4','d','W','H','K','1','u','g','x','7','J','T','t','B','2','S',
  'V','q','p','F','h','z','Q','Y','P','e','r','l','A','I','G','3',' ','-',',','@');

  acak: array[1..66] of string=('h','z','#','>','$','o','9','l','Z','J','L','7',']','g','T','y','{','S','[','X',
  'P','u','c','&','w','d','i','v','(','8','n','0',')','F','1','G','%','}','q','4','b','W','f','j','e','p',
  '6','<','s','N','m','x','a','R','K','3','t','2','5','k','C','r','/','?','_',',');

implementation

uses
  DM_Main;


{ TSfConfig }

constructor TSfDatConfig.Create(ADatFileName: String; AAutoPath: Boolean);
begin
  if not AAutoPath then
    _datFileName := ADatFileName
  else
    _datFileName := ExtractFilePath(Application.ExeName) + ADatFileName;

  _conf := FetchConfig;
end;

function TSfDatConfig.FetchConfig: TSfConfigList;
type
  RecPersh = record
    Nama : string[60];
    Alamat : string[150];
    Psw : string[20];
    UserDb : string[20];
    PswPusat : string[20];
    UserDbPusat : string[20];
    PswBackup : string[20];
    UserDbBackup : string[20];
    UserName : string[20];
    Login : string[20];
    Shift : string[6];
    LoginSvr : string[20];
    PswSvr : string[20];
    Data1 : string[20];
    Data2 : string[20];
//    AppName : string[20];
  end;
var
  Persh : RecPersh;
  FilePersh : file of RecPersh;
  namafile, hrf, hsl,
  fileimg : string;
  I, X, posisi : Integer;
begin
  Result := TSfConfigList.Create;

  namafile := _datFileName;

  AssignFile(Filepersh, namafile);

  if FileExists(namafile) then
  begin
    Reset(Filepersh);

    if not Eof(Filepersh) then
    begin
      posisi := 0;

      Seek(Filepersh, posisi);
      System.Read(Filepersh, Persh);

      hsl:='';

      hrf := Persh.Nama;
      for I := 0 to Length(hrf) do
      begin
        for X := 1 to 66 do
        begin
          if hrf[I]=acak[X] then
            hsl:=hsl+abc[X];
        end;
      end;
      Result.Add('company_name', TSfConfig.Create('company_name', hsl));
      Result.Add('application_name', TSfConfig.Create('application_name', 'PFACTORYSTAR 1.1'));
      nmPersh := hsl;

      hsl:='';
      hrf := Persh.PswPusat;
      for I := 0 to Length(hrf) do
        begin
        for X := 1 to 66 do
        begin
          if hrf[I]=acak[X] then
            hsl:=hsl+abc[X];
        end;
      end;
      pswdb:= hsl;

      Result.Add('pusat_dbpassword', TSfConfig.Create('pusat_password', hsl));
      Result.Add('pusat_password', TSfConfig.Create('pusat_password', hsl));

      hsl:='';
      hrf := Persh.UserDbPusat;
      for I := 0 to Length(hrf) do
      begin
        for X := 1 to 66 do
        begin
          if hrf[I]=acak[X] then
            hsl:=hsl+abc[X];
        end;
      end;
      userdb:= hsl;
      Result.Add('pusat_dbuser', TSfConfig.Create('pusat_dbuser', hsl));

      hsl:='';
      hrf := Persh.UserDb;
      for I := 0 to Length(hrf) do
      begin
        for X := 1 to 66 do
        begin
          if hrf[I]=acak[X] then
            hsl:=hsl+abc[X];
        end;
      end;
      userdbloc:= hsl;
      Result.Add('lokal_dbuser', TSfConfig.Create('lokal_dbuser', hsl));

      hsl:='';
      hrf := Persh.Psw;
      for I := 0 to Length(hrf) do
      begin
        for X := 1 to 66 do
        begin
          if hrf[I]=acak[X] then
            hsl:=hsl+abc[X];
        end;
      end;
      pswdbloc:= hsl;
      Result.Add('lokal_dbpassword', TSfConfig.Create('lokal_dbpassword', hsl));

      hsl:='';
      hrf := Persh.UserDbBackup;
      for I := 0 to Length(hrf) do
      begin
        for X := 1 to 66 do
        begin
          if hrf[I]=acak[X] then
            hsl:=hsl+abc[X];
        end;
      end;
      userdbbu:= hsl;
      Result.Add('backup_dbuser', TSfConfig.Create('backup_dbuser', hsl));

      hsl:='';
      hrf := Persh.PswBackup;
      for I := 0 to Length(hrf) do
      begin
        for X := 1 to 66 do
        begin
          if hrf[I]=acak[X] then
            hsl:=hsl+abc[X];
        end;
      end;
      pswdbbu:= hsl;
      Result.Add('backup_dbpassword', TSfConfig.Create('backup_dbpassword', hsl));
    end;
  end;
end;

function TSfDatConfig.Read(AName: String): TSfConfig;
begin
  AName := LowerCase(AName);

  if _conf.ContainsKey(AName) then
    Result := _conf[AName]
  else
    Result := nil;
end;

function TSfDatConfig.ReadString(AName, ADefault: String): String;
var
  conf : TSfConfig;
begin
  conf := Self.Read(AName);

  if conf = nil then
    Result := ADefault
  else
    Result := conf.Value;
end;

end.
