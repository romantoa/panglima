unit DM_Main;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  dxSkinsCore, dxSkinDevExpressStyle, dxSkinOffice2010Blue,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVS2010,
  cxClasses, cxLookAndFeels, dxSkinSharp, dxSkinSharpPlus, Generics.Collections,

  Forms,
  SfUniConnection,
  SfLanguage,
  SfConfig,
  User,
  Session,
  Logable,
  Configable,
  CsLogger,
//  FileLogger,
  System.ImageList, Vcl.ImgList, Vcl.Controls, cxGraphics,
  dxSkinTheBezier, cxImageList, cxLookAndFeelPainters, dxAlertWindow,
  SfRegistryConfig, dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinMoneyTwins, dxSkinOffice2010Black, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Light,
  SfAuth, System.IniFiles, SfDatConfig;
//  Fsf_MdiChild,
//  Fsf_ModalEdit;

//type
//  TMdiFormList = TDictionary<string, TMdiChildType>;

type
  TModifState = ( msInsert = 0, msEdit = 1, msView = 2 );
  TPrintMode = ( pmDirect, pmPreview );
  TGuidType = (gt8, gt12, gt32);
  TPaymentStatus = ( pmsDraft, pmsPaid );

  TRegisterInfo = record
    Shift : integer;
    WarehouseId : integer;
  end;

type
  TDMMain = class(TDataModule)
    ilDev32: TcxImageList;
    ilDev16: TcxImageList;
    alrMain: TdxAlertWindowManager;
    ilFlat32: TcxImageList;
    cxLookAndFeelController1: TcxLookAndFeelController;
    procedure DataModuleCreate(Sender: TObject);
  private
    _lang : TSfLanguage;
    _conn : TSfUniConnection;
//    _appConf : TSfConfig;
//    _logger : TFileLogger;
    _logger : ILogable;
    _session : TSession;
    _sysConf : IConfigable;
    _datConf : TSfDatConfig;

    function GetSession: TSession;
    procedure SetSession(const Value: TSession);
    function GetServerDate: TDateTime;

  public
    LoginInfo : RLoginResult;
    Terminated : Boolean;

    RegisterInfo : TRegisterInfo;
    IniFile : TIniFile;

    property Conn : TSfUniConnection read _conn;
    property Lang : TSfLanguage read _lang;
//    property AppConf : TSfConfig read _appConf;
//    property SysConf: IConfigable read _sysConf;
    property DatConf: TSfDatConfig read _datConf;

    property Logger: ILogable read _logger;
    property Session: TSession read GetSession write SetSession;
    property ServerDate: TDateTime read GetServerDate;
    property Alert: TdxAlertWindowManager read alrMain;
  end;

var
  DMMain: TDMMain;
  nPortpu,nPortbu,nPortloc,nPortlat: Integer;
  db_pu, db_lat, db_loc,db_bu,serverlokal, serverpusat,backupserver, serverlat,
  pswdb,userdb,userdbloc,pswdbloc,userdbbu,pswdbbu,nmPersh : string;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses F_Login, F_ServerPicker;

{$R *.dfm}

{ TDMMain }

procedure TDMMain.DataModuleCreate(Sender: TObject);
begin
  try
    IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'database.ini');

    db_pu := IniFile.ReadString('database', 'pusat', 'default');
    db_lat := IniFile.ReadString('database', 'latihan', 'default');
    db_loc := IniFile.ReadString('database', 'lokal', 'default');
    db_bu := IniFile.ReadString('database', 'backup', 'default');
    serverlokal := IniFile.ReadString('Server', 'lokal', 'default');
    serverpusat := IniFile.ReadString('Server', 'pusat', 'default');
    backupserver := IniFile.ReadString('Server', 'backup', 'default');
    serverlat := IniFile.ReadString('Server', 'latihan', 'default');
    nPortloc :=  IniFile.ReadInteger('port', 'lokal', 3306);
    nPortbu :=  IniFile.ReadInteger('port', 'backup', 3306);
    nPortpu :=  IniFile.ReadInteger('port', 'pusat', 3306);
    nPortlat :=  IniFile.ReadInteger('port', 'latihan', 3306);

    _logger := TCsLogger.Create;
    _sysConf := TSfRegistryConfig.Create(_logger);
    _datConf := TSfDatConfig.Create('Persh.dat');

    FServerPicker := TFServerPicker.Create(Application);
    FServerPicker.ShowModal;
    if FServerPicker.ModalResult = mrOk then
    begin
      IniFile.WriteString('terkini', 'nmserver', LowerCase(FServerPicker.cmbServer.Text));
      if LowerCase(FServerPicker.cmbServer.Text)='server lokal' then
      begin
        IniFile.WriteString('terkini', 'server',serverlokal);
        IniFile.WriteString('terkini', 'db',db_loc);
        IniFile.WriteInteger('terkini', 'port',nPortloc);
      end else if LowerCase(FServerPicker.cmbServer.Text)= 'server pusat' then
      begin
        IniFile.WriteString('terkini', 'server',serverpusat);
        IniFile.WriteString('terkini', 'db',db_pu);
        IniFile.WriteInteger('terkini', 'port',nPortpu);
      end else if LowerCase(FServerPicker.cmbServer.Text)= 'server backup' then
      begin
        IniFile.WriteString('terkini', 'server',backupserver);
        IniFile.WriteString('terkini', 'db',db_bu);
        IniFile.WriteInteger('terkini', 'port',nPortbu);
      end else if LowerCase(FServerPicker.cmbServer.Text)= 'server latihan' then
      begin
        IniFile.WriteString('terkini', 'server',serverlat);
        IniFile.WriteString('terkini', 'db',db_lat);
        IniFile.WriteInteger('terkini', 'port',nPortlat);
      end;

      _conn := TSfUniConnection.Create;

      FLogin := TFLogin.Create(Application);
      FLogin.Position := poScreenCenter;
      FLogin.ShowModal;
      if FLogin.ModalResult = mrOk then
      begin
        //TODO: Lang Code should be fetched from config
        _lang := TSfLanguage.Create('ID');
  //      _appConf := TSfConfig.Create;
        _session := TSession.Create;
        // Dummy
        _session.User.Id := 1;
      end
      else
        Terminated := True;
      FLogin.Free;
    end
    else
      Terminated := True;
    FServerPicker.Free;
  finally
    //iniFile.Free;
  end;
end;

function TDMMain.GetServerDate: TDateTime;
begin
  //Dummy
  Result := Date;
end;

function TDMMain.GetSession: TSession;
begin
  Result := _session;
end;

procedure TDMMain.SetSession(const Value: TSession);
begin
  _session := Value;
end;

end.
