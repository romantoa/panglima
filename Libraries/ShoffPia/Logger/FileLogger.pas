unit FileLogger;

interface

uses
  Classes, Logable, SysUtils, Forms;

type
  TFileLogger = class(TInterfacedObject, ILogable)
  private
    procedure CreateLogFile;
    procedure WriteLog(AMessage: String);
  public
    procedure Error(Msg : string; Msg2 : string = '');
    procedure Debug(Msg : string; Msg2 : string = '');
  end;

const
  BreakingLine = '//----------------------------------------------------------------------------//';

implementation

{ TFileLogger }

procedure TFileLogger.CreateLogFile;
var
  F : TextFile;
  FN : String;
begin
  // Getting the filename for the logfile (In this case the Filename is 'application-exename.log'
  FN := ChangeFileExt(Application.Exename, '.log');
  // Assigns Filename to variable F
  AssignFile(F, FN);
  // Rewrites the file F
  Rewrite(F);
  // Open file for appending
  Append(F);
  // Write text to Textfile F
  WriteLn(F, BreakingLine);
  WriteLn(F, 'This Logfile was created on ' + DateTimeToStr(Now));
  WriteLn(F, BreakingLine);
  WriteLn(F, '');
  // finally close the file
  CloseFile(F);
end;

procedure TFileLogger.Debug(Msg, Msg2: string);
begin
  if Msg2 <> '' then
    Msg := '[' + Msg2 + '] ' + Msg;

  WriteLog('[DEBUG] ' + Msg);
end;

procedure TFileLogger.Error(Msg, Msg2: string);
begin
  if Msg2 <> '' then
    Msg := '[' + Msg2 + '] ' + Msg;

  WriteLog('[ERROR] ' + Msg);
end;

procedure TFileLogger.WriteLog(AMessage: String);
var
  F : TextFile;
  FN : String;
begin
  // Getting the filename for the logfile (In this case the Filename is 'application-exename.log'
  FN := ChangeFileExt(Application.Exename, '.log');

  //Checking for file
  if (not FileExists(FN)) then
  begin
    // if file is not available then create a new file
    CreateLogFile;
  end;

  // Assigns Filename to variable F
  AssignFile(F, FN);
  // start appending text
  Append(F);
  //Write a new line with current date and message to the file
  WriteLn(F, DateTimeToStr(Now) + ': ' + AMessage);
  // Close file
  CloseFile(F)
end;

end.
