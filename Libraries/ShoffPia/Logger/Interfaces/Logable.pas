unit Logable;

interface

uses
  Classes;

type
  ILogable = interface
    procedure Error(Msg: string; Msg2: string = '');
    procedure Debug(Msg: string; Msg2: string = '');
end;

implementation

end.
