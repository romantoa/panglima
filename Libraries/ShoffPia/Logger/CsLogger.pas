unit CsLogger;

interface

uses
  Classes, Logable, CodeSiteLogging;

type
  TCsLogger = class(TInterfacedObject, ILogable)
    procedure Error(Msg : string; Msg2 : string = '');
    procedure Debug(Msg : string; Msg2 : string = '');
  end;

implementation

{ TLogger }

procedure TCsLogger.Debug(Msg : string; Msg2 : string = '');
begin
  if Msg2 <> '' then
    CodeSite.Send(Msg, Msg2)
  else
    CodeSite.Send(Msg);
end;

procedure TCsLogger.Error(Msg : string; Msg2 : string = '');
begin
  if Msg2 <> '' then
    CodeSite.SendError(Msg, [Msg2])
  else
    CodeSite.SendError(Msg);
end;

end.
