unit SfUniConnection;

interface

uses
  Classes,
  DBAccess,
  Uni,
  MySqlUniProvider,
  SysUtils,
  System.IniFiles,
  Vcl.Forms;

type
  TSfUniConnection = class(TObject)
    class function Connection: TUniConnection;
  end;

implementation

{ TSfUniConnection }

uses DM_Main;

class function TSfUniConnection.Connection: TUniConnection;
type
  RDefault = record
    Server : String;
    Port : Integer;
    Database : String;
    Username : String;
  end;
var
  serverName : string;
  iniFile : TIniFile;
  default : RDefault;
begin
  Result := TUniConnection.Create(nil);

  iniFile := TIniFile.Create(ExtractFilePath(Application.EXEName) + '\database.ini');

  serverName := iniFile.ReadString('terkini', 'nmserver', 'default');
  serverName := StringReplace(serverName, 'server', '', [rfReplaceAll]);
  serverName := Trim(serverName);

  default.Server := iniFile.ReadString('default', 'server', '');
  default.Port := iniFile.ReadInteger('default', 'port', 0);
  default.Database := iniFile.ReadString('default', 'db', '');
  default.Username := iniFile.ReadString('default', 'username', '');

  with Result do
  begin
    ProviderName := 'MySQL';
    LoginPrompt := False;

    Server := iniFile.ReadString('server', serverName, default.Server);
    Port := iniFile.ReadInteger('port', serverName, default.Port);
    Database := iniFile.ReadString('database', serverName, default.Database);
    Username := DMMain.DatConf.ReadString(serverName + '_dbuser', default.Username);
    Password := DMMain.DatConf.ReadString(serverName + '_dbpassword', '');
  end;

  iniFile.Free;
end;

end.
