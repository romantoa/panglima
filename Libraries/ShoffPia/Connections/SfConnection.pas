unit SfConnection;

interface

uses
  Classes,
  FireDAC.Comp.Client;

type
  TSfConnection = class(TObject)
    class function Connection : TFDConnection;
  end;

implementation

{ TShfConnection }

class function TSfConnection.Connection: TFDConnection;
begin
  Result := TFDConnection.Create(nil);

  with Result do
  begin
    DriverName := 'MySQL';
    LoginPrompt := False;

    Params.Clear;
    Params.Values['Server'] := 'localhost';
    Params.Values['Database'] := 'accounting';
    Params.Values['DriverId'] := 'MySQL';
    Params.Values['User_Name'] := 'root';
  end;
end;

end.
