unit SfMenuManager;

interface

uses
  Classes,
  DB,
  DM_Main,
  SfUniConnection,
  cxTL,
  cxControls,
  Vcl.Controls,
  Vcl.StdCtrls,
  System.SysUtils,
  Uni,
  DBAccess;

type
  TSfMenuManager = class(TComponent)
    public
      function GetMenuList(ARootCode: String) : TDataSet;
      function ConstructMenu(ARootCode: String) : TcxTreeList;
  end;

implementation

{ TSfMenuManager }

function TSfMenuManager.ConstructMenu(ARootCode: String): TcxTreeList;
var
  tl : TcxTreeList;
  cl : TcxTreeListColumn;
  node, parent : TcxTreeListNode;
  I: Integer;
  ds : TDataSet;
  parentCode : String;
begin
  tl := TcxTreeList.Create(nil);
//  tl.Parent := grpDataMasterControl;
  tl.Align := alClient;
  tl.OptionsView.ScrollBars := ssVertical;
  tl.OptionsView.Headers := False;
  tl.OptionsData.Editing := False;
  tl.OptionsData.Deleting := False;
  tl.BorderStyle := cxcbsNone;
  tl.LookAndFeel.NativeStyle := True;

  tl.Bands.Add;

  // Code
  cl := tl.CreateColumn(tl.Bands[0]);
  cl.Visible := False;
  cl.DataBinding.ValueType := 'String';

  // Caption
  cl := tl.CreateColumn(tl.Bands[0]);
  cl.Visible := True;
  cl.DataBinding.ValueType := 'String';

  // Level
  cl := tl.CreateColumn(tl.Bands[0]);
  cl.Visible := False;
  cl.DataBinding.ValueType := 'Integer';

  // Form
  cl := tl.CreateColumn(tl.Bands[0]);
  cl.Visible := False;
  cl.DataBinding.ValueType := 'String';

  ds := GetMenuList(ARootCode);
  I := 0 ;

  while not ds.Eof do
  begin
    if (ds.FieldByName('parent_id').IsNull) or
      (ds.FieldByName('parent_id').AsInteger = 0) or
      (ds.FieldByName('level').AsInteger = 1) then
    begin
      node := tl.Root.AddChild;
    end
    else
    begin
      parentCode := ds.FieldByName('code').AsString;
      parentCode := parentCode.Substring(0, parentCode.Length-4);
      parent := tl.FindNodeByText(parentCode, tl.Columns[0], nil, False, True, False, tlfmExact, nil, False);

      if (ds.FieldByName('level').AsInteger = 1) or (parent = nil) then
        node := tl.Root.AddChild
      else
      begin
        node := parent.AddChild;
        parent := nil;
      end;
    end;

    node.Values[0] := ds.FieldByName('code').AsString;
    node.Values[1] := ds.FieldByName('label').AsString;
    node.Values[2] := ds.FieldByName('level').AsInteger;
    node.Values[3] := ds.FieldByName('form').AsString;

    ds.Next;
  end;
  tl.Root.Expand(True);
  Result := tl;
end;

function TSfMenuManager.GetMenuList(ARootCode: String): TDataSet;
//var
//  q : TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  (Result as TUniQuery).Connection := DMMain.Conn.Connection;

  try
    try
      (Result as TUniQuery).SQL.Text := 'select * from sys_menus where level > 0 and active = 1 and code like ' +
                      QuotedStr(ARootCode+'%') + ' order by level, code';
      (Result as TUniQuery).Connection.Connected := True;
      (Result as TUniQuery).Open;
    except
      raise;
    end;
  finally
//    q.Connection.Free;
//    q.Free;
  end;
end;

end.
