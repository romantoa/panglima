unit SfModuleManager;

interface

uses
  Classes,
  StrUtils,
  Generics.Collections,
  Uni, DBAccess,
//  DM_Main,
  Configable,
  System.SysUtils;

type
  TSfModuleFields = record
      Id: integer;
      Name: string;
      Caption: string;
      Width: Variant;
      FieldType: String;
      IsFilter: Boolean;
  end;

  TSfModuleFieldArray = array of TSfModuleFields;
  TSfSequenceResetter = (sqrNever, sqrYearly, sqrMonthly);
  TSfSequenceSetting = record
    Name: String;
    Prefix: String;
    Length: Integer;
    Padder: String;
    ResetAt: TSfSequenceResetter;
  end;

  TSfModule = record
    Id: integer;
    Name: string;
    Caption: string;
    Table: string;
    Fields: TSfModuleFieldArray;
    SequenceSetting: TSfSequenceSetting;
    Childs: TList<TSfModule>;
  end;

  TSfModuleManager = class(TObject)
    private
      function GetTableName: string;
    protected

    public
      property TableName: string read GetTableName;
      class function GetModuleProperties(AModuleName: string) : TSfModule;
      class function GetModuleFields(AModuleId: integer) : TSfModuleFieldArray;
      class function GetModuleChilds(AModuleId: integer) : TList<TSfModule>;
      class function GetSequenceSetting(AModule: TSfModule) : TSfSequenceSetting;
  end;

implementation

{ TSfModuleManager }

uses DM_Main;

class function TSfModuleManager.GetModuleChilds(
  AModuleId: integer): TList<TSfModule>;
var
  q : TUniQuery;
  m : TSfModule;
  i : integer;
begin
  Result := TList<TSfModule>.Create;
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  try
    try
      q.SQL.Add('SELECT st.*, m.name as module_name');
      q.SQL.Add('FROM sys_module_structures as st');
      q.SQL.Add('JOIN sys_modules as m on m.id = st.module_id');
      q.SQL.Add('WHERE st.parent_id = :module_id');

      q.Params.ParamByName('module_id').Value := AModuleId;
      q.Connection.Connected := True;
      q.Open;

      while not q.Eof do
      begin
        m := TSfModuleManager.GetModuleProperties(q.FieldByName('module_name').AsString);
        Result.Add(m);
        q.Next;
      end;
    except
      raise;
    end;
  finally
    q.Connection.Free;
    q.Free;
  end;
end;

class function TSfModuleManager.GetModuleFields(AModuleId: integer): TSfModuleFieldArray;
var
  f : TSfModuleFields;
  q : TUniQuery;
  i : integer;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  try
    try
      q.SQL.Add('select mf.*, ifnull(t.value, mf.caption) as caption2 from sys_module_fields as mf');
      q.SQL.Add('left join sys_translations as t on t.key = mf.caption and t.language = :language');
      q.SQL.Add('where mf.module_id = :module_id');
      q.SQL.Add('order by mf.id');
      q.Params.ParamByName('module_id').Value := AModuleId;
      q.Params.ParamByName('language').Value := 1;
      q.Connection.Connected := True;
      q.Open;

      i := 0;
      SetLength(Result,q.RecordCount);

      while not q.Eof do
      begin
        if LeftStr(f.Name,1) <> '_' then
        begin
          f.Id := q.FieldByName('Id').AsInteger;
          f.Name := q.FieldByName('name').AsString;
          f.Caption := q.FieldByName('caption2').AsString;
          f.Width := q.FieldByName('width').Value;
          f.FieldType := q.FieldByName('field_type').AsString;
          f.IsFilter := (q.FieldByName('is_filter').AsInteger = 1);
          Result[i] := f;
          Inc(i);
        end;

        q.Next;
      end;
    except
      raise;
    end;
  finally
    q.Free;
    q.Connection.Free;
  end;
end;

class function TSfModuleManager.GetModuleProperties(
  AModuleName: string): TSfModule;
var
  q : TUniQuery;
begin
  q := TUniQuery.Create(nil);

  q.Connection := DMMain.Conn.Connection;

  try
    try
      q.SQL.Text := 'select m.*, t.name as table_name from sys_modules m'
                    + ' join sys_tables t on t.id = m.table_id'
                    + ' where m.name = :name';
      q.Params.ParamByName('name').Value := AModuleName;
      q.Connection.Connected := True;

      q.Open;

      Result.Id := q.FieldByName('id').AsInteger;
      Result.Name := q.FieldByName('name').AsString;
      Result.Caption := q.FieldByName('caption').AsString;
      Result.Table := q.FieldByName('table_name').AsString;
      Result.SequenceSetting.Prefix := q.FieldByName('prefix').AsString;
      Result.Fields := TSfModuleManager.GetModuleFields(Result.Id);
      Result.Childs := TSfModuleManager.GetModuleChilds(Result.Id);
//      Result.SequenceSetting := GetSequenceSetting(Result);
    except
      raise;
    end;
  finally
    q.Connection.Free;
    q.Free;
  end;
end;

class function TSfModuleManager.GetSequenceSetting(
  AModule: TSfModule): TSfSequenceSetting;
var
  q : TUniQuery;
  I: Integer;
begin
  q := TUniQuery.Create(nil);

  q.Connection := DMMain.Conn.Connection;

  try
    try
      q.SQL.Add('SELECT max(src.prefix) as prefix, max(src.length) as length, max(src.padder) as padder, max(src.resetat) as resetat');
      q.SQL.Add('FROM (');
      q.SQL.Add('  SELECT');
      q.SQL.Add('    case `key` when :pr then value else '''' end as prefix,');
      q.SQL.Add('    case `key` when :ln then value else -1 end as length,');
      q.SQL.Add('    case `key` when :pd then value else '''' end as padder,');
      q.SQL.Add('    case `key` when :rs then value else -1 end as resetat');
      q.SQL.Add('  FROM'); 
      q.SQL.Add('    sys_configs'); 
      q.SQL.Add('  WHERE `key` in (');
      q.SQL.Add('    ''_stockin_sequence_prefix'','); 
      q.SQL.Add('    ''_stockin_sequence_length'','); 
      q.SQL.Add('    ''_stockin_sequence_padder'','); 
      q.SQL.Add('    ''_stockin_sequence_resetat'')');
      q.SQL.Add('  ) as src');

      q.Params.ParamByName('pr').Value := '_' + AModule.Table + '_sequence_prefix';
      q.Params.ParamByName('ln').Value := '_' + AModule.Table + '_sequence_length';
      q.Params.ParamByName('pd').Value := '_' + AModule.Table + '_sequence_padder';
      q.Params.ParamByName('rs').Value := '_' + AModule.Table + '_sequence_resetat';
      q.Connection.Connected := True;
      DMMain.Logger.Debug(q.SQL.Text);
      q.Open;

      for I := 0 to Length(AModule.Fields)-1 do
      begin
        if AModule.Fields[I].FieldType = 'AUTO' then
        begin
          Result.Name := AModule.Fields[I].Name;
          Break;
        end;
      end;

      Result.Prefix := q.FieldByName('prefix').AsString; 
      Result.Length := q.FieldByName('length').AsInteger;
      Result.Padder := q.FieldByName('padder').AsString;
      Result.ResetAt := TSfSequenceResetter(q.FieldByName('resetat').AsInteger);
    except
      raise;
    end;
  finally
    q.Connection.Free;
    q.Free;
  end;  
end;

function TSfModuleManager.GetTableName: string;
begin

end;

end.
