unit FrReport;

interface

uses
  Classes,
  Reportable,
  Vcl.Dialogs;

type
  TFrReport = class(TInterfacedObject, IReportable)
    public
      procedure Design(AName: String);
      procedure Preview(AName: String);
  end;

implementation

{ TFastreportReport }

procedure TFrReport.Design(AName: String);
begin
  //
end;

procedure TFrReport.Preview(AName: String);
begin
  ShowMessage('PREVIEW : '+ AName);
end;

end.
