unit SfConfig;

interface

uses
  Classes, System.Generics.Collections;

type
  TSfConfig = class(TObject)
  public
    Id : Integer;
    Key : String;
    Value : String;
    DataType : String;

    constructor Create(AId: integer; AKey, AValue, ADataType: String); overload;
    constructor Create(AKey, AValue : String); overload;
  end;

type
  TSfConfigList = TDictionary<String, TSfConfig>;

implementation

{ TSfConfig }

constructor TSfConfig.Create(AId: integer; AKey, AValue, ADataType: String);
begin
  Id := AId;
  Key := AKey;
  Value := AValue;
  DataType := ADataType;
end;

constructor TSfConfig.Create(AKey, AValue: String);
begin
  Id := 0;
  Key := AKey;
  Value := AValue;
  DataType := 'STR';
end;

end.

