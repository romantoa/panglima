unit SfIniConfig;

interface

uses
  Classes, SysUtils, IniFiles, Configable, Registry, Windows, StrUtils,
  SfStringUtils, Forms;

type
  TSfIniConfig = class(TIniFile, IInterface, IConfigable)
  private
    FRefCount : Integer;
    _iniFile : TIniFile;
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  public
//    function ReadString(ASection, AName: string; Default: string) : string;
//    function ReadInteger(ASection, AName: string; Default: Integer) : Integer;
//    function ReadBoolean(ASection, AName: string; Default: Boolean) : Boolean;
//
//    function WriteString(ASection, AName: string; AValue: string) : Boolean;
//    function WriteBoolean(ASection, AName: string; AValue: Boolean) : Boolean;
//    function WriteInteger(ASection, AName: string; AValue: Integer) : boolean;

    constructor Create(AFileName: String; AAutoPath: Boolean);
    destructor Destroy;
  end;

implementation

{ TSfIniConfig }

constructor TSfIniConfig.Create(AFileName: String; AAutoPath: Boolean);
begin
  if AAutoPath then
    _iniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + AFileName)
  else
    _iniFile := TIniFile.Create(AFileName);
end;

destructor TSfIniConfig.Destroy;
begin
  if Assigned(_iniFile) then
    _iniFile.Free;
end;

function TSfIniConfig.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

//function TSfIniConfig.ReadBoolean(ASection, AName: string;
//  Default: Boolean): Boolean;
//begin
//
//end;
//
//function TSfIniConfig.ReadInteger(ASection, AName: string;
//  Default: Integer): Integer;
//begin
//
//end;
//
//function TSfIniConfig.ReadString(ASection, AName, Default: string): string;
//begin
//
//end;
//
//function TSfIniConfig.WriteBoolean(ASection, AName: string;
//  AValue: Boolean): Boolean;
//begin
//
//end;
//
//function TSfIniConfig.WriteInteger(ASection, AName: string;
//  AValue: Integer): boolean;
//begin
//
//end;
//
//function TSfIniConfig.WriteString(ASection, AName, AValue: string): Boolean;
//begin
//
//end;

function TSfIniConfig._AddRef: Integer;
begin
  Result := InterlockedIncrement(FRefCount);
end;

function TSfIniConfig._Release: Integer;
begin
  Result := InterlockedDecrement(FRefCount);
  if Result = 0 then
    Destroy;
end;

end.

