unit SfDbConfig;

interface

uses
  Classes,
  Generics.Collections,
  Uni,
  DBAccess,
  SfUniConnection,
  Configable,
  SysUtils;

type
  TConfig = class(TObject)
    Id : Integer;
    Key : String;
    Value : String;
    DataType : String;
  end;

TConfigList = TDictionary<String, TConfig>;

type
  TSfConfig = class(TObject)
    private
      _conf : TConfigList;

      function FetchConfig: TConfigList;
    public
      function Read(AName: String) : TConfig;

      constructor Create;
  end;

implementation

{ TSfConfig }

constructor TSfConfig.Create;
begin
  _conf := FetchConfig;
end;

function TSfConfig.FetchConfig: TConfigList;
var
  item : TConfig;
  q : TUniQuery;
begin
  Result := TConfigList.Create;

  q := TUniQuery.Create(nil);
  q.Connection := TSfUniConnection.Connection;
  q.SQL.Text := 'select * from sys_configs';

  try
    try
      q.Connection.Connected := True;
      q.Open;

      if not q.IsEmpty then
      begin
        q.First;

        while not q.Eof do
        begin
          item := TConfig.Create;

          item.Id := q.FieldByName('id').AsInteger;
          item.Key := q.FieldByName('key').AsString;
          item.Value := q.FieldByName('value').AsString;
          item.DataType := q.FieldByName('data_type').AsString;

          Result.Add(item.Key, item);
          q.Next;
        end;
      end;
    except
      raise;
    end;
  finally
    q.Connection.Free;
    q.Free;
  end;
end;

function TSfConfig.Read(AName: String): TConfig;
begin
  AName := UpperCase(AName);

  if _conf.ContainsKey(AName) then
    Result := _conf[AName]
  else
    Result := nil;
end;

end.
