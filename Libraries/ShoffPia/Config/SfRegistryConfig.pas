unit SfRegistryConfig;

interface

uses
  Classes, SysUtils, IniFiles, Configable, Registry, Windows, StrUtils,
  SfStringUtils,
  Logable;

type
  TSfRegistryConfig = class(TInterfacedObject, IConfigable)
  private
//    _registry : TRegistry;
    _logger : ILogable;
    _regPath : string;

    procedure InitRegistrySetting;
  public
    procedure Delete(AName: String);

    function ReadString(ASection, AName: string; ADefault: string = '') : string;
    function ReadInteger(ASection, AName: string; ADefault: Integer = 0) : integer;
    function ReadBoolean(ASection, AName: string; ADefault: Boolean = False) : Boolean;

    function WriteString(ASection, AName: string; AValue: string) : Boolean;
    function WriteBoolean(ASection, AName: string; AValue: Boolean) : Boolean;
    function WriteInteger(ASection, AName: string; AValue: Integer) : boolean;

    constructor Create; overload;
    constructor Create(Logger : ILogable); overload;
  end;

implementation

{ TAppConfig }

constructor TSfRegistryConfig.Create;
begin
//  inherited Create;
  InitRegistrySetting;
end;

constructor TSfRegistryConfig.Create(Logger: ILogable);
begin
  inherited Create;
  InitRegistrySetting;
  _logger := Logger;
end;

procedure TSfRegistryConfig.Delete(AName: String);
var
  Registry : TRegistry;
begin
  Registry := TRegistry.Create(KEY_READ);
  Registry.RootKey := HKEY_CURRENT_USER;
  try
    try
      if Registry.OpenKey(_regPath, True) then
        Registry.DeleteValue(_regPath + AName)
      else
        _logger.Error('DFFB64F0 : Failed opening registry key');
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('A0636FDD : ' + E.Message);
    end;
  finally
    Registry.Free;
  end;
end;

procedure TSfRegistryConfig.InitRegistrySetting;
var
  Registry : TRegistry;
  OpenResult : Boolean;
begin
  Registry := TRegistry.Create(KEY_READ);
  Registry.RootKey := HKEY_CURRENT_USER;
  //TODO: Inject this hardcoded constant
  _regPath := 'Software\Panglima\POS\';

  try
  if not Registry.KeyExists(_regPath) then
  begin
    try
      Registry.Access := KEY_WRITE;

      if not Registry.OpenKey(_regPath, True) then
        _logger.Error('B88BD4A7 : Unable to create Registry Key. Exiting.');
        
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('A546CA43 : ' + E.Message);
    end;
  end;
  finally
    Registry.Free;
  end;

end;

function TSfRegistryConfig.ReadString(ASection, AName: String; ADefault: string = ''): string;
var
  Registry : TRegistry;
begin
  Registry := TRegistry.Create(KEY_READ);
  Registry.RootKey := HKEY_CURRENT_USER;
  try
    try
//      if not Registry.KeyExists(_regPath + AName) then
//        WriteInteger(AName, 0);

      if Registry.OpenKey(_regPath, True) then
      begin
        Result := Registry.ReadString(AName);
        if Result <> '' then
          Result := ADefault;
      end
      else
      begin
        _logger.Error('EC56837F : Failed opening registry key');
      end;
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('A69A64A6 : ' + E.Message);
    end;
  finally
    Registry.Free;
  end;
end;

function TSfRegistryConfig.WriteBoolean(ASection, AName: string; AValue: Boolean): Boolean;
var
  Registry : TRegistry;
begin
  Registry := TRegistry.Create(KEY_SET_VALUE);
  Registry.RootKey := HKEY_CURRENT_USER;
  try
    try
      if Registry.OpenKey(_regPath, True) then
        Registry.WriteBool(AName, AValue)
      else
        _logger.Error('A08D9F58 : Failed opening registry key');
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('D95EDE38 : ' + E.Message);
    end;
  finally
    Registry.Free;
  end;
end;

function TSfRegistryConfig.WriteInteger(ASection, AName: string; AValue: Integer): boolean;
var
  Registry : TRegistry;
begin
  Registry := TRegistry.Create(KEY_SET_VALUE);
  Registry.RootKey := HKEY_CURRENT_USER;
  try
    try
      if Registry.OpenKey(_regPath, True) then
        Registry.WriteInteger(AName, AValue)
      else
        _logger.Error('FE8AEAEE : Failed opening registry key');
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('F9237AFE : ' + E.Message);
    end;
  finally
    Registry.Free;
  end;
end;

function TSfRegistryConfig.WriteString(ASection, AName, AValue: string): Boolean;
var
  Registry : TRegistry;
begin
  Registry := TRegistry.Create(KEY_SET_VALUE);
  Registry.RootKey := HKEY_CURRENT_USER;
  try
    try
      if Registry.OpenKey(_regPath, True) then
        Registry.WriteString(AName, AValue)
      else
        _logger.Error('BDA38893 : Failed opening registry key');
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('FC78D9D1 : ' + E.Message);
    end;
  finally
    Registry.Free;
  end;
end;

function TSfRegistryConfig.ReadInteger(ASection, AName: string; ADefault: Integer = 0): Integer;
var
  Registry : TRegistry;
begin
  Registry := TRegistry.Create(KEY_READ);
  Registry.RootKey := HKEY_CURRENT_USER;
  try
    try
//      if not Registry.KeyExists(_regPath + AName) then
//        WriteInteger(AName, 0);

      if Registry.OpenKey(_regPath, True) then
        Result := Registry.ReadInteger(AName)
      else
      begin
        _logger.Error('EC56837F : Failed opening registry key');
      end;
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('A69A64A6 : ' + E.Message);
    end;
  finally
    Registry.Free;
  end;
end;

function TSfRegistryConfig.ReadBoolean(ASection, AName: string; ADefault: Boolean = False): Boolean;
var
  Registry : TRegistry;
begin
  Registry := TRegistry.Create(KEY_READ);
  Registry.RootKey := HKEY_CURRENT_USER;
  try
    try
//      if not Registry.KeyExists(_regPath + AName) then
//        WriteBoolean(AName, False);

      if Registry.OpenKey(_regPath, True) then
        Result := Registry.ReadBool(AName)
      else
      begin
        _logger.Error('EC56837F : Failed opening registry key');
      end;
    except on E: Exception do
      if Assigned(_logger) then
        _logger.Error('A69A64A6 : ' + E.Message);
    end;
  finally
    Registry.Free;
  end;
end;

end.

