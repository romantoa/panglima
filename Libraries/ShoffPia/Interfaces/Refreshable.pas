unit Refreshable;

interface

type
  IRefreshable = interface
    procedure RefreshData;
  end;

implementation

end.
