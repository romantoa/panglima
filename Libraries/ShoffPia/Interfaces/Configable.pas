unit Configable;

interface

uses
  Classes;

type
  IConfigable = interface
    function ReadString(const Section: string; const Ident: string; const Default: string): string;
    function ReadInteger(const Section: string; const Ident: string; Default: Longint): Longint;
    function ReadBool(const Section: string; const Ident: string; Default: Boolean): Boolean;

    procedure WriteString(const Section: String; const Ident: String; const Value: String);
    procedure WriteInteger(const Section: string; const Ident: string; Value: Longint);
    procedure WriteBool(const Section: string; const Ident: string; Value: Boolean);
  end;

implementation

end.
