unit Reportable;

interface

uses
  Classes;

type
  IReportable = interface
    procedure Design(AName: string);
    procedure Preview(AName: string);
  end;

implementation

end.
