unit SfStringUtils;

interface

uses
  Classes,
  SysUtils,
  StrUtils;

type
  TsfGuidType = ( gtDefault, gtL8, gtR12 );

type
  TSfStringUtils = class(TObject)
  public
    class procedure StrSplit(StrValue: string; Delimiter: char; Res : TStrings);
    class function GenerateGuid(AType: TsfGuidType) : String;
  end;

implementation

class function TSfStringUtils.GenerateGuid(AType: TsfGuidType): String;
var
  uid : TGUID;
  suid : string;
begin
  CreateGUID(uid);
  suid := ReplaceStr(ReplaceStr(GUIDToString(uid),'{',''),'}','');

  if AType = gtL8 then
    Result := LeftStr(suid, 8)
  else if AType = gtR12 then
    Result := RightStr(suid, 12)
  else
    Result := suid;
end;

class procedure TSfStringUtils.StrSplit(StrValue: string; Delimiter: char; Res : TStrings);
begin
  Res.Clear;
  Res.Delimiter := Delimiter;
  Res.DelimitedText := StrValue;
  Res.StrictDelimiter := True;
end;

end.
