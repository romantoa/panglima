unit SfCollectionUtils;

interface

uses
  Classes,
  Generics.Collections,
  dxMDaset,
  DB;

function DictionaryToDataSet(Val: TDictionary<integer,string>; AValueMaxLen: integer) : TDataSet;

implementation

function DictionaryToDataSet(Val: TDictionary<integer,string>; AValueMaxLen: integer) : TDataSet;
var
  x: TdxMemData;
  f1, f2: TField;
  id, nama: string;
  key: integer;
begin
  x := TdxMemData.Create(nil);
  f1 := TIntegerField.Create(nil);
  f1.FieldName := 'id';
  f1.Dataset := x;

  f2 := TStringField.Create(nil);
  f2.FieldName := 'name';
  f2.Dataset := x;
  f2.Size := AValueMaxLen;

  x.Close;
  x.Open;

  for key in Val.Keys do
  begin
    x.Append;
    x.FieldByName('id').Value := key;
    Val.TryGetValue(key,nama);
    x.FieldByName('name').Value := nama;
    x.Post;
  end;

  Result := x;
end;

end.
