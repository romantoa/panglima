unit SfLookupUtils;

interface

uses
  cxDBLookupComboBox,
  cxTextEdit,
  DB;

procedure SetLookupProperties(ALkCombobox: TcxLookupComboBox; ADataSet: TDataSet);

implementation

procedure SetLookupProperties(ALkCombobox: TcxLookupComboBox; ADataSet: TDataSet);
var
  ds : TDataSource;
begin
  ds := TDataSource.Create(nil);
  ds.DataSet := ADataSet;

  with ALkCombobox.Properties do
  begin
    BeginUpdate;
    ListOptions.ShowHeader := False;
    IncrementalFilteringOptions := [ifoHighlightSearchText,ifoUseContainsOperator];
    ListSource := ds;
    ListFieldNames := 'name';
    KeyFieldNames := 'id';
    EndUpdate;
  end;
end;

end.
