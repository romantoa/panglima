unit SfGridUtils;

interface

uses
  cxGridTableView;

function GetColumnIndexByName(AGv: TcxGridTableView; AColumnName: String) : Integer;

implementation

function GetColumnIndexByName(AGv: TcxGridTableView; AColumnName: String) : Integer;
var
  I: Integer;
  Found: Boolean;
begin
  for I := 0 to AGv.ColumnCount-1 do
  begin
    if AGv.Columns[I].Name = AColumnName then
    begin
      Result := I;
      Found := True;
      Break;
    end;
  end;

  if not Found then
    Result := -1;
end;

end.
