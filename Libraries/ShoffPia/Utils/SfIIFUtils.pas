unit SfIIFUtils;

interface

uses
  Classes, Variants, JSON;

function IIF(ACondition : Boolean; ATrueVal, AFalseVal : String) : String; overload;
function IfEmpty(AVal, ARepVal : String) : String;
function IfNull(AVal, ArepVal: String) : String;
function IfVarNull(AVal, AReplacementVal : Variant) : Variant;
function IfJsonNull(APair : TJsonPair; ARepVal: String) : String;
function NullIf(AVal, ACondVal : Variant) : Variant;

implementation

{ TShUtils }

function IfEmpty(AVal, ARepVal: String): String;
begin
  Result := IIF(AVal = '', ARepVal, AVal);
end;

function IfNull(AVal, ArepVal: String): String;
begin
  Result := IIF(AVal = 'null', ArepVal, AVal);
end;

function IIF(ACondition: Boolean; ATrueVal, AFalseVal: String) : String;
begin
  if Acondition then
    Result := ATrueVal
  else
    Result := AFalseVal;
end;

function IfVarNull(AVal, AReplacementVal : Variant) : Variant;
begin
  if Aval = (null) then
    Result := AReplacementVal
  else
    Result := AVal;
end;

function IfJsonNull(APair : TJsonPair; ARepVal: String) : String;
begin
  if APair.Null then
    Result := ARepVal
  else
    Result := APair.Value;
end;

function NullIf(AVal, ACondVal : Variant) : Variant;
begin
  if AVal = ACondVal then
    Result := Null
  else
    Result := AVal;
end;

end.
