unit SfDateTimeUtils;

interface

uses
  Classes,
  SysUtils;

type
  TDateTimeUtils = class(TObject)
    public
      class function FormatSettings : TFormatSettings;
  end;

implementation

{ TDateTimeUtils }

class function TDateTimeUtils.FormatSettings: TFormatSettings;
begin
  Result := TFormatSettings.Create;
  Result.DateSeparator := '-';
  Result.ShortDateFormat := 'yyyy-MM-dd';
  Result.TimeSeparator := ':';
  Result.ShortTimeFormat := 'hh:mm';
  Result.LongTimeFormat := 'hh:mm:ss';
end;

end.
