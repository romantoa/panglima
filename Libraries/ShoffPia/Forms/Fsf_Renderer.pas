unit Fsf_Renderer;

interface

uses
  Classes,
  SfModuleManager,
  SysUtils,
  cxGroupBox,
  cxGrid,
  Windows;

type
  TFsfRenderer = class(TObject)
    public
      class function CreateGrid(AName: String) : TcxGrid;
  end;

implementation

{ TFsfRenderer }

{ TFsfRenderer }

class function TFsfRenderer.CreateGrid(AName: String): TcxGrid;
begin
  Result := TcxGrid.Create(Self);
  Result.Name := 'grd_' + AName;
//  Result.Parent := Self;
  Result.Align := alClient;

  ResultLevel := Result.Levels.Add;
  ResultLevel.Name := Result.Name + 'Lvl1';

  gvData := Result.CreateView(TcxResultTableView) as TcxResultTableView;
  gvData.Name := 'gvData';

  ResultLevel.ResultView := gvData;

  // Options
  gvData.OptionsView.GroupByBox := False;
  gvData.OptionsView.Indicator := True;
  gvData.OptionsView.IndicatorWidth := 10;
  gvData.OptionsSelection.CellSelect := False;

  // Properties
  Result.AlignWithMargins := True;
  Result.Margins.Top := 3;
  Result.Margins.Bottom := 0;
  Result.Margins.Left := 0;
  Result.Margins.Right := 0;
end;

end.
