unit Fsf_Base;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp, dxSkinSharpPlus,
  dxSkinTheBezier, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, DM_Main, cxButtonEdit, cxPC, dxRibbon,

  SfModuleManager,
  Generics.Collections,
  DB, Fsf_MdiChild, dxSkinWhiteprint, cxGroupBox, SfModel, System.Actions,
  Vcl.ActnList;

type
  TFsfBase = class(TForm)
    procedure FormShow(Sender: TObject);
    procedure comboboxPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormCreate(Sender: TObject);
  private
    procedure BuildComboboxes;
    function GetActionRibbon: TdxRibbon;
  protected
    procedure FitToParent;
  public
    _module : TSfModule;
    _lksDset: TDictionary<String, TDataSet>;

    procedure DoClose;

    procedure PrepareLookups;
    property ActionRibbon: TdxRibbon read GetActionRibbon;
  end;

type
  TFsfBaseType = class of TFsfBase;

var
  FsfBase: TFsfBase;

implementation

{$R *.dfm}

procedure TFsfBase.comboboxPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  //
end;

procedure TFsfBase.DoClose;
var
  ts : TcxTabSheet;
begin
  if Self.Parent is TcxTabSheet then
  begin
    ts := (Self.Parent as TcxTabSheet);
    ts.PageControl.CloseTab(ts.TabIndex);
  end
  else
    Self.Close;
end;

procedure TFsfBase.FitToParent;
begin
  Self.Width := Self.Parent.Width;
  Self.Height := Self.Parent.Height;
  Self.Update;
end;

procedure TFsfBase.FormCreate(Sender: TObject);
begin
  if _module.Id = 0 then
  begin
    _module := TSfModuleManager.GetModuleProperties(_module.Name);
    _lksDset := TDictionary<string, TDataSet>.Create;
    PrepareLookups;
  end;
end;

procedure TFsfBase.FormShow(Sender: TObject);
begin
  // TODO: This should be handled by renderer
//  BuildComboboxes;
end;

function TFsfBase.GetActionRibbon: TdxRibbon;
begin
  Result := (FindComponent('__action_ribbon') as TdxRibbon);
end;

procedure TFsfBase.PrepareLookups;
var
  I, J: Integer;
  nm: string;
begin
  // Master
  for I := 0 to Length(_module.Fields) - 1 do
  begin
    if _module.Fields[I].FieldType = 'FK' then
    begin
      nm := StringReplace(_module.Fields[I].Name, '_id', '', [rfReplaceAll]);
      if not _lksDset.ContainsKey(nm) then
        _lksDset.Add(nm, TSfModel.Fetch(nm));
    end;
  end;

  // Detail
  for I := 0 to _module.Childs.Count - 1 do
  begin
    for J := 0 to Length(_module.Childs[I].Fields) - 1 do
    begin
      if _module.Childs[I].Fields[J].FieldType = 'FK' then
      begin
        nm := StringReplace(_module.Childs[I].Fields[J].Name, '_id', '',
          [rfReplaceAll]);
        if not _lksDset.ContainsKey(nm) then
          _lksDset.Add(nm, TSfModel.Fetch(nm));
      end;
    end;
  end;
end;

procedure TFsfBase.BuildComboboxes;
var
  cmp : TComponent;
  I: Integer;
begin
  {$REGION 'Create Second Buttons'}
  for I := 0 to ComponentCount-1 do
  begin
    cmp := Components[I];
    if cmp is TcxLookupComboBox then
    begin
      with TcxCustomEditProperties((cmp as TcxLookupComboBox).Properties) do
      begin
        Images := DMMain.ilDev16;
        with Buttons do
        begin
          with Add as TcxEditButton do
          begin
            Default := False;
            Kind := bkGlyph;
            ImageIndex := 14;
          end;
        end;
        OnButtonClick := comboboxPropertiesButtonClick;
      end;
    end;
  end;
  {$ENDREGION}

  {$REGION 'Assign ListSources'}

  {$ENDREGION}
end;

end.
