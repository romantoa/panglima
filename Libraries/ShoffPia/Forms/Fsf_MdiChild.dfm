object FsfMdiChild: TFsfMdiChild
  Left = 0
  Top = 0
  Caption = '...'
  ClientHeight = 402
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object __module_ribbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 686
    Height = 24
    BarManager = bmModuleWrapper
    ColorSchemeName = 'VS2010'
    ShowTabHeaders = False
    Contexts = <>
    TabOrder = 0
    TabStop = False
  end
  object bmModuleWrapper: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.SkinName = 'VS2010'
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 336
    Top = 208
    PixelsPerInch = 96
  end
end
