unit Fsf_Edit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinDevExpressStyle, dxSkinOffice2010Blue,
  dxSkinSharp, dxSkinSharpPlus, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVS2010, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxGroupBox, Generics.Collections, cxDBLookupComboBox, cxCheckbox,

  DM_Main, SfModuleManager, Refreshable, SfModel, Fsf_Base,
  System.StrUtils, Data.DB, dxSkinTheBezier,
  dxBarBuiltInMenu, cxPC, Uni, DBAccess, cxGrid, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxGridLevel, cxCalendar,
  dxRibbonSkins, dxRibbonCustomizationForm, dxBar, dxRibbon, dxSkinWhiteprint,
  System.DateUtils, Vcl.ComCtrls, dxCore, cxDateUtils, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxMaskEdit, cxTextEdit, cxLabel;

type
  TsfFormData = record
      Name : String;
      Data : TDictionary<String, Variant>;
      Childs : TList<TsfFormData>;
  end;

type
  TFsfEdit = class(TFsfBase)
    bmEdit: TdxBarManager;
    barIndexAction: TdxBar;
    barIndexView: TdxBar;
    btnSaveAndClose: TdxBarLargeButton;
    btnPrint: TdxBarLargeButton;
    btnSave: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    __action_ribbon: TdxRibbon;
    rbn1File: TdxRibbonTab;
    btnReset: TdxBarLargeButton;
    btnSearch: TdxBarLargeButton;
    procedure FormShow(Sender: TObject);
    procedure DetailActionClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure _numberKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    __detail: TcxPageControl;

    procedure LoadData(ADataSet: TDataSet = nil); virtual;
    function GetFormData : TsfFormData; virtual;

  public
    Id: Integer;
    ModifState: TModifState;

    _dataSources: TList<TDataSet>;
    _caller: IRefreshable;

    constructor CreateByRefreshable(AOwner: TComponent; ACaller: IRefreshable;
      AModule: TSfModule; ALookups: TDictionary<String, TDataSet>);

    procedure RenderLookups; virtual;
    procedure RenderDetails; virtual;
    procedure RefreshDetail; virtual;
    procedure DoReset; virtual;
    procedure DoSearchMode; virtual;
    procedure SetDefaultValues; virtual;

    function Save: TSfActionResult; virtual;
    function MakeTransactionNumber(APrefix, APadder: String; ALength: Integer; AResetBy: String) : String; virtual;

//    property Caller: IRefreshable read _caller write _caller;
  end;

  TEditType = class of TFsfEdit;

var
  FsfEdit: TFsfEdit;

implementation

{$R *.dfm}

procedure TFsfEdit.btnCloseClick(Sender: TObject);
begin
  DoClose;
end;

procedure TFsfEdit.btnResetClick(Sender: TObject);
begin
  inherited;
  DoReset;
  SetDefaultValues;
end;

procedure TFsfEdit.btnSaveClick(Sender: TObject);
var
  ARes : TSfActionResult;
begin
  inherited;
  ARes := Save;

  if ARes.Success then
  begin
    if _caller <> nil then
      _caller.RefreshData;

    if ModifState = msInsert then
    begin
      if Sender = btnSave then
      begin
        DoReset;
        SetDefaultValues;
      end
      else if Sender = btnSaveAndClose then
        DoClose;
    end;

    DMMain.alrMain.Show('Sukses', ARes.Message);
  end
  else
  begin
    DMMain.alrMain.Show('Gagal', ARes.Message);
  end;
end;

procedure TFsfEdit.btnSearchClick(Sender: TObject);
begin
  inherited;
  DoSearchMode;
end;

constructor TFsfEdit.CreateByRefreshable(AOwner: TComponent; ACaller: IRefreshable;
  AModule: TSfModule; ALookups: TDictionary<String, TDataSet>);
begin
  _module := AModule;
  _caller := ACaller;
  _lksDset := ALookups;

  inherited Create(AOwner);

  btnSaveAndClose.Visible := ivAlways;
  btnSave.Visible := ivNever;
end;

procedure TFsfEdit.DetailActionClick(Sender: TObject);
var
  nm, btnName: String;
  gv: TcxCustomGridView;
  grd: TcxGrid;
begin
  inherited;
  btnName := TcxButton(Sender).Name;
  nm := (TcxButton(Sender).Parent.Parent as TcxTabSheet).Name;
  gv := (FindComponent('gv' + nm) as TcxCustomGridView);
  grd := (FindComponent('gr' + nm) as TcxGrid);
  if LeftStr(btnName, 2) = 'c_' then
  begin
    gv.DataController.Insert;
    grd.SetFocus;
  end
  else if LeftStr(btnName, 2) = 'u_' then
    gv.DataController.Edit
  else if LeftStr(btnName, 2) = 'd_' then
  begin
    if MessageDlg('Anda yakin ingin menghapus?', mtConfirmation, mbYesNo, 0) = mrYes
    then
      gv.DataController.DeleteFocused;
  end;
end;

procedure TFsfEdit.FormCreate(Sender: TObject);
begin
  inherited;
  if _module.Childs.Count > 0 then
    RenderDetails;

//  PrepareLookups;
  RenderLookups;
end;

procedure TFsfEdit.FormShow(Sender: TObject);
begin
  inherited;

  if Self.ModifState = msEdit then
  begin
    LoadData;
    RefreshDetail;
  end
  else //if Self.ModifState = msInsert then
  begin
    SetDefaultValues;
  end;
end;

function TFsfEdit.GetFormData: TsfFormData;
begin
  // Master

  // More

  // Detail
end;

procedure TFsfEdit.LoadData(ADataSet: TDataSet = nil);
var
  Data: TDataSet;
  wr: TWinControl;
  comp: TcxCustomEdit;
  I: Integer;
  nm, fn: string;
begin
  if ADataSet = nil then
    Data := TSfModel.LoadSingle(_module.Table, Id)
  else
    Data := ADataSet;

  Self.Id := Data.FieldByName('id').AsInteger;
  wr := (FindComponent('__master') as TWinControl);
  if wr <> nil then
  begin
    for I := 0 to wr.ControlCount - 1 do
    begin
      nm := wr.Controls[I].Name;
      fn := RightStr(nm, Length(nm) - 1);
      if (LeftStr(nm, 1) = '_') then
      begin
        (wr.Controls[I] as TcxCustomEdit).EditValue :=
          Data.FieldByName(fn).Value;
      end;
    end;
  end;

  //TODO : Should find any component with name started by '__master'
  wr := (FindComponent('__master2') as TWinControl);
  if wr <> nil then
  begin
    for I := 0 to wr.ControlCount - 1 do
    begin
      nm := wr.Controls[I].Name;
      fn := RightStr(nm, Length(nm) - 1);
      if (LeftStr(nm, 1) = '_') then
      begin
        (wr.Controls[I] as TcxCustomEdit).EditValue :=
          Data.FieldByName(fn).Value;
      end;
    end;
  end;
end;

function TFsfEdit.MakeTransactionNumber(APrefix, APadder: String;
  ALength: Integer; AResetBy: String): String;
var
  ASeqInt : Integer;
  ASeqStr : String;
  q : TUniQuery;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  q.SQL.Add('select ifnull(max(number),0) as maxnum from ' + _module.Table);
  q.SQL.Add('where left(number, 3) = :prefix');
  q.SQL.Add('and YEAR(dateissued) = :year');

  q.Params.ParamByName('prefix').Value := APrefix;
  q.Params.ParamByName('year').Value := YearOf(date);

  q.Connection.Connected := True;
  q.Open;

  ASeqStr := q.FieldByName('maxnum').AsString;
  ASeqStr := RightStr(ASeqStr, ALength);
  ASeqInt := StrToInt(ASeqStr) + 1;

  Result := APrefix + '.' + Format('%.*d', [ALength, ASeqInt]);
end;

procedure TFsfEdit.RefreshDetail;
var
  I, J, ri : Integer;
  AGv : TcxGridTableView;
  ACol : TcxGridColumn;
  ADs : TDataSet;
begin
//  for I := 0 to _module.Childs.Count-1 do
//  begin
//    for J := 0 to __detail.PageCount-1 do
//    begin
//      if '_' + __detail.Pages[J].Name = _module.Childs[I].Name then
//      begin
//        ATs := __detail.Pages[J];
//        Break;
//      end;
//    end;
//
//
//  end;
  for I := 0 to _module.Childs.Count-1 do
  begin
    AGv := (FindComponent('gv_' + _module.Childs[I].Table) as TcxGridTableView);
    if AGv <> nil then
    begin
      ADs := TSfModel.FetchChild(_module.Childs[I].Table, _module.Table + '_id', Self.Id);

      AGv.DataController.BeginUpdate;
      AGv.DataController.RecordCount := 0;
      try
        try
          ri := 0;

          while not ADs.Eof do
          begin
            AGv.DataController.AppendRecord;
            for J := 0 to AGv.ColumnCount - 1 do
            begin
              AGv.DataController.Values[ri, J] :=
                ADs.FieldByName(_module.Childs[I].Fields[J].Name).Value;
            end;
            ADs.Next;
            Inc(ri);
          end;

          AGv.DataController.EndUpdate;
        except
          on E: Exception do
            ShowMessage(E.Message);
        end;
      finally
        // _dataSource.Free;
      end;
    end;
  end;
end;

procedure TFsfEdit.RenderDetails;
var
  tsDet: TcxTabSheet;
  gr: TcxGrid;
  lvl: TcxGridLevel;
  gv: TcxGridTableView;
  I, J: Integer;
  p, detailPanel: TcxGroupBox;
  b: TcxButton;
  foreignTable: String;
  ds: TDataSource;
begin
  {*
   * Naming :
   * - PageControl :
   *      '__detail'
   *
   * - TabSheet :
   *      '_formula_item'
   *
   * - ActionButtons :
   *      'c_formula_item',
   *      'u_formula_item',
   *      'd_formula_item'
   *
   * - Grid :
   *      'gr_formula_item',
   *      'Lvl1',
   *      'gv_formula_item'
   *
   * - GridViewColumn :
   *      '_formula_item_id',
   *      '_formula_item_formula_id',
   *      '_formula_item_name'
   *}

  __detail := TcxPageControl.Create(Self);
  __detail.Name := '__detail';
  __detail.Parent := Self;
  __detail.Align := alClient;
//  __detail.TabPosition := tpTop;
  __detail.AlignWithMargins := True;

  for I := 0 to _module.Childs.Count - 1 do
  begin
    // Add Page
    tsDet := TcxTabSheet.Create(Self);
    tsDet.Caption := DMMain.Lang.LBL
      ('LBL_MOD_' + UpperCase(_module.Childs[I].Name));
    tsDet.Name := '_' + _module.Childs[I].Name;
    tsDet.PageControl := __detail;

    // Add Action Panel
    p := TcxGroupBox.Create(Self);
    p.PanelStyle.Active := True;
    p.Caption := '';
    p.Height := 35;
    p.Align := alTop;
    p.Parent := tsDet;

    b := TcxButton.Create(Self);
    b.Name := 'c_' + _module.Childs[I].Name;
    b.Parent := p;
    b.PaintStyle := bpsGlyph;
    b.OptionsImage.Images := DMMain.ilDev16;
    b.OptionsImage.ImageIndex := 0;
    b.Align := alLeft;
    b.Width := 30;
    b.AlignWithMargins := True;
    b.Margins.Top := 0;
    b.Margins.Left := 0;
    b.Margins.Bottom := 0;
    b.Margins.Right := 2;
    b.OnClick := DetailActionClick;

    b := TcxButton.Create(Self);
    b.Name := 'u_' + _module.Childs[I].Name;
    b.Parent := p;
    b.PaintStyle := bpsGlyph;
    b.OptionsImage.Images := DMMain.ilDev16;
    b.OptionsImage.ImageIndex := 1;
    b.Align := alLeft;
    b.Width := 30;
    b.AlignWithMargins := True;
    b.Margins.Top := 0;
    b.Margins.Left := 0;
    b.Margins.Bottom := 0;
    b.Margins.Right := 2;
    b.OnClick := DetailActionClick;

    b := TcxButton.Create(Self);
    b.Name := 'v_' + _module.Childs[I].Name;
    b.Parent := p;
    b.PaintStyle := bpsGlyph;
    b.OptionsImage.Images := DMMain.ilDev16;
    b.OptionsImage.ImageIndex := 2;
    b.Align := alLeft;
    b.Visible := False;
    b.Width := 30;
    b.AlignWithMargins := True;
    b.Margins.Top := 0;
    b.Margins.Left := 0;
    b.Margins.Bottom := 0;
    b.Margins.Right := 2;
    b.OnClick := DetailActionClick;

    b := TcxButton.Create(Self);
    b.Name := 'd_' + _module.Childs[I].Name;
    b.Parent := p;
    b.PaintStyle := bpsGlyph;
    b.OptionsImage.Images := DMMain.ilDev16;
    b.OptionsImage.ImageIndex := 3;
    b.Align := alLeft;
    b.Width := 30;
    b.AlignWithMargins := True;
    b.Margins.Top := 0;
    b.Margins.Left := 0;
    b.Margins.Bottom := 0;
    b.Margins.Right := 2;
    b.OnClick := DetailActionClick;

    // Add Detail Grid
    gr := TcxGrid.Create(Self);
    gr.Name := 'gr_' + _module.Childs[I].Name;
    gr.Parent := Self;
    gr.Align := alClient;
    gr.Font.Size := 10;

    lvl := gr.Levels.Add;
    lvl.Name := gr.Name + 'Lvl1';

    gv := gr.CreateView(TcxGridTableView) as TcxGridTableView;
    gv.Name := 'gv_' + _module.Childs[I].Name;

    lvl.GridView := gv;

    // Options
    gv.DataController.Options := [dcoImmediatePost];

    gv.OptionsView.GroupByBox := False;
    gv.OptionsView.Indicator := True;
    gv.OptionsView.IndicatorWidth := 10;

    gv.OptionsData.Inserting := True;
    gv.OptionsData.Editing := True;
    gv.OptionsData.Deleting := True;
    gv.OptionsData.Appending := True;

    gv.OptionsBehavior.FocusCellOnTab := True;
    gv.OptionsBehavior.FocusCellOnCycle := True;
    gv.OptionsBehavior.FocusFirstCellOnNewRecord := True;
    gv.OptionsBehavior.GoToNextCellOnEnter := True;

    // Columns
    for J := 0 to Length(_module.Childs[I].Fields) - 1 do
    begin
      with gv.CreateColumn do
      begin
        Name := '_' + _module.Childs[I].Table + '_' + _module.Childs[I].Fields[J].Name;
        Caption := DMMain.Lang.LBL(_module.Childs[I].Fields[J].Caption);

//        // Column Size & Visibility
        if (_module.Childs[I].Fields[J].Width = 0) or
          (_module.Childs[I].Fields[J].FieldType = 'PK') or
          (_module.Childs[I].Fields[J].Name = _module.Table + '_id') then
          Visible := False
        else if _module.Childs[I].Fields[J].Width = (Null) then
          Width := 100
        else
          Width := _module.Childs[I].Fields[J].Width;

        // Column Display Format
        if _module.Childs[I].Fields[J].FieldType = 'DATETIME' then
        begin
          PropertiesClass := TcxDateEditProperties;
          TcxDateEditProperties(Properties).BeginUpdate;
          TcxDateEditProperties(Properties).DisplayFormat := 'dd-mm-yyyy';
          TcxDateEditProperties(Properties).Alignment.Horz := taCenter;
          TcxDateEditProperties(Properties).UseLeftAlignmentOnEditing := False;
          TcxDateEditProperties(Properties).EndUpdate;
        end
        else if _module.Childs[I].Fields[J].FieldType = 'FK' then
        begin
          PropertiesClass := TcxLookupComboBoxProperties;
          foreignTable := StringReplace(_module.Childs[I].Fields[J].Name, '_id', '', [rfReplaceAll]);
          if Assigned(_lksDset) and _lksDset.ContainsKey(foreignTable) then
          begin
            ds := TDataSource.Create(nil);
            ds.DataSet := _lksDset[foreignTable];

            TcxLookupComboBoxProperties(Properties).BeginUpdate;
            TcxLookupComboBoxProperties(Properties).ListOptions.ShowHeader := False;
            TcxLookupComboBoxProperties(Properties).ListSource := ds;
            TcxLookupComboBoxProperties(Properties).ListFieldNames := 'name';
            TcxLookupComboBoxProperties(Properties).KeyFieldNames := 'id';
            TcxLookupComboBoxProperties(Properties).EndUpdate;
          end;
        end;
      end;
    end;

    gr.Parent := tsDet;
    gr.Align := alClient;
  end;

  __detail.Update;
end;

procedure TFsfEdit.RenderLookups;
var
  I: Integer;
  wr: TWinControl;
  dset: TDataSet;
  nm, tableName: string;
begin
  for I := 0 to Self.ComponentCount - 1 do
  begin
    nm := Self.Components[I].Name;
    if (LeftStr(nm, 1) = '_') and (RightStr(nm, 3) = '_id') and
      (Self.Components[I] is TcxLookupComboBox) then
    begin
      tableName := Copy(nm, 2, Length(nm) - 4);
      dset := _lksDset[tableName];
      if dset = nil then
        dset := TSfModel.Fetch(tableName);

      with (Self.Components[I] as TcxLookupComboBox).Properties do
      begin
        BeginUpdate;
        ListSource := TDataSource.Create(nil);
        ListSource.DataSet := dset;
        ListFieldNames := 'name';
        KeyFieldNames := 'id';
        ListOptions.ShowHeader := False;
        IncrementalFilteringOptions := [ifoHighlightSearchText,ifoUseContainsOperator];
        EndUpdate;
      end;
    end;
  end;

  // wr := (FindComponent('__master') as TWinControl);
  // if wr <> nil then
  // begin
  // for I := 0 to wr.ControlCount-1 do
  // begin
  // nm := wr.Controls[I].Name;
  // if (LeftStr(nm, 1) = '_')
  // and (RightStr(nm, 3) = '_id')
  // and (wr.Controls[I] is TcxLookupComboBox) then
  // begin
  // tableName := Copy(nm, 2, length(nm)-4);
  // dset := _lksDset[tableName];
  // if dset = nil then
  // dset := TSfModel.Fetch(tableName);
  //
  // with (wr.Controls[I] as TcxLookupComboBox).Properties do
  // begin
  // BeginUpdate;
  // ListSource := TDataSource.Create(nil);
  // ListSource.DataSet := dset;
  // ListFieldNames := 'name';
  // KeyFieldNames := 'id';
  // ListOptions.ShowHeader := False;
  // EndUpdate;
  // end;
  // end;
  // end;
  // end;
end;

procedure TFsfEdit.DoReset;
var
  I: Integer;
  gv: TcxCustomGridTableView;
begin
  Self.Id := 0;
  Self.ModifState := msInsert;

  for I := 0 to ComponentCount-1 do
  begin
    if (Components[I] is TcxCustomEdit)
      and (not (Components[I] is TcxLabel)) then
      (Components[I] as TcxCustomEdit).Clear
    else if (Components[I] is TcxCustomGridTableView) then
    begin
      gv := (Components[I] as TcxCustomGridTableView);
      gv.BeginUpdate;
      gv.DataController.RecordCount := 0;
      gv.EndUpdate;
    end;
  end;
end;

procedure TFsfEdit.DoSearchMode;
var
  c : TcxCustomEdit;
begin
  DoReset;

  c := (FindComponent('_number') as TcxCustomEdit);
  if c <> nil then
  begin
    c.Enabled := True;
    c.Clear;
    c.SetFocus;
    c.OnKeyDown := _numberKeyDown;
  end;
end;

function TFsfEdit.Save : TSfActionResult;
var
  q, qChild: TUniQuery;
  childSql, insChildSql: TStrings;
  childSqls: TList<TUniQuery>;
  I, J, ri: Integer;
  insValSql: TStringList;
  cn, ss: string;
  x: TcxCustomEdit;
  gv: TcxGridTableView;

  colName : String;
  col : TcxGridColumn;
  insChildColumns : TList<TcxGridColumn>;
begin
  inherited;
  // TODO : Should be handled by SfModel
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  try
    try
      if Self.ModifState = msInsert then
      begin
        {$REGION 'Master Insert'}
        insValSql := TStringList.Create;
        q.SQL.Add('insert into ' + _module.Table + '(');

        for I := 0 to Length(_module.Fields) - 1 do
        begin
          if (_module.Fields[I].Name <> 'id') and
            (Self.FindComponent('_' + _module.Fields[I].Name) <> nil)
          then
          begin
            q.SQL.Add(_module.Fields[I].Name + ',');
            insValSql.Add(':' + _module.Fields[I].Name + ',');
          end;
        end;

        q.SQL.Add('_created_at, _created_by)');
        q.SQL.Add('values(');
        q.SQL.Add(insValSql.Text);
        q.SQL.Add(':_created_at, :_created_by)');

        q.Params.ParamByName('_created_at').Value := DMMain.ServerDate;
        q.Params.ParamByName('_created_by').Value := DMMain.Session.User.Id;
        {$ENDREGION}
      end
      else
      begin
        {$REGION 'Master Update'}
        q.SQL.Add('update ' + _module.Table + ' set');

        for I := 0 to Length(_module.Fields) - 1 do
        begin
          if (_module.Fields[I].Name <> 'id') and
            (Self.FindComponent('_' + _module.Fields[I].Name) <> nil) then
            q.SQL.Add(_module.Fields[I].Name + ' = :' + _module.Fields[I]
              .Name + ',');
        end;

        q.SQL.Add('_updated_at = :_updated_at, _updated_by = :_updated_by');
        q.SQL.Add('where id = :id');

        q.Params.ParamByName('_updated_at').Value := DMMain.ServerDate;
        q.Params.ParamByName('_updated_by').Value := DMMain.Session.User.Id;
        q.Params.ParamByName('id').Value := Self.Id;
        {$ENDREGION}
      end;

      // Master Parameter Values
      for I := 0 to Length(_module.Fields) - 1 do
      begin
        if _module.Fields[I].Name <> 'id' then
        begin
          x := (Self.FindComponent('_' + _module.Fields[I].Name)
            as TcxCustomEdit);

          if x <> nil then
            q.Params.ParamByName(_module.Fields[I].Name).Value :=
              x.EditingValue;
        end;
      end;

      q.Connection.Connected := True;
      q.Connection.StartTransaction;
      q.ExecSQL;

      if ModifState = msInsert then
        Self.Id := q.LastInsertId;

      {$REGION 'Child Insert/Update'}

      for I := 0 to _module.Childs.Count - 1 do
      begin
        gv := (Self.FindComponent('gv_' + LowerCase(_module.Childs[I].Name)) as TcxGridTableView);
        if gv <> nil then
        begin
          // Take all rows in grid
          childSql := TStringList.Create;
          insChildColumns := TList<TcxGridColumn>.Create;

          childSql.Add('DELETE FROM ' + _module.Childs[I].Table);
          childSql.Add('WHERE ' + _module.Table + '_id = :parent_id;');

          childSql.Add('insert into ' + _module.Childs[I].Table + '(');

          for J := 0 to Length(_module.Childs[I].Fields) - 1 do
          begin
            colName := '_' + _module.Childs[I].Table + '_' + _module.Childs[I].Fields[J].Name;
            col := TcxGridColumn(gv.FindItemByName(colName));
            if ( _module.Childs[I].Fields[J].Name <> 'id' ) and
              ( _module.Childs[I].Fields[J].Name <> (_module.Table + '_id') ) and
              ( col <> nil )
            then
            begin
              childSql.Add(_module.Childs[I].Fields[J].Name + ',');
              // Get All Related Grid Columns for Insert Values
              insChildColumns.Add(col);
            end;
          end;

          for J := 0 to gv.ColumnCount - 1 do
          begin
            cn := gv.Columns[I].Name;
          end;

          childSql.Add(_module.Table + '_id, _created_at, _created_by) values ');

          // Values Section of Insert SQL
          insChildSql := TStringList.Create;
          insChildSql.Delimiter := ',';
          insChildSql.QuoteChar := #0;

          for ri := 0 to gv.DataController.RecordCount-1 do
          begin
            ss := '';
            for J := 0 to insChildColumns.Count-1 do
            begin
                ss := ss + QuotedStr(gv.DataController.Values[ri,
                        insChildColumns[J].Index]) + ',';
            end;

            ss := '(' + ss + ':parent_id, :_created_at, :_created_by)';
            insChildSql.Add(ss);
          end;

          childSql.Add(insChildSql.DelimitedText);

          childSqls := TList<TUniQuery>.Create;

          qChild := TUniQuery.Create(nil);
          qChild.Connection := DMMain.Conn.Connection;
          qChild.SQL.Add(childSql.Text);

          childSqls.Add(qChild);
        end;
      end;

      {$ENDREGION}

      if childSql <> nil then
      begin
        for I := 0 to childSqls.Count - 1 do
        begin
          childSqls[I].Params.ParamByName('parent_id').Value := Self.Id;
          childSqls[I].Params.ParamByName('_created_at').Value := DMMain.ServerDate;
          childSqls[I].Params.ParamByName('_created_by').Value := DMMain.Session.User.Id;

          childSqls[I].ExecSQL;
        end;
      end;

      q.Connection.Commit;

      Result.Success := True;
      // TODO: Utilize language manager
      if ModifState = msInsert then
      begin
        Result.Message := 'Berhasil Menambah Data';
      end
      else
        Result.Message := 'Berhasil Memperbaharui Data';
    except
      on E: Exception do
      begin
        q.Connection.Rollback;
        Result.Success := False;
        Result.Message := 'Gagal menyimpan data';
        raise;
      end;
    end;
  finally
    
  end;
end;

procedure TFsfEdit.SetDefaultValues;
var
  ce : TcxCustomEdit;
begin
  ce := (FindComponent('_number') as TcxCustomEdit);
  if ce <> nil then
  begin
    ce.Enabled := False;
    (ce as TcxTextEdit).EditValue := MakeTransactionNumber(_module.SequenceSetting.Prefix, '0', 9, 'y');
  end;
  ce := (FindComponent('_dateissued') as TcxCustomEdit);
  if ce <> nil then
  begin
    ce.Enabled := False;
    (ce as TcxDateEdit).EditValue := DMMain.ServerDate;
  end;
  ce := (FindComponent('_employee_id') as TcxCustomEdit);
  if ce <> nil then
  begin
    ce.Enabled := False;
    (ce as TcxLookupComboBox).EditValue := DMMain.LoginInfo.User.EmployeeId;
  end;
end;

procedure TFsfEdit._numberKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ADataSet : TDataSet;
  ANumber : String;
begin
  inherited;
  if Key = 13 then
  begin
    ANumber := TcxTextEdit(Sender).EditingValue;
    if Length(ANumber) < 13 then
    begin
      ANumber := StringReplace(ANumber, '.', '', [rfReplaceAll]);
      ANumber := UpperCase(LeftStr(ANumber, 3)) + '.' +
        RightStr(ANumber, Length(ANumber) - 3).PadLeft(9, '0');
    end;
    ADataSet := TSfModel.LoadSingle(_module.Table, ANumber);
    LoadData(ADataSet);
    RefreshDetail;
  end;
end;

end.
