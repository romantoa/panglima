unit Fsf_ItemEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Fsf_Modal, DM_Main, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp,
  dxSkinSharpPlus, dxSkinTheBezier, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVS2010, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxGroupBox, cxTextEdit, Fsf_Base;

type
  TFsfItemEdit = class(TFsfBase)
    pnlAction: TcxGroupBox;
    brnCancel: TcxButton;
    btnSave: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure MakeHiddenFields;

  public
    ModifState : TModifState;

  end;

var
  FsfItemEdit: TFsfItemEdit;

implementation

{$R *.dfm}

procedure TFsfItemEdit.btnSaveClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOk;
end;

procedure TFsfItemEdit.FormCreate(Sender: TObject);
begin
  inherited;
//  MakeHiddenFields;
  Position := poScreenCenter;
end;

procedure TFsfItemEdit.FormShow(Sender: TObject);
begin
  Self.Height := Self.Height + pnlAction.Height;
  inherited;

  MakeHiddenFields;
end;

procedure TFsfItemEdit.MakeHiddenFields;
var
  id, master_id : TcxTextEdit;
begin
  // Id
  id := TcxTextEdit.Create(Self);
  id.Visible := False;
  id.Left := 0;
  id.Top := 0;
  id.Name := '_id';
  id.EditValue := 0;
  id.Parent := Self;

  // Master Id
  master_id := TcxTextEdit.Create(Self);
  master_id.Visible := False;
  master_id.Left := 0;
  master_id.Top := 0;
  master_id.Name := '_' + _module.Name + '_id';
  master_id.EditValue := 0;
  master_id.Parent := Self;
end;

end.
