object FsfEdit: TFsfEdit
  Left = 0
  Top = 0
  Caption = 'FsfEdit'
  ClientHeight = 395
  ClientWidth = 710
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object __action_ribbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 710
    Height = 95
    ApplicationButton.Text = 'File'
    ApplicationButton.Visible = False
    BarManager = bmEdit
    Style = rs2013
    ColorSchemeName = 'VS2010'
    ShowTabHeaders = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object rbn1File: TdxRibbonTab
      Active = True
      Caption = 'File'
      Groups = <
        item
          Caption = 'Action'
          ToolbarName = 'barIndexAction'
        end
        item
          Caption = 'View'
          ToolbarName = 'barIndexView'
        end>
      Index = 0
    end
  end
  object bmEdit: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = DMMain.ilDev16
    ImageOptions.LargeImages = DMMain.ilDev32
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 112
    Top = 200
    PixelsPerInch = 96
    object barIndexAction: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 763
      FloatTop = 8
      FloatClientWidth = 59
      FloatClientHeight = 198
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnSave'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnSaveAndClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barIndexView: TdxBar
      Caption = 'Custom 3'
      CaptionButtons = <>
      DockedLeft = 134
      DockedTop = 0
      FloatLeft = 763
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 137
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnReset'
        end
        item
          Visible = True
          ItemName = 'btnSearch'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnPrint'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object btnSaveAndClose: TdxBarLargeButton
      Caption = 'Simpan && Tutup'
      Category = 0
      Hint = 'Simpan & Tutup'
      Visible = ivNever
      OnClick = btnSaveClick
      LargeImageIndex = 17
    end
    object btnPrint: TdxBarLargeButton
      Caption = 'Cetak'
      Category = 0
      Hint = 'Cetak'
      Visible = ivAlways
      LargeImageIndex = 5
    end
    object btnSave: TdxBarLargeButton
      Caption = 'Simpan'
      Category = 0
      Hint = 'Simpan'
      Visible = ivAlways
      OnClick = btnSaveClick
      LargeImageIndex = 4
      SyncImageIndex = False
      ImageIndex = 4
    end
    object btnClose: TdxBarLargeButton
      Caption = 'Tutup'
      Category = 0
      Hint = 'Tutup'
      Visible = ivAlways
      OnClick = btnCloseClick
      LargeImageIndex = 9
      SyncImageIndex = False
      ImageIndex = 9
    end
    object btnReset: TdxBarLargeButton
      Caption = 'Reset'
      Category = 0
      Hint = 'Reset'
      Visible = ivAlways
      OnClick = btnResetClick
      LargeImageIndex = 15
    end
    object btnSearch: TdxBarLargeButton
      Caption = 'Cari'
      Category = 0
      Hint = 'Cari'
      Visible = ivAlways
      OnClick = btnSearchClick
      LargeImageIndex = 2
    end
  end
end
