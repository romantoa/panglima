unit Fsf_Index;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,

  System.Generics.Collections,
  cxGrid,
  cxGridLevel,
  cxGridTableView,
  cxGroupBox,
  cxTextEdit,
  cxButtonEdit,
  cxLabel,
  Fsf_MdiChild,
  SfModel,
  SfModuleManager,
  Uni,
  DB, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp,
  dxSkinSharpPlus, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVS2010, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxCalendar, cxGridCustomTableView,
  cxGridCustomView, cxClasses, cxDBLookupComboBox, dxBar, F_JournalEdit,
  dxBarBuiltInMenu, cxPC, dxSkinTheBezier, cxFindPanel,
  cxDataControllerConditionalFormattingRulesManagerDialog, Refreshable,
  System.StrUtils, cxContainer, cxMaskEdit, Fsf_Base, DM_Main, Fsf_Edit,
  dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxSkinWhiteprint,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2010Black, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Light;

type
  TFsfIndex = class(TFsfBase, IRefreshable)
    bmIndex: TdxBarManager;
    barIndexAction: TdxBar;
    barIndexView: TdxBar;
    btnAdd: TdxBarLargeButton;
    btnDelete: TdxBarLargeButton;
    btnPrint: TdxBarLargeButton;
    btnExportCsv: TdxBarButton;
    btnRefresh: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    __action_ribbon: TdxRibbon;
    rbn1File: TdxRibbonTab;
    grdDataLevel1: TcxGridLevel;
    grdData: TcxGrid;
    gvData: TcxGridTableView;
    btnEdit: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gvDataCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure pgIndexTabClosed(Sender: TObject; ATabIndex: Integer;
      var ACanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAddClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure gvDataDataControllerBeforePost(ADataController
      : TcxCustomDataController);
    procedure dxBarLargeButton1Click(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
  private
    { Private declarations }

  protected
    _dataSource: TDataSet;

    procedure RenderIndexView;
    procedure InitViewProperties;
    procedure RenderGridView;
    procedure RenderSearchBox;
    procedure RenderFilterBox;

    procedure OpenEditForm(AModifState: TModifState; AId: Integer = 0);

  public
    gbSearchBox, gbFilterBox: TcxGroupBox;
    txtSearch: TcxButtonEdit;

    procedure RefreshData; virtual;
    procedure Edit;
    procedure View;
    procedure Add; virtual;
    procedure Delete; virtual;
    procedure AfterDelete; virtual;
    procedure Print;
    procedure ExportCsv;

    function Save: TSfActionResult;

  end;

var
  FsfIndex: TFsfIndex;

implementation

{$R *.dfm}

uses F_Home;

{ TFsfIndex }

procedure TFsfIndex.Add;
begin
  // OpenEditForm(msInsert);
  grdData.SetFocus;
  gvData.DataController.Append;
end;

procedure TFsfIndex.gvDataCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  Self.Edit;
end;

procedure TFsfIndex.gvDataDataControllerBeforePost(ADataController
  : TcxCustomDataController);
var
  ARes: TSfActionResult;
begin
  inherited;
  ARes := Save;
  if ARes.Success then
  begin
    DMMain.alrMain.Show('Sukses', ARes.Message);
  end
  else
  begin
    DMMain.alrMain.Show('Gagal', ARes.Message);
    Abort;
  end;
end;

procedure TFsfIndex.pgIndexTabClosed(Sender: TObject; ATabIndex: Integer;
  var ACanClose: Boolean);
begin
  ShowMessage(IntToStr(ATabIndex));
end;

procedure TFsfIndex.AfterDelete;
begin
  //
end;

procedure TFsfIndex.btnAddClick(Sender: TObject);
begin
  Add;
end;

procedure TFsfIndex.btnDeleteClick(Sender: TObject);
begin
  Delete;
  AfterDelete;
end;

procedure TFsfIndex.btnEditClick(Sender: TObject);
begin
  Edit;
end;

procedure TFsfIndex.btnRefreshClick(Sender: TObject);
begin
  inherited;
  RefreshData;
end;

procedure TFsfIndex.Delete;
var
  AId: Integer;
begin
  if MessageDlg('Anda yakin ingin menghapus?', mtConfirmation, mbYesNo, 0) = mrYes
  then
  begin
    AId := gvData.DataController.Values
      [gvData.DataController.GetFocusedRecordIndex, 0];

    TSfModel.Delete(_module, AId);

    DMMain.Alert.Show('Delete Success!!', 'Berhasil menghapus Data');

    RefreshData;
  end;
end;

procedure TFsfIndex.dxBarLargeButton1Click(Sender: TObject);
begin
  inherited;
  gvData.DataController.Post;
end;

procedure TFsfIndex.btnCloseClick(Sender: TObject);
begin
  DoClose;
end;

procedure TFsfIndex.Edit;
var
  AId: Integer;
begin
  AId := gvData.DataController.Values
    [gvData.DataController.GetFocusedRecordIndex, 0];

  OpenEditForm(msEdit, AId);
end;

procedure TFsfIndex.ExportCsv;
begin
  //
end;

procedure TFsfIndex.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFsfIndex.FormCreate(Sender: TObject);
begin
  inherited;

  Self.Position := poScreenCenter;

  Self.Caption := DMMain.Lang.LBL('LBL_MOD_' + _module.Name);

  RenderIndexView;
end;

procedure TFsfIndex.FormShow(Sender: TObject);
begin
  RefreshData;
end;

procedure TFsfIndex.InitViewProperties;
begin
  // TODO : Read Config
  Self.Font.Size := 10;
  Self.Caption := DMMain.Lang.LBL('LBL_MOD_' + _module.Name);
end;

procedure TFsfIndex.OpenEditForm(AModifState: TModifState; AId: Integer);
var
  c: TEditType;
  f: TFsfEdit;
  ts: TcxTabSheet;
  tsName: string;
begin
   ts := TcxTabSheet.Create(Self);
   ts.Caption := 'Ubah ' + DMMain.Lang.LBL('LBL_MOD_' + _module.Name);
   ts.PageControl := (FHome.ActiveMDIChild.FindComponent('__pg') as TcxPageControl);
    //  ts.OnShow := (FHome.ActiveMDIChild as TFsfMdiChild).TabsheetOnShow;
   (FHome.ActiveMDIChild as TFsfMdiChild).tabsheets.Add('modif' + (FHome.ActiveMDIChild as TFsfMdiChild).tabsheets.Count.ToString, ts);

   c := TEditType(FindClass('TF' + _module.Name + 'Edit'));
   if c <> nil then
   begin
     f := c.CreateByRefreshable(Application, Self, _module, _lksDset) as TFsfEdit;
     if Assigned(f) then
     begin
       f.Parent := ts;
       f.Hide;
       f.WindowState := wsMaximized;
       f.Left := 0;
       f.Top := 0;
       f.BorderStyle := bsNone;
       f.Position := poDefault;
       f.ModifState := AModifState;
       f.Id := AId;

       f.Width := f.Parent.Width;
       f.Height := f.Parent.Height;
       f.Update;

       f.Show;
       ts.Show;
     end;
   end;
end;

procedure TFsfIndex.Print;
begin
  //
end;

procedure TFsfIndex.RenderFilterBox;
var
  I: Integer;
  ctrl: TControl;
  LBL: TcxLabel;
  ALabelLeft, AControlleft, Atop: Integer;
begin
  gbFilterBox := TcxGroupBox.Create(Self);
  gbFilterBox.Align := alTop;
  gbFilterBox.Height := 100;
  gbFilterBox.Top := 0;
  gbFilterBox.Parent := Self;
  gbFilterBox.Visible := True;

  Atop := gbFilterBox.Top + 10;
  ALabelLeft := gbFilterBox.Left + 10;
  AControlleft := gbFilterBox.Left + 150;

  for I := 0 to Length(_module.Fields) - 1 do
  begin
    if _module.Fields[I].IsFilter then
    begin
      LBL := TcxLabel.Create(Self);
      LBL.Caption := DMMain.Lang.LBL(_module.Fields[I].Caption);
      LBL.Name := '_filterlabel_' + _module.Fields[I].Name;
      LBL.Left := ALabelLeft;
      LBL.Top := Atop;
      LBL.AutoSize := True;
      LBL.Visible := True;
      LBL.Transparent := True;
      LBL.Parent := gbFilterBox;

      if _module.Fields[I].FieldType = 'FK' then
      begin
        ctrl := TcxLookupComboBox.Create(Self);
      end
      else
      begin
        ctrl := TcxTextEdit.Create(Self);
        (ctrl as TcxTextEdit).Text := '';
      end;

      ctrl.Name := '_filter_' + _module.Fields[I].Name;
      ctrl.Width := 200;
      ctrl.Top := Atop;
      ctrl.Left := AControlleft;
      ctrl.Visible := True;
      Atop := Atop + ctrl.Height + 3;
      ctrl.Parent := gbFilterBox;
    end;
  end;

  if Atop = (gbFilterBox.Top + 10) then
    gbFilterBox.Visible := False
  else
    gbFilterBox.Height := Atop - gbFilterBox.Top + 10;
end;

procedure TFsfIndex.RenderGridView;
var
  I: Integer;
  foreignTable: string;
  ds: TDataSource;
begin

{$REGION 'CreateGrid'}
  // Grid := TcxGrid.Create(Self);
  // Grid.Name := 'grdData';
  // Grid.Parent := Self;
  // Grid.Align := alClient;

  // GridLevel := Grid.Levels.Add;
  // GridLevel.Name := Grid.Name + 'Lvl1';

  // gvData := Grid.CreateView(TcxGridTableView) as TcxGridTableView;
  // gvData.Name := 'gvData';

  // GridLevel.GridView := gvData;

  // Options
  gvData.OptionsView.GroupByBox := False;
  gvData.OptionsView.Indicator := True;
  gvData.OptionsView.IndicatorWidth := 10;
  // gvData.OptionsSelection.CellSelect := False;

  gvData.OptionsData.Inserting := True;
  gvData.OptionsData.Editing := True;
  gvData.OptionsData.Deleting := True;
  gvData.OptionsData.Appending := True;

  gvData.OptionsBehavior.FocusCellOnTab := True;
  gvData.OptionsBehavior.FocusCellOnCycle := True;
  gvData.OptionsBehavior.FocusFirstCellOnNewRecord := True;
  gvData.OptionsBehavior.GoToNextCellOnEnter := True;

  // Properties
  grdData.AlignWithMargins := True;
  grdData.Margins.Top := 3;
  grdData.Margins.Bottom := 0;
  grdData.Margins.Left := 0;
  grdData.Margins.Right := 0;

  // Find Panel
  gvData.FindPanel.DisplayMode := fpdmManual;
  gvData.FindPanel.ShowCloseButton := False;
  gvData.Controller.ShowFindPanel;

  // Events
  gvData.OnCellDblClick := gvDataCellDblClick;
{$ENDREGION}
{$REGION 'CreateGridFields'}
  for I := 0 to Length(_module.Fields) - 1 do
  begin
    with gvData.CreateColumn do
    begin
      Name := _module.Table + '_' + _module.Fields[I].Name;
      Caption := DMMain.Lang.LBL(_module.Fields[I].Caption);

      // Column Size & Visibility
      if (_module.Fields[I].Width = 0) or (_module.Fields[I].FieldType = 'PK')
      then
        Visible := False
      else if _module.Fields[I].Width = (Null) then
        Width := 100
      else
        Width := _module.Fields[I].Width;

      // Column Display Format
      if _module.Fields[I].FieldType = 'DATETIME' then
      begin
        PropertiesClass := TcxDateEditProperties;
        TcxDateEditProperties(Properties).BeginUpdate;
        TcxDateEditProperties(Properties).DisplayFormat := 'dd-mm-yyyy';
        TcxDateEditProperties(Properties).Alignment.Horz := taCenter;
        TcxDateEditProperties(Properties).UseLeftAlignmentOnEditing := False;
        TcxDateEditProperties(Properties).EndUpdate;
      end
      else if _module.Fields[I].FieldType = 'FK' then
      begin
        PropertiesClass := TcxLookupComboBoxProperties;
        foreignTable := StringReplace(_module.Fields[I].Name, '_id', '',
          [rfReplaceAll]);
        if _lksDset.ContainsKey(foreignTable) then
        begin
          ds := TDataSource.Create(nil);
          ds.DataSet := _lksDset[foreignTable];

          TcxLookupComboBoxProperties(Properties).BeginUpdate;
          TcxLookupComboBoxProperties(Properties).ListOptions.ShowHeader := False;
          TcxLookupComboBoxProperties(Properties).IncrementalFilteringOptions := [ifoHighlightSearchText,ifoUseContainsOperator];
          TcxLookupComboBoxProperties(Properties).ListSource := ds;
          TcxLookupComboBoxProperties(Properties).ListFieldNames := 'name';
          TcxLookupComboBoxProperties(Properties).KeyFieldNames := 'id';
          TcxLookupComboBoxProperties(Properties).EndUpdate;
        end;
      end;
    end;
  end;
{$ENDREGION}
end;

procedure TFsfIndex.RenderIndexView;
begin
  InitViewProperties;
  PrepareLookups;
  // RenderPageControl;
  RenderGridView;
  // RenderFilterBox;
  // RenderSearchBox;
end;

procedure TFsfIndex.RenderSearchBox;
var
  LBL: TcxLabel;
begin
  gbSearchBox := TcxGroupBox.Create(Self);
  LBL := TcxLabel.Create(Self);

  LBL.Caption := DMMain.Lang.LBL('LBL_INDEX_SEARCH');
  LBL.Top := 7;
  LBL.Parent := gbSearchBox;
  LBL.Left := 15;
  LBL.Transparent := True;
  LBL.Style.TextColor := clGrayText;
  LBL.Style.Font.Style := [fsItalic];

  gbSearchBox.Align := alTop;
  gbSearchBox.Height := LBL.Height + 2 * LBL.Top;
  gbSearchBox.Top := 2;
  gbSearchBox.Parent := Self;

  txtSearch := TcxButtonEdit.Create(Self);
  txtSearch.Width := 200;
  txtSearch.Name := 'txtSearch';
  txtSearch.Text := '';
  txtSearch.Parent := gbSearchBox;
  txtSearch.Top := 5;
  txtSearch.Left := 10; // LBL.Width + LBL.Left;
  txtSearch.Properties.Images := DMMain.ilDev16;
  txtSearch.Properties.Buttons[0].Kind := bkGlyph;
  txtSearch.Properties.Buttons[0].ImageIndex := 13;

  LBL.BringToFront;
end;

function TFsfIndex.Save: TSfActionResult;
var
  q: TUniQuery;
  I, J, ri: Integer;
  insValSql: TStringList;
  cn, ss: string;
  x: TcxGridColumn;

  AEditState: TSfEditState;
  val : Variant;
begin
  inherited;
  // TODO : Should be handled by SfModel
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;
  ri := gvData.DataController.GetFocusedRecordIndex;
  if gvData.DataController.Values[ri, 0] = Null then
    AEditState := edsInsert
  else
    AEditState := edsEdit;

  try
    try
      if AEditState = edsInsert then
      begin
{$REGION 'Master Insert'}
        insValSql := TStringList.Create;
        q.SQL.Add('insert into ' + _module.Table + '(');

        for I := 0 to Length(_module.Fields) - 1 do
        begin
          if (_module.Fields[I].Name <> 'id') and
            (TcxGridColumn(gvData.FindItemByName(_module.Table + '_' +
            _module.Fields[I].Name)) <> nil) then
          begin
            q.SQL.Add(_module.Fields[I].Name + ',');
            insValSql.Add(':' + _module.Fields[I].Name + ',');
          end;
        end;

        q.SQL.Add('_created_at, _created_by)');
        q.SQL.Add('values(');
        q.SQL.Add(insValSql.Text);
        q.SQL.Add(':_created_at, :_created_by)');

        q.Params.ParamByName('_created_at').Value := DMMain.ServerDate;
        q.Params.ParamByName('_created_by').Value := DMMain.Session.User.Id;
{$ENDREGION}
      end
      else
      begin
{$REGION 'Master Update'}
        q.SQL.Add('update ' + _module.Table + ' set');

        for I := 0 to Length(_module.Fields) - 1 do
        begin
          if (_module.Fields[I].Name <> 'id') and
            (TcxGridColumn(gvData.FindItemByName(_module.Table + '_' +
            _module.Fields[I].Name)) <> nil) then
            q.SQL.Add(_module.Fields[I].Name + ' = :' + _module.Fields[I]
              .Name + ',');
        end;

        q.SQL.Add('_updated_at = :_updated_at, _updated_by = :_updated_by');
        q.SQL.Add('where id = :id');

        q.Params.ParamByName('_updated_at').Value := DMMain.ServerDate;
        q.Params.ParamByName('_updated_by').Value := DMMain.Session.User.Id;
        q.Params.ParamByName('id').Value := gvData.DataController.Values[ri, 0];
{$ENDREGION}
      end;

      // Master Parameter Values
      for I := 0 to Length(_module.Fields) - 1 do
      begin
        if _module.Fields[I].Name <> 'id' then
        begin
          x := (gvData.FindItemByName(_module.Table + '_' + _module.Fields[I]
            .Name) as TcxGridColumn);

          if x <> nil then
          begin
            val := gvData.DataController.Values[ri, x.Index];
            q.Params.ParamByName(_module.Fields[I].Name).Value := val;
          end;
        end;
      end;

      q.Connection.Connected := True;
      q.ExecSQL;

      if AEditState = edsInsert then
        gvData.DataController.Values[ri, 0] := q.LastInsertId;

      // TODO: Utilize language manager
      Result.Success := True;
      if AEditState = edsInsert then
        Result.&Message := 'Berhasil Menambah Data'
      else
        Result.&Message := 'Berhasil Memperbaharui Data';
    except
      on E: Exception do
      begin
        Result.Success := False;
        Result.&Message := E.Message;
      end;
    end;
  finally

  end;
end;

procedure TFsfIndex.View;
begin
  //
end;

procedure TFsfIndex.RefreshData;
var
  ri, J: Integer;
begin
  _dataSource := TSfModel.Fetch(_module.Table);

  gvData.DataController.BeginUpdate;
  gvData.DataController.RecordCount := 0;
  try
    try
      ri := 0;

      while not _dataSource.Eof do
      begin
        gvData.DataController.AppendRecord;
        for J := 0 to gvData.ColumnCount - 1 do
        begin
          gvData.DataController.Values[ri, J] :=
            _dataSource.FieldByName(_module.Fields[J].Name).Value;
        end;
        _dataSource.Next;
        Inc(ri);
      end;

      gvData.DataController.EndUpdate;
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
  finally
    // _dataSource.Free;
  end;
end;

end.
