object FsfIndex: TFsfIndex
  Left = 0
  Top = 0
  Caption = 'FsfIndex'
  ClientHeight = 397
  ClientWidth = 582
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object __action_ribbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 582
    Height = 95
    ApplicationButton.Text = 'File'
    ApplicationButton.Visible = False
    BarManager = bmIndex
    Style = rs2013
    ColorSchemeName = 'VS2010'
    ShowTabHeaders = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object rbn1File: TdxRibbonTab
      Active = True
      Caption = 'File'
      Groups = <
        item
          Caption = 'Action'
          ToolbarName = 'barIndexAction'
        end
        item
          Caption = 'View'
          ToolbarName = 'barIndexView'
        end>
      Index = 0
    end
  end
  object grdData: TcxGrid
    Left = 0
    Top = 95
    Width = 582
    Height = 302
    Align = alClient
    TabOrder = 1
    object gvData: TcxGridTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnBeforePost = gvDataDataControllerBeforePost
    end
    object grdDataLevel1: TcxGridLevel
      GridView = gvData
    end
  end
  object bmIndex: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = DMMain.ilDev16
    ImageOptions.LargeImages = DMMain.ilDev32
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 112
    Top = 200
    PixelsPerInch = 96
    object barIndexAction: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 763
      FloatTop = 8
      FloatClientWidth = 59
      FloatClientHeight = 198
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnAdd'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnEdit'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barIndexView: TdxBar
      Caption = 'Custom 3'
      CaptionButtons = <>
      DockedLeft = 240
      DockedTop = 0
      FloatLeft = 763
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 137
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnPrint'
        end
        item
          Visible = True
          ItemName = 'btnExportCsv'
        end
        item
          Visible = True
          ItemName = 'btnRefresh'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object btnAdd: TdxBarLargeButton
      Caption = 'Tambah'
      Category = 0
      Hint = 'Tambah'
      Visible = ivAlways
      OnClick = btnAddClick
      LargeImageIndex = 0
    end
    object btnDelete: TdxBarLargeButton
      Caption = 'Hapus'
      Category = 0
      Hint = 'Hapus'
      Visible = ivAlways
      OnClick = btnDeleteClick
      LargeImageIndex = 16
      SyncImageIndex = False
      ImageIndex = 16
    end
    object btnPrint: TdxBarLargeButton
      Caption = 'Cetak'
      Category = 0
      Hint = 'Cetak'
      Visible = ivAlways
      LargeImageIndex = 5
    end
    object btnExportCsv: TdxBarButton
      Caption = 'Export'
      Category = 0
      Hint = 'Export'
      Visible = ivAlways
      ImageIndex = 6
      LargeImageIndex = 6
    end
    object btnRefresh: TdxBarButton
      Caption = 'Refresh'
      Category = 0
      Hint = 'Refresh'
      Visible = ivAlways
      ImageIndex = 7
      LargeImageIndex = 7
      OnClick = btnRefreshClick
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = 'Simpan'
      Category = 0
      Hint = 'Simpan'
      Visible = ivAlways
      OnClick = dxBarLargeButton1Click
      LargeImageIndex = 4
      SyncImageIndex = False
      ImageIndex = 4
    end
    object btnClose: TdxBarLargeButton
      Caption = 'Tutup'
      Category = 0
      Hint = 'Tutup'
      Visible = ivAlways
      OnClick = btnCloseClick
      LargeImageIndex = 9
      SyncImageIndex = False
      ImageIndex = 9
    end
    object btnEdit: TdxBarLargeButton
      Caption = 'Edit'
      Category = 0
      Hint = 'Edit'
      Visible = ivNever
      OnClick = btnEditClick
      LargeImageIndex = 1
    end
  end
end
