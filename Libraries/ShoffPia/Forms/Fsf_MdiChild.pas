unit Fsf_MdiChild;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Data.DB,
  Generics.Collections,

  SfModel, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinDevExpressStyle, dxSkinOffice2010Blue, dxSkinSharp,
  dxSkinSharpPlus, dxSkinTheBezier, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVS2010, dxBarBuiltInMenu, cxPC, DM_Main,
  dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxBar, cxClasses,
  dxSkinWhiteprint, cxContainer, cxEdit, cxGroupBox, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2010Black,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Light;

type
  TFsfMdiChild = class(TForm)
    __module_ribbon: TdxRibbon;
    bmModuleWrapper: TdxBarManager;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pgTabClosed(Sender: TObject; ATabIndex: Integer;
      var ACanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    procedure RenderPageControl;
  protected

  public
    pg : TcxPageControl;
    tabsheets: TDictionary<String, TcxTabSheet>;

    procedure CreateIndexTab;
    procedure CreateEditTab;

    property ModuleRibbon: TdxRibbon read __module_ribbon;
  end;

  TMdiChildType = class of TFsfMdiChild;

var
  FsfMdiChild: TFsfMdiChild;

implementation

{$R *.dfm}

uses F_Home;

{ TFMdiChild }

procedure TFsfMdiChild.CreateEditTab;
begin
  //
end;

procedure TFsfMdiChild.CreateIndexTab;
begin
  //
end;

procedure TFsfMdiChild.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFsfMdiChild.FormCreate(Sender: TObject);
begin
  RenderPageControl;
end;

procedure TFsfMdiChild.pgTabClosed(Sender: TObject; ATabIndex: Integer;
  var ACanClose: Boolean);
begin
  if ATabIndex = 0 then
  begin
    Self.Close;
  end;
end;

procedure TFsfMdiChild.RenderPageControl;
var
  ts: TcxTabSheet;
begin
  pg := TcxPageControl.Create(Self);
  pg.Name := '__pg';
  pg.Parent := Self;
  pg.Align := alClient;
  pg.TabPosition := tpTop;
  pg.Options := [pcoCloseButton];
  pg.Properties.CloseButtonMode := cbmEveryTab;
  pg.OnCanCloseEx := pgTabClosed;
  tabsheets := TDictionary<String, TcxTabSheet>.Create;
  ts := TcxTabSheet.Create(Self);
  ts.Caption := 'Daftar'; ////// + DMMain.Lang.LBL('LBL_MOD_' + _moduleName);
  ts.PageControl := pg;
//  ts.OnShow := TabsheetOnShow;
  ts.Name := '__index';
  tabsheets.Add('index', ts);
end;

initialization
  RegisterClass(TFsfMdiChild);
finalization
  UnRegisterClass(TFsfMdiChild);

end.
