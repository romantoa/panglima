unit SfLanguage;

interface

uses
  Classes,
  Generics.Collections,
  SfUniConnection,
  DBAccess,
  Uni,
  SysUtils,
  Configable;

type
  TSfTranslation = class(TObject)
    public
      Id : Integer;
      Key : String;
      Value : String;
      Language : String;
  end;

  TSfTransList = TDictionary<String, TSfTranslation>;

  TSfLanguage = class(TObject)
    private
      _lbl : TSfTransList;

      function FetchTranslations(ALangCode: String) : TSfTransList;

    public
      constructor Create(ALangCode: String);
      function LBL(AName: String) : String;
  end;

implementation

{ TSfLanguageManager }

constructor TSfLanguage.Create(ALangCode: String);
begin
  _lbl := FetchTranslations(ALangCode);
end;

function TSfLanguage.FetchTranslations(ALangCode: String): TSfTransList;
var
  item : TSfTranslation;
  q : TUniQuery;
begin
  Result := TSfTransList.Create;

  q := TUniQuery.Create(nil);
  q.Connection := TSfUniConnection.Connection;
  q.SQL.Text := 'select * from sys_translations where language = :lang_code';
  q.Params.ParamByName('lang_code').Value := ALangCode;
  
  try
    try
      q.Connection.Connected := True;
      q.Open;

      if not q.IsEmpty then
      begin
        q.First;
        while not q.Eof do
        begin
          item := TSfTranslation.Create;

          item.Id := q.FieldByName('id').AsInteger;
          item.Key := q.FieldByName('key').AsString;
          item.Value := q.FieldByName('value').AsString;
          item.Language := q.FieldByName('language').AsString;
          
          Result.Add(item.Key, item);
          q.Next;
        end;
      end;
    except
      raise;
    end;
  finally
    q.Connection.Free;
    q.Free;
  end;
end;

function TSfLanguage.LBL(AName: String): String;
begin
  AName := UpperCase(AName);
  if _lbl.ContainsKey(AName) then
    Result := _lbl[AName].Value
  else
    Result := AName;
end;

end.
