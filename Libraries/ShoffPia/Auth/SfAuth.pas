unit SfAuth;

interface

uses
  Classes,
  Uni,
  SysUtils,
  Shift,
  User;

type
  RLoginResult = record
    Success : Boolean;
    &Message : String;
    Token : String;
    User : RUser;
    Shift : RShift;
    Editable : Boolean;
  end;

type
  TSfAuth = class(TObject)
    class function AttemptLogin(AUsername, APassword : String; AShift: RShift) : RLoginResult;
  end;

implementation

{ TSfAuth }

uses DM_Main;

class function TSfAuth.AttemptLogin(AUsername, APassword: String; AShift: RShift): RLoginResult;
var
  q : TUniQuery;
  editable : boolean;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  q.SQL.Add('select * from users where Nama = ' + QuotedStr(AUsername) +
            ' AND `Status` = ''A''');

  q.Connection.Connected := True;
  q.Open;

  if q.IsEmpty then
  begin
    Result.Success := False;
    Result.Message := 'user_not_found';
  end
  else
  begin
    editable := (not q.FieldByName('editable').IsNull)
                and (q.FieldByName('editable').AsBoolean);
    q.Close;
    q.SQL.Clear;

    q.SQL.Add('select * from users where Nama = ' + QuotedStr(AUsername));
    if editable then
      q.SQL.Add('and `password`=PASSWORD(' + QuotedStr(APassword) + ')')
    else
      q.SQL.Add('and `password` = ' + QuotedStr(APassword));

    q.Open;

    if q.IsEmpty then
    begin
      Result.Success := False;
      Result.Message := 'password_not_correct';
    end
    else
    begin
      q.Close;
      q.SQLUpdate.Clear;
      q.SQLUpdate.Add('update users set last_login=NOW() where nama=' + QuotedStr(AUsername));
      q.Execute;

      with Result do
      begin
        Success := True;
        &Message := 'login_success';
        Token := 'F0E4CDBCF2E0';
        Shift := AShift;

        User.Id := q.FieldByName('id').AsInteger;
        //TODO: Dummy
        User.EmployeeId := 1; //q.FieldByName('employee_id').AsInteger;
        User.Username := q.FieldByName('login').AsString;
        User.Name := q.FieldByName('namalengkap').AsString;
        User.NickName := q.FieldByName('nama').AsString;
      end;
    end;
  end;
end;

end.
