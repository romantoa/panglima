unit SfModel;

interface

uses
  Classes,
  DB,
  Uni,
  DBAccess,
  StrUtils,
  SysUtils,
  SfUniConnection,
  SfModuleManager,
  System.Generics.Collections, System.Variants;

type
  TSfStringVariantPair = TDictionary<string, variant>;

type
  TQueryType = TUniQuery;

type
  TSfActionResult = record
    Success : Boolean;
    MessageTitle : String;
    &Message : String;
    Data : TSfStringVariantPair;
  end;

type
  TSfEditState = (edsInsert, edsEdit);

type
  TSfModel = class(TObject)
    private

    protected
      _module : TSfModule;
      _indexSql : string;

    public
      class function GetTableColumns(ATableName: string) : TDataSet;
      class function BuildIndexSql(ATableName: string) : string;
      class function Fetch(ATableName: string) : TDataSet; //Simple Select
      class function LoadSingle(ATableName: string; AId: integer) : TDataSet; overload;
      class function LoadSingle(ATableName: string; ANumber: string) : TDataSet; overload;
      class function FetchChild(ATableName: string; AParentField: String; AParentId: integer) : TDataSet;
      class function DataTypeOf(AFieldType: String) : TFieldType;
      class function GenerateSequence(AModule: TSfModule) : String;

      function Insert(AParams: TSfStringVariantPair) : TSfActionResult; overload;
      function Insert(AParams: TList<TSfStringVariantPair>) : TSfActionResult; overload;
      class function Insert(AModule: TSfModule; AParams: TList<TSfStringVariantPair>) : TSfActionResult; overload;
      function Update(AId: Integer; AParams: TSfStringVariantPair) : TSfActionResult; overload;
      class function Update(AModule: TSfModule; AId: integer; AParams: TSfStringVariantPair) : TSfActionResult; overload;
      class function Delete(AModule: TSfModule; AId: integer) : TSfActionResult;

      constructor Create; overload;
      constructor Create(AModuleName: string); overload;
//      constructor Create(AModule: TSfModule); overload;
  end;

  TBaseModelType = class of TSfModel;

implementation

{ TBaseModel }

uses DM_Main;

class function TSfModel.BuildIndexSql(ATableName: string): string;
begin
  Result := 'select * from ' + ATableName + ' where _deleted_at is null';
end;

constructor TSfModel.Create;
begin
  _module := TSfModuleManager.GetModuleProperties(_module.Name);
end;

//
//constructor TSfModel.Create(AModule: TSfModule);
//begin
//  _module := AModule;
//end;

constructor TSfModel.Create(AModuleName: string);
begin
  _module.Name := AModuleName;
  inherited Create;
end;

class function TSfModel.DataTypeOf(AFieldType: String): TFieldType;
var
  FieldTypeMap : TDictionary<string, TFieldType>;
begin
  FieldTypeMap := TDictionary<string, TFieldType>.Create;
  FieldTypeMap.Add('STR', ftString);
  FieldTypeMap.Add('INT', ftInteger);
  FieldTypeMap.Add('TINYINT', ftSmallint);
  FieldTypeMap.Add('DEC', ftFloat);
  FieldTypeMap.Add('MONEY', ftCurrency);
  FieldTypeMap.Add('DATE', ftDate);
  FieldTypeMap.Add('DATETIME', ftDateTime);
  FieldTypeMap.Add('TIME', ftTime);
  FieldTypeMap.Add('TXT', ftMemo);
  FieldTypeMap.Add('UID', ftGuid);

  Result := FieldTypeMap[AFieldType];
end;

class function TSfModel.Delete(AModule: TSfModule; AId: integer) : TSfActionResult;
var
  q : TUniQuery;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  try
    try
      q.SQL.Add('UPDATE ' + AModule.Table);
      q.SQL.Add('SET _deleted_at = :deleted_at, _deleted_by = :deleted_by');
      q.SQL.Add('WHERE id = :id');

      q.Params.ParamByName('deleted_at').Value := DMMain.ServerDate;
      q.Params.ParamByName('deleted_by').Value := DMMain.Session.User.Id;
      q.Params.ParamByName('id').Value := AId;

      q.Connection.Connected := True;
      q.ExecSQL;
    except on E: Exception do
      raise;
    end;
  finally
    q.Free;
  end;
end;

class function TSfModel.Fetch(ATableName: string) : TDataSet;
begin
  Result := TUniQuery.Create(nil);

  (Result as TUniQuery).Connection := DMMain.Conn.Connection;

  try
    try
      (Result as TUniQuery).SQL.Text := BuildIndexSql(ATableName);
      (Result as TUniQuery).Connection.Connected := True;
      (Result as TUniQuery).Open;
    except
      raise;
    end;
  finally
//    q.Connection.Free;
//    q.Free;
  end;
end;

class function TSfModel.FetchChild(ATableName, AParentField: String;
  AParentId: integer): TDataSet;
var
  q : TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  (Result as TUniQuery).Connection := DMMain.Conn.Connection;

  try
    try
      (Result as TUniQuery).SQL.Add('SELECT * FROM ' + ATableName);
      (Result as TUniQuery).SQL.Add('WHERE ' + AParentField + ' = :parent_id');

      (Result as TUniQuery).Params.ParamByName('parent_id').Value := AParentId;

      (Result as TUniQuery).Connection.Connected := True;
      (Result as TUniQuery).Open;
    except
      raise;
    end;
  finally
//    q.Connection.Free;
//    q.Free;
  end;
end;

class function TSfModel.GenerateSequence(AModule: TSfModule): String;
var
  q : TUniQuery;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  q.SQL.Add('SELECT IFNULL(MAX(' + AModule.SequenceSetting.Name + '),'''') as max_number');
  q.SQL.Add('FROM ' + AModule.Table);
  if AModule.SequenceSetting.ResetAt = sqrMonthly then
    q.SQL.Add('WHERE month(dateissued) = month(curdate())')
  else if AModule.SequenceSetting.ResetAt = sqrYearly then
    q.SQL.Add('WHERE year(dateissued) = year(curdate())')
  else
    q.SQL.Add('WHERE');

end;

class function TSfModel.GetTableColumns(ATableName: string): TDataSet;
begin
  Result := TUniQuery.Create(nil);
  (Result as TUniQuery).Connection := DMMain.Conn.Connection;

  try
    try
      (Result as TUniQuery).SQL.Add('SELECT * FROM INFORMATION_SCHEMA.COLUMNS');
      (Result as TUniQuery).SQL.Add('WHERE TABLE_NAME = :tablename');

      (Result as TUniQuery).Params.ParamByName('tablename').Value := ATableName;

      (Result as TUniQuery).Connection.Connected := True;
      (Result as TUniQuery).Open;
    except
      raise;
    end;
  finally
//    q.Connection.Free;
//    q.Free;
  end;
end;

function TSfModel.Insert(AParams: TList<TSfStringVariantPair>): TSfActionResult;
begin
  Result := TSfModel.Insert(_module, AParams);
end;

function TSfModel.Insert(AParams: TSfStringVariantPair): TSfActionResult;
var
  AParamList : TList<TSfStringVariantPair>;
begin
  AParamList := TList<TSfStringVariantPair>.Create;
  AParamList.Add(AParams);
  Result := TSfModel.Insert(_module, AParamList);
end;

class function TSfModel.Insert(AModule: TSfModule; AParams: TList<TSfStringVariantPair>): TSfActionResult;
var
  q : TUniQuery;
  I, J : Integer;
  insValSql, insFieldSql, subInsert : TStringList;
  Key : String;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  insValSql := TStringList.Create;
  insValSql.Delimiter := ',';
  insValSql.QuoteChar := #0;

  insFieldSql := TStringList.Create;
  insFieldSql.Delimiter := ',';
  insFieldSql.QuoteChar := #0;

  try
    try
      for I := 0 to AParams.Count -1 do
      begin
        subInsert := TStringList.Create;
        subInsert.Delimiter := ',';
        subInsert.QuoteChar := #0;

        for Key in AParams[I].Keys do
        begin
          for J := 0 to Length(AModule.Fields) - 1 do
          begin
            if (AModule.Fields[J].Name <> 'id') and
              (AModule.Fields[J].Name = Key)
            then
            begin
              if I = 0 then
                insFieldSql.Add(Key);

              subInsert.Add(QuotedStr(VarToStr(AParams[I][Key])));

              Break;
            end;
          end;
        end;

        insValSql.Add('(' + subInsert.DelimitedText + ', :_created_at, :_created_by)');
      end;

      q.SQL.Add('insert into ' + AModule.Table + '(');
      q.SQL.Add(insFieldSql.DelimitedText);
      q.SQL.Add(', _created_at, _created_by)');
      q.SQL.Add('values');
      q.SQL.Add(insValSql.DelimitedText);

      q.Params.ParamByName('_created_at').Value := DMMain.ServerDate;
      q.Params.ParamByName('_created_by').Value := DMMain.Session.User.Id;

      q.Connection.Connected := True;
      q.ExecSql;
      DMMain.Logger.Debug(q.Sql.Text);

      Result.Success := True;
      Result.MessageTitle := 'Sukses';
      Result.Message := 'Berhasil menambah data baru';
      Result.Data := TDictionary<String, Variant>.Create;
      Result.Data.Add('Id', q.LastInsertId);
    except on E: Exception do
      begin
        Result.Success := False;
        Result.MessageTitle := 'Error';
        Result.Message := 'Check application log for details!';
        DMMain.Logger.Error(E.Message);
      end;
    end;
  finally

  end;
end;

class function TSfModel.LoadSingle(ATableName: string;
  ANumber: string): TDataSet;
var
  q : TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  (Result as TUniQuery).Connection := DMMain.Conn.Connection;

  try
    try
      (Result as TUniQuery).SQL.Add('SELECT * FROM ' + ATableName);
      (Result as TUniQuery).SQL.Add('WHERE number = :number');

      (Result as TUniQuery).Params.ParamByName('number').Value := ANumber;

      (Result as TUniQuery).Connection.Connected := True;
      (Result as TUniQuery).Open;
    except
      raise;
    end;
  finally
//    q.Connection.Free;
//    q.Free;
  end;
end;

function TSfModel.Update(AId: Integer;
  AParams: TSfStringVariantPair): TSfActionResult;
begin
  Result := TSfModel.Update(_module, AId, AParams);
end;

class function TSfModel.Update(AModule: TSfModule; AId: integer;
  AParams: TSfStringVariantPair): TSfActionResult;
var
  q : TUniQuery;
  I : Integer;
  updPairSql, updatedFields : TStringList;
  Key : String;
  x : variant;
begin
  q := TUniQuery.Create(nil);
  q.Connection := DMMain.Conn.Connection;

  updPairSql := TStringList.Create;
  updPairSql.Delimiter := ',';
  updPairSql.QuoteChar := #0;

  updatedFields := TStringList.Create;

  try
    try
      for Key in AParams.Keys do
      begin
        for I := 0 to Length(AModule.Fields) - 1 do
        begin
          if (AModule.Fields[I].Name <> 'id') and
            (AModule.Fields[I].Name = Key)
          then
          begin
            updPairSql.Add(Key + ' = :' + Key);
            updatedFields.Add(Key);
            Break;
          end;
        end;
      end;

      q.SQL.Add('update ' + AModule.Table + ' set');
      q.SQL.Add(updPairSql.DelimitedText);
      q.SQL.Add(', _updated_at = :_updated_at, _updated_by = :_updated_by');
      q.SQL.Add('where id = :id');

      q.Params.ParamByName('_updated_at').Value := DMMain.ServerDate;
      q.Params.ParamByName('_updated_by').Value := DMMain.Session.User.Id;
      q.Params.ParamByName('id').Value := AId;

      for I := 0 to updatedFields.Count - 1 do
      begin
        x := AParams[updatedFields[I]];
        q.Params.ParamByName(updatedFields[I]).Value := x;
      end;

      q.Connection.Connected := True;
      q.ExecSql;

      Result.Success := True;
      Result.MessageTitle := 'Sukses';
      Result.Message := 'Berhasil memperbaharui data';
    except on E: Exception do
      begin
        Result.Success := False;
        Result.MessageTitle := 'Error';
        Result.Message := 'Check application log for details!';
        DMMain.Logger.Error(E.Message);
      end;
    end;
  finally

  end;
end;

class function TSfModel.LoadSingle(ATableName: string; AId: integer): TDataSet;
var
  q : TUniQuery;
begin
  Result := TUniQuery.Create(nil);
  (Result as TUniQuery).Connection := DMMain.Conn.Connection;

  try
    try
      (Result as TUniQuery).SQL.Add('SELECT * FROM ' + ATableName);
      (Result as TUniQuery).SQL.Add('WHERE id = :id');

      (Result as TUniQuery).Params.ParamByName('id').Value := AId;

      (Result as TUniQuery).Connection.Connected := True;
      (Result as TUniQuery).Open;
    except
      raise;
    end;
  finally
//    q.Connection.Free;
//    q.Free;
  end;
end;

end.
